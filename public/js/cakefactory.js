var CAKE_FOLDER_LOCATION = "public/images/cakes/";
var SUBMIT_LOCATION = "thankyou/";
var cakeFactory;


$(document).ready(
    function()
    {
        var $cakeFactoryElement = $("#cakeFactory");
        cakeFactory = new CakeFactory( $cakeFactoryElement );

        if ( viewProperties == null ) {
            cakeFactory.failedToLoad();
            return;
        }
        cakeFactory.applyViewProperties();
        if ( cakeFactory.availableCupcakes == null ) {
            cakeFactory.failedToLoad();
            return;
        }
        cakeFactory.preloadImages();
    }
);

var CakeFactory = function( element ){
    this.$element  = $(element);
    this.factoryEnabled = false;
};

var CakeInterface = function(){
    this.$element = $("<div>", {class: "cakeFactoryInterface"});
    this.$feedback = $("<p>", {class: "cakeFactoryInfo"});
    this.$submitButton = $("<button>", {class: "cakeFactoryButton"}).text( "Save" );
    this.$element.append( this.$feedback );
    this.$element.append( this.$submitButton );
    this.showDefaultInfo();
};

var CakePlacer = function(){
    this.$element = $("<div>", {class: "cakePlacer"});
};

var CakeGrabber = function( noOTiles ){
    this.$element = $("<div>", {class: "cakeGrabber placerWindow"});
    this.placeHolderTiles = [];
    for( var i = 0; i < noOTiles; i ++ )
    {
        var $placeHolderTile = $("<div>", {class: "cakeTilePlaceholder factoryTile"});
        this.placeHolderTiles.push( $placeHolderTile );
        this.$element.append( $placeHolderTile );
    }
};

var CakeDropper = function( noOTiles ){
    this.$element = $("<div>", {class: "cakeDropper placerWindow"});
    this.placeHolderTiles = [];
    for( var i = 0; i < noOTiles; i ++ )
    {
        var $placeHolderTile = $("<div>", {class: "cakeTilePlaceholder factoryTile"});
        this.placeHolderTiles.push( $placeHolderTile );
        this.$element.append( $placeHolderTile );
    }
};


var CakeTile = function( tileID, preferredGrabIndex, imageLocation, tileWidth, tileHeight ){
    this.tileID = tileID;
    this.preferredGrabIndex = preferredGrabIndex;
    this.dropIndex = -1;
    this.$currentPlaceholder = null;
    this.isDragging = false;

    $img = $("<img>", {src:imageLocation, width: tileWidth, height: tileHeight});
    this.$element = $("<div>", {class: "cakeTile factoryTile", tileID: tileID}).append( $img );
    $( this.$element ).draggable({start: cakeFactory.cakePlacer.tileDragStarted, stop: cakeFactory.cakePlacer.tileDragStopped, drag: cakeFactory.cakePlacer.tileDragMove});
    $( this.$element ).css({position:'absolute'});
};


CakeFactory.prototype = {

    applyViewProperties: function () {

        /** @namespace viewProperties.availableCupcakes */
        if (typeof viewProperties.availableCupcakes === 'undefined') {
            return;
        }
        this.availableCupcakes = viewProperties.availableCupcakes;

        /** @return int */
        function SortAvailable(a, b){
            /** @namespace a.priority */
            /** @namespace b.priority */
            if( a.priority < b.priority )
            {
                return -1;
            }
            if(a.priority > b.priority )
            {
                return 1;
            }
            /** @namespace a.id */
            /** @namespace b.id */
            if(a.id < b.id )
            {
                return -1;
            }
            if(a.id > b.id )
            {
                return 1;
            }
            return 0;
        }

        this.availableCupcakes.sort(SortAvailable);
    },

    preloadImages: function () {
        var imagesOnly = [];
        for( var i = 0; i < cakeFactory.availableCupcakes.length ; i ++ )
        {
            imagesOnly.push( CAKE_FOLDER_LOCATION + this.availableCupcakes[ i ]["fileName"] );
        }
        PreloadImages(imagesOnly, cakeFactory.createNewApp);
    },

    createNewApp: function ( failedToLoadURLs ) {
        if( failedToLoadURLs.length > 0 )
        {
            cakeFactory.failedToLoad();
            return;
        }
        cakeFactory.createFactory();
        cakeFactory.enable( true );
    },

    createFactory: function () {

        var $relativeContainer = $("<div>", {class: "relativeContainer"});
        this.$element.append( $relativeContainer );

        var $layoutLayer = $("<div>", {class: "layoutLayer"});
        $relativeContainer.append( $layoutLayer );

        this.cakePlacer = new CakePlacer();
        this.cakePlacer.createPlacer();
        this.cakeInterface = new CakeInterface();
        $layoutLayer.append( this.cakePlacer.$grabberTitle );
        $layoutLayer.append( this.cakePlacer.cakeGrabber.$element );
        $layoutLayer.append( this.cakeInterface.$element );
        $layoutLayer.append( this.cakePlacer.$dropperTitle );
        $layoutLayer.append( this.cakePlacer.cakeDropper.$element );
        $relativeContainer.append( this.cakePlacer.$element );

        this.cakePlacer.findPositions();

        $(window).resize( function() {
            cakeFactory.cakePlacer.findPlaceholders( true, 'fast' ); // I would like to find a better way than this but can't see any right now
        } );
    },

    failedToLoad: function () {
        var $feedback = $("<p>");
        $feedback.text("There was an error while retrieving our content. If the problem persists please contact an administrator");
        $feedback.css({color:'#ff0000'});
        cakeFactory.$element.append( $feedback );
    },

    enable: function ( enable ) {
        this.cakePlacer.enable( enable );
        this.cakeInterface.enable( enable );
        this.factoryEnabled = enable;
    }
};


CakeInterface.prototype = {
    showDefaultInfo: function () {
        this.$feedback.text("Drag the cupcakes you would like from the \"Our Cupcakes\" list to your \"Party Menu\", then click save to submit");
        this.$feedback.css({color:'#000000'});
    },
    saveSelection: function () {
        var submitTiles = [];
        jQuery.each( cakeFactory.cakePlacer.cakeTiles, function( tileID, cakeTile ) {
            if( cakeTile.dropIndex >= 0 )
            {
                submitTiles.push( cakeTile );
            }
        });

        /** @return int */
        function SortOnDropIndex(a, b){
            /** @namespace a.priority */
            /** @namespace b.priority */
            if( a.dropIndex < b.dropIndex )
            {
                return -1;
            }
            if(a.dropIndex > b.dropIndex )
            {
                return 1;
            }
            return 0;
        }

        submitTiles.sort(SortOnDropIndex);
        var submitIDs = [];
        for( var i = 0; i < submitTiles.length ; i ++ )
        {
            submitIDs.push( submitTiles[ i ].tileID );
        }

        cakeFactory.enable( false );

        var json = JSON.stringify({action: "save", submitIDs: submitIDs});
        redirectJsonPost( SUBMIT_LOCATION, "params", json );
    },

    enable: function ( enable ) {
        this.enableSubmit( enable );
    },

    enableSubmit: function ( enable ) {
        if( enable )
        {
            var canEnable = false;
            jQuery.each( cakeFactory.cakePlacer.cakeTiles, function( tileID, cakeTile ) {
                if( cakeTile.dropIndex >= 0 )
                {
                    canEnable = true;
                }
            });
            enable = canEnable;
        }

        enableCursorPointer(this.$submitButton, enable);
        if( enable )
        {
            this.$submitButton.unbind( "click", this.saveSelection );
            this.$submitButton.on("click", {}, this.saveSelection );
            this.$submitButton.removeClass( "disabled" );
        }
        else
        {
            this.$submitButton.unbind( "click", this.saveSelection );
            this.$submitButton.addClass( "disabled" );
        }
    }
};

CakePlacer.prototype = {

    createPlacer: function () {

        this.cakeGrabber = new CakeGrabber( cakeFactory.availableCupcakes.length );
        this.cakeDropper = new CakeDropper( cakeFactory.availableCupcakes.length );

        this.$grabberTitle = $("<h1>", {class: "placerTitle"}).text( "Our Cupcakes" );
        this.$dropperTitle = $("<h1>", {class: "placerTitle"}).text( "Party Menu" );
        this.cakeTiles = {};
        this.cakeTilesByGrabIndex = [];


        var $temporaryFactoryTileElement = $("<div>", {class: "factoryTile"});
        var tileWidth = cssAttributePixelValue( $temporaryFactoryTileElement, 'width' );
        var tileHeight = cssAttributePixelValue( $temporaryFactoryTileElement, 'height' );

        for( var i = 0; i < cakeFactory.availableCupcakes.length; i ++ )
        {
            var cakeTile = new CakeTile( cakeFactory.availableCupcakes[ i ]["id"], i,  CAKE_FOLDER_LOCATION + cakeFactory.availableCupcakes[ i ]["fileName"] , tileWidth, tileHeight );
            this.cakeTiles[ cakeTile.tileID ] = cakeTile;
            this.cakeTilesByGrabIndex[ cakeTile.preferredGrabIndex ] = cakeTile;
            this.$element.append( cakeTile.$element );
        }
    },
    findPositions: function ( animate, speed ) {
        var nextGrabIndex = 0;
        for( var i = 0; i < this.cakeTilesByGrabIndex.length; i ++ )
        {
            var cakeTile = this.cakeTilesByGrabIndex[ i ];
            var $placeHolder;
            if (cakeTile.dropIndex >= 0) {
                $placeHolder = this.cakeDropper.placeHolderTiles[cakeTile.dropIndex];
            }
            else
            {
                $placeHolder = this.cakeGrabber.placeHolderTiles[nextGrabIndex];
                nextGrabIndex++;
            }
            cakeTile.$currentPlaceholder = $placeHolder;
            if( !cakeTile.isDragging )
            {
                cakeTile.findPlaceholderPosition( animate, speed );
            }
        }
    },

    findPlaceholders: function ( animate, speed ) {
        jQuery.each( this.cakeTiles, function( tileID, cakeTile ) {
            cakeTile.findPlaceholderPosition( animate, speed );
        });
    },
    tileDragStarted: function () {
        var tileID = $(this).attr( "tileID" );
        var cakeTile = cakeFactory.cakePlacer.cakeTiles[ tileID ];
        cakeFactory.cakePlacer.bringTileToFront( tileID );
        cakeTile.isDragging = true;
    },

    tileDragStopped: function () {
        var tileID = $(this).attr( "tileID" );
        var cakeTile = cakeFactory.cakePlacer.cakeTiles[ tileID ];
        if( cakeTile.isDragging )
        {
            cakeTile.isDragging = false;
            cakeTile.findPlaceholderPosition( true );
        }
        cakeFactory.cakeInterface.enableSubmit( cakeFactory.factoryEnabled );
    },

    tileDragMove: function () {
        var tileID = $(this).attr( "tileID" );
        var cakeTile = cakeFactory.cakePlacer.cakeTiles[ tileID ];
        if( cakeTile.dropIndex >= 0 )
        {
            var isInGrabArea = cakeFactory.cakePlacer.checkIsInGrabArea( tileID );
            if( isInGrabArea )
            {
                jQuery.each( cakeFactory.cakePlacer.cakeTiles, function( checkTileID, checkCakeTile ) {
                    if( checkCakeTile.dropIndex > cakeTile.dropIndex )
                    {
                        checkCakeTile.dropIndex --;
                    }
                });
                cakeTile.dropIndex = -1;
                cakeFactory.cakePlacer.findPositions( true, 'fast' );
                return;
            }
        }
        var nearestDropIndex = cakeFactory.cakePlacer.checkForDropIndex( tileID );
        if( nearestDropIndex == cakeTile.dropIndex )
        {
            //Do nothing. No new Position found
            return;
        }
        jQuery.each( cakeFactory.cakePlacer.cakeTiles, function( checkTileID, checkCakeTile ) {
            if( checkTileID == tileID )
            {

            }
            else if( cakeTile.dropIndex < 0 || checkCakeTile.dropIndex < cakeTile.dropIndex )
            {
                if( checkCakeTile.dropIndex >= nearestDropIndex )
                {
                    checkCakeTile.dropIndex ++;
                }
            }
            else
            {
                if( checkCakeTile.dropIndex <= nearestDropIndex )
                {
                    checkCakeTile.dropIndex --;
                }
            }
        });
        cakeTile.dropIndex = nearestDropIndex;
        cakeFactory.cakePlacer.findPositions( true, 'fast' );
    },

    bringTileToFront: function ( tileID ) {
        var selectedTile = this.cakeTiles[ tileID ];
        var currentZIndex = selectedTile.$element.zIndex();

        var max = Math.max.apply(null, selectedTile.$element.siblings().map(function () {
            var zIndex = $(this).zIndex();
            if( zIndex > currentZIndex )
            {
                $(this).zIndex( zIndex - 1 );
            }
            return zIndex;
        }));
        selectedTile.$element.zIndex( ++ max  );
    },

    checkForDropIndex: function ( tileID ) {
        var selectedTile = this.cakeTiles[ tileID ];
        if( !checkForMinimumCrossover( this.cakeDropper.$element, selectedTile.$element, 0.5 ) )
        {
            return selectedTile.dropIndex;
        }
        var filledDropSlots = [];
        jQuery.each( this.cakeTiles, function( checkTileID, checkCakeTile ) {
            if( checkCakeTile.dropIndex >= 0 )
            {
                filledDropSlots.push( checkCakeTile );
            }
        });
        if( filledDropSlots.length == 0 || ( filledDropSlots.length == 1 && filledDropSlots[ 0 ].tileID == tileID ) )
        {
            return 0;
        }

        var validDropSlots = [];
        var checkCakeTile;
        var i;
        for( i = 0; i < filledDropSlots.length; i ++ )
        {
            checkCakeTile = filledDropSlots[ i ];
            if( checkForMinimumCrossover( this.cakeDropper.placeHolderTiles[ checkCakeTile.dropIndex ], selectedTile.$element, 0 ) )
            {
                validDropSlots.push( checkCakeTile );
            }
        }
        if( validDropSlots.length == 0 )
        {
            return selectedTile.dropIndex < 0 ? filledDropSlots.length : filledDropSlots.length - 1;
        }
        if( validDropSlots.length == 1 && validDropSlots[ 0 ].tileID == tileID )
        {
            return validDropSlots[ 0 ].dropIndex;
        }

        var bestSlot = validDropSlots[ 0 ];
        var shortestDistance = distanceBetweenCentres( this.cakeDropper.placeHolderTiles[ bestSlot.dropIndex ], selectedTile.$element );
        for( i = 1; i < validDropSlots.length; i ++ )
        {
            checkCakeTile = validDropSlots[ i ];
            var distance = distanceBetweenCentres( this.cakeDropper.placeHolderTiles[ checkCakeTile.dropIndex ], selectedTile.$element );
            if( distance < shortestDistance )
            {
                bestSlot = checkCakeTile;
                shortestDistance = distance;
            }
        }
        if( selectedTile.dropIndex < 0 )
        {
            var firstEmptySlotDistance = distanceBetweenCentres( this.cakeDropper.placeHolderTiles[ filledDropSlots.length ], selectedTile.$element );
            if( firstEmptySlotDistance < shortestDistance )
            {
                return filledDropSlots.length;
            }
        }

        return bestSlot.dropIndex;
    },

    checkIsInGrabArea: function ( tileID ) {
        var selectedTile = this.cakeTiles[ tileID ];
        return checkForMinimumCrossover( this.cakeGrabber.$element, selectedTile.$element, 0.5 );
    },

    enable: function ( enable ) {
        jQuery.each( cakeFactory.cakePlacer.cakeTiles, function( tileID, cakeTile ) {
            cakeTile.enable( enable );
        });
    }
};

CakeTile.prototype = {

    findPlaceholderPosition: function ( animate, speed ) {
        if( !this.$currentPlaceholder )
        {
            return;
        }
        this.$element.stop();
        var offsetTo = this.$currentPlaceholder.offset();
        if( !animate )
        {
            this.$element.offset({ left: offsetTo.left, top: offsetTo.top });
        }
        else
        {
            var left = ( offsetTo.left ) - ( this.$element.offset().left - this.$element.position().left );
            var top = ( offsetTo.top) - ( this.$element.offset().top - this.$element.position().top );
            this.$element.animate({ left: left, top: top }, speed );
        }
    },

    enable: function ( enable ) {

        this.$element.draggable("option", "disabled", !enable );
        enableCursorPointer( this.$element, enable );
    }
};

var PreloadImages = function(imageUrls, callback) {
    var i,
        j,
        loaded = 0,
        failedToLoadURLs = [];

    function checkComplete() {
        if( !callback || loaded + failedToLoadURLs.length < imageUrls.length )
        {
            return;
        }
        callback( failedToLoadURLs );
    }

    for (i = 0, j = imageUrls.length; i < j; i++) {
        (function (img, src) {
            img.onload = function () {
                loaded ++;
                checkComplete();
            };

            img.onerror = function () {
                failedToLoadURLs.push( src );
                checkComplete();
            };
            img.onabort = function () {
                failedToLoadURLs.push( src );
                checkComplete();
            };

            img.src = src;
        } (new Image(), imageUrls[i]));
    }
};


function enableCursorPointer( $element, enable ) {

    if (enable)
    {
        $element.hover(
            function() {
                $(this).css('cursor', 'pointer');
            },
            function() {
                $(this).css('cursor', '');
            }
        );

    } else {
        $element.hover(
            function() {
                $(this).css('cursor', '');
            },
            function() {
                $(this).css('cursor', '');
            }
        );
    }
}


function checkForMinimumCrossover( $targetElement, $movingElement, minimumCrossover )
{
    var minimumLeft = $targetElement.offset().left - $movingElement.width() * (1 - minimumCrossover );
    var maximumLeft = $targetElement.offset().left + $targetElement.width() - $movingElement.width() * minimumCrossover;
    var minimumTop = $targetElement.offset().top - $movingElement.height() * (1 - minimumCrossover );
    var maximumTop = $targetElement.offset().top + $targetElement.height() - $movingElement.height() * minimumCrossover;

    return $movingElement.offset().left >= minimumLeft && $movingElement.offset().left <= maximumLeft && $movingElement.offset().top >= minimumTop && $movingElement.offset().top <= maximumTop;
}

function distanceBetweenCentres( $targetElement, $movingElement )
{
    var distanceLeft = ( $targetElement.offset().left + $targetElement.width() / 2 ) - ( $movingElement.offset().left + $movingElement.width() / 2 );
    var distanceTop = ( $targetElement.offset().top + $targetElement.height() / 2 ) - ( $movingElement.offset().top + $movingElement.height() / 2 );
    return Math.sqrt( distanceLeft * distanceLeft + distanceTop * distanceTop );
}

function cssAttributePixelValue( $temporaryElement, attribute )
{
    $temporaryElement.hide().appendTo("body");
    var value = elementCssPixelValue( $temporaryElement, attribute );
    $temporaryElement.remove();
    return value;
}

function elementCssPixelValue( $element, attribute )
{
    var stringValue = $element.css( attribute );
    return parseInt( stringValue.substr( 0,stringValue.length - 2) );
}