var WELCOME_LOCATION = "";
var JSON_EDITOR_LOCATION = "http://www.jsoneditoronline.org/";
var thankYouInterface;


$(document).ready(
    function()
    {
        var $thankYouInterfaceElement = $("#thankYouInterface");
        thankYouInterface = new ThankYouInterface( $thankYouInterfaceElement );

        if ( viewProperties == null ) {
            thankYouInterface.failedToLoad();
            return;
        }
        thankYouInterface.applyViewProperties();
        if ( thankYouInterface.orderCupcakes == null || thankYouInterface.orderCupcakes.length == 0 ) {
            thankYouInterface.failedToLoad();
            return;
        }
        thankYouInterface.insertJsonText();
    }
);

var ThankYouInterface = function( element ){
    this.$element  = $(element);
    this.orderCupcakes = null;
    this.$jsonTextArea = this.$element.find( "#jsonTextArea" );
    this.$jsonEditorButton = this.$element.find( "#jsonEditorButton" );
    this.$orderAgainButton = this.$element.find( "#orderAgainButton" );

    this.$jsonEditorButton.on("click", {}, openJsonEditor );
    this.$orderAgainButton.on("click", {}, orderAgain );

    function openJsonEditor()
    {
        window.open( JSON_EDITOR_LOCATION, '_blank');
    }

    function orderAgain()
    {
        window.location = providedApplicationRoot + WELCOME_LOCATION;
    }
};

ThankYouInterface.prototype = {

    applyViewProperties: function () {
        /** @namespace viewProperties.orderCupcakes */
        if (typeof viewProperties.orderCupcakes === 'undefined') {
            return;
        }
        this.orderCupcakes = viewProperties.orderCupcakes;
    },

    failedToLoad: function () {
        var $feedback = $("<p>");
        $feedback.text("No order received");
        $feedback.css({color:'#ff0000'});
        var $errorContainer = $("#errorContainer");
        thankYouInterface.$element.hide();
        $errorContainer.append( $feedback );
    },

    insertJsonText: function () {
        thankYouInterface.$jsonTextArea.text(JSON.stringify(thankYouInterface.orderCupcakes));
    }

};