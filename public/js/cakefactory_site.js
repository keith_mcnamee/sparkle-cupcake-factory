var viewProperties;
var providedApplicationRoot = "/";

$(document).ready(
    function()
    {
        parseViewProperties();

        function parseViewProperties()
        {
            if ( typeof applicationRoot !== 'undefined' ) {
                providedApplicationRoot = applicationRoot;
            }

            if ( typeof jsonViewProperties === 'undefined' ) {
                return;
            }
            viewProperties = parseJsonString( jsonViewProperties );
        }
    }
);

function parseJsonString( string )
{
    var vars = null;
    try
    {
        vars = jQuery.parseJSON( string );
    }
    catch (err)
    {
        // will return null
    }
    return vars;
}

function redirectJsonPost(location, name, json)
{
    var form = '';
    form += "<input type='hidden' name='"+name+"' value='"+json+"'>";

    $("<form action='"+location+"' method='POST'>"+form+"</form>").appendTo("body").submit();
}