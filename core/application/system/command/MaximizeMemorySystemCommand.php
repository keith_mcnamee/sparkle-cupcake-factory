<?php


class MaximizeMemorySystemCommand extends CoreBase
{
    public function command()
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 0);
    }
} 