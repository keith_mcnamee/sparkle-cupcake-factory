<?php


class SystemCorePath
{

    public function __construct()
    {
        $this->getIncludes();
    }

    private function getIncludes()
    {
        require_once( DIR_CORE . "/application/system/includes/SystemCoreIncludes.php" );
    }

    /**
     * @return MaximizeMemorySystemCommand
     */
    public function getMaximizeMemoryCommand()
    {
        return new MaximizeMemorySystemCommand();
    }
}