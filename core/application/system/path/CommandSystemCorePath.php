<?php


class CommandSystemCorePath
{

    /**
     * @var MaximizeMemorySystemCommand
     */
    private $maximizeMemory;

    public function __destruct()
    {
        $this->maximizeMemory = null;
    }

    /**
     * @param $singleInstance
     * @return MaximizeMemorySystemCommand
     */
    private function createMaximizeMemory( $singleInstance )
    {
        $instance = new MaximizeMemorySystemCommand();
        $instance->init();
        if( !$singleInstance )
        {
            $this->maximizeMemory = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return MaximizeMemorySystemCommand
     */
    public function getMaximizeMemory( $singleInstance = true )
    {
        return $this->maximizeMemory && !$singleInstance ? $this->maximizeMemory : $this->createMaximizeMemory( $singleInstance );
    }
}