<?php


class ResponseDatabaseParser extends CoreBase
{

    /**
     * @param mysqli_stmt $query
     * @param bool $singleItem
     * @return array
     */
    public function responseParse( mysqli_stmt $query, $singleItem = false )
    {
        $meta = $query->result_metadata();
        $fieldNames = array();
        $fields = array();
        while ( $field = $meta->fetch_field() )
        {
            $var = $field->name;
            $fieldNames[ ] = $var;
            $$var = null;
            $fields[ $var ] = &$$var;
        }

        call_user_func_array( array ( $query, 'bind_result' ), $fields );

        $affectedRows = array();
        $rowIndex = - 1;
        while ( $query->fetch() )
        {
            $rowIndex ++;
            $affectedRows[ $rowIndex ] = array();
            for ( $fieldIndex = 0; $fieldIndex < count( $fieldNames ); $fieldIndex ++ )
            {
                $affectedRows[ $rowIndex ][ $fieldNames[ $fieldIndex ] ] = $fields[ $fieldNames[ $fieldIndex ] ];
            }

            if( $singleItem )
            {
                return $affectedRows[0];
            }
        }

        return $affectedRows;
    }
}