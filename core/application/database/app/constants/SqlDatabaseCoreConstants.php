<?php


class SqlDatabaseCoreConstants
{

    /*  shared  */
    public static $BALANCING_FILE_INFO_TABLE = "balancing_file_info";
    public static $LOCALIZATION_FILE_INFO_TABLE = "localization_file_info";
    public static $REQUEST_INFO_TABLE = "request_info";

    /*  Generic  */

    public static $REF = "ref";
    public static $RECOGNIZER = "recognizer";
    public static $UPDATED = "updated";
    public static $CREATED = "created";

    /*  DownloadInfo  */

    public static $VERSION_INFO = "version";
    public static $FILE_LOCATION_INFO = "file_location";
    public static $LAST_MODIFIED_DATE = "last_modified_date";
    public static $FILE_SIZE = "file_size";

    /*  LocalizationDownloadInfo  */

    public static $LANGUAGE_INFO = "language";

    /*  Update Info  */

    public static $RANDOM_ID = "random_id";
    public static $EXPIRED_DATE = "expired_date";
}