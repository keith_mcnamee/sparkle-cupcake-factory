<?php


class DatabaseCoreConstants
{
    public static $STRING_ENTRY_BIND_TYPE = "s";
    public static $INTEGER_ENTRY_BIND_TYPE = "i";
    public static $DOUBLE_ENTRY_BIND_TYPE = "d";
    public static $BLOB_ENTRY_BIND_TYPE = "b";

    public static $ITEM_STRING_SEPARATOR = "**SEP**";

}