<?php


class BasicDatabaseRequest extends CoreBase
{

    /**
     * @var DatabaseCoreModel
     */
    private $coreDatabaseModel;

    /**
     * @var SqlBindDatabaseConstructor
     */
    private $sqlBindConstructor;

    /**
     * @var ResponseDatabaseParser
     */
    private $responseParser;

    public function __destruct()
    {
        $this->coreDatabaseModel = null;
        $this->sqlBindConstructor = null;
        $this->responseParser = null;

        parent::__destruct();
    }

    /**
     * @param InsertRequestDatabaseCoreVO $insertVO
     * @param SelectRequestDatabaseCoreVO $selectVO
     * @param ConnectionDatabaseCoreVO $databaseVO
     * @return array
     * @throws Exception
     */
    public function insert( InsertRequestDatabaseCoreVO $insertVO, SelectRequestDatabaseCoreVO $selectVO = null, ConnectionDatabaseCoreVO $databaseVO = null )
    {
        $closeConnection = true;
        if( !$databaseVO )
        {
            $closeConnection = false;
            $configVO = $this->getCoreDatabaseModel()->getCurrentConfigVO();
            $databaseVO = $this->databaseConnect( $configVO );
            $this->validateResponse( $databaseVO );
        }

        $sqlBindVO = $this->getSqlBindConstructor()->insertVoConstruct( $insertVO, $databaseVO->getMysqli() );

        $query = $this->request( $sqlBindVO, $databaseVO );
        $this->validateResponse( $databaseVO );

        if( !$selectVO )
        {
            $this->closeDatabaseConnection( $databaseVO );
            return null;
        }
        $selectBindVO = $this->getSqlBindConstructor()->selectInsertedVoConstruct( $insertVO, $selectVO, $query->insert_id, $query->affected_rows, $databaseVO->getMysqli() );
        $selectQuery = $this->request( $selectBindVO, $databaseVO );
        $this->validateResponse( $databaseVO );

        $entries = $this->getResponseParser()->responseParse( $selectQuery );

        if( $closeConnection )
        {
            $databaseVO->getMysqli()->close();
        }

        return $entries;
    }

    /**
     * @param SelectRequestDatabaseCoreVO $selectVO
     * @param ConnectionDatabaseCoreVO $databaseVO
     * @param bool $singleEntry
     * @throws Exception
     * @return array
     */
    public function select( SelectRequestDatabaseCoreVO $selectVO, ConnectionDatabaseCoreVO $databaseVO = null, $singleEntry = false )
    {
        $closeConnection = true;
        if( !$databaseVO )
        {
            $closeConnection = false;
            $configVO = $this->getCoreDatabaseModel()->getCurrentConfigVO();
            $databaseVO = $this->databaseConnect( $configVO );
            $this->validateResponse( $databaseVO );
        }

        $sqlBindVO = $this->getSqlBindConstructor()->selectVoConstruct( $selectVO, $databaseVO->getMysqli() );

        $query = $this->request( $sqlBindVO, $databaseVO );
        $this->validateResponse( $databaseVO );

        $entries = $this->getResponseParser()->responseParse( $query, $singleEntry );

        if( $closeConnection )
        {
            $databaseVO->getMysqli()->close();
        }

        return $entries;
    }

    /**
     * @param UpdateRequestDatabaseCoreVO $updateVO
     * @param SelectRequestDatabaseCoreVO $selectVO
     * @param ConnectionDatabaseCoreVO $databaseVO
     * @return array
     * @throws Exception
     */
    public function update( UpdateRequestDatabaseCoreVO $updateVO, SelectRequestDatabaseCoreVO $selectVO = null, ConnectionDatabaseCoreVO $databaseVO = null )
    {
        $closeConnection = true;
        if( !$databaseVO )
        {
            $closeConnection = false;
            $configVO = $this->getCoreDatabaseModel()->getCurrentConfigVO();
            $databaseVO = $this->databaseConnect( $configVO );
            $this->validateResponse( $databaseVO );
        }

        $sqlBindVO = $this->getSqlBindConstructor()->updateVoConstruct( $updateVO, $databaseVO->getMysqli() );
        $query = $this->request( $sqlBindVO, $databaseVO );
        $this->validateResponse( $databaseVO );

        if( !$selectVO )
        {
            $this->closeDatabaseConnection( $databaseVO );
            return null;
        }

        $selectBindVO = $this->getSqlBindConstructor()->selectUpdatedVoConstruct( $updateVO, $selectVO, $databaseVO->getMysqli() );
        $selectQuery = $this->request( $selectBindVO, $databaseVO );
        $this->validateResponse( $databaseVO );

        $entries = $this->getResponseParser()->responseParse( $selectQuery );

        if( $closeConnection )
        {
            $databaseVO->getMysqli()->close();
        }

        return $entries;
    }

    /**
     * @param DeleteRequestDatabaseCoreVO $deleteVO
     * @param SelectRequestDatabaseCoreVO $selectVO
     * @param ConnectionDatabaseCoreVO $databaseVO
     * @return array
     * @throws Exception
     */
    public function delete( DeleteRequestDatabaseCoreVO $deleteVO, SelectRequestDatabaseCoreVO $selectVO = null, ConnectionDatabaseCoreVO $databaseVO = null )
    {
        $closeConnection = true;
        if( !$databaseVO )
        {
            $closeConnection = false;
            $configVO = $this->getCoreDatabaseModel()->getCurrentConfigVO();
            $databaseVO = $this->databaseConnect( $configVO );
            $this->validateResponse( $databaseVO );
        }

        $entries = null;
        if( $selectVO )
        {
            $selectBindVO = $this->getSqlBindConstructor()->selectDeletedVoConstruct( $deleteVO, $selectVO, $databaseVO->getMysqli() );
            $selectQuery = $this->request( $selectBindVO, $databaseVO );
            $this->validateResponse( $databaseVO );

            $entries = $this->getResponseParser()->responseParse( $selectQuery );
        }

        $sql = $this->getSqlBindConstructor()->deleteVoConstruct( $deleteVO, $databaseVO->getMysqli() );
        $query = $this->request( $sql, $databaseVO );
        $this->validateResponse( $databaseVO );

        $entriesDeleted = $query->affected_rows;

        if( $closeConnection )
        {
            $databaseVO->getMysqli()->close();
        }

        return $entries;
    }

    /**
     * @param ConnectionDatabaseCoreVO $databaseVO
     * @throws Exception
     */
    private function validateResponse( ConnectionDatabaseCoreVO $databaseVO )
    {
        if($databaseVO->isFailed())
        {
            $error = $databaseVO->getError();
            if( $this->getCoreApp()->getInfoHelper()->isTestServer() && $databaseVO->getPrivateError() )
            {
                $error .= " ".$databaseVO->getPrivateError();
            }
            throw new Exception( $error );
        }
    }

    /**
     * @param SqlBindDatabaseCoreVO $bindVO
     * @param ConnectionDatabaseCoreVO $databaseVO
     * @return mysqli_stmt
     */
    private function request( SqlBindDatabaseCoreVO $bindVO, ConnectionDatabaseCoreVO $databaseVO )
    {
        $bindParam = array();
        $bind = $bindVO->getBind();
        $values = $bindVO->getValues();

        $bindParam[] = & $bind;

        for( $i = 0; $i < count($values); $i ++ )
        {
            $bindParam[] = & $values[$i];
        }

        $query = $databaseVO->getMysqli()->stmt_init();

        $prepared =  $query->prepare($bindVO->getSql());

        if( !$prepared )
        {
            $databaseVO->setFailed( true );
            $databaseVO->setError( "Invalid Sql." );
            return null;
        }

        if( count($bindParam) > 1 )
        {
            call_user_func_array(array($query, 'bind_param'), $bindParam);
        }
        $query->execute();

        if( $query->errno )
        {
            $databaseVO->setFailed( true );
            $databaseVO->setError( "Invalid sql parameters." );
            $databaseVO->setPrivateError( $query->errno );
        }
        return $query;
    }

    /**
     * @param ConfigDatabaseCoreVO $configVO
     * @return ConnectionDatabaseCoreVO
     */
    public function databaseConnect( ConfigDatabaseCoreVO $configVO )
    {
        $databaseVO = new ConnectionDatabaseCoreVO();

        $mysqli = new mysqli( $configVO->getHostName(), $configVO->getUser(), $configVO->getPassword(), $configVO->getDatabaseName()  );

        $databaseVO->setMysqli( $mysqli );

        if ( mysqli_connect_errno() )
        {
            $databaseVO->setFailed( true );
            $databaseVO->setError( "Database Connection Error [1]." );
        }

        return $databaseVO;
    }


    /**
     * @param ConnectionDatabaseCoreVO $databaseVO
     */
    public function closeDatabaseConnection( ConnectionDatabaseCoreVO $databaseVO )
    {
        $databaseVO->getMysqli()->close();
    }

    /**
     * @return DatabaseCoreModel
     */
    private function getCoreDatabaseModel()
    {
        if( !$this->coreDatabaseModel )
        {
            $this->coreDatabaseModel = $this->getCoreDatabase()->getDatabaseModel();
        }
        return $this->coreDatabaseModel;
    }

    /**
     * @return SqlBindDatabaseConstructor
     */
    private function getSqlBindConstructor()
    {
        if( !$this->sqlBindConstructor )
        {
            $this->sqlBindConstructor = $this->getCoreDatabase()->getSqlBindConstructor();
        }
        return $this->sqlBindConstructor;
    }

    /**
     * @return ResponseDatabaseParser
     */
    private function getResponseParser()
    {
        if( !$this->responseParser )
        {
            $this->responseParser = $this->getCoreDatabase()->getResponseParser();
        }
        return $this->responseParser;
    }
} 