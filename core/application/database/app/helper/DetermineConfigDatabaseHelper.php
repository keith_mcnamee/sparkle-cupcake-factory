<?php


class DetermineConfigDatabaseHelper extends CoreBase
{

    /**
     * @param array $configVOs
     * @param bool $checkServerID
     * @param bool $checkIsStaging
     * @return ConfigDatabaseCoreVO
     */
    public function singleConfig( array $configVOs, $checkServerID = false, $checkIsStaging = false )
    {
        $configVOs = $this->multipleConfigs( $configVOs, $checkServerID, $checkIsStaging );
        foreach( $configVOs as $vo )
        {
            /* @var $vo ConfigDatabaseCoreVO */
            return $vo;
        }
        return null;
    }

    /**
     * @param array $configVOs
     * @param bool $checkServerID
     * @param bool $checkIsStaging
     * @return array
     */
    public function multipleConfigs( array $configVOs, $checkServerID = false, $checkIsStaging = false )
    {
        $serverConfigID = null;
        $isStagingDatabase = false;
        $coreRequestModel = $this->getCoreRequest()->getRequestModel();
        if( $checkIsStaging )
        {
            $isUsingStagingDatabase = $coreRequestModel->getServerConfigVO()->isUsingStagingDatabase();
            $isStagingDirectory = $this->getCoreApp()->getInfoHelper()->isStagingDirectory();
            $isStagingDatabase = $isStagingDirectory && $isUsingStagingDatabase;
        }
        if( $checkServerID )
        {
            $serverConfigID = $coreRequestModel->getServerConfigVO()->getConfigID();
        }

        $returnVOs = array();
        $databaseConfigIDs = array();
        foreach( $configVOs as $configVO )
        {
            /* @var $configVO ConfigDatabaseCoreVO */
            $databaseConfigIDs[ $configVO->getConfigID() ] = true;
        }
        $databaseConfigIDs = CoreHelper::getArrayHelper()->dictionaryKeyToArray( $databaseConfigIDs );
        foreach( $databaseConfigIDs as $databaseConfigID )
        {
            /* @var $databaseConfigID string */
            $thisConfigVOs = $configVOs;
            if( count( $databaseConfigIDs ) > 1 )
            {
                $thisConfigVOs = $this->filterByConfigID( $thisConfigVOs, $databaseConfigID );
            }

            if( $checkServerID )
            {
                $thisConfigVOs = $this->filterByServerID( $thisConfigVOs, $serverConfigID );
            }

            if( $checkIsStaging )
            {
                $thisConfigVOs = $this->filterByIsStaging( $thisConfigVOs, $isStagingDatabase );
            }

            if( count( $thisConfigVOs ) == 1 )
            {
                $returnVOs[ $databaseConfigID ] = $thisConfigVOs[ 0 ];
            }
            else
            {
                $returnVOs[ $databaseConfigID ] = null;
            }
        }

        return $returnVOs;
    }

    /**
     * @param array $configVOs
     * @param string $databaseConfigID
     * @return array
     */
    private function filterByConfigID( array $configVOs, $databaseConfigID )
    {
        $typeVOs = array();

        foreach( $configVOs as $vo )
        {
            /* @var $vo ConfigDatabaseCoreVO */
            if( $vo->getConfigID() == $databaseConfigID )
            {
                $typeVOs[] = $vo;
            }
        }

        return $typeVOs;
    }

    /**
     * @param array $configVOs
     * @param string $serverConfigID
     * @return array
     */
    private function filterByServerID( array $configVOs, $serverConfigID )
    {
        $typeVOs = array();

        foreach( $configVOs as $vo )
        {
            /* @var $vo ConfigDatabaseCoreVO */
            if( $vo->getServerConfigType() == $serverConfigID )
            {
                $typeVOs[] = $vo;
            }
        }

        return $typeVOs;
    }


    /**
     * @param array $configVOs
     * @param boolean $isStaging
     * @return array
     */
    private function filterByIsStaging( array $configVOs, $isStaging )
    {
        $typeVOs = array();
        foreach( $configVOs as $vo )
        {
            /* @var $vo ConfigDatabaseCoreVO */
            if( $vo->isStagingDatabase() == $isStaging )
            {
                $typeVOs[] = $vo;
            }
        }

        return $typeVOs;
    }
}