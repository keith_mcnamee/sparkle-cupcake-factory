<?php


class InitializeDatabaseCommand extends CoreBase
{
    /**
     * @param string $configID
     * @param array $configVOs
     */
    public function command( $configID, array $configVOs )
    {
        $coreDatabaseModel = $this->getCoreDatabase()->getDatabaseModel();
        $coreDatabaseModel->setDefaultConfigID( $configID );
        $coreDatabaseModel->setConfigVOs( $configVOs );
    }
} 