<?php


class ProcessQueueDatabaseCommand extends CoreBase
{

    /**
     * @var BasicDatabaseRequest
     */
    private $basicDatabaseRequest;

    public function init()
    {
        parent::init();

        $this->basicDatabaseRequest = $this->getCoreDatabase()->getRequest()->getBasic();
    }

    public function __destruct()
    {
        $this->basicDatabaseRequest = null;

        parent::__destruct();
    }

    /**
     * @param QueueDatabaseCoreVO $queueVO
     * @param string $databaseID
     */
    public function command( QueueDatabaseCoreVO $queueVO = null, $databaseID = null )
    {
        if( !$queueVO )
        {
            $queueVO = $this->getCoreDatabase()->getModel()->getModel()->getQueueVO();
        }
        $typeQueue = $queueVO->getTypeQueue();

        foreach( $typeQueue as $itemType )
        {
            /* @var $itemType string */
            switch( $itemType )
            {
                case $queueVO::$INSERT_QUEUE_ITEM:
                    $this->processInsert( $queueVO, $databaseID );
                    break;

                case $queueVO::$UPDATE_QUEUE_ITEM:
                    $this->processUpdate( $queueVO, $databaseID );
                    break;

                case $queueVO::$DELETE_QUEUE_ITEM:
                    $this->processDelete( $queueVO, $databaseID );
                    break;
            }
        }

        $queueVO->reset();
    }

    /**
     * @param QueueDatabaseCoreVO $queueVO
     * @param string $databaseID
     */
    private function processInsert( QueueDatabaseCoreVO $queueVO, $databaseID = null )
    {
        $insertQueue = $queueVO->getInsertQueue();
        if( empty( $insertQueue ) )
        {
            return;
        }
        $nextQueue = $insertQueue[0];
        foreach( $nextQueue as $insertVO )
        {
            /* @var $insertVO InsertRequestDatabaseCoreVO */
            $this->basicDatabaseRequest->insert( $insertVO, null, $databaseID );
        }
        $insertQueue = CoreHelper::getArrayCommand()->deleteObjectAtIndex( $insertQueue, 0 );
        $queueVO->setInsertQueue( $insertQueue );
    }

    /**
     * @param QueueDatabaseCoreVO $queueVO
     * @param string $databaseID
     */
    private function processUpdate( QueueDatabaseCoreVO $queueVO, $databaseID = null )
    {
        $updateQueue = $queueVO->getUpdateQueue();
        if( empty( $updateQueue ) )
        {
            return;
        }
        $nextQueue = $updateQueue[0];
        foreach( $nextQueue as $updateVO )
        {
            /* @var $updateVO UpdateRequestDatabaseCoreVO */
            $this->basicDatabaseRequest->update( $updateVO, null, $databaseID );
        }
        $updateQueue = CoreHelper::getArrayCommand()->deleteObjectAtIndex( $updateQueue, 0 );
        $queueVO->setUpdateQueue( $updateQueue );
    }

    /**
     * @param QueueDatabaseCoreVO $queueVO
     * @param string $databaseID
     */
    private function processDelete( QueueDatabaseCoreVO $queueVO, $databaseID = null )
    {
        $deleteQueue = $queueVO->getDeleteQueue();
        if( empty( $deleteQueue ) )
        {
            return;
        }
        $nextQueue = $deleteQueue[0];
        foreach( $nextQueue as $deleteVO )
        {
            /* @var $deleteVO DeleteRequestDatabaseCoreVO */
            $this->basicDatabaseRequest->delete( $deleteVO, null, $databaseID );
        }
        $deleteQueue = CoreHelper::getArrayCommand()->deleteObjectAtIndex( $deleteQueue, 0 );
        $queueVO->setInsertQueue( $deleteQueue );
    }
}