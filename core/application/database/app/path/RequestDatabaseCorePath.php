<?php


class RequestDatabaseCorePath
{

    /**
     * @var BasicDatabaseRequest
     */
    private $basic;

    public function __destruct()
    {
        $this->basic = null;
    }

    /**
     * @param $singleInstance
     * @return BasicDatabaseRequest
     */
    private function createBasic( $singleInstance )
    {
        $instance = new BasicDatabaseRequest();
        $instance->init();
        if( !$singleInstance )
        {
            $this->basic = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return BasicDatabaseRequest
     */
    public function getBasic( $singleInstance = true )
    {
        return $this->basic && !$singleInstance ? $this->basic : $this->createBasic( $singleInstance );
    }
}