<?php


class ModelDatabaseCorePath
{

    /**
     * @var DatabaseCoreModel
     */
    private $model;

    public function __destruct()
    {
        $this->model = null;
    }

    /**
     * @param $singleInstance
     * @return DatabaseCoreModel
     */
    private function createModel( $singleInstance )
    {
        $instance = new DatabaseCoreModel();
        $instance->init();
        if( !$singleInstance )
        {
            $this->model = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return DatabaseCoreModel
     */
    public function getModel( $singleInstance = false )
    {
        return $this->model && !$singleInstance ? $this->model : $this->createModel( $singleInstance );
    }
}