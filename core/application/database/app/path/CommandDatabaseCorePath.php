<?php


class CommandDatabaseCorePath
{
    /**
     * @var InitializeDatabaseCommand
     */
    private $initialize;

    /**
     * @var ProcessQueueDatabaseCommand
     */
    private $processQueue;

    public function __destruct()
    {
        $this->initialize = null;
        $this->processQueue = null;
    }

    /**
     * @param $singleInstance
     * @return InitializeDatabaseCommand
     */
    private function createInitialize( $singleInstance )
    {
        $instance = new InitializeDatabaseCommand();
        $instance->init();
        if( !$singleInstance )
        {
            $this->initialize = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return ProcessQueueDatabaseCommand
     */
    private function createProcessQueue( $singleInstance )
    {
        $instance = new ProcessQueueDatabaseCommand();
        $instance->init();
        if( !$singleInstance )
        {
            $this->processQueue = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return InitializeDatabaseCommand
     */
    public function getInitialize( $singleInstance = true )
    {
        return $this->initialize && !$singleInstance ? $this->initialize : $this->createInitialize( $singleInstance );
    }

    /**
     * @param $singleInstance
     * @return ProcessQueueDatabaseCommand
     */
    public function getProcessQueue( $singleInstance = true )
    {
        return $this->processQueue && !$singleInstance ? $this->processQueue : $this->createProcessQueue( $singleInstance );
    }
}