<?php


class ConstructDatabaseCorePath
{

    /**
     * @var BasicDatabaseConstructor
     */
    private $basic;

    /**
     * @var ConditionDatabaseConstructor
     */
    private $condition;

    /**
     * @var SqlBindDatabaseConstructor
     */
    private $sqlBind;

    public function __destruct()
    {
        $this->basic = null;
        $this->condition = null;
        $this->sqlBind = null;
    }

    /**
     * @param $singleInstance
     * @return BasicDatabaseConstructor
     */
    private function createBasic( $singleInstance )
    {
        $instance = new BasicDatabaseConstructor();
        $instance->init();
        if( !$singleInstance )
        {
            $this->basic = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return ConditionDatabaseConstructor
     */
    private function createCondition( $singleInstance )
    {
        $instance = new ConditionDatabaseConstructor();
        $instance->init();
        if( !$singleInstance )
        {
            $this->condition = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return SqlBindDatabaseConstructor
     */
    private function createSqlBind( $singleInstance )
    {
        $instance = new SqlBindDatabaseConstructor();
        $instance->init();
        if( !$singleInstance )
        {
            $this->sqlBind = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return BasicDatabaseConstructor
     */
    public function getBasic( $singleInstance = true )
    {
        return $this->basic && !$singleInstance ? $this->basic : $this->createBasic( $singleInstance );
    }

    /**
     * @param $singleInstance
     * @return ConditionDatabaseConstructor
     */
    public function getCondition( $singleInstance = true )
    {
        return $this->condition && !$singleInstance ? $this->condition : $this->createCondition( $singleInstance );
    }

    /**
     * @param $singleInstance
     * @return SqlBindDatabaseConstructor
     */
    public function getSqlBind( $singleInstance = true )
    {
        return $this->sqlBind && !$singleInstance ? $this->sqlBind : $this->createSqlBind( $singleInstance );
    }
}