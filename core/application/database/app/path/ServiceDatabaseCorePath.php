<?php


class ServiceDatabaseCorePath
{

    /**
     * @var DetermineConfigDatabaseCoreService
     */
    private $determineConfig;

    public function __destruct()
    {
        $this->determineConfig = null;
    }

    /**
     * @param $singleInstance
     * @return DetermineConfigDatabaseCoreService
     */
    private function createDetermineConfig( $singleInstance )
    {
        $instance = new DetermineConfigDatabaseCoreService();
        $instance->init();
        if( !$singleInstance )
        {
            $this->determineConfig = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return DetermineConfigDatabaseCoreService
     */
    public function getDetermineConfig( $singleInstance = true )
    {
        return $this->determineConfig && !$singleInstance ? $this->determineConfig : $this->createDetermineConfig( $singleInstance );
    }
}