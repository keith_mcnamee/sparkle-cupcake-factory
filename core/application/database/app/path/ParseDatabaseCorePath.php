<?php


class ParseDatabaseCorePath
{

    /**
     * @var ResponseDatabaseParser
     */
    private $response;

    public function __destruct()
    {
        $this->response = null;
    }

    /**
     * @param $singleInstance
     * @return ResponseDatabaseParser
     */
    private function createResponse( $singleInstance )
    {
        $instance = new ResponseDatabaseParser();
        $instance->init();
        if( !$singleInstance )
        {
            $this->response = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return ResponseDatabaseParser
     */
    public function getResponse( $singleInstance = true )
    {
        return $this->response && !$singleInstance ? $this->response : $this->createResponse( $singleInstance );
    }
}