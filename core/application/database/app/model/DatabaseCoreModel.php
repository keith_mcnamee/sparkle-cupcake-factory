<?php


class DatabaseCoreModel extends CoreBase
{

    /**
     * @var array
     */
    private $configVOs;

    /**
     * @var string
     */
    private $defaultConfigID;

    /**
     * @var string
     */
    private $currentConfigID;

    /**
     * @var QueueDatabaseCoreVO
     */
    private $queueVO;

    public function __construct()
    {
        $this->setConfigVOs( array () );
        $this->setQueueVO( new QueueDatabaseCoreVO() );
    }

    /**
     * @return ConfigDatabaseCoreVO
     */
    public function defaultConfigVO()
    {
        $configID = $this->getDefaultConfigID();
        $configVO = $this->getConfigVO( $configID );

        return $configVO;
    }

    /**
     * @param ConfigDatabaseCoreVO $object
     * @param int $key
     */
    public function addConfigVO( ConfigDatabaseCoreVO $object, $key )
    {
        $this->setConfigVOs( CoreHelper::getArrayHelper()->setObjectForKey( $this->getConfigVOs(), $object, $key ) );
    }


    /**
     * @return ConfigDatabaseCoreVO
     */
    public function getCurrentConfigVO()
    {
        $configID = $this->getCurrentConfigID();
        $configID = $configID ? $configID : $this->getDefaultConfigID();

        return $this->getConfigVO( $configID );
    }

    /**
     * @param int $key
     * @return ConfigDatabaseCoreVO
     */
    public function getConfigVO( $key )
    {
        $object = CoreHelper::getArrayHelper()->getObjectForKey( $this->getConfigVOs(), $key );

        return $object;
    }

    /**
     * @return array
     */
    public function getConfigVOs()
    {
        return $this->configVOs;
    }

    /**
     * @param array $value
     */
    public function setConfigVOs( array $value )
    {
        $this->configVOs = $value;
    }

    /**
     * @return string
     */
    public function getDefaultConfigID()
    {
        return $this->defaultConfigID;
    }

    /**
     * @param string $value
     */
    public function setDefaultConfigID( $value )
    {
        $this->defaultConfigID = $value;
    }

    /**
     * @return QueueDatabaseCoreVO
     */
    public function getQueueVO()
    {
        return $this->queueVO;
    }

    /**
     * @param QueueDatabaseCoreVO $value
     */
    public function setQueueVO( QueueDatabaseCoreVO $value )
    {
        $this->queueVO = $value;
    }

    /**
     * @return string
     */
    public function getCurrentConfigID()
    {
        return $this->currentConfigID;
    }

    /**
     * @param string $value
     */
    public function setCurrentConfigID( $value )
    {
        $this->currentConfigID = $value;
    }
} 