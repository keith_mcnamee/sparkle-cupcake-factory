<?php


class BasicDatabaseConstructor extends CoreBase
{

    /**
     * @var ConditionDatabaseConstructor
     */
    private $conditionConstructor;

    public function __destruct()
    {
        $this->conditionConstructor = null;

        parent::__destruct();
    }

    /**
     * @param array $referenceVOs
     * @param string $tableName
     * @return DeleteRequestDatabaseCoreVO
     */
    public function referencedVOsDeleteVO( array $referenceVOs, $tableName )
    {
        $deleteVO = new DeleteRequestDatabaseCoreVO( $tableName );

        $deleteVO->addCondition( $this->getConditionConstructor()->inUnknowns( SqlDatabaseCoreConstants::$REF, count( $referenceVOs ) )  );

        foreach( $referenceVOs as $referenceVO )
        {
            /* @var $referenceVO ReferenceVO */
            $deleteVO->addIntValueToCondition( $referenceVO->getRef() );
        }

        return $deleteVO;
    }
    /**
     * @param array $refs
     * @param string $tableName
     * @return DeleteRequestDatabaseCoreVO
     */
    public function referencedRefsDeleteVO( array $refs, $tableName )
    {
        $deleteVO = new DeleteRequestDatabaseCoreVO( $tableName );

        $deleteVO->addCondition( $this->getConditionConstructor()->inUnknowns( SqlDatabaseCoreConstants::$REF, count( $refs ) )  );

        foreach( $refs as $ref )
        {
            /* @var $ref int */
            $deleteVO->addIntValueToCondition( $ref );
        }

        return $deleteVO;
    }

    /**
     * @return ConditionDatabaseConstructor
     */
    private function getConditionConstructor()
    {
        if( !$this->conditionConstructor )
        {
            $this->conditionConstructor = $this->getCoreDatabase()->getConditionConstructor();
        }
        return $this->conditionConstructor;
    }
}