<?php


class ConditionDatabaseConstructor extends CoreBase
{
    /**
     * @param string $column
     * @param string $existing
     * @param boolean $isOr
     * @param boolean $isNotEqual
     * @return string
     */
    public function equalsUnknown( $column, $existing = null, $isOr = false, $isNotEqual = false )
    {
        $andOr = $this->andOr( $existing, $isOr );
        $equals = $isNotEqual ? "!=" : "=";
        $sql = $andOr." ".$column." ".$equals." ?";
        return $sql;
    }

    /**
     * @param string $column
     * @param boolean $isGreaterThan
     * @param boolean $equalTo
     * @param string $existing
     * @param boolean $isOr
     * @return string
     */
    public function compareUnknown( $column, $isGreaterThan = false, $equalTo = false, $existing = null, $isOr = false )
    {
        $andOr = $this->andOr( $existing, $isOr );
        $compare = $isGreaterThan ? ">" : "<";
        $compare = $equalTo ? $compare. "= " : $compare;
        $sql = $andOr." ".$column." ".$compare." ?";
        return $sql;
    }

    /**
     * @param string $column1
     * @param string $column2
     * @param boolean $isGreaterThan
     * @param string $existing
     * @param boolean $isOr
     * @return string
     */
    public function compareSelf( $column1, $column2, $isGreaterThan = false, $existing = null, $isOr = false )
    {
        $andOr = $this->andOr( $existing, $isOr );
        $compare = $isGreaterThan ? ">" : "<";
        $sql = $andOr." ".$column1." ".$compare." ".$column2;
        return $sql;
    }

    /**
     * @param string $column
     * @param int $unknownsCount
     * @param string $existing
     * @param boolean $isOr
     * @param boolean $isNotIn
     * @return string
     */
    public function inUnknowns( $column, $unknownsCount, $existing = null, $isOr = false, $isNotIn = false )
    {
        $andOr = $this->andOr( $existing, $isOr );
        $in = $isNotIn ? "not in" : "in";
        $sql = $andOr." ".$column." ".$in." ( ";
        for( $i = 0; $i < $unknownsCount; $i++ )
        {
            $sql .= "?, ";
        }

        $sql = substr($sql, 0, -2);

        $sql .= " )";

        return $sql;
    }

    /**
     * @param string $existing
     * @param boolean $isOr
     * @return string
     */
    public function andOr( $existing = null, $isOr = false )
    {
        if( !$existing )
        {
            return "";
        }
        else if( !$isOr )
        {
            return $existing." AND";
        }
        else
        {
            return $existing." OR";
        }
    }
} 