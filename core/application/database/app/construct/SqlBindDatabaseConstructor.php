<?php


class SqlBindDatabaseConstructor extends CoreBase
{

    /**
     * @var ConditionDatabaseConstructor
     */
    private $conditionConstructor;

    public function __destruct()
    {
        $this->conditionConstructor = null;

        parent::__destruct();
    }

    /**
     * @param InsertRequestDatabaseCoreVO $insertVO
     * @param mysqli $mysqli
     * @return SqlBindDatabaseCoreVO
     * @throws Exception
     */
    public function insertVoConstruct( InsertRequestDatabaseCoreVO $insertVO, mysqli $mysqli )
    {
        $columnMultipleValues = $insertVO->getColumnMultipleValues();
        if ( empty( $columnMultipleValues ) )
        {
            throw new Exception( "No database columns requests" );
        }

        $columnSql = $this->columnSqlConstruct( $columnMultipleValues );
        $entries = $this->entriesConstruct( $columnMultipleValues );
        $entrySql = $this->entrySqlConstruct( $entries );
        $bind = $this->entryBindConstruct( $entries );
        $values = $this->entryValueConstruct( $entries, $mysqli );

        $sql = "INSERT INTO ".$insertVO->getTableName()." ( ".$columnSql." ) VALUES ".$entrySql.";";

        $sqlBindVO = new SqlBindDatabaseCoreVO( $sql, $bind, $values );

        return $sqlBindVO;
    }

    /**
     * @param SelectRequestDatabaseCoreVO $selectVO
     * @param mysqli $mysqli
     * @return SqlBindDatabaseCoreVO
     * @throws Exception
     */
    public function selectVoConstruct( SelectRequestDatabaseCoreVO $selectVO, mysqli $mysqli )
    {
        $columns = $selectVO->getColumns();
        if ( !$selectVO->isSelectAll() && empty( $columns ) )
        {
            throw new Exception( "No database columns requests" );
        }

        $selectSql = $selectVO->isSelectAll() ? $this->allColumnsConstruct() : $this->columnSqlConstruct( $columns );

        $conditionSql = $this->andConditionSqlConstruct( $selectVO->getConditionVOs() );
        $bind = $this->conditionBindConstruct( $selectVO->getConditionVOs() );
        $values = $this->conditionValueConstruct( $selectVO->getConditionVOs(), $mysqli );

        $conditionSql = $this->whereConstruct( $conditionSql );

        $orderBySql = $this->orderConstruct( $selectVO->getOrderByVOs() );
        $orderBySql = $this->orderByConstruct( $orderBySql );

        $limit = $this->limitConstruct( $selectVO->getLimitFrom(), $selectVO->getLimitSpan() );

        $sql = "SELECT ".$selectSql." FROM ".$selectVO->getTableName().$conditionSql.$orderBySql.$limit.";";

        $sqlBindVO = new SqlBindDatabaseCoreVO( $sql, $bind, $values );


        return $sqlBindVO;
    }
    /**
     * @param UpdateRequestDatabaseCoreVO $selectVO
     * @param mysqli $mysqli
     * @return SqlBindDatabaseCoreVO
     * @throws Exception
     */
    public function updateVoConstruct( UpdateRequestDatabaseCoreVO $selectVO, mysqli $mysqli )
    {
        $columns = $selectVO->getColumnValues();
        if ( empty( $columns ) )
        {
            throw new Exception( "No database columns requests" );
        }

        $setSql = $this->setSqlConstruct( $columns );
        $setBind = $this->setBindConstruct( $columns );
        $setValues = $this->setValueConstruct( $columns, $mysqli );

        $conditionSql = $this->andConditionSqlConstruct( $selectVO->getConditionVOs() );
        $bind = $this->conditionBindConstruct( $selectVO->getConditionVOs(), $setBind );
        $values = $this->conditionValueConstruct( $selectVO->getConditionVOs(), $mysqli, $setValues );

        $conditionSql = $this->whereConstruct( $conditionSql );

        $sql = "UPDATE ".$selectVO->getTableName()." SET ".$setSql.$conditionSql.";";

        $sqlBindVO = new SqlBindDatabaseCoreVO( $sql, $bind, $values );

        return $sqlBindVO;
    }

    /**
     * @param DeleteRequestDatabaseCoreVO $deleteVO
     * @param mysqli $mysqli
     * @return SqlBindDatabaseCoreVO
     * @throws Exception
     */
    public function deleteVoConstruct( DeleteRequestDatabaseCoreVO $deleteVO, mysqli $mysqli )
    {
        $conditionSql = $this->andConditionSqlConstruct( $deleteVO->getConditionVOs() );
        $conditionSql = $this->whereConstruct( $conditionSql );
        $bind = $this->conditionBindConstruct( $deleteVO->getConditionVOs() );
        $values = $this->conditionValueConstruct( $deleteVO->getConditionVOs(), $mysqli );

        $sql = "DELETE FROM ".$deleteVO->getTableName().$conditionSql.";";

        $sqlBindVO = new SqlBindDatabaseCoreVO( $sql, $bind, $values );

        return $sqlBindVO;
    }

    /**
     * @param InsertRequestDatabaseCoreVO $insertVO
     * @param SelectRequestDatabaseCoreVO $selectVO
     * @param int $insertId
     * @param int $affectedRows
     * @param mysqli $mysqli
     * @return SqlBindDatabaseCoreVO
     * @throws Exception
     */
    public function selectInsertedVoConstruct( InsertRequestDatabaseCoreVO $insertVO, SelectRequestDatabaseCoreVO $selectVO, $insertId, $affectedRows, mysqli $mysqli )
    {

        $columns = $selectVO->getColumns();

        if ( !$selectVO->isSelectAll() && empty( $columns ) )
        {
            throw new Exception( "No database columns requests" );
        }
        if ( !$insertVO->getPrimaryKey() )
        {
            throw new Exception( "No Primary Key" );
        }

        $selectSql = $selectVO->isSelectAll() ? $this->allColumnsConstruct() : $this->columnSqlConstruct( $columns );

        $insertedPrimaryKeys = $insertVO->getInsertedPrimaryKeys();
        $conditionSql = $this->insetRetrieveStringConstruct( $insertVO->getPrimaryKey(), $insertedPrimaryKeys );
        $conditionSql = $this->whereConstruct( $conditionSql );
        $sql = "SELECT ".$selectSql." FROM ".$selectVO->getTableName().$conditionSql.";";

        $bind = $this->insetRetrieveBindConstruct( $insertVO );
        if( empty( $insertedPrimaryKeys ) )
        {
            $values = $this->addSanitizedValues( array(), array( $insertId, $insertId + $affectedRows - 1 ), $mysqli );
        }
        else
        {
            $values = $this->addSanitizedKeys( array(), $insertedPrimaryKeys, $mysqli );
        }

        $bindVO = new SqlBindDatabaseCoreVO( $sql, $bind, $values );

        return $bindVO;
    }


    /**
     * @param UpdateRequestDatabaseCoreVO $updateVO
     * @param SelectRequestDatabaseCoreVO $selectVO
     * @param mysqli $mysqli
     * @return SqlBindDatabaseCoreVO
     * @throws Exception
     */
    public function selectUpdatedVoConstruct( UpdateRequestDatabaseCoreVO $updateVO, SelectRequestDatabaseCoreVO $selectVO, mysqli $mysqli )
    {

        $columns = $selectVO->getColumns();
        if ( !$selectVO->isSelectAll() && empty( $columns ) )
        {
            throw new Exception( "No database columns requests" );
        }
        $selectSql = $selectVO->isSelectAll() ? $this->allColumnsConstruct() : $this->columnSqlConstruct( $columns );

        $conditionSql = $this->andEqualsConditionSqlConstruct( $updateVO->getConditionVOs() );
        $conditionSql = $this->whereConstruct( $conditionSql );

        $bind = $this->andEqualsConditionBindConstruct( $updateVO->getConditionVOs() );
        $values = $this->andEqualsConditionValueConstruct( $updateVO->getConditionVOs(), $mysqli );

        $sql = "SELECT ".$selectSql." FROM ".$selectVO->getTableName().$conditionSql.";";

        $bindVO = new SqlBindDatabaseCoreVO( $sql, $bind, $values );

        return $bindVO;
    }


    /**
     * @param DeleteRequestDatabaseCoreVO $deleteVO
     * @param SelectRequestDatabaseCoreVO $selectVO
     * @param mysqli $mysqli
     * @return SqlBindDatabaseCoreVO
     * @throws Exception
     */
    public function selectDeletedVoConstruct( DeleteRequestDatabaseCoreVO $deleteVO, SelectRequestDatabaseCoreVO $selectVO, mysqli $mysqli )
    {

        $columns = $selectVO->getColumns();
        if ( !$selectVO->isSelectAll() && empty( $columns ) )
        {
            throw new Exception( "No database columns requests" );
        }
        $selectSql = $selectVO->isSelectAll() ? $this->allColumnsConstruct() : $this->columnSqlConstruct( $columns );

        $conditionSql = $this->andEqualsConditionSqlConstruct( $deleteVO->getConditionVOs() );
        $conditionSql = $this->whereConstruct( $conditionSql );

        $bind = $this->andEqualsConditionBindConstruct( $deleteVO->getConditionVOs() );
        $values = $this->andEqualsConditionValueConstruct( $deleteVO->getConditionVOs(), $mysqli );

        $sql = "SELECT ".$selectSql." FROM ".$selectVO->getTableName().$conditionSql.";";

        $bindVO = new SqlBindDatabaseCoreVO( $sql, $bind, $values );

        return $bindVO;
    }

    /**
     * @param array $columns
     * @return string
     */
    public function columnSqlConstruct( array $columns )
    {
        $string = "";
        foreach( $columns as $column => $nullValue )
        {
            /* @var $nullValue null */
            $string .= $column.", ";
        }

        if( $string != "" )
        {
            $string = substr($string, 0, -2);
        }

        return $string;
    }

    /**
     * @param array $columnValues
     * @return array
     * @throws Exception
     */

    public function entriesConstruct( array $columnValues )
    {
        $entries = array();
        $columnIndex =-1;
        foreach( $columnValues as $column => $values )
        {
            /* @var $values array */
            $columnIndex ++;
            if( $columnIndex == 0 )
            {
                $exampleValues = $values;
                $entryIndex = -1;
                foreach( $exampleValues as $value )
                {
                    /* @var $value mixed */
                    $entryIndex ++;
                    $entries[ $entryIndex ] = array();
                }
            }
            else
            {
                if( count($values) != count($entries) )
                {
                    throw new Exception( "Invalid inset columns" );
                }
            }
        }

        foreach( $columnValues as $column => $values )
        {
            /* @var $values array */
            $entryIndex = -1;
            foreach( $values as $value )
            {
                /* @var $value mixed */
                $entryIndex ++;
                $entries[ $entryIndex ][] = $value;
            }
        }
        return $entries;
    }

    /**
     * @param array $entries
     * @param string $existing
     * @return string
     */
    public function entrySqlConstruct( array $entries, $existing = "" )
    {

        foreach( $entries as $entry )
        {
            /* @var $entry array */
            $existing .= "( ";

            $entryString = "";
            foreach( $entry as $value )
            {
                /* @var $value mixed */
                $entryString .= "?, ";
            }
            $entryString = substr($entryString, 0, -2);

            $existing .= $entryString." ), ";
        }

        if( $existing != "" )
        {
            $existing = substr($existing, 0, -2);
        }

        return $existing;
    }

    /**
     * @param array $entries
     * @param string $existing
     * @return string
     */
    public function entryBindConstruct( array $entries, $existing = "" )
    {
        foreach( $entries as $entry )
        {
            /* @var $entry array */
            foreach( $entry as $entryValueVO )
            {
                /* @var $entryValueVO ValueEntryDatabaseCoreVO */
                $existing .= $entryValueVO->getBindType();
            }
        }

        return $existing;
    }

    /**
     * @param array $entries
     * @param mysqli $mysqli
     * @param array $existing
     * @return array
     */
    public function entryValueConstruct( array $entries, mysqli $mysqli, $existing = null )
    {
        if( !$existing )
        {
            $existing = array();
        }
        foreach( $entries as $entry )
        {
            /* @var $entry array */
            foreach( $entry as $entryValueVO )
            {
                /* @var $entryValueVO ValueEntryDatabaseCoreVO */
                $value = $this->sanitizeValue( $entryValueVO->getValue(), $mysqli );
                $existing[] = $value;
            }
        }

        return $existing;
    }

    /**
     * @param array $columns
     * @param string $existing
     * @return string
     */
    public function setSqlConstruct( array $columns, $existing = "" )
    {
        foreach( $columns as $column => $entryValueVO )
        {
            /* @var $entryValueVO ValueEntryDatabaseCoreVO */
            $existing .= $column." = ?, ";
        }

        if( $existing != "" )
        {
            $existing = substr($existing, 0, -2);
        }

        return $existing;
    }

    /**
     * @param array $columns
     * @param string $existing
     * @return string
     */
    public function setBindConstruct( array $columns, $existing = "" )
    {
        foreach( $columns as $column => $entryValueVO )
        {
            /* @var $entryValueVO ValueEntryDatabaseCoreVO */
            $existing .= $entryValueVO->getBindType();
        }

        return $existing;
    }

    /**
     * @param array $columns
     * @param mysqli $mysqli
     * @param array $existing
     * @return string
     */
    public function setValueConstruct( array $columns, mysqli $mysqli, $existing = null )
    {
        if( !$existing )
        {
            $existing = array();
        }

        foreach( $columns as $column => $entryValueVO )
        {
            /* @var $entryValueVO ValueEntryDatabaseCoreVO */
            $value = $this->sanitizeValue( $entryValueVO->getValue(), $mysqli );
            $existing[] = $value;
        }

        return $existing;
    }

    /**
     * @param string $primaryKey
     * @param array $insertedPrimaryKeys
     * @return string
     */
    public function insetRetrieveStringConstruct( $primaryKey, array $insertedPrimaryKeys )
    {
        if( empty( $insertedPrimaryKeys ) )
        {
            $string = $primaryKey." >= ? AND ".$primaryKey." <= ?";
        }
        else
        {
            $string = $this->getConditionConstructor()->inUnknowns( $primaryKey, count( $insertedPrimaryKeys ) );
        }

        return $string;
    }

    /**
     * @param InsertRequestDatabaseCoreVO $insertVO
     * @return string
     */
    public function insetRetrieveBindConstruct( InsertRequestDatabaseCoreVO $insertVO )
    {
        $insertedPrimaryKeys = $insertVO->getInsertedPrimaryKeys();
        if( empty( $insertedPrimaryKeys ) )
        {
            $bind = DatabaseCoreConstants::$INTEGER_ENTRY_BIND_TYPE.DatabaseCoreConstants::$INTEGER_ENTRY_BIND_TYPE;
        }
        else
        {
            $bind = "";
            $primaryKey = $insertVO->getPrimaryKey();
            $columnValues = $insertVO->getColumnValuesFor( $primaryKey );
            $entryValueVO = $columnValues[ 0 ];
            /* @var $entryValueVO ValueEntryDatabaseCoreVO */
            $bindType = $entryValueVO->getBindType();
            foreach( $insertedPrimaryKeys as $key => $nullValue )
            {
                /* @var $nullValue null */
                $bind .= $bindType;
            }
        }

        return $bind;
    }

    /**
     * @param string $primaryKey
     * @param int $insertId
     * @param int $affectedRows
     * @return string
     */
    public function insetRetrieveStringTestConstruct( $primaryKey, $insertId, $affectedRows )
    {
        $string = $primaryKey." >= ".$insertId." AND ".$primaryKey." <= ".($insertId + $affectedRows - 1);

        return $string;
    }

    /**
     * @param array $conditionVOs
     * @param string $existing
     * @return string
     */
    public function andEqualsConditionSqlConstruct( array $conditionVOs, $existing = "" )
    {
        foreach( $conditionVOs as $conditionVO )
        {
            /* @var $conditionVO ConditionDatabaseCoreVO */
            $andOr = "";
            if( $existing )
            {
                $andOr = !$conditionVO->isOrCondition() ? " AND " : " OR ";
            }
            $existing .= $andOr." ( ".$conditionVO->getConditionString() .") ";
        }

        return $existing;
    }

    /**
     * @param array $conditionVOs
     * @param string $existing
     * @return string
     */
    public function andEqualsConditionBindConstruct( array $conditionVOs, $existing = "" )
    {
        foreach( $conditionVOs as $conditionVO )
        {
            /* @var $conditionVO ConditionDatabaseCoreVO */
            foreach( $conditionVO->getEntryValueVOs() as $entryValueVO )
            {
                /* @var $entryValueVO ValueEntryDatabaseCoreVO */
                $existing .= $entryValueVO->getBindType();
            }
        }

        return $existing;
    }

    /**
     * @param array $conditionVOs
     * @param mysqli $mysqli
     * @param array $existing
     * @return string
     */
    public function andEqualsConditionValueConstruct( array $conditionVOs, mysqli $mysqli, $existing = null )
    {
        if( !$existing )
        {
            $existing = array();
        }

        foreach( $conditionVOs as $conditionVO )
        {
            /* @var $conditionVO ConditionDatabaseCoreVO */
            foreach( $conditionVO->getEntryValueVOs() as $entryValueVO )
            {
                /* @var $entryValueVO ValueEntryDatabaseCoreVO */
                $value = $this->sanitizeValue( $entryValueVO->getValue(), $mysqli );
                $existing[] = $value;
            }
        }

        return $existing;
    }

    /**
     * @param array $conditionVOs
     * @param string $existing
     * @return string
     */
    public function andConditionSqlConstruct( array $conditionVOs, $existing = "" )
    {
        foreach( $conditionVOs as $conditionVO )
        {
            /* @var $conditionVO ConditionDatabaseCoreVO */
            $andOr = "";
            if( $existing )
            {
                $andOr = !$conditionVO->isOrCondition() ? " AND " : " OR ";
            }
            $existing .= $andOr." ( ".$conditionVO->getConditionString()." ) ";
        }

        return $existing;
    }

    /**
     * @param array $conditionVOs
     * @param string $existing
     * @return string
     */
    public function conditionBindConstruct( array $conditionVOs, $existing = "" )
    {
        foreach( $conditionVOs as $conditionVO )
        {
            /* @var $conditionVO ConditionDatabaseCoreVO */
            foreach( $conditionVO->getEntryValueVOs() as $entryValueVO )
            {
                /* @var $entryValueVO ValueEntryDatabaseCoreVO */
                $existing .= $entryValueVO->getBindType();
            }
        }

        return $existing;
    }

    /**
     * @param array $conditionVOs
     * @param mysqli $mysqli
     * @param array $existing
     * @return array
     */
    public function conditionValueConstruct( array $conditionVOs, mysqli $mysqli, $existing = null )
    {
        if( !$existing )
        {
            $existing = array();
        }
        foreach( $conditionVOs as $conditionVO )
        {
            /* @var $conditionVO ConditionDatabaseCoreVO */
            foreach( $conditionVO->getEntryValueVOs() as $entryValueVO )
            {
                /* @var $entryValueVO ValueEntryDatabaseCoreVO */
                $value = $this->sanitizeValue( $entryValueVO->getValue(), $mysqli );
                $existing[] = $value;
            }
        }

        return $existing;
    }

    /**
     * @param array $orderByVOs
     * @return string
     */
    public function orderConstruct( array $orderByVOs )
    {
        $string = "";
        foreach( $orderByVOs as $orderByVO )
        {
            /* @var $orderByVO OrderByRequestDatabaseCoreVO */
            $ascDesc = $orderByVO->isDescending() ? "DESC" : "ASC";
            $columnName = $orderByVO->getColumnName();
            $string .= $orderByVO->getColumnName()." ".$ascDesc.", ";
        }

        if( $string != "" )
        {
            $string = substr($string, 0, -2);
        }

        return $string;
    }

    /**
     * @param string $conditions
     * @return string
     */
    public function whereConstruct( $conditions )
    {
        if($conditions)
        {
            $conditions = " WHERE ".$conditions;
            return $conditions;
        }
        return "";
    }

    /**
     * @param string $orderBy
     * @return string
     */
    public function orderByConstruct( $orderBy )
    {
        if($orderBy)
        {
            $orderBy = " ORDER BY ".$orderBy;
            return $orderBy;
        }
        return "";
    }

    /**
     * @param int $limitFrom
     * @param int $limitSpan
     * @return string
     */
    public function limitConstruct( $limitFrom, $limitSpan )
    {
        $limitFrom = (int)$limitFrom;
        $limitSpan = (int)$limitSpan;
        if( $limitSpan > 0 )
        {
            return " LIMIT ".$limitFrom.", ".$limitSpan."";
        }
        return "";
    }


    /**
     * @return string
     */
    public function allColumnsConstruct()
    {
        return "* ";
    }

    /**
     * @param array $current
     * @param array $new
     * @param mysqli $mysqli
     * @return array
     */
    private function addSanitizedValues( array $current, array $new, mysqli $mysqli  )
    {
        foreach($new as $value)
        {
            /* @var $value mixed */
            $value = $this->sanitizeValue( $value, $mysqli );
            $current[] = $value;
        }
        return $current;
    }

    /**
     * @param array $current
     * @param array $new
     * @param mysqli $mysqli
     * @return array
     */
    private function addSanitizedKeys( array $current, array $new, mysqli $mysqli  )
    {
        foreach($new as $key => $value)
        {
            /* @var $value mixed */
            $key = $this->sanitizeValue( $key, $mysqli );
            $current[] = $key;
        }
        return $current;
    }

    /**
     * @param mixed $value
     * @param mysqli $mysqli
     * @return string
     */
    private function sanitizeValue( $value, mysqli $mysqli )
    {
        $value = CoreHelper::getStringHelper()->stripSanitization( $value ); // Input should now be normalized
        $value = $mysqli->real_escape_string( $value );
        return $value;
    }

    /**
     * @return ConditionDatabaseConstructor
     */
    private function getConditionConstructor()
    {
        if( !$this->conditionConstructor )
        {
            $this->conditionConstructor = $this->getCoreDatabase()->getConditionConstructor();
        }
        return $this->conditionConstructor;
    }
} 