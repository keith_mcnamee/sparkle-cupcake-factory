<?php


class QueueDatabaseCoreVO
{

    public static $INSERT_QUEUE_ITEM = "product";
    public static $UPDATE_QUEUE_ITEM = "update";
    public static $DELETE_QUEUE_ITEM = "delete";

    /**
     * @var array
     */
    private $insertQueue;

    /**
     * @var array
     */
    private $updateQueue;

    /**
     * @var array
     */
    private $deleteQueue;

    /**
     * @var array
     */
    private $typeQueue;

    function __construct()
    {
        $this->reset();
    }

    public function reset()
    {
        $this->setInsertQueue( array() );
        $this->setUpdateQueue( array() );
        $this->setDeleteQueue( array() );
        $this->setTypeQueue( array() );
    }

    /**
     * @param InsertRequestDatabaseCoreVO $insertVO
     */
    public function addInsertVO( InsertRequestDatabaseCoreVO $insertVO )
    {
        $itemType = self::$INSERT_QUEUE_ITEM;
        self::typeAdded( $itemType );
        $insertQueue = $this->getInsertQueue();
        $insertQueue[ count( $insertQueue ) - 1 ][] = $insertVO;
        $this->setInsertQueue( $insertQueue );
    }

    /**
     * @param UpdateRequestDatabaseCoreVO $updateVO
     */
    public function addUpdateVO( UpdateRequestDatabaseCoreVO $updateVO )
    {
        $itemType = self::$UPDATE_QUEUE_ITEM;
        self::typeAdded( $itemType );
        $updateQueue = $this->getUpdateQueue();
        $updateQueue[ count( $updateQueue ) - 1 ][] = $updateVO;
        $this->setUpdateQueue( $updateQueue );
    }

    /**
     * @param DeleteRequestDatabaseCoreVO $deleteVO
     */
    public function addDeleteVO( DeleteRequestDatabaseCoreVO $deleteVO )
    {
        $itemType = self::$DELETE_QUEUE_ITEM;
        self::typeAdded( $itemType );
        $deleteQueue = $this->getDeleteQueue();
        $deleteQueue[ count( $deleteQueue ) - 1 ][] = $deleteVO;
        $this->setDeleteQueue( $deleteQueue );
    }

    /**
     * @param string $itemType
     */
    private function typeAdded( $itemType )
    {
        $typeQueue = $this->getTypeQueue();
        $lastType = count( $typeQueue ) > 0 ? $typeQueue[ count( $typeQueue ) - 1 ] : null;
        if( $lastType == $itemType )
        {
            return;
        }
        switch( $itemType )
        {
            case self::$INSERT_QUEUE_ITEM:
                $insertQueue = CoreHelper::getArrayHelper()->addObject( $this->getInsertQueue(), array() );
                $this->setInsertQueue( $insertQueue );
                break;

            case self::$UPDATE_QUEUE_ITEM:
                $updateQueue = CoreHelper::getArrayHelper()->addObject( $this->getUpdateQueue(), array() );
                $this->setUpdateQueue( $updateQueue );
                break;

            case self::$DELETE_QUEUE_ITEM:
                $deleteQueue = CoreHelper::getArrayHelper()->addObject( $this->getDeleteQueue(), array() );
                $this->setDeleteQueue( $deleteQueue );
                break;
            default:
                return;
        }
        $typeQueue[] = $itemType;
        $this->setTypeQueue( $typeQueue );
    }


    /**
     * @return array
     */
    public function getInsertQueue()
    {
        return $this->insertQueue;
    }

    /**
     * @param array $value
     */
    public function setInsertQueue( array $value )
    {
        $this->insertQueue = $value;
    }

    /**
     * @return array
     */
    public function getUpdateQueue()
    {
        return $this->updateQueue;
    }

    /**
     * @param array $value
     */
    public function setUpdateQueue( array $value )
    {
        $this->updateQueue = $value;
    }

    /**
     * @return array
     */
    public function getDeleteQueue()
    {
        return $this->deleteQueue;
    }

    /**
     * @param array $value
     */
    public function setDeleteQueue( array $value )
    {
        $this->deleteQueue = $value;
    }

    /**
     * @return array
     */
    public function getTypeQueue()
    {
        return $this->typeQueue;
    }

    /**
     * @param array $value
     */
    public function setTypeQueue( $value )
    {
        $this->typeQueue = $value;
    }
}