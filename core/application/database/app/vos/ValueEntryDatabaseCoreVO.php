<?php


class ValueEntryDatabaseCoreVO
{

    /**
     * @var mixed
     */
    private $value;

    /**
     * @var string
     */
    private $bindType;

    /**
     * @param mixed $value
     * @param string $bindType
     */
    function __construct( $value, $bindType )
    {
        $this->setValue( $value );
        $this->setBindType( $bindType );
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue( $value )
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getBindType()
    {
        return $this->bindType;
    }

    /**
     * @param string $value
     */
    public function setBindType( $value )
    {
        $this->bindType = $value;
    }
}