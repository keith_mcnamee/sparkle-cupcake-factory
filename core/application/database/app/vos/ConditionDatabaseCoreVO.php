<?php


class ConditionDatabaseCoreVO
{

    /**
     * @var string
     */
    private $conditionString;

    /**
     * @var boolean
     */
    private $orCondition;

    /**
     * @var array
     */
    private $entryValueVOs;

    /**
     * @param string $conditionString
     * @param bool $orCondition
     */
    function __construct( $conditionString, $orCondition = false )
    {
        $this->setConditionString( $conditionString );
        $this->setOrCondition( $orCondition );

        $this->setEntryValueVOs( array() );
    }

    /**
     * @return string
     */
    public function getConditionString()
    {
        return $this->conditionString;
    }

    /**
     * @param string $value
     */
    public function setConditionString( $value )
    {
        $this->conditionString = $value;
    }

    /**
     * @param string $value
     */
    public function addStringEntryValue( $value )
    {
        $this->addEntryValue( (string)$value, DatabaseCoreConstants::$STRING_ENTRY_BIND_TYPE );
    }

    /**
     * @param boolean $value
     */
    public function addBooleanEntryValue( $value )
    {
        $this->addIntEntryValue( (boolean)$value );
    }

    /**
     * @param int $value
     */
    public function addIntEntryValue( $value )
    {
        $this->addEntryValue( (int)$value, DatabaseCoreConstants::$INTEGER_ENTRY_BIND_TYPE );
    }

    /**
     * @param mixed $value
     * @param string $bindType
     */
    public function addEntryValue( $value, $bindType )
    {
        $entryValue = new ValueEntryDatabaseCoreVO( $value, $bindType);
        $this->setEntryValueVOs( CoreHelper::getArrayHelper()->addObject( $this->getEntryValueVOs(), $entryValue ) );
    }

    /**
     * @return array
     */
    public function getEntryValueVOs()
    {
        return $this->entryValueVOs;
    }

    /**
     * @param array $value
     */
    public function setEntryValueVOs( array $value )
    {
        $this->entryValueVOs = $value;
    }

    /**
     * @return boolean
     */
    public function isOrCondition()
    {
        return $this->orCondition;
    }

    /**
     * @param boolean $value
     */
    public function setOrCondition( $value )
    {
        $this->orCondition = $value;
    }
} 