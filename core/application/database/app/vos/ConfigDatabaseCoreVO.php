<?php


class ConfigDatabaseCoreVO
{
    /**
     * @var string
     */
    private $configID;

    /**
     * @var string
     */
    private $serverConfigType;

    /**
     * @var boolean
     */
    private $stagingDatabase;

    /**
     * @var string
     */
    private $databaseName;

    /**
     * @var string
     */
    private $hostName;

    /**
     * @var string
     */
    private $user;

    /**
     * @var string
     */
    private $password;

    /**
     * @param string $configID
     */
    public function __construct( $configID )
    {
        $this->setConfigID( $configID );
    }

    /**
     * @return string
     */
    public function getDatabaseName()
    {
        return $this->databaseName;
    }

    /**
     * @param string $value
     */
    public function setDatabaseName( $value )
    {
        $this->databaseName = $value;
    }

    /**
     * @return string
     */
    public function getHostName()
    {
        return $this->hostName;
    }

    /**
     * @param string $value
     */
    public function setHostName( $value )
    {
        $this->hostName = $value;
    }

    /**
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param string $value
     */
    public function setUser( $value )
    {
        $this->user = $value;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $value
     */
    public function setPassword( $value )
    {
        $this->password = $value;
    }

    /**
     * @return string
     */
    public function getServerConfigType()
    {
        return $this->serverConfigType;
    }

    /**
     * @param string $value
     */
    public function setServerConfigType( $value )
    {
        $this->serverConfigType = $value;
    }

    /**
     * @return boolean
     */
    public function isStagingDatabase()
    {
        return $this->stagingDatabase;
    }

    /**
     * @param boolean $value
     */
    public function setStagingDatabase( $value )
    {
        $this->stagingDatabase = $value;
    }

    /**
     * @return string
     */
    public function getConfigID()
    {
        return $this->configID;
    }

    /**
     * @param string $value
     */
    public function setConfigID( $value )
    {
        $this->configID = $value;
    }
} 