<?php


class SqlBindDatabaseCoreVO
{

    /**
     * @var string
     */
    private $sql;

    /**
     * @var string
     */
    private $bind;

    /**
     * @var array
     */
    private $values;

    /**
     * @param string $sql
     * @param string $bind
     * @param array $values
     */
    function __construct( $sql, $bind, array $values )
    {
        $this->setSql( $sql );
        $this->setBind( $bind );
        $this->setValues( $values );
    }

    /**
     * @return string
     */
    public function getSql()
    {
        return $this->sql;
    }

    /**
     * @param string $value
     */
    public function setSql( $value )
    {
        $this->sql = $value;
    }

    /**
     * @return string
     */
    public function getBind()
    {
        return $this->bind;
    }

    /**
     * @param string $value
     */
    public function setBind( $value )
    {
        $this->bind = $value;
    }

    /**
     * @return array
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * @param array $value
     */
    public function setValues( $value )
    {
        $this->values = $value;
    }
} 