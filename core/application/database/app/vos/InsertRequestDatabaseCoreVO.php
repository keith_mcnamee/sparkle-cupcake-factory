<?php


class InsertRequestDatabaseCoreVO
{

    /**
     * @var string
     */
    private $tableName;

    /**
     * @var array
     */
    private $columnMultipleValues;

    /**
     * @var string
     */
    private $primaryKey;

    /**
     * @var array
     */
    private $insertedPrimaryKeys;

    /**
     * @var int
     */
    private $columnCount;

    /**
     * @param string $tableName
     * @param string $primaryKey
     */
    function __construct( $tableName, $primaryKey = null )
    {
        $this->setTableName( $tableName );
        $this->setPrimaryKey( $primaryKey );

        $this->setColumnMultipleValues( array () );
        $this->setInsertedPrimaryKeys( array () );

    }

    public function resetColumnCount()
    {
        $this->setColumnCount( 0 );
    }

    /**
     * @param string $column
     * @param boolean $primaryKey
     * @param string $value
     */
    public function addStringColumnValue( $column, $value, $primaryKey = false )
    {
        $this->addColumnValue( $column, (string)$value, DatabaseCoreConstants::$STRING_ENTRY_BIND_TYPE, $primaryKey );
    }

    /**
     * @param string $column
     * @param boolean $primaryKey
     * @param boolean $value
     */
    public function addBooleanColumnValue( $column, $value, $primaryKey = false )
    {
        $this->addIntColumnValue( $column, (boolean)$value, $primaryKey );
    }

    /**
     * @param string $column
     * @param boolean $primaryKey
     * @param int $value
     */
    public function addIntColumnValue( $column, $value, $primaryKey = false )
    {
        $this->addColumnValue( $column, (int)$value, DatabaseCoreConstants::$INTEGER_ENTRY_BIND_TYPE, $primaryKey );
    }

    /**
     * @param string $column
     * @param mixed $value
     * @param boolean $primaryKey
     * @param string $bindType
     */
    public function addColumnValue( $column, $value, $bindType, $primaryKey = false )
    {
        $columnValues = $this->getColumnValuesFor( $column );

        if( !$columnValues )
        {
            $columnValues = array();
        }
        $entryValueVO = new ValueEntryDatabaseCoreVO( $value, $bindType );

        $columnValues[] = $entryValueVO;

        $this->setColumnMultipleValues( CoreHelper::getArrayHelper()->setObjectForKey( $this->getColumnMultipleValues(), $columnValues, $column ) );
        $this->setColumnCount( $this->getColumnCount() + 1 );

        if( $primaryKey )
        {
            $this->setInsertedPrimaryKeys( CoreHelper::getArrayHelper()->setObjectForKey( $this->getInsertedPrimaryKeys(), true, $value ) );
        }
    }

    /**
     * @param $key
     * @return array
     */
    public function getColumnValuesFor( $key )
    {
        $object = CoreHelper::getArrayHelper()->getObjectForKey( $this->getColumnMultipleValues(), $key );

        return $object;
    }

    /**
     * @return string
     */
    public function getTableName()
    {
        return $this->tableName;
    }

    /**
     * @param string $value
     */
    public function setTableName( $value )
    {
        $this->tableName = $value;
    }

    /**
     * @return array
     */
    public function getColumnMultipleValues()
    {
        return $this->columnMultipleValues;
    }

    /**
     * @param array $value
     */
    public function setColumnMultipleValues( $value )
    {
        $this->columnMultipleValues = $value;
    }

    /**
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->primaryKey;
    }

    /**
     * @param string $value
     */
    public function setPrimaryKey( $value )
    {
        $this->primaryKey = $value;
    }

    /**
     * @return int
     */
    public function getColumnCount()
    {
        return $this->columnCount;
    }

    /**
     * @param int $value
     */
    public function setColumnCount( $value )
    {
        $this->columnCount = $value;
    }

    /**
     * @return array
     */
    public function getInsertedPrimaryKeys()
    {
        return $this->insertedPrimaryKeys;
    }

    /**
     * @param array $value
     */
    public function setInsertedPrimaryKeys( $value )
    {
        $this->insertedPrimaryKeys = $value;
    }

} 