<?php


class ConnectionDatabaseCoreVO
{

    /**
     * @var mysqli
     */
    private $mysqli;

    /**
     * @var bool
     */
    private $failed;

    /**
     * @var string
     */
    private $error;

    /**
     * @var string
     */
    private $privateError;


    public function __construct()
    {
    }

    /**
     * @return boolean
     */
    public function isFailed()
    {
        return $this->failed;
    }

    /**
     * @param boolean $value
     */
    public function setFailed( $value )
    {
        $this->failed = $value;
    }

    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param string $value
     */
    public function setError( $value )
    {
        $this->error = $value;
    }

    /**
     * @return mysqli
     */
    public function getMysqli()
    {
        return $this->mysqli;
    }

    /**
     * @param mysqli $value
     */
    public function setMysqli( $value )
    {
        $this->mysqli = $value;
    }

    /**
     * @return string
     */
    public function getPrivateError()
    {
        return $this->privateError;
    }

    /**
     * @param string $value
     */
    public function setPrivateError( $value )
    {
        $this->privateError = $value;
    }

} 