<?php


class SelectRequestDatabaseCoreVO
{

    /**
     * @var string
     */
    private $tableName;

    /**
     * @var boolean
     */
    private $selectAll;

    /**
     * @var array
     */
    private $columns;

    /**
     * @var array
     */
    private $conditionVOs;

    /**
     * @var array
     */
    private $orderByVOs;

    /**
     * @var int
     */
    private $limitFrom;

    /**
     * @var int
     */
    private $limitSpan;

    /**
     * @param string $tableName
     */
    function __construct( $tableName )
    {

        $this->setTableName( $tableName );

        $this->setColumns( array () );
        $this->setConditionVOs( array () );
        $this->setOrderByVOs( array () );
    }

    /**
     * @param int $from
     * @param int $span
     */
    public function addLimit( $from, $span )
    {
        $this->setLimitFrom( (int)$from );
        $this->setLimitSpan( (int)$span );
    }

    /**
     * @param string $key
     */
    public function addColumn( $key )
    {
        $this->setColumns( CoreHelper::getArrayHelper()->setObjectForKey( $this->getColumns(), true, $key ) );
    }

    /**
     * @param string $conditionString
     * @param bool $orCondition
     */
    public function addCondition( $conditionString, $orCondition = false )
    {
        $conditionVO = new ConditionDatabaseCoreVO( $conditionString, $orCondition );
        $this->setConditionVOs( CoreHelper::getArrayHelper()->addObject( $this->getConditionVOs(), $conditionVO ) );
    }

    /**
     * @param string $value
     */
    public function addStringValueToCondition( $value )
    {
        $conditionVO = $this->getLatestCondition();
        if( !$conditionVO )
        {
            return;
        }
        $conditionVO->addStringEntryValue( $value );
    }

    /**
     * @param boolean $value
     */
    public function addBooleanValueToCondition( $value )
    {
        $this->addIntValueToCondition( (boolean)$value );
    }

    /**
     * @param string $value
     */
    public function addIntValueToCondition( $value )
    {
        $conditionVO = $this->getLatestCondition();
        if( !$conditionVO )
        {
            return;
        }
        $conditionVO->addIntEntryValue( $value );
    }

    /**
     * @param mixed $value
     * @param string $bindType
     */
    public function addValueToCondition( $value, $bindType )
    {
        $conditionVO = $this->getLatestCondition();
        if( !$conditionVO )
        {
            return;
        }
        $conditionVO->addEntryValue( $value, $bindType );
    }

    /**
     * @return ConditionDatabaseCoreVO
     */
    public function getLatestCondition()
    {
        $conditionVOs = $this->getConditionVOs();
        if( empty( $conditionVOs ) )
        {
            return null;
        }

        $conditionVO = $conditionVOs[ count($conditionVOs) - 1 ];
        /* @var $conditionVO ConditionDatabaseCoreVO */
        return $conditionVO;
    }

    /**
     * @param string $columnName
     * @param boolean $descending
     */
    public function addOrderBy( $columnName, $descending = false )
    {
        $vo = new OrderByRequestDatabaseCoreVO( $columnName, $descending );
        $this->setOrderByVOs( CoreHelper::getArrayHelper()->addObject( $this->getOrderByVOs(), $vo ) );
    }

    /**
     * @return string
     */
    public function getTableName()
    {
        return $this->tableName;
    }

    /**
     * @param string $value
     */
    public function setTableName( $value )
    {
        $this->tableName = $value;
    }

    /**
     * @return array
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * @param array $value
     */
    public function setColumns( $value )
    {
        $this->columns = $value;
    }

    /**
     * @return array
     */
    public function getConditionVOs()
    {
        return $this->conditionVOs;
    }

    /**
     * @param array $value
     */
    public function setConditionVOs( $value )
    {
        $this->conditionVOs = $value;
    }

    /**
     * @return array
     */
    public function getOrderByVOs()
    {
        return $this->orderByVOs;
    }

    /**
     * @param array $value
     */
    public function setOrderByVOs( $value )
    {
        $this->orderByVOs = $value;
    }

    /**
     * @return int
     */
    public function getLimitFrom()
    {
        return $this->limitFrom;
    }

    /**
     * @param int $value
     */
    public function setLimitFrom( $value )
    {
        $this->limitFrom = $value;
    }

    /**
     * @return int
     */
    public function getLimitSpan()
    {
        return $this->limitSpan;
    }

    /**
     * @param int $value
     */
    public function setLimitSpan( $value )
    {
        $this->limitSpan = $value;
    }

    /**
     * @return boolean
     */
    public function isSelectAll()
    {
        return $this->selectAll;
    }

    /**
     * @param boolean $value
     */
    public function setSelectAll( $value )
    {
        $this->selectAll = $value;
    }
} 