<?php


class DeleteRequestDatabaseCoreVO
{

    /**
     * @var string
     */
    private $tableName;

    /**
     * @var array
     */
    private $conditionVOs;

    /**
     * @param string $tableName
     */
    function __construct( $tableName )
    {
        $this->setTableName( $tableName );

        $this->setConditionVOs( array () );
    }

    /**
     * @param string $conditionString
     * @param bool $orCondition
     */
    public function addCondition( $conditionString, $orCondition = false )
    {
        $conditionVO = new ConditionDatabaseCoreVO( $conditionString, $orCondition );
        $this->setConditionVOs( CoreHelper::getArrayHelper()->addObject( $this->getConditionVOs(), $conditionVO ) );
    }

    /**
     * @param string $value
     */
    public function addStringValueToCondition( $value )
    {
        $conditionVO = $this->getLatestCondition();
        if( !$conditionVO )
        {
            return;
        }
        $conditionVO->addStringEntryValue( $value );
    }

    /**
     * @param boolean $value
     */
    public function addBooleanValueToCondition( $value )
    {
        $this->addIntValueToCondition( (boolean)$value );
    }

    /**
     * @param string $value
     */
    public function addIntValueToCondition( $value )
    {
        $conditionVO = $this->getLatestCondition();
        if( !$conditionVO )
        {
            return;
        }
        $conditionVO->addIntEntryValue( $value );
    }

    /**
     * @param mixed $value
     * @param string $bindType
     */
    public function addValueToCondition( $value, $bindType )
    {
        $conditionVO = $this->getLatestCondition();
        if( !$conditionVO )
        {
            return;
        }
        $conditionVO->addEntryValue( $value, $bindType );
    }

    /**
     * @return ConditionDatabaseCoreVO
     */
    public function getLatestCondition()
    {
        $conditionVOs = $this->getConditionVOs();
        if( empty( $conditionVOs ) )
        {
            return null;
        }

        $conditionVO = $conditionVOs[ count($conditionVOs) - 1 ];
        /* @var $conditionVO ConditionDatabaseCoreVO */
        return $conditionVO;
    }

    /**
     * @return string
     */
    public function getTableName()
    {
        return $this->tableName;
    }

    /**
     * @param string $value
     */
    public function setTableName( $value )
    {
        $this->tableName = $value;
    }

    /**
     * @return array
     */
    public function getConditionVOs()
    {
        return $this->conditionVOs;
    }

    /**
     * @param array $value
     */
    public function setConditionVOs( $value )
    {
        $this->conditionVOs = $value;
    }

} 