<?php


class UpdateRequestDatabaseCoreVO
{

    /**
     * @var string
     */
    private $tableName;

    /**
     * @var array
     */
    private $columnValues;

    /**
     * @var array
     */
    private $conditionVOs;

    /**
     * @param string $tableName
     */
    function __construct( $tableName )
    {
        $this->setTableName( $tableName );

        $this->setColumnValues( array () );
        $this->setConditionVOs( array () );
    }

    /**
     * @param string $column
     * @param string $value
     */
    public function addStringColumnValue( $column, $value )
    {
        $this->addColumnValue( $column, (string)$value, DatabaseCoreConstants::$STRING_ENTRY_BIND_TYPE );
    }

    /**
     * @param string $column
     * @param boolean $value
     */
    public function addBooleanColumnValue( $column, $value )
    {
        $this->addIntColumnValue( $column, (boolean)$value );
    }

    /**
     * @param string $column
     * @param int $value
     */
    public function addIntColumnValue( $column, $value )
    {
        $this->addColumnValue( $column, (int)$value, DatabaseCoreConstants::$INTEGER_ENTRY_BIND_TYPE );
    }

    /**
     * @param string $column
     * @param mixed $value
     * @param string $bindType
     */
    public function addColumnValue( $column, $value, $bindType )
    {
        $entryValueVO = new ValueEntryDatabaseCoreVO( $value, $bindType );

        $this->setColumnValues( CoreHelper::getArrayHelper()->setObjectForKey( $this->getColumnValues(), $entryValueVO, $column ) );
    }

    /**
     * @param string $conditionString
     * @param bool $orCondition
     */
    public function addCondition( $conditionString, $orCondition = false )
    {
        $conditionVO = new ConditionDatabaseCoreVO( $conditionString, $orCondition );
        $this->setConditionVOs( CoreHelper::getArrayHelper()->addObject( $this->getConditionVOs(), $conditionVO ) );
    }

    /**
     * @param string $value
     */
    public function addStringValueToCondition( $value )
    {
        $conditionVO = $this->getLatestCondition();
        if( !$conditionVO )
        {
            return;
        }
        $conditionVO->addStringEntryValue( $value );
    }

    /**
     * @param boolean $value
     */
    public function addBooleanValueToCondition( $value )
    {
        $this->addIntValueToCondition( (boolean)$value );
    }

    /**
     * @param string $value
     */
    public function addIntValueToCondition( $value )
    {
        $conditionVO = $this->getLatestCondition();
        if( !$conditionVO )
        {
            return;
        }
        $conditionVO->addIntEntryValue( $value );
    }

    /**
     * @param mixed $value
     * @param string $bindType
     */
    public function addValueToCondition( $value, $bindType )
    {
        $conditionVO = $this->getLatestCondition();
        if( !$conditionVO )
        {
            return;
        }
        $conditionVO->addEntryValue( $value, $bindType );
    }

    /**
     * @return ConditionDatabaseCoreVO
     */
    public function getLatestCondition()
    {
        $conditionVOs = $this->getConditionVOs();
        if( empty( $conditionVOs ) )
        {
            return null;
        }

        $conditionVO = $conditionVOs[ count($conditionVOs) - 1 ];
        /* @var $conditionVO ConditionDatabaseCoreVO */
        return $conditionVO;
    }

    /**
     * @return string
     */
    public function getTableName()
    {
        return $this->tableName;
    }

    /**
     * @param string $value
     */
    public function setTableName( $value )
    {
        $this->tableName = $value;
    }

    /**
     * @return array
     */
    public function getColumnValues()
    {
        return $this->columnValues;
    }

    /**
     * @param array $value
     */
    public function setColumnValues( $value )
    {
        $this->columnValues = $value;
    }

    /**
     * @return array
     */
    public function getConditionVOs()
    {
        return $this->conditionVOs;
    }

    /**
     * @param array $value
     */
    public function setConditionVOs( $value )
    {
        $this->conditionVOs = $value;
    }
} 