<?php


class OrderByRequestDatabaseCoreVO
{

    /**
     * @var string
     */
    private $columnName;

    /**
     * @var boolean
     */
    private $descending;

    /**
     * @param string $columnName
     * @param boolean $descending
     */
    function __construct( $columnName, $descending = false )
    {
        $this->setColumnName( $columnName );
        $this->setDescending( $descending );
    }

    /**
     * @return string
     */
    public function getColumnName()
    {
        return $this->columnName;
    }

    /**
     * @param string $value
     */
    public function setColumnName( $value )
    {
        $this->columnName = $value;
    }

    /**
     * @return boolean
     */
    public function isDescending()
    {
        return $this->descending;
    }

    /**
     * @param boolean $value
     */
    public function setDescending( $value )
    {
        $this->descending = $value;
    }
} 