<?php


class DatabaseCorePath
{

    /**
     * @var BalancingDatabaseCorePath
     */
    private $balancing;

    /**
     * @var LocalizationDatabaseCorePath
     */
    private $localization;

    /**
     * @var RequestInfoDatabaseCorePath
     */
    private $requestInfo;

    /**
     * @var DatabaseCoreModel
     */
    private $databaseModel;

    public function __construct()
    {
        $this->getIncludes();
    }

    public function __destruct()
    {
        $this->databaseModel = null;
    }

    private function getIncludes()
    {
        require_once( DIR_CORE . "/application/database/app/includes/DatabaseCoreIncludes.php" );
    }

    /**
     * @return BalancingDatabaseCorePath
     */
    public function getBalancing()
    {
        if( !$this->balancing )
        {
            require_once( DIR_CORE . "/application/database/balancing/BalancingDatabaseCorePath.php" );
            $this->balancing = new BalancingDatabaseCorePath();
        }
        return $this->balancing;
    }

    /**
     * @return LocalizationDatabaseCorePath
     */
    public function getLocalization()
    {
        if( !$this->localization )
        {
            require_once( DIR_CORE . "/application/database/localization/LocalizationDatabaseCorePath.php" );
            $this->localization = new LocalizationDatabaseCorePath();
        }
        return $this->localization;
    }

    /**
     * @return RequestInfoDatabaseCorePath
     */
    public function getRequestInfo()
    {
        if( !$this->requestInfo )
        {
            require_once( DIR_CORE . "/application/database/requestInfo/RequestInfoDatabaseCorePath.php" );
            $this->requestInfo = new RequestInfoDatabaseCorePath();
        }
        return $this->requestInfo;
    }

    /**
     * @return DatabaseCoreModel
     */
    public function getDatabaseModel()
    {
        if( !$this->databaseModel )
        {
            $this->databaseModel = new DatabaseCoreModel();
        }
        return $this->databaseModel;
    }

    /**
     * @return InitializeDatabaseCommand
     */
    public function getInitializeCommand()
    {
        return new InitializeDatabaseCommand();
    }

    /**
     * @return BasicDatabaseConstructor
     */
    public function getBasicConstructor()
    {
        return new BasicDatabaseConstructor();
    }

    /**
     * @return ConditionDatabaseConstructor
     */
    public function getConditionConstructor()
    {
        return new ConditionDatabaseConstructor();
    }

    /**
     * @return SqlBindDatabaseConstructor
     */
    public function getSqlBindConstructor()
    {
        return new SqlBindDatabaseConstructor();
    }

    /**
     * @return ResponseDatabaseParser
     */
    public function getResponseParser()
    {
        return new ResponseDatabaseParser();
    }
    /**
     * @return BasicDatabaseRequest
     */
    public function getBasicRequest()
    {
        return new BasicDatabaseRequest();
    }

    /**
     * @return DetermineConfigDatabaseHelper
     */
    public function getDetermineConfigService()
    {
        return new DetermineConfigDatabaseHelper();
    }

    /**
     * @return ProcessQueueDatabaseService
     */
    public function getProcessQueueService()
    {
        return new ProcessQueueDatabaseService();
    }
}