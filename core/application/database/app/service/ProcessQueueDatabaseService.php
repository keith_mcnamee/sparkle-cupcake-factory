<?php


class ProcessQueueDatabaseService extends CoreBase
{

    /**
     * @var BasicDatabaseRequest
     */
    private $basicDatabaseRequest;

    public function __destruct()
    {
        $this->basicDatabaseRequest = null;

        parent::__destruct();
    }

    /**
     * @param QueueDatabaseCoreVO $queueVO
     */
    public function processQueue( QueueDatabaseCoreVO $queueVO = null )
    {
        if( !$queueVO )
        {
            $queueVO = $this->getCoreDatabase()->getDatabaseModel()->getQueueVO();
        }
        $typeQueue = $queueVO->getTypeQueue();

        foreach( $typeQueue as $itemType )
        {
            /* @var $itemType string */
            switch( $itemType )
            {
                case QueueDatabaseCoreVO::$INSERT_QUEUE_ITEM:
                    $this->processInsert( $queueVO );
                    break;

                case QueueDatabaseCoreVO::$UPDATE_QUEUE_ITEM:
                    $this->processUpdate( $queueVO );
                    break;

                case QueueDatabaseCoreVO::$DELETE_QUEUE_ITEM:
                    $this->processDelete( $queueVO );
                    break;
            }
        }

        $queueVO->reset();
    }

    /**
     * @param QueueDatabaseCoreVO $queueVO
     */
    private function processInsert( QueueDatabaseCoreVO $queueVO )
    {
        $insertQueue = $queueVO->getInsertQueue();
        if( empty( $insertQueue ) )
        {
            return;
        }
        $nextQueue = $insertQueue[0];
        foreach( $nextQueue as $insertVO )
        {
            /* @var $insertVO InsertRequestDatabaseCoreVO */
            $this->getBasicDatabaseRequest()->insert( $insertVO );
        }
        $insertQueue = CoreHelper::getArrayHelper()->deleteObjectAtIndex( $insertQueue, 0 );
        $queueVO->setInsertQueue( $insertQueue );
    }

    /**
     * @param QueueDatabaseCoreVO $queueVO
     */
    private function processUpdate( QueueDatabaseCoreVO $queueVO )
    {
        $updateQueue = $queueVO->getUpdateQueue();
        if( empty( $updateQueue ) )
        {
            return;
        }
        $nextQueue = $updateQueue[0];
        foreach( $nextQueue as $updateVO )
        {
            /* @var $updateVO UpdateRequestDatabaseCoreVO */
            $this->getBasicDatabaseRequest()->update( $updateVO );
        }
        $updateQueue = CoreHelper::getArrayHelper()->deleteObjectAtIndex( $updateQueue, 0 );
        $queueVO->setUpdateQueue( $updateQueue );
    }

    /**
     * @param QueueDatabaseCoreVO $queueVO
     */
    private function processDelete( QueueDatabaseCoreVO $queueVO )
    {
        $deleteQueue = $queueVO->getDeleteQueue();
        if( empty( $deleteQueue ) )
        {
            return;
        }
        $nextQueue = $deleteQueue[0];
        foreach( $nextQueue as $deleteVO )
        {
            /* @var $deleteVO DeleteRequestDatabaseCoreVO */
            $this->getBasicDatabaseRequest()->delete( $deleteVO );
        }
        $deleteQueue = CoreHelper::getArrayHelper()->deleteObjectAtIndex( $deleteQueue, 0 );
        $queueVO->setInsertQueue( $deleteQueue );
    }

    /**
     * @return BasicDatabaseRequest
     */
    private function getBasicDatabaseRequest()
    {
        if( !$this->basicDatabaseRequest )
        {
            $this->basicDatabaseRequest = $this->getCoreDatabase()->getBasicRequest();
        }
        return $this->basicDatabaseRequest;
    }
}