<?php


class DetermineConfigDatabaseCoreService extends CoreBase
{

    /**
     * @param array $configVOs
     * @param bool $checkServerID
     * @param bool $checkIsStaging
     * @return array
     */
    public function findByMultipleConfigIDs( array $configVOs, $checkServerID = false, $checkIsStaging = false )
    {
        $serverConfigID = null;
        $isStagingDatabase = false;
        $coreRequestModel = $this->getCoreRequest()->getModel()->getModel();
        if( $checkIsStaging )
        {
            $isUsingStagingDatabase = $coreRequestModel->getServerConfigVO()->isUsingStagingDatabase();
            $isStagingDirectory = $this->getCoreApp()->getService()->getInfo()->isStagingDirectory();
            $isStagingDatabase = $isStagingDirectory && $isUsingStagingDatabase;
        }
        if( $checkServerID )
        {
            $serverConfigID = $coreRequestModel->getServerConfigVO()->getConfigID();
        }

        $returnVOs = array();
        $databaseConfigIDs = array();
        foreach( $configVOs as $configVO )
        {
            /* @var $configVO ConfigDatabaseCoreVO */
            $databaseConfigIDs[ $configVO->getConfigID() ] = true;
        }
        $databaseConfigIDs = CoreHelper::getArrayCommand()->dictionaryKeyToArray( $databaseConfigIDs );
        foreach( $databaseConfigIDs as $databaseConfigID )
        {
            /* @var $databaseConfigID string */
            $thisConfigVOs = $configVOs;
            if( count( $databaseConfigIDs ) > 1 )
            {
                $thisConfigVOs = $this->filterByConfigID( $thisConfigVOs, $databaseConfigID );
            }

            if( $checkServerID )
            {
                $thisConfigVOs = $this->filterByServerID( $thisConfigVOs, $serverConfigID );
            }

            if( $checkIsStaging )
            {
                $thisConfigVOs = $this->filterByIsStaging( $thisConfigVOs, $isStagingDatabase );
            }

            if( count( $thisConfigVOs ) == 1 )
            {
                $returnVOs[ $databaseConfigID ] = $thisConfigVOs[ 0 ];
            }
            else
            {
                $returnVOs[ $databaseConfigID ] = null;
            }
        }

        return $returnVOs;
    }

    /**
     * @param array $configVOs
     * @param bool $checkServerID
     * @param bool $checkIsStaging
     * @return ConfigDatabaseCoreVO
     */
    public function findSingleEntry( array $configVOs, $checkServerID = false, $checkIsStaging = false )
    {
        $configVOs = $this->findByMultipleConfigIDs( $configVOs, $checkServerID, $checkIsStaging );
        foreach( $configVOs as $vo )
        {
            /* @var $vo ConfigDatabaseCoreVO */
            return $vo;
        }
        return null;
    }

    /**
     * @param array $configVOs
     * @param string $databaseConfigID
     * @return array
     */
    public function filterByConfigID( array $configVOs, $databaseConfigID )
    {
        $typeVOs = array();

        foreach( $configVOs as $vo )
        {
            /* @var $vo ConfigDatabaseCoreVO */
            if( $vo->getConfigID() == $databaseConfigID )
            {
                $typeVOs[] = $vo;
            }
        }

        return $typeVOs;
    }

    /**
     * @param array $configVOs
     * @param string $serverConfigID
     * @return array
     */
    public function filterByServerID( array $configVOs, $serverConfigID )
    {
        $typeVOs = array();

        foreach( $configVOs as $vo )
        {
            /* @var $vo ConfigDatabaseCoreVO */
            if( $vo->getServerConfigType() == $serverConfigID )
            {
                $typeVOs[] = $vo;
            }
        }

        return $typeVOs;
    }


    /**
     * @param array $configVOs
     * @param boolean $isStaging
     * @return array
     */
    public function filterByIsStaging( array $configVOs, $isStaging )
    {
        $typeVOs = array();
        foreach( $configVOs as $vo )
        {
            /* @var $vo ConfigDatabaseCoreVO */
            if( $vo->isStagingDatabase() == $isStaging )
            {
                $typeVOs[] = $vo;
            }
        }

        return $typeVOs;
    }
}