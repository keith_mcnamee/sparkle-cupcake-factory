<?php


class BalancingDatabaseCoreConstructor extends CoreBase
{

    /**
     * @var ConditionDatabaseConstructor
     */
    private $conditionConstructor;

    public function __destruct()
    {
        $this->conditionConstructor = null;

        parent::__destruct();
    }

    /**
     * @return SelectRequestDatabaseCoreVO
     */
    public function select()
    {
        $selectVO = new SelectRequestDatabaseCoreVO( SqlDatabaseCoreConstants::$BALANCING_FILE_INFO_TABLE );

        $selectVO->addColumn( SqlDatabaseCoreConstants::$REF );
        $selectVO->addColumn( SqlDatabaseCoreConstants::$VERSION_INFO );
        $selectVO->addColumn( SqlDatabaseCoreConstants::$FILE_LOCATION_INFO );
        $selectVO->addColumn( SqlDatabaseCoreConstants::$LAST_MODIFIED_DATE );
        $selectVO->addColumn( SqlDatabaseCoreConstants::$FILE_SIZE );

        return $selectVO;
    }
    /**
     * @param FileInfoBalancingCoreVO $fileInfoVO
     * @return InsertRequestDatabaseCoreVO
     */
    public function insert( FileInfoBalancingCoreVO $fileInfoVO )
    {
        $insertVO = new InsertRequestDatabaseCoreVO( SqlDatabaseCoreConstants::$BALANCING_FILE_INFO_TABLE, SqlDatabaseCoreConstants::$REF );

        $insertVO->addStringColumnValue( SqlDatabaseCoreConstants::$VERSION_INFO, $fileInfoVO->getVersion() );
        $insertVO->addStringColumnValue( SqlDatabaseCoreConstants::$FILE_LOCATION_INFO, $fileInfoVO->getFileLocation() );
        $insertVO->addStringColumnValue( SqlDatabaseCoreConstants::$LAST_MODIFIED_DATE, CoreHelper::getDateHelper()->stringFromUnix( $fileInfoVO->getLastModifiedDate() ) );
        $insertVO->addStringColumnValue( SqlDatabaseCoreConstants::$FILE_SIZE, $fileInfoVO->getFileSize() );
        $insertVO->addStringColumnValue( SqlDatabaseCoreConstants::$UPDATED, CoreHelper::getDateHelper()->nowString() );
        $insertVO->addStringColumnValue( SqlDatabaseCoreConstants::$CREATED, CoreHelper::getDateHelper()->nowString() );

        return $insertVO;
    }

    /**
     * @param FileInfoBalancingCoreVO $fileInfoVO
     * @return UpdateRequestDatabaseCoreVO
     */
    public function update( FileInfoBalancingCoreVO $fileInfoVO )
    {
        $updateVO = new UpdateRequestDatabaseCoreVO( SqlDatabaseCoreConstants::$BALANCING_FILE_INFO_TABLE );

        $updateVO->addStringColumnValue( SqlDatabaseCoreConstants::$VERSION_INFO, $fileInfoVO->getVersion() );
        $updateVO->addStringColumnValue( SqlDatabaseCoreConstants::$FILE_LOCATION_INFO, $fileInfoVO->getFileLocation() );
        $updateVO->addStringColumnValue( SqlDatabaseCoreConstants::$LAST_MODIFIED_DATE, CoreHelper::getDateHelper()->stringFromUnix( $fileInfoVO->getLastModifiedDate() ) );
        $updateVO->addStringColumnValue( SqlDatabaseCoreConstants::$FILE_SIZE, $fileInfoVO->getFileSize() );
        $updateVO->addStringColumnValue( SqlDatabaseCoreConstants::$UPDATED, CoreHelper::getDateHelper()->nowString() );

        $updateVO->addCondition( $this->getConditionConstructor()->equalsUnknown( SqlDatabaseCoreConstants::$REF ) );
        $updateVO->addIntValueToCondition( $fileInfoVO->getRef() );

        return $updateVO;
    }

    /**
     * @return ConditionDatabaseConstructor
     */
    private function getConditionConstructor()
    {
        if( !$this->conditionConstructor )
        {
            $this->conditionConstructor = $this->getCoreDatabase()->getConditionConstructor();
        }
        return $this->conditionConstructor;
    }
} 