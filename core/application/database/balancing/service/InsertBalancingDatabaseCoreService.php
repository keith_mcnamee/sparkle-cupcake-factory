<?php


class InsertBalancingDatabaseCoreService extends CoreBase
{

    /**
     * @var string
     */
    private $processFailMessage;

    /**
     * @param FileInfoBalancingCoreVO $configVO
     */
    public function insertConfig( FileInfoBalancingCoreVO $configVO )
    {

        if( empty( $configVO ) )
        {
            $this->getDebugService()->trace( DebugViewText::$NO_LOCALIZATION_TO_UPDATE, true );
            return;
        }
        $coreBalancingModel = $this->getCoreBalancing()->getBalancingModel();

        $currentVO = $coreBalancingModel->getFileInfoVO();

        $version = $currentVO ? $currentVO->getVersion() : 0;
        $configVersion = (float)CoreHelper::getVersionHelper()->validVersion( $version );
        $currentVersion = $currentVO ? (float)CoreHelper::getVersionHelper()->validVersion( $version ) : 0;

        if( $configVersion && $configVersion < $currentVersion )
        {
            $this->processFailMessage = DebugViewText::$TOO_LOW_BALANCING_VERSION;
            return;
        }
        $versionChanged = $configVersion > $currentVersion;

        if ( !CoreHelper::getFileHelper()->fileContentExists( $configVO->getFileLocation() ) )
        {
            $this->processFailMessage = DebugViewText::$INVALID_BALANCING_CONTENT;
            return;
        }

        $lastModifiedDate = CoreHelper::getFileHelper()->lastModifiedDate( $configVO->getFileLocation() );
        $fileSize = CoreHelper::getFileHelper()->fileSize( $configVO->getFileLocation() );
        $contentChanged = !$currentVO || $lastModifiedDate != $currentVO->getLastModifiedDate() || $fileSize != $currentVO->getFileSize();

        $locationChanged = !$currentVO || CoreHelper::getStringHelper()->stripSanitization( $configVO->getFileLocation() ) != CoreHelper::getStringHelper()->stripSanitization( $currentVO->getFileLocation() );

        $versionChanged = true;//TODO Delete this. For testing only
        if( ! ( $versionChanged  || $contentChanged  || $locationChanged ) )
        {
            $this->getDebugService()->trace( DebugViewText::$BALANCING_EXISTS, true );
            return;
        }

        $coreBalancingModel->setContentChanged( true );

        if( $versionChanged )
        {
            $version = CoreHelper::getVersionHelper()->validVersion( $configVersion );
        }
        else
        {
            $version = CoreHelper::getVersionHelper()->nextVersion( $currentVersion );
        }

        $coreDatabaseModel = $this->getCoreDatabase()->getDatabaseModel();
        $balancingDatabaseConstructor = $this->getCoreBalancingDatabase()->getDatabaseConstructor();
        if( !$locationChanged )
        {
            $currentVO->setVersion( $version );
            $currentVO->setFileLocation( $configVO->getFileLocation() );
            $currentVO->setLastModifiedDate( $lastModifiedDate );
            $currentVO->setFileSize( $fileSize );
            $updateVO = $balancingDatabaseConstructor->update( $currentVO );
            $coreDatabaseModel->getQueueVO()->addUpdateVO( $updateVO );
            return;
        }
        $configVO->setVersion( $version );
        $configVO->setLastModifiedDate( $lastModifiedDate );
        $configVO->setFileSize( $fileSize );
        $insertVO = $balancingDatabaseConstructor->insert( $configVO );
        $coreDatabaseModel->getQueueVO()->addInsertVO( $insertVO );
    }

    /**
     * @return string
     */
    public function getProcessFailMessage()
    {
        return $this->processFailMessage;
    }
}