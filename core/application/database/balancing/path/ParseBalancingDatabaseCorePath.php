<?php


class ParseBalancingDatabaseCorePath
{

    /**
     * @var BalancingDatabaseCoreParser
     */
    private $parse;

    public function __destruct()
    {
        $this->parse = null;
    }

    /**
     * @param $singleInstance
     * @return BalancingDatabaseCoreParser
     */
    private function createParse( $singleInstance )
    {
        $instance = new BalancingDatabaseCoreParser();
        $instance->init();
        if( !$singleInstance )
        {
            $this->parse = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return BalancingDatabaseCoreParser
     */
    public function getParse( $singleInstance = true )
    {
        return $this->parse && !$singleInstance ? $this->parse : $this->createParse( $singleInstance );
    }
}