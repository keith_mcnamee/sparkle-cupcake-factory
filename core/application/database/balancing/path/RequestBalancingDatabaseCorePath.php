<?php


class RequestBalancingDatabaseCorePath
{

    /**
     * @var BalancingDatabaseCoreRequest
     */
    private $request;

    public function __destruct()
    {
        $this->request = null;
    }

    /**
     * @param $singleInstance
     * @return BalancingDatabaseCoreRequest
     */
    private function createRequest( $singleInstance )
    {
        $instance = new BalancingDatabaseCoreRequest();
        $instance->init();
        if( !$singleInstance )
        {
            $this->request = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return BalancingDatabaseCoreRequest
     */
    public function getRequest( $singleInstance = true )
    {
        return $this->request && !$singleInstance ? $this->request : $this->createRequest( $singleInstance );
    }
}