<?php


class ConstructBalancingDatabaseCorePath
{

    /**
     * @var BalancingDatabaseCoreConstructor
     */
    private $construct;

    public function __destruct()
    {
        $this->construct = null;
    }

    /**
     * @param $singleInstance
     * @return BalancingDatabaseCoreConstructor
     */
    private function createConstruct( $singleInstance )
    {
        $instance = new BalancingDatabaseCoreConstructor();
        $instance->init();
        if( !$singleInstance )
        {
            $this->construct = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return BalancingDatabaseCoreConstructor
     */
    public function getConstruct( $singleInstance = true )
    {
        return $this->construct && !$singleInstance ? $this->construct : $this->createConstruct( $singleInstance );
    }
}