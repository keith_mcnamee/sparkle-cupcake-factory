<?php


class BalancingDatabaseCorePath extends CoreBase
{

    public function __construct()
    {
        $this->getCoreBalancing();
        $this->getIncludes();
    }

    private function getIncludes()
    {
        require_once( DIR_CORE . "/application/database/balancing/includes/BalancingDatabaseCoreIncludes.php" );
    }

    /**
     * @return BalancingDatabaseCoreConstructor
     */
    public function getDatabaseConstructor()
    {
        return new BalancingDatabaseCoreConstructor();
    }

    /**
     * @return BalancingDatabaseCoreParser
     */
    public function getDatabaseParser()
    {
        return new BalancingDatabaseCoreParser();
    }
    /**
     * @return BalancingDatabaseCoreRequest
     */
    public function getDatabaseRequest()
    {
        return new BalancingDatabaseCoreRequest();
    }

    /**
     * @return InsertBalancingDatabaseCoreService
     */
    public function getInsertService()
    {
        return new InsertBalancingDatabaseCoreService();
    }
}