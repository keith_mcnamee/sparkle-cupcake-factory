<?php


class BalancingDatabaseCoreRequest extends CoreBase
{

    /**
     * @var BasicDatabaseRequest
     */
    private $basicDatabaseRequest;

    /**
     * @var ConditionDatabaseConstructor
     */
    private $conditionConstructor;

    /**
     * @var BalancingDatabaseCoreConstructor
     */
    private $balancingDatabaseConstructor;

    /**
     * @var BalancingDatabaseCoreParser
     */
    private $balancingDatabaseParser;

    public function __destruct()
    {
        $this->basicDatabaseRequest = null;
        $this->conditionConstructor = null;
        $this->balancingDatabaseConstructor = null;
        $this->balancingDatabaseParser = null;

        parent::__destruct();
    }

    /**
     * @return FileInfoBalancingCoreVO
     */
    public function fileInfoRequest()
    {
        $selectVO = $this->getBalancingDatabaseConstructor()->select();

        $selectVO->addOrderBy( SqlDatabaseCoreConstants::$VERSION_INFO, true );
        $selectVO->addOrderBy( SqlDatabaseCoreConstants::$CREATED, true );
        $selectVO->addLimit( 0, 1 );

        $versionVOs = $this->fileInfosRequest( $selectVO );
        $versionVO = CoreHelper::getArrayHelper()->firstObject( $versionVOs );
        /* @var $versionVO FileInfoBalancingCoreVO */

        return $versionVO;
    }

    /**
     * @param FileInfoBalancingCoreVO $vo
     * @return array
     */
    public function fileInfoCompareRequest( FileInfoBalancingCoreVO $vo )
    {
        $selectVO = $this->getBalancingDatabaseConstructor()->select();


        $selectVO->addCondition( $this->getConditionConstructor()->equalsUnknown( SqlDatabaseCoreConstants::$REF ) );
        $selectVO->addIntValueToCondition( $vo->getRef() );

        $selectVO->addCondition( $this->getConditionConstructor()->compareUnknown( SqlDatabaseCoreConstants::$VERSION_INFO, true, true ), true );
        $selectVO->addIntValueToCondition( $vo->getVersion() );

        $selectVO->addOrderBy( SqlDatabaseCoreConstants::$VERSION_INFO, true );
        $selectVO->addOrderBy( SqlDatabaseCoreConstants::$CREATED, true );

        $versionVOs = $this->fileInfosRequest( $selectVO );

        return $versionVOs;
    }

    /**
     * @param SelectRequestDatabaseCoreVO $selectVO
     * @return array
     */
    public function fileInfosRequest( SelectRequestDatabaseCoreVO $selectVO = null )
    {
        if( !$selectVO )
        {
            $selectVO = $this->getBalancingDatabaseConstructor()->select();
        }
        $entries = $this->getBasicDatabaseRequest()->select( $selectVO );

        $vos = $this->getBalancingDatabaseParser()->fileInfos( $entries );
        return $vos;
    }

    /**
     * @return BasicDatabaseRequest
     */
    private function getBasicDatabaseRequest()
    {
        if( !$this->basicDatabaseRequest )
        {
            $this->basicDatabaseRequest = $this->getCoreDatabase()->getBasicRequest();
        }
        return $this->basicDatabaseRequest;
    }

    /**
     * @return ConditionDatabaseConstructor
     */
    private function getConditionConstructor()
    {
        if( !$this->conditionConstructor )
        {
            $this->conditionConstructor = $this->getCoreDatabase()->getConditionConstructor();
        }
        return $this->conditionConstructor;
    }

    /**
     * @return BalancingDatabaseCoreConstructor
     */
    private function getBalancingDatabaseConstructor()
    {
        if( !$this->balancingDatabaseConstructor )
        {
            $this->balancingDatabaseConstructor = $this->getCoreBalancingDatabase()->getDatabaseConstructor();
        }
        return $this->balancingDatabaseConstructor;
    }

    /**
     * @return BalancingDatabaseCoreParser
     */
    private function getBalancingDatabaseParser()
    {
        if( !$this->balancingDatabaseParser )
        {
            $this->balancingDatabaseParser = $this->getCoreBalancingDatabase()->getDatabaseParser();
        }
        return $this->balancingDatabaseParser;
    }
} 