<?php


class RequestInfoDatabaseCoreCommand extends CoreBase
{

    /**
     * @var RequestInfoDatabaseCoreModel
     */
    private $coreRequestInfoDatabaseModel;

    /**
     * @var RequestInfoDatabaseCoreRequest
     */
    private $requestInfoRequest;


    public function init()
    {
        parent::init();

        $this->coreRequestInfoDatabaseModel = $this->getCoreRequestInfoDatabase()->getModel()->getModel();
        $this->requestInfoRequest = $this->getCoreRequestInfoDatabase()->getRequest()->getRequest();
    }

    public function __destruct()
    {
        $this->coreRequestInfoDatabaseModel = null;
        $this->requestInfoRequest = null;

        parent::__destruct();
    }

    /**
     * @param int $timeout
     * @param string $requiredMatchID
     */
    public function defineInfoCommand( $timeout = 0, $requiredMatchID = null  )
    {
        //TODO switch back to default
        $timeout = $timeout > 0 ? $timeout : RequestInfoDatabaseCoreConstants::$TESTING_TIMEOUT;
        $currentVO = $this->requestInfoRequest->select();
        if( !$requiredMatchID )
        {
            if ( $currentVO )
            {
                $no = CoreHelper::getDateService()->now();
                if( $currentVO->getExpiredDate() > CoreHelper::getDateService()->now() )
                {
                    $this->coreRequestInfoDatabaseModel->setBlockingVO( $currentVO );
                    return;
                }
            }
            else
            {
                $currentVO = new RequestInfoCoreVO();
            }
        }
        else
        {
            if( $currentVO->getRandomID() != $requiredMatchID )
            {
                $this->coreRequestInfoDatabaseModel->setBlockingVO( $currentVO );
                return;
            }
        }
        $randomID = CoreHelper::getIdService()->randomID();
        $currentVO->setRandomID( $randomID );
        $currentVO->setExpiredDate( CoreHelper::getDateService()->now() + $timeout );

        if( $currentVO->getRef() )
        {
            $this->requestInfoRequest->update( array( $currentVO->getRef() => $currentVO ) );
        }
        else
        {
            $vos = $this->requestInfoRequest->insert( array( $currentVO ), true );
            $currentVO = CoreHelper::getArrayService()->firstObject( $vos );
            /* @var $currentVO RequestInfoCoreVO */
        }
        $this->coreRequestInfoDatabaseModel->setRequestInfoVO( $currentVO );
    }

    public function releaseInfoCommand()
    {
        $currentVO = $this->coreRequestInfoDatabaseModel->getRequestInfoVO();
        $currentVO->setExpiredDate( CoreHelper::getDateService()->now() );
        $this->requestInfoRequest->update( array( $currentVO->getRef() => $currentVO ) );
    }
} 