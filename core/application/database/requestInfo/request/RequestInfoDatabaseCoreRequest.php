<?php


class RequestInfoDatabaseCoreRequest extends CoreBase
{

    /**
     * @var BasicDatabaseRequest
     */
    private $basicDatabaseRequest;

    /**
     * @var RequestInfoDatabaseCoreConstructor
     */
    private $requestInfoConstructor;

    /**
     * @var RequestInfoDatabaseCoreParser
     */
    private $requestInfoParser;

    public function __destruct()
    {
        $this->basicDatabaseRequest = null;
        $this->requestInfoConstructor = null;
        $this->requestInfoParser = null;

        parent::__destruct();
    }

    /**
     * @return RequestInfoCoreVO
     */
    public function select()
    {
        $selectVO = $this->getRequestInfoConstructor()->select();
        $entry = $this->getBasicDatabaseRequest()->select( $selectVO, null, true );

        $vo = $this->getRequestInfoParser()->parseEntry( $entry );
        return $vo;
    }

    /**
     * @param array $generalInfoVOs
     * @param boolean $select
     * @return array
     */
    public function insert( array $generalInfoVOs, $select = false )
    {
        $selectVO = $select ? $this->getRequestInfoConstructor()->select( $generalInfoVOs ) : null;
        $insertVO = $this->getRequestInfoConstructor()->insert( $generalInfoVOs );
        $entries = $this->getBasicDatabaseRequest()->insert( $insertVO, $selectVO );

        $vos = $select ? $this->getRequestInfoParser()->byRef( $entries ) : null;
        return $vos;
    }

    /**
     * @param array $generalInfoVOs
     * @param boolean $select
     * @return array
     */
    public function update( array $generalInfoVOs, $select = false )
    {
        $vos = $select ? array() : null;
        foreach( $generalInfoVOs as $generalInfoVO )
        {
            /* @var $generalInfoVO RequestInfoCoreVO */
            $selectVO = $select ? $this->getRequestInfoConstructor()->select( array( $generalInfoVO->getRef() => true) ) : null;
            $updateVO = $this->getRequestInfoConstructor()->update( $generalInfoVO );
            $entries = $this->getBasicDatabaseRequest()->update( $updateVO, $selectVO );
            if( $select )
            {
                $thisVOs = $this->getRequestInfoParser()->byRef( $entries );
                $vos = CoreHelper::getArrayHelper()->dictionaryMerge( $vos, $thisVOs);
            }

        }

        return $vos;
    }

    /**
     * @return BasicDatabaseRequest
     */
    private function getBasicDatabaseRequest()
    {
        if( !$this->basicDatabaseRequest )
        {
            $this->basicDatabaseRequest = $this->getCoreDatabase()->getBasicRequest();
        }
        return $this->basicDatabaseRequest;
    }

    /**
     * @return RequestInfoDatabaseCoreConstructor
     */
    private function getRequestInfoConstructor()
    {
        if( !$this->requestInfoConstructor )
        {
            $this->requestInfoConstructor = $this->getCoreRequestInfoDatabase()->getRequestInfoConstructor();
        }
        return $this->requestInfoConstructor;
    }

    /**
     * @return RequestInfoDatabaseCoreParser
     */
    private function getRequestInfoParser()
    {
        if( !$this->requestInfoParser )
        {
            $this->requestInfoParser = $this->getCoreRequestInfoDatabase()->getRequestInfoParser();
        }
        return $this->requestInfoParser;
    }
}