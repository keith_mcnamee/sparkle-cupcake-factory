<?php


class RequestInfoDatabaseCorePath
{

    /**
     * @var RequestInfoDatabaseCoreModel
     */
    private $requestInfoModel;

    public function __construct()
    {
        $this->getIncludes();
    }

    public function __destruct()
    {
        $this->requestInfoModel = null;
    }

    private function getIncludes()
    {
        require_once( DIR_CORE . "/application/database/requestInfo/includes/RequestInfoDatabaseCoreIncludes.php" );
    }

    /**
     * @return RequestInfoDatabaseCoreModel
     */
    public function getRequestInfoModel()
    {
        if( !$this->requestInfoModel )
        {
            $this->requestInfoModel = new RequestInfoDatabaseCoreModel();
        }
        return $this->requestInfoModel;
    }

    /**
     * @return RequestInfoDatabaseCoreConstructor
     */
    public function getRequestInfoConstructor()
    {
        return new RequestInfoDatabaseCoreConstructor();
    }

    /**
     * @return RequestInfoDatabaseCoreParser
     */
    public function getRequestInfoParser()
    {
        return new RequestInfoDatabaseCoreParser();
    }

    /**
     * @return RequestInfoDatabaseCoreRequest
     */
    public function getRequestInfoRequest()
    {
        return new RequestInfoDatabaseCoreRequest();
    }

    /**
     * @return RegisterRequestInfoDatabaseCoreService
     */
    public function getRegisterRequestInfoService()
    {
        return new RegisterRequestInfoDatabaseCoreService();
    }
}