<?php


class RequestRequestInfoDatabaseCorePath
{

    /**
     * @var RequestInfoDatabaseCoreRequest
     */
    private $request;

    public function __destruct()
    {
        $this->request = null;
    }

    /**
     * @param $singleInstance
     * @return RequestInfoDatabaseCoreRequest
     */
    private function createRequest( $singleInstance )
    {
        $instance = new RequestInfoDatabaseCoreRequest();
        $instance->init();
        if( !$singleInstance )
        {
            $this->request = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return RequestInfoDatabaseCoreRequest
     */
    public function getRequest( $singleInstance = false )
    {
        return $this->request && !$singleInstance ? $this->request : $this->createRequest( $singleInstance );
    }
}