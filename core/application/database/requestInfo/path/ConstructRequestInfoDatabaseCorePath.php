<?php


class ConstructRequestInfoDatabaseCorePath
{

    /**
     * @var RequestInfoDatabaseCoreConstructor
     */
    private $construct;

    public function __destruct()
    {
        $this->construct = null;
    }

    /**
     * @param $singleInstance
     * @return RequestInfoDatabaseCoreConstructor
     */
    private function createConstruct( $singleInstance )
    {
        $instance = new RequestInfoDatabaseCoreConstructor();
        $instance->init();
        if( !$singleInstance )
        {
            $this->construct = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return RequestInfoDatabaseCoreConstructor
     */
    public function getConstruct( $singleInstance = false )
    {
        return $this->construct && !$singleInstance ? $this->construct : $this->createConstruct( $singleInstance );
    }
}