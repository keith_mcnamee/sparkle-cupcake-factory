<?php


class ParseRequestInfoDatabaseCorePath
{

    /**
     * @var RequestInfoDatabaseCoreParser
     */
    private $parse;

    public function __destruct()
    {
        $this->parse = null;
    }

    /**
     * @param $singleInstance
     * @return RequestInfoDatabaseCoreParser
     */
    private function createParse( $singleInstance )
    {
        $instance = new RequestInfoDatabaseCoreParser();
        $instance->init();
        if( !$singleInstance )
        {
            $this->parse = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return RequestInfoDatabaseCoreParser
     */
    public function getParse( $singleInstance = false )
    {
        return $this->parse && !$singleInstance ? $this->parse : $this->createParse( $singleInstance );
    }
}