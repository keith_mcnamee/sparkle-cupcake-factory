<?php


class CommandRequestInfoDatabaseCorePath
{

    /**
     * @var RequestInfoDatabaseCoreCommand
     */
    private $command;

    public function __destruct()
    {
        $this->command = null;
    }

    /**
     * @param $singleInstance
     * @return RequestInfoDatabaseCoreCommand
     */
    private function createCommand( $singleInstance )
    {
        $instance = new RequestInfoDatabaseCoreCommand();
        $instance->init();
        if( !$singleInstance )
        {
            $this->command = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return RequestInfoDatabaseCoreCommand
     */
    public function getCommand( $singleInstance = false )
    {
        return $this->command && !$singleInstance ? $this->command : $this->createCommand( $singleInstance );
    }
}