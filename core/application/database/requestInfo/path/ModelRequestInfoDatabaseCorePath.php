<?php


class ModelRequestInfoDatabaseCorePath
{

    /**
     * @var RequestInfoDatabaseCoreModel
     */
    private $model;

    public function __destruct()
    {
        $this->model = null;
    }

    /**
     * @param $singleInstance
     * @return RequestInfoDatabaseCoreModel
     */
    private function createModel( $singleInstance )
    {
        $instance = new RequestInfoDatabaseCoreModel();
        $instance->init();
        if( !$singleInstance )
        {
            $this->model = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return RequestInfoDatabaseCoreModel
     */
    public function getModel( $singleInstance = false )
    {
        return $this->model && !$singleInstance ? $this->model : $this->createModel( $singleInstance );
    }
}