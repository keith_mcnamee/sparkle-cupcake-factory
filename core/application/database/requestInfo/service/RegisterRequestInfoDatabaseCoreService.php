<?php


class RegisterRequestInfoDatabaseCoreService extends CoreBase
{

    /**
     * @var RequestInfoDatabaseCoreModel
     */
    private $coreRequestInfoDatabaseModel;

    /**
     * @var RequestInfoDatabaseCoreRequest
     */
    private $requestInfoRequest;

    public function __destruct()
    {
        $this->coreRequestInfoDatabaseModel = null;
        $this->requestInfoRequest = null;

        parent::__destruct();
    }

    /**
     * @param int $timeout
     * @param string $requiredMatchID
     */
    public function defineInfo( $timeout = 0, $requiredMatchID = null  )
    {
        //TODO switch back to default
        $timeout = $timeout > 0 ? $timeout : RequestInfoDatabaseCoreConstants::$TESTING_TIMEOUT;
        $currentVO = $this->getRequestInfoRequest()->select();
        if( !$requiredMatchID )
        {
            if ( $currentVO )
            {
                $no = CoreHelper::getDateHelper()->now();
                if( $currentVO->getExpiredDate() > CoreHelper::getDateHelper()->now() )
                {
                    $this->getCoreRequestInfoDatabaseModel()->setBlockingVO( $currentVO );
                    return;
                }
            }
            else
            {
                $currentVO = new RequestInfoCoreVO();
            }
        }
        else
        {
            if( $currentVO->getRandomID() != $requiredMatchID )
            {
                $this->getCoreRequestInfoDatabaseModel()->setBlockingVO( $currentVO );
                return;
            }
        }
        $randomID = CoreHelper::getIdHelper()->randomID();
        $currentVO->setRandomID( $randomID );
        $currentVO->setExpiredDate( CoreHelper::getDateHelper()->now() + $timeout );

        if( $currentVO->getRef() )
        {
            $this->getRequestInfoRequest()->update( array( $currentVO->getRef() => $currentVO ) );
        }
        else
        {
            $vos = $this->getRequestInfoRequest()->insert( array( $currentVO ), true );
            $currentVO = CoreHelper::getArrayHelper()->firstObject( $vos );
            /* @var $currentVO RequestInfoCoreVO */
        }
        $this->getCoreRequestInfoDatabaseModel()->setRequestInfoVO( $currentVO );
    }

    public function releaseInfo()
    {
        $currentVO = $this->getCoreRequestInfoDatabaseModel()->getRequestInfoVO();
        $currentVO->setExpiredDate( CoreHelper::getDateHelper()->now() );
        $this->getRequestInfoRequest()->update( array( $currentVO->getRef() => $currentVO ) );
    }

    /**
     * @return RequestInfoDatabaseCoreModel
     */
    private function getCoreRequestInfoDatabaseModel()
    {
        if( !$this->coreRequestInfoDatabaseModel )
        {
            $this->coreRequestInfoDatabaseModel = $this->getCoreRequestInfoDatabase()->getRequestInfoModel();
        }
        return $this->coreRequestInfoDatabaseModel;
    }

    /**
     * @return RequestInfoDatabaseCoreRequest
     */
    private function getRequestInfoRequest()
    {
        if( !$this->requestInfoRequest )
        {
            $this->requestInfoRequest = $this->getCoreRequestInfoDatabase()->getRequestInfoRequest();
        }
        return $this->requestInfoRequest;
    }
} 