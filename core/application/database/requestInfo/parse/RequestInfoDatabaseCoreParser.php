<?php


class RequestInfoDatabaseCoreParser extends CoreBase
{

    /**
     * @param array $entries
     * @return array
     */
    public function byRef( array $entries )
    {
        $vos = array();
        foreach( $entries as $entry )
        {
            /* @var $entry array */
            $vo = $this->parseEntry( $entry );
            $vos[ $vo->getRef() ] = $vo;
        }

        return $vos;
    }

    /**
     * @param array $data
     * @param RequestInfoCoreVO $vo
     * @return RequestInfoCoreVO
     */
    public function parseEntry( array $data, RequestInfoCoreVO $vo = null )
    {
        if( !$vo )
        {
            $vo = new RequestInfoCoreVO();
        }

        $expiredDateString = CoreHelper::getArrayHelper()->stringFromDictionary( $data, SqlDatabaseCoreConstants::$EXPIRED_DATE );
        $ref = CoreHelper::getArrayHelper()->intFromDictionary( $data, SqlDatabaseCoreConstants::$REF, 0, true );
        $randomID = CoreHelper::getArrayHelper()->stringFromDictionary( $data, SqlDatabaseCoreConstants::$RANDOM_ID );

        $expiredDate = CoreHelper::getDateHelper()->unixFromString( $expiredDateString );

        $expiredDate = $expiredDate >= CoreHelper::getDateHelper()->minimumDate() ? $expiredDate : CoreHelper::getDateHelper()->unixDay0();

        $vo->setRef( $ref );
        $vo->setRandomID( $randomID );
        $vo->setExpiredDate( $expiredDate );

        return $vo;
    }
    
}