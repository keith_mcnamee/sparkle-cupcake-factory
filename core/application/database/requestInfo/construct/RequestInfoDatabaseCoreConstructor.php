<?php


class RequestInfoDatabaseCoreConstructor extends CoreBase
{

    /**
     * @var ConditionDatabaseConstructor
     */
    private $conditionConstructor;

    public function __destruct()
    {
        $this->conditionConstructor = null;

        parent::__destruct();
    }

    /**
     * @param array $generalInfoRefs
     * @return SelectRequestDatabaseCoreVO
     */
    public function select( array $generalInfoRefs = null )
    {
        $selectVO = new SelectRequestDatabaseCoreVO( SqlDatabaseCoreConstants::$REQUEST_INFO_TABLE );

        $selectVO->addColumn( SqlDatabaseCoreConstants::$REF );
        $selectVO->addColumn( SqlDatabaseCoreConstants::$RANDOM_ID );
        $selectVO->addColumn( SqlDatabaseCoreConstants::$EXPIRED_DATE );

        $selectVO->addOrderBy( SqlDatabaseCoreConstants::$EXPIRED_DATE, true );
        $selectVO->addLimit( 0, 1 );

        if( $generalInfoRefs )
        {
            $selectVO->addCondition( $this->getConditionConstructor()->inUnknowns( SqlDatabaseCoreConstants::$REF, count( $generalInfoRefs ) ));

            foreach( $generalInfoRefs as $generalInfoRef => $nullValue )
            {
                /* @var $nullValue null */
                $selectVO->addIntValueToCondition( $generalInfoRef );
            }
        }

        return $selectVO;
    }

    /**
     * @param array $generalInfoVOs
     * @return InsertRequestDatabaseCoreVO
     */
    public function insert( array $generalInfoVOs )
    {
        $insertVO = new InsertRequestDatabaseCoreVO( SqlDatabaseCoreConstants::$REQUEST_INFO_TABLE, SqlDatabaseCoreConstants::$REF );

        foreach( $generalInfoVOs as $generalInfoVO )
        {
            /* @var $generalInfoVO RequestInfoCoreVO */

            $insertVO->addStringColumnValue( SqlDatabaseCoreConstants::$RANDOM_ID, $generalInfoVO->getRandomID() );
            $insertVO->addStringColumnValue( SqlDatabaseCoreConstants::$EXPIRED_DATE, CoreHelper::getDateHelper()->stringFromUnix( $generalInfoVO->getExpiredDate() ) );
            $insertVO->addStringColumnValue( SqlDatabaseCoreConstants::$UPDATED, CoreHelper::getDateHelper()->nowString() );
            $insertVO->addStringColumnValue( SqlDatabaseCoreConstants::$CREATED, CoreHelper::getDateHelper()->nowString() );
        }

        return $insertVO;
    }

    /**
     * @param RequestInfoCoreVO $generalInfoVO
     * @return UpdateRequestDatabaseCoreVO
     */
    public function update( RequestInfoCoreVO $generalInfoVO )
    {
        $updateVO = new UpdateRequestDatabaseCoreVO( SqlDatabaseCoreConstants::$REQUEST_INFO_TABLE );

        $updateVO->addStringColumnValue( SqlDatabaseCoreConstants::$RANDOM_ID, $generalInfoVO->getRandomID() );
        $updateVO->addStringColumnValue( SqlDatabaseCoreConstants::$EXPIRED_DATE, CoreHelper::getDateHelper()->stringFromUnix( $generalInfoVO->getExpiredDate() ) );
        $updateVO->addStringColumnValue( SqlDatabaseCoreConstants::$UPDATED, CoreHelper::getDateHelper()->nowString() );

        $condition = $this->getConditionConstructor()->equalsUnknown( SqlDatabaseCoreConstants::$REF );
        $updateVO->addCondition( $condition );
        $updateVO->addStringValueToCondition( $generalInfoVO->getRef() );

        return $updateVO;
    }

    /**
     * @return ConditionDatabaseConstructor
     */
    private function getConditionConstructor()
    {
        if( !$this->conditionConstructor )
        {
            $this->conditionConstructor = $this->getCoreDatabase()->getConditionConstructor();
        }
        return $this->conditionConstructor;
    }
}