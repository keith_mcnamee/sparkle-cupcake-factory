<?php


class RequestInfoDatabaseCoreModel extends CoreBase
{
    /**
     * @var RequestInfoCoreVO
     */
    private $requestInfoVO;

    /**
     * @var RequestInfoCoreVO
     */
    private $blockingVO;

    /**
     * @return RequestInfoCoreVO
     */
    public function getBlockingVO()
    {
        return $this->blockingVO;
    }

    /**
     * @param RequestInfoCoreVO $value
     */
    public function setBlockingVO( $value )
    {
        $this->blockingVO = $value;
    }

    /**
     * @return RequestInfoCoreVO
     */
    public function getRequestInfoVO()
    {
        return $this->requestInfoVO;
    }

    /**
     * @param RequestInfoCoreVO $value
     */
    public function setRequestInfoVO( $value )
    {
        $this->requestInfoVO = $value;
    }
} 