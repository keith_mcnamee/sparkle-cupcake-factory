<?php


class RequestInfoCoreVO extends ReferenceVO
{
    /**
     * @var string
     */
    private $randomID;

    /**
     * @var int
     */
    private $expiredDate;

    /**
     * @return string
     */
    public function getRandomID()
    {
        return $this->randomID;
    }

    /**
     * @param string $value
     */
    public function setRandomID( $value )
    {
        $this->randomID = $value;
    }

    /**
     * @return int
     */
    public function getExpiredDate()
    {
        return $this->expiredDate;
    }

    /**
     * @param int $value
     */
    public function setExpiredDate( $value )
    {
        $this->expiredDate = $value;
    }
} 