<?php


class LocalizationDatabaseCoreConstructor extends CoreBase
{

    /**
     * @var ConditionDatabaseConstructor
     */
    private $conditionConstructor;

    public function __destruct()
    {
        $this->conditionConstructor = null;

        parent::__destruct();
    }

    /**
     * @return SelectRequestDatabaseCoreVO
     */
    public function select()
    {
        $selectVO = new SelectRequestDatabaseCoreVO( SqlDatabaseCoreConstants::$LOCALIZATION_FILE_INFO_TABLE );

        $selectVO->addColumn( SqlDatabaseCoreConstants::$REF );
        $selectVO->addColumn( SqlDatabaseCoreConstants::$VERSION_INFO );
        $selectVO->addColumn( SqlDatabaseCoreConstants::$FILE_LOCATION_INFO );
        $selectVO->addColumn( SqlDatabaseCoreConstants::$LANGUAGE_INFO );
        $selectVO->addColumn( SqlDatabaseCoreConstants::$LAST_MODIFIED_DATE );
        $selectVO->addColumn( SqlDatabaseCoreConstants::$FILE_SIZE );

        return $selectVO;
    }
    /**
     * @param array $fileInfoVOs
     * @return InsertRequestDatabaseCoreVO
     */
    public function insert( $fileInfoVOs )
    {
        $insertVO = new InsertRequestDatabaseCoreVO( SqlDatabaseCoreConstants::$LOCALIZATION_FILE_INFO_TABLE, SqlDatabaseCoreConstants::$REF );

        foreach( $fileInfoVOs as $fileInfoVO )
        {
            /* @var $fileInfoVO FileInfoLocalizationCoreVO */
            $this->addValues( $insertVO, $fileInfoVO->getVersion(), $fileInfoVO->getFileLocation(), $fileInfoVO->getLanguage(), $fileInfoVO->getLastModifiedDate(), $fileInfoVO->getFileSize() );
        }

        return $insertVO;
    }

    /**
     * @param InsertRequestDatabaseCoreVO $insertVO
     * @param string $version
     * @param string $fileLocation
     * @param string $language
     * @param int $lastModifiedDate
     * @param int $fileSize
     */
    public function addValues( InsertRequestDatabaseCoreVO $insertVO, $version, $fileLocation,  $language, $lastModifiedDate, $fileSize )
    {
        $insertVO->addStringColumnValue( SqlDatabaseCoreConstants::$VERSION_INFO, $version );
        $insertVO->addStringColumnValue( SqlDatabaseCoreConstants::$FILE_LOCATION_INFO, $fileLocation );
        $insertVO->addStringColumnValue( SqlDatabaseCoreConstants::$LANGUAGE_INFO, $language );
        $insertVO->addStringColumnValue( SqlDatabaseCoreConstants::$LAST_MODIFIED_DATE, CoreHelper::getDateHelper()->stringFromUnix( $lastModifiedDate ) );
        $insertVO->addStringColumnValue( SqlDatabaseCoreConstants::$FILE_SIZE, $fileSize );
        $insertVO->addStringColumnValue( SqlDatabaseCoreConstants::$UPDATED, CoreHelper::getDateHelper()->nowString() );
        $insertVO->addStringColumnValue( SqlDatabaseCoreConstants::$CREATED, CoreHelper::getDateHelper()->nowString() );
    }

    /**
     * @param FileInfoLocalizationCoreVO $fileInfoVO
     * @return UpdateRequestDatabaseCoreVO
     */
    public function update( $fileInfoVO )
    {
        $updateVO = new UpdateRequestDatabaseCoreVO( SqlDatabaseCoreConstants::$LOCALIZATION_FILE_INFO_TABLE );

        $updateVO->addStringColumnValue( SqlDatabaseCoreConstants::$VERSION_INFO, $fileInfoVO->getVersion() );
        $updateVO->addStringColumnValue( SqlDatabaseCoreConstants::$FILE_LOCATION_INFO, $fileInfoVO->getFileLocation() );
        $updateVO->addStringColumnValue( SqlDatabaseCoreConstants::$LAST_MODIFIED_DATE, CoreHelper::getDateHelper()->stringFromUnix( $fileInfoVO->getLastModifiedDate() ) );
        $updateVO->addStringColumnValue( SqlDatabaseCoreConstants::$FILE_SIZE, $fileInfoVO->getFileSize() );
        $updateVO->addStringColumnValue( SqlDatabaseCoreConstants::$UPDATED, CoreHelper::getDateHelper()->nowString() );

        $condition = $this->getConditionConstructor()->equalsUnknown( SqlDatabaseCoreConstants::$REF );
        $updateVO->addCondition( $condition );
        $updateVO->addStringValueToCondition( $fileInfoVO->getRef() );

        return $updateVO;
    }

    /**
     * @return ConditionDatabaseConstructor
     */
    private function getConditionConstructor()
    {
        if( !$this->conditionConstructor )
        {
            $this->conditionConstructor = $this->getCoreDatabase()->getConditionConstructor();
        }
        return $this->conditionConstructor;
    }
} 