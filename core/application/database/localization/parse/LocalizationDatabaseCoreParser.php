<?php


class LocalizationDatabaseCoreParser extends CoreBase
{

    /**
     * @param array $entries
     * @return FileInfoLocalizationCoreVO
     */
    public function fileInfos( array $entries )
    {
        $vos = array();
        foreach( $entries as $entry )
        {
            /* @var $entry array */
            $vo = $this->fileInfo( $entry );
            if( !isset( $vos[ $vo->getLanguage() ] ) )
            {
                $vos[ $vo->getLanguage() ] = array();
            }
            $vos[ $vo->getLanguage() ][ $vo->getRef() ] = $vo;
        }

        return $vos;
    }

    /**
     * @param array $entries
     * @return FileInfoLocalizationCoreVO
     */
    public function unfilteredFileInfos( array $entries )
    {
        $vos = array();
        foreach( $entries as $entry )
        {
            /* @var $entry array */
            $vo = $this->fileInfo( $entry );
            $vos[] = $vo;
        }

        return $vos;
    }

    /**
     * @param array $data
     * @param FileInfoLocalizationCoreVO $vo
     * @return FileInfoLocalizationCoreVO
     */
    public function fileInfo( array $data, FileInfoLocalizationCoreVO $vo = null )
    {
        if( !$vo )
        {
            $vo = new FileInfoLocalizationCoreVO();
        }

        $lastModifiedDateString = CoreHelper::getArrayHelper()->stringFromDictionary( $data, SqlDatabaseCoreConstants::$LAST_MODIFIED_DATE, true );
        $ref = CoreHelper::getArrayHelper()->intFromDictionary( $data, SqlDatabaseCoreConstants::$REF, 0, true );
        $version = CoreHelper::getArrayHelper()->stringFromDictionary( $data, SqlDatabaseCoreConstants::$VERSION_INFO, true );
        $fileLocation = CoreHelper::getArrayHelper()->stringFromDictionary( $data, SqlDatabaseCoreConstants::$FILE_LOCATION_INFO, true );
        $language = CoreHelper::getArrayHelper()->stringFromDictionary( $data, SqlDatabaseCoreConstants::$LANGUAGE_INFO, true );
        $fileSize = CoreHelper::getArrayHelper()->intFromDictionary( $data, SqlDatabaseCoreConstants::$FILE_SIZE, 0, true );
        $lastModifiedDate = CoreHelper::getDateHelper()->unixFromString( $lastModifiedDateString );

        $vo->setRef( $ref );
        $vo->setVersion( $version );
        $vo->setFileLocation( $fileLocation );
        $vo->setLanguage( $language );
        $vo->setLastModifiedDate( $lastModifiedDate );
        $vo->setFileSize( $fileSize );

        return $vo;
    }
} 