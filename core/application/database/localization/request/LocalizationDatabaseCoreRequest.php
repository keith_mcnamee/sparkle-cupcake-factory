<?php


class LocalizationDatabaseCoreRequest extends CoreBase
{

    /**
     * @var BasicDatabaseRequest
     */
    private $basicDatabaseRequest;

    /**
     * @var ConditionDatabaseConstructor
     */
    private $conditionConstructor;

    /**
     * @var LocalizationDatabaseCoreConstructor
     */
    private $localizationConstructor;

    /**
     * @var LocalizationDatabaseCoreParser
     */
    private $localizationParser;

    public function __destruct()
    {
        $this->basicDatabaseRequest = null;
        $this->conditionConstructor = null;
        $this->localizationConstructor = null;
        $this->localizationParser = null;

        parent::__destruct();
    }

    /**
     * @param string $localizationType
     * @return FileInfoLocalizationCoreVO
     */
    public function fileInfoRequestType( $localizationType )
    {
        $selectVO = $this->getLocalizationConstructor()->select();

        $condition = $this->getConditionConstructor()->equalsUnknown( SqlDatabaseCoreConstants::$LANGUAGE_INFO );
        $selectVO->addCondition( $condition );
        $selectVO->addStringValueToCondition( $localizationType );

        $selectVO->addOrderBy( SqlDatabaseCoreConstants::$VERSION_INFO, true );
        $selectVO->addOrderBy( SqlDatabaseCoreConstants::$CREATED, true );
        $selectVO->addLimit( 0, 1 );

        $languageVOs = $this->fileInfosRequest( $selectVO );

        if( !isset( $languageVOs[ $localizationType ] ) )
        {
            return null;
        }

        $versionVOs = $languageVOs[ $localizationType ];
        $versionVO = CoreHelper::getArrayHelper()->firstObject( $versionVOs );
        /* @var $versionVO FileInfoLocalizationCoreVO */

        return $versionVO;
    }

    /**
     * @return array
     */
    public function allFileInfosRequest()
    {
        $selectVO = $this->getLocalizationConstructor()->select();

        $selectVO->addOrderBy( SqlDatabaseCoreConstants::$VERSION_INFO, true );
        $selectVO->addOrderBy( SqlDatabaseCoreConstants::$CREATED, true );

        $vos = $this->fileInfosRequest( $selectVO, true );

        return $vos;
    }

    /**
     * @param SelectRequestDatabaseCoreVO $selectVO
     * @param boolean $keepSorting
     * @return array
     */
    public function fileInfosRequest( SelectRequestDatabaseCoreVO $selectVO = null, $keepSorting = false )
    {
        if( !$selectVO )
        {
            $selectVO = $this->getLocalizationConstructor()->select();
        }
        $entries = $this->getBasicDatabaseRequest()->select( $selectVO );

        if( !$keepSorting )
        {
            $vos = $this->getLocalizationParser()->fileInfos( $entries );
        }
        else
        {
            $vos = $this->getLocalizationParser()->unfilteredFileInfos( $entries );
        }
        return $vos;
    }

    /**
     * @return BasicDatabaseRequest
     */
    private function getBasicDatabaseRequest()
    {
        if( !$this->basicDatabaseRequest )
        {
            $this->basicDatabaseRequest = $this->getCoreDatabase()->getBasicRequest();
        }
        return $this->basicDatabaseRequest;
    }

    /**
     * @return ConditionDatabaseConstructor
     */
    private function getConditionConstructor()
    {
        if( !$this->conditionConstructor )
        {
            $this->conditionConstructor = $this->getCoreDatabase()->getConditionConstructor();
        }
        return $this->conditionConstructor;
    }

    /**
     * @return LocalizationDatabaseCoreConstructor
     */
    private function getLocalizationConstructor()
    {
        if( !$this->localizationConstructor )
        {
            $this->localizationConstructor = $this->getCoreLocalizationDatabase()->getLocalizationConstructor();
        }
        return $this->localizationConstructor;
    }

    /**
     * @return LocalizationDatabaseCoreParser
     */
    private function getLocalizationParser()
    {
        if( !$this->localizationParser )
        {
            $this->localizationParser = $this->getCoreLocalizationDatabase()->getLocalizationParser();
        }
        return $this->localizationParser;
    }
} 