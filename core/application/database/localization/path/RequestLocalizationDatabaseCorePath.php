<?php


class RequestLocalizationDatabaseCorePath
{

    /**
     * @var LocalizationDatabaseCoreRequest
     */
    private $request;

    public function __destruct()
    {
        $this->request = null;
    }

    /**
     * @param $singleInstance
     * @return LocalizationDatabaseCoreRequest
     */
    private function createRequest( $singleInstance )
    {
        $instance = new LocalizationDatabaseCoreRequest();
        $instance->init();
        if( !$singleInstance )
        {
            $this->request = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return LocalizationDatabaseCoreRequest
     */
    public function getRequest( $singleInstance = true )
    {
        return $this->request && !$singleInstance ? $this->request : $this->createRequest( $singleInstance );
    }
}