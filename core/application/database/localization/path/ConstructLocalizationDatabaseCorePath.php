<?php


class ConstructLocalizationDatabaseCorePath
{

    /**
     * @var LocalizationDatabaseCoreConstructor
     */
    private $construct;

    public function __destruct()
    {
        $this->construct = null;
    }

    /**
     * @param $singleInstance
     * @return LocalizationDatabaseCoreConstructor
     */
    private function createConstruct( $singleInstance )
    {
        $instance = new LocalizationDatabaseCoreConstructor();
        $instance->init();
        if( !$singleInstance )
        {
            $this->construct = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return LocalizationDatabaseCoreConstructor
     */
    public function getConstruct( $singleInstance = true )
    {
        return $this->construct && !$singleInstance ? $this->construct : $this->createConstruct( $singleInstance );
    }
}