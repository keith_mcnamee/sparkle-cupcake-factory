<?php


class ParseLocalizationDatabaseCorePath
{

    /**
     * @var LocalizationDatabaseCoreParser
     */
    private $parse;

    public function __destruct()
    {
        $this->parse = null;
    }

    /**
     * @param $singleInstance
     * @return LocalizationDatabaseCoreParser
     */
    private function createParse( $singleInstance )
    {
        $instance = new LocalizationDatabaseCoreParser();
        $instance->init();
        if( !$singleInstance )
        {
            $this->parse = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return LocalizationDatabaseCoreParser
     */
    public function getParse( $singleInstance = true )
    {
        return $this->parse && !$singleInstance ? $this->parse : $this->createParse( $singleInstance );
    }
}