<?php

require_once( DIR_CORE . "/application/database/localization/construct/LocalizationDatabaseCoreConstructor.php" );

require_once( DIR_CORE . "/application/database/localization/parse/LocalizationDatabaseCoreParser.php" );

require_once( DIR_CORE . "/application/database/localization/request/LocalizationDatabaseCoreRequest.php" );
 