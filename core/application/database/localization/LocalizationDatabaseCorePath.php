<?php


class LocalizationDatabaseCorePath extends CoreBase
{

    public function __construct()
    {
        $this->getCoreLocalization();
        $this->getIncludes();
    }

    private function getIncludes()
    {
        require_once( DIR_CORE . "/application/database/localization/includes/LocalizationDatabaseCoreIncludes.php" );
    }

    /**
     * @return LocalizationDatabaseCoreConstructor
     */
    public function getLocalizationConstructor()
    {
        return new LocalizationDatabaseCoreConstructor();
    }

    /**
     * @return LocalizationDatabaseCoreParser
     */
    public function getLocalizationParser()
    {
        return new LocalizationDatabaseCoreParser();
    }

    /**
     * @return LocalizationDatabaseCoreRequest
     */
    public function getLocalizationRequest()
    {
        return new LocalizationDatabaseCoreRequest();
    }
}