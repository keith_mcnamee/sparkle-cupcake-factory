<?php


class ReadExternalCorePath
{

    public function __construct()
    {
        $this->getIncludes();
    }

    private function getIncludes()
    {
        require_once( DIR_CORE . "/application/readExternal/includes/ReadExternalCoreIncludes.php" );
    }

    /**
     * @return ReadExternalCoreParser
     */
    public function getReadExternalParser()
    {
        return new ReadExternalCoreParser();
    }
}