<?php


class ParseReadExternalCorePath
{

    /**
     * @var ReadExternalCoreParser
     */
    private $parse;

    public function __destruct()
    {
        $this->parse = null;
    }

    /**
     * @param $singleInstance
     * @return ReadExternalCoreParser
     */
    private function createParse( $singleInstance )
    {
        $instance = new ReadExternalCoreParser();
        $instance->init();
        if( !$singleInstance )
        {
            $this->parse = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return ReadExternalCoreParser
     */
    public function getParse( $singleInstance = true )
    {
        return $this->parse && !$singleInstance ? $this->parse : $this->createParse( $singleInstance );
    }
}