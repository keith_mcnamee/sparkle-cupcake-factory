<?php


class ReadExternalCoreParser extends CoreBase
{

    /**
     * @param DOMElement $element
     * @param string $attributeName
     * @param boolean $sanitize
     * @return string
     */
    public function textFromAttribute( DOMElement $element, $attributeName = null, $sanitize = false )
    {
        if( !$attributeName )
        {
            $attributeName = ReadExternalCoreConstants::$VALUE_ATTRIBUTE;
        }
        $value = $element->getAttribute( $attributeName );

        if( $value && $sanitize )
        {
            $value = CoreHelper::getStringHelper()->sanitizeInput( $value );
        }

        return $value;
    }

    /**
     * @param DOMElement $element
     * @param boolean $sanitize
     * @return string
     */
    public function textFromElement( DOMElement $element, $sanitize = false )
    {
        if ( !$element )
        {
            return null;
        }
        $innerHTML = '';
        $children = $element->childNodes;
        foreach ( $children as $child )
        {
            /* @var $child DOMNode */
            $innerHTML .= trim( $child->ownerDocument->saveXML( $child ) );
        }
        $fixedValues = array_map( 'strip_tags', array_map( 'trim', explode( "<br/>", trim( $innerHTML ) ) ) );
        foreach ( $fixedValues as $value )
        {
            /* @var $value string */
            if ( empty( $value ) )
            {
                continue;
            }

            $value = str_replace( array ( '! ' ), '', $value );

            if( $sanitize )
            {
                $value = CoreHelper::getStringHelper()->sanitizeInput( $value );
            }
            $value = str_replace("\n", "", $value);
            $value = trim( $value );

            return $value;
        }

        return null;
    }

    /**
     * @param DOMElement $element
     * @param boolean $sanitize
     * @return array
     */
    public function arrayFromColumns( DOMElement $element, $sanitize = false )
    {
        $columns = array();
        foreach ( $element->getElementsByTagName( ReadExternalCoreConstants::$TD_ELEMENT ) as $td )
        {
            $value = $this->textFromElement( $td, $sanitize );
            if ( !$value )
            {
                continue;
            }
            $columns[] = $value;
        }
        return $columns;
    }
} 