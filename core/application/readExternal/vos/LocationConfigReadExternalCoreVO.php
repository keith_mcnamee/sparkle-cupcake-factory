<?php


class LocationConfigReadExternalCoreVO
{
    /**
     * @var string
     */
    private $resourceURL;


    public function __construct()
    {

    }

    /**
     * @return string
     */
    public function getResourceURL()
    {
        return $this->resourceURL;
    }

    /**
     * @param string $value
     */
    public function setResourceURL( $value )
    {
        $this->resourceURL = $value;
    }
} 