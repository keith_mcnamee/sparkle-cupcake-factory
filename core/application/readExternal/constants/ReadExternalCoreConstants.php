<?php


class ReadExternalCoreConstants
{
    public static $SELECT_ELEMENT = "select";
    public static $OPTION_ELEMENT = "option";
    public static $TABLE_ELEMENT = "table";
    public static $TBODY_ELEMENT = "tbody";
    public static $TR_ELEMENT = "tr";
    public static $TD_ELEMENT = "td";
    public static $A_ELEMENT = "a";

    public static $NAME_ATTRIBUTE = "name";
    public static $VALUE_ATTRIBUTE = "value";
    public static $CLASS_ATTRIBUTE = "class";

} 