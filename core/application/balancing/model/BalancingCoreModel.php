<?php


class BalancingCoreModel extends CoreBase
{

    /**
     * @var FileInfoBalancingCoreVO
     */
    private $fileInfoVO;

    /**
     * @var boolean
     */
    private $enabled;

    /**
     * @var string
     */
    private $processFailMessage;

    /**
     * @var boolean
     */
    private $contentChanged;

    /**
     * @return FileInfoBalancingCoreVO
     */
    public function getFileInfoVO()
    {
        return $this->fileInfoVO;
    }

    /**
     * @param FileInfoBalancingCoreVO $value
     */
    public function setFileInfoVO( FileInfoBalancingCoreVO $value = null )
    {
        $this->fileInfoVO = $value;
    }

    /**
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param boolean $value
     */
    public function setEnabled( $value )
    {
        $this->enabled = $value;
    }

    /**
     * @return string
     */
    public function getProcessFailMessage()
    {
        return $this->processFailMessage;
    }

    /**
     * @return boolean
     */
    public function isContentChanged()
    {
        return $this->contentChanged;
    }

    /**
     * @param boolean $value
     */
    public function setContentChanged( $value )
    {
        $this->contentChanged = $value;
    }

} 