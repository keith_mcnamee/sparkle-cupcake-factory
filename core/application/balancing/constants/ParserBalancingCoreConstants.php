<?php


class ParserBalancingCoreConstants
{
    public static $ITEM_ELEMENT = "item";

    public static $CATEGORY_ATTRIBUTE = "category";
    public static $TYPE_ATTRIBUTE = "type";

    public static $DEFAULT_TYPE = "default";
}