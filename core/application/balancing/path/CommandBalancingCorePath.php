<?php


class CommandBalancingCorePath
{

    /**
     * @var InitializeBalancingCoreCommand
     */
    private $initialize;

    /**
     * @var InsertBalancingCoreCommand
     */
    private $insert;

    public function __destruct()
    {
        $this->initialize = null;
        $this->insert = null;
    }

    /**
     * @param $singleInstance
     * @return InitializeBalancingCoreCommand
     */
    private function createInitialize( $singleInstance )
    {
        $instance = new InitializeBalancingCoreCommand();
        $instance->init();
        if( !$singleInstance )
        {
            $this->initialize = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return InsertBalancingCoreCommand
     */
    private function createInsert( $singleInstance )
    {
        $instance = new InsertBalancingCoreCommand();
        $instance->init();
        if( !$singleInstance )
        {
            $this->insert = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return InitializeBalancingCoreCommand
     */
    public function getInitialize( $singleInstance = true )
    {
        return $this->initialize && !$singleInstance ? $this->initialize : $this->createInitialize( $singleInstance );
    }

    /**
     * @param $singleInstance
     * @return InsertBalancingCoreCommand
     */
    public function getInsert( $singleInstance = true )
    {
        return $this->insert && !$singleInstance ? $this->insert : $this->createInsert( $singleInstance );
    }
}