<?php


class FileInfoBalancingCoreVO extends ReferenceVO
{
    /**
     * @var string
     */
    private $version;

    /**
     * @var string
     */
    private $fileLocation;

    /**
     * @var int
     */
    private $lastModifiedDate;

    /**
     * @var int
     */
    private $fileSize;

    /**
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param string $value
     */
    public function setVersion( $value )
    {
        $this->version = $value;
    }

    /**
     * @return string
     */
    public function getFileLocation()
    {
        return $this->fileLocation;
    }

    /**
     * @param string $value
     */
    public function setFileLocation( $value )
    {
        $this->fileLocation = $value;
    }

    /**
     * @return int
     */
    public function getLastModifiedDate ()
    {
        return $this->lastModifiedDate;
    }

    /**
     * @param int $value
     */
    public function setLastModifiedDate ( $value )
    {
        $this->lastModifiedDate = $value;
    }

    /**
     * @return int
     */
    public function getFileSize ()
    {
        return $this->fileSize;
    }

    /**
     * @param int $value
     */
    public function setFileSize ( $value )
    {
        $this->fileSize = $value;
    }
} 