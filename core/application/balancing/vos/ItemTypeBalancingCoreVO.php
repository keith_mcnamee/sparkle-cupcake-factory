<?php


class ItemTypeBalancingCoreVO
{
    /**
     * @var string
     */
    private $category;

    /**
     * @var string
     */
    private $type;

    /**
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param string $value
     */
    public function setCategory( $value )
    {
        $this->category = $value;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $value
     */
    public function setType( $value )
    {
        $this->type = $value;
    }
} 