<?php


class InitializeBalancingCoreCommand extends CoreBase
{

    /**
     * @var BalancingCoreModel
     */
    private $coreBalancingModel;

    public function init()
    {
        parent::init();

        $this->coreBalancingModel = $this->getCoreBalancing()->getModel()->getModel();
    }

    public function __destruct()
    {
        $this->coreBalancingModel = null;

        parent::__destruct();
    }

    public function command()
    {
        $fileInfoVO = $this->getCoreBalancingDatabase()->getRequest()->getRequest()->fileInfoRequest();

        $this->coreBalancingModel->setFileInfoVO( $fileInfoVO );
    }
} 