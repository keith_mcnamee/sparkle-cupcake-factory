<?php


class InsertBalancingCoreCommand extends CoreBase
{

    /**
     * @param FileInfoBalancingCoreVO $configVO
     */
    public function command( FileInfoBalancingCoreVO $configVO )
    {

        if( empty( $configVO ) )
        {
            $this->getDebugCommand()->trace( DebugViewText::$NO_LOCALIZATION_TO_UPDATE, true );
            return;
        }
        $coreBalancingModel = $this->getCoreBalancing()->getModel()->getModel();

        $currentVO = $coreBalancingModel->getFileInfoVO();

        $version = $currentVO ? $currentVO->getVersion() : 0;
        $configVersion = (float)CoreHelper::getVersionService()->validVersion( $version );
        $currentVersion = $currentVO ? (float)CoreHelper::getVersionService()->validVersion( $version ) : 0;

        if( $configVersion && $configVersion < $currentVersion )
        {
            $coreBalancingModel->setProcessFailMessage( DebugViewText::$TOO_LOW_BALANCING_VERSION );
            return;
        }
        $versionChanged = $configVersion > $currentVersion;

        if ( !CoreHelper::getFileService()->fileContentExists( $configVO->getFileLocation() ) )
        {
            $coreBalancingModel->setProcessFailMessage( DebugViewText::$INVALID_BALANCING_CONTENT );
            return;
        }

        $lastModifiedDate = CoreHelper::getFileService()->lastModifiedDate( $configVO->getFileLocation() );
        $fileSize = CoreHelper::getFileService()->fileSize( $configVO->getFileLocation() );
        $contentChanged = !$currentVO || $lastModifiedDate != $currentVO->getLastModifiedDate() || $fileSize != $currentVO->getFileSize();

        $locationChanged = !$currentVO || CoreHelper::getStringService()->stripSanitization( $configVO->getFileLocation() ) != CoreHelper::getStringService()->stripSanitization( $currentVO->getFileLocation() );

        $versionChanged = true;//TODO Delete this. For testing only
        if( ! ( $versionChanged  || $contentChanged  || $locationChanged ) )
        {
            $this->getDebugCommand()->trace( DebugViewText::$BALANCING_EXISTS, true );
            return;
        }

        $coreBalancingModel->setContentChanged( true );

        if( $versionChanged )
        {
            $version = CoreHelper::getVersionService()->validVersion( $configVersion );
        }
        else
        {
            $version = CoreHelper::getVersionService()->nextVersion( $currentVersion );
        }

        $coreDatabaseModel = $this->getCoreDatabase()->getModel()->getModel();
        $balancingDatabaseConstructor = $this->getCoreBalancingDatabase()->getConstruct()->getConstruct();
        if( !$locationChanged )
        {
            $currentVO->setVersion( $version );
            $currentVO->setFileLocation( $configVO->getFileLocation() );
            $currentVO->setLastModifiedDate( $lastModifiedDate );
            $currentVO->setFileSize( $fileSize );
            $updateVO = $balancingDatabaseConstructor->update( $currentVO );
            $coreDatabaseModel->getQueueVO()->addUpdateVO( $updateVO );
            return;
        }
        $configVO->setVersion( $version );
        $configVO->setLastModifiedDate( $lastModifiedDate );
        $configVO->setFileSize( $fileSize );
        $insertVO = $balancingDatabaseConstructor->insert( $configVO );
        $coreDatabaseModel->getQueueVO()->addInsertVO( $insertVO );
    }
}