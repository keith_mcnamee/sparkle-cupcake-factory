<?php


class BalancingCorePath
{

    public function __construct()
    {
        $this->getIncludes();
    }

    private function getIncludes()
    {
        require_once( DIR_CORE . "/application/balancing/includes/BalancingCoreIncludes.php" );
    }

    /**
     * @var BalancingCoreModel
     */
    private $balancingModel;

    public function __destruct()
    {
        $this->balancingModel = null;
    }

    /**
     * @return BalancingCoreModel
     */
    public function getBalancingModel()
    {
        if( !$this->balancingModel )
        {
            $this->balancingModel = new BalancingCoreModel();
        }
        return $this->balancingModel;
    }
}