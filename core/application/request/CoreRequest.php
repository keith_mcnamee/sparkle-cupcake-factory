<?php


class CoreRequest 
{

    /**
     * @var RequestCoreHelper
     */
    private static $helper;

    /**
     * @return RequestCorePath
     */
    private static function requestPath()
    {
        return CorePath::getInstance()->getApplication()->getRequest();
    }

    /**
     * @return RequestCoreHelper
     */
    public static function getHelper()
    {
        if( !self::$helper )
        {
            self::$helper = self::requestPath()->getRequestHelper();
        }
        return self::$helper;
    }
    
}