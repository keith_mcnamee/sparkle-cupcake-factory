<?php


class RedirectHeadersRequestCoreCommand extends CoreBase
{

    /**
     * @param string $toLocation
     */
    public function command( $toLocation )
    {
        $queryString = CoreRequest::getService()->rewriteQueryString();
        $postParams = $_POST;
        if( $queryString )
        {
            $toLocation .= "?".$queryString;
        }
        $sendRequestCommand = $this->getCoreRequest()->getCommand()->getSend();
        if( $postParams && !empty( $postParams ) )
        {
            $sendRequestCommand->postRedirectView( $toLocation, $postParams );
        }
        else
        {
            $sendRequestCommand->redirectView( $toLocation );
        }
    }

    /**
     * @param string $path
     */
    public function internalLocation( $path )
    {
        $currentUrl = CoreRequest::getService()->baseURL();
        $redirectTo = $currentUrl.$path;
        $this->command( $redirectTo );
    }
}