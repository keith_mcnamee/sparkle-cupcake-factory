<?php


class ProcessPostRequestCoreCommand extends CoreBase
{
    /**
     * @param string $name
     * @param boolean $discrete
     * @param array $params
     * @param boolean $allowTesting
     * @param string $requestParam
     */
    public function command( $name, $discrete, $params = null, $allowTesting = false, $requestParam = null )
    {
        $currentUrl = CoreRequest::getService()->baseURL();
        $postTo = $currentUrl.$name."/";

        $infoAppService = $this->getCoreApp()->getService()->getInfo();
        $debug = $allowTesting && ( $infoAppService->isTestServer() || $infoAppService->isStagingDirectory() );

        $sendRequestCommand = $this->getCoreRequest()->getCommand()->getSend();
        if( $debug )
        {
            $sendRequestCommand->postRedirectViewAsJson( $postTo, $params, $requestParam );
        }
        else if( $discrete )
        {
            $sendRequestCommand->postDiscrete( $postTo, $params );
        }
        else
        {
            $sendRequestCommand->postDisplayInline( $postTo, $params );
        }
    }
} 