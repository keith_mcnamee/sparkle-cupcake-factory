<?php


class InitializeRequestCoreCommand extends CoreBase
{

    /**
     * @param array $serverConfigVOs
     */
    public function initialize( array $serverConfigVOs )
    {
        $configVO = $this->getCoreRequest()->getConfigHelper()->configVO( InternalRequestCoreConstants::$DEFAULT_REQUEST_CONFIG_ID );
        $serverConfigVO = $this->getCoreRequest()->getServerConfigHelper()->configVO( $serverConfigVOs );

        $coreRequestModel = $this->getCoreRequest()->getRequestModel();
        $coreRequestModel->setConfigVO( $configVO );
        $coreRequestModel->setServerConfigVO( $serverConfigVO );
    }
} 