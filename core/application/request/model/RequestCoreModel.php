<?php


class RequestCoreModel extends CoreBase
{

    /**
     * @var ConfigRequestCoreVO
     */
    private $configVO;

    /**
     * @var ServerConfigRequestCoreVO
     */
    private $serverConfigVO;

    /**
     * @var string
     */
    private $redirectTo;

    /**
     * @var boolean
     */
    private $silentRequest;

    /**
     * @var array
     */
    private $requests;

    public function __construct()
    {

    }

    /**
     * @return ConfigRequestCoreVO
     */
    public function getConfigVO()
    {
        return $this->configVO;
    }

    /**
     * @param ConfigRequestCoreVO $value
     */
    public function setConfigVO( ConfigRequestCoreVO $value )
    {
        $this->configVO = $value;
    }

    /**
     * @return ServerConfigRequestCoreVO
     */
    public function getServerConfigVO()
    {
        return $this->serverConfigVO;
    }

    /**
     * @param ServerConfigRequestCoreVO $value
     */
    public function setServerConfigVO( ServerConfigRequestCoreVO $value )
    {
        $this->serverConfigVO = $value;
    }

    /**
     * @return string
     */
    public function getRedirectTo()
    {
        return $this->redirectTo;
    }

    /**
     * @param string $value
     */
    public function setRedirectTo( $value )
    {
        $this->redirectTo = $value;
    }

    /**
     * @return boolean
     */
    public function isSilentRequest()
    {
        return $this->silentRequest;
    }

    /**
     * @param boolean $value
     */
    public function setSilentRequest( $value )
    {
        $this->silentRequest = $value;
    }

    /**
     * @return array
     */
    public function getRequests()
    {
        return $this->requests;
    }

    /**
     * @param array $value
     */
    public function setRequests( array $value )
    {
        $this->requests = $value;
    }
} 