<?php


class RequestCoreParser extends CoreBase
{

    /**
     * @param array $requests
     * @return array
     */
    public function appType( array $requests )
    {
        $retrieveConstructor = $this->getCoreRequest()->getRetrieveConstructor();
        foreach( $requests as $request )
        {
            /* @var $request array */
            $paths = $retrieveConstructor->arrayConstruct( $request, ParameterRequestCoreConstants::$PATH_VALUE, null, true, true );
            if( $paths && count( $paths ) > 0 )
            {
                return $paths[ 0 ];
            }
        }

        return null;
    }

    /**
     * @param array $data
     * @param string $variableName
     * @param mixed $currentValue
     * @return mixed
     */
    public function singleVariable( array $data, $variableName, $currentValue )
    {
        if( isset( $data[ $variableName ] ) )
        {
            return $data[ $variableName ];
        }
        return $currentValue;
    }
}