<?php


class RequestCoreConfig extends CoreBase
{

    /**
     * @var array
     */
    private $configVOs;

    public function __destruct()
    {
        $this->configVOs = null;
    }

    /**
     * @return array
     */
    public function getConfigVOs()
    {
        return $this->configVOs ? $this->configVOs : $this->createConfigVOs();
    }

    /**
     * @return array
     */
    protected function createConfigVOs()
    {
        $this->configVOs = array();

        $this->configVOs[] = $this->defaultConfigVO();

        return $this->configVOs;
    }


    /**
     * @return ConfigRequestCoreVO
     */
    protected function defaultConfigVO()
    {
        $configVO = new ConfigRequestCoreVO( InternalRequestCoreConstants::$DEFAULT_REQUEST_CONFIG_ID );

        $configVO->addPostBasic();
        $configVO->addPostJson( ParameterRequestCoreConstants::$PARAMS_JSON );

        $configVO->addGetBasic();
        $configVO->addGetJson( ParameterRequestCoreConstants::$PARAMS_JSON );

        return $configVO;
    }
} 