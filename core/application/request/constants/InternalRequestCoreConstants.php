<?php


class InternalRequestCoreConstants extends CoreBase
{
    public static $POST = "post";
    public static $GET = "get";

    //Request Config IDs
    public static $DEFAULT_REQUEST_CONFIG_ID = "default";
    public static $LOCALHOST_SERVER_CONFIG_ID = "localhost";
}