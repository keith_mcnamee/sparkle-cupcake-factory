<?php


class ParameterRequestCoreConstants extends CoreBase
{

    // App Types
    public static $EXAMPLE_APP_TYPE = "example";

    //Json Keys
    public static $PARAMS_JSON = "params";

    // Values
    public static $PATH_VALUE = "path";
} 