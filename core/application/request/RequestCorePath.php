<?php


class RequestCorePath
{

    /**
     * @var RequestCoreConfig
     */
    private $requestConfig;

    /**
     * @var RequestCoreModel
     */
    private $requestModel;

    public function __construct()
    {
        $this->getIncludes();
    }

    public function __destruct()
    {
        $this->requestConfig = null;
        $this->requestModel = null;
    }

    private function getIncludes()
    {
        require_once( DIR_CORE . "/application/request/includes/RequestCoreIncludes.php" );
    }

    /**
     * @return RequestCoreConfig
     */
    public function getRequestConfig()
    {
        if( !$this->requestConfig )
        {
            $this->requestConfig = new RequestCoreConfig();
        }
        return $this->requestConfig;
    }

    /**
     * @return RequestCoreModel
     */
    public function getRequestModel()
    {
        if( !$this->requestModel )
        {
            $this->requestModel = new RequestCoreModel();
        }
        return $this->requestModel;
    }

    /**
     * @return InitializeRequestCoreCommand
     */
    public function getInitializeCommand()
    {
        return new InitializeRequestCoreCommand();
    }

    /**
     * @return RetrieveRequestCoreConstructor
     */
    public function getRetrieveConstructor()
    {
        return new RetrieveRequestCoreConstructor();
    }

    /**
     * @return ConfigRequestCoreHelper
     */
    public function getConfigHelper()
    {
        return new ConfigRequestCoreHelper();
    }

    /**
     * @return RequestCoreHelper
     */
    public function getRequestHelper()
    {
        return new RequestCoreHelper();
    }

    /**
     * @return ServerConfigRequestCoreHelper
     */
    public function getServerConfigHelper()
    {
        return new ServerConfigRequestCoreHelper();
    }

    /**
     * @return RequestCoreParser
     */
    public function getRequestParser()
    {
        return new RequestCoreParser();
    }

    /**
     * @return RequestCoreSend
     */
    public function getRequestSend()
    {
        return new RequestCoreSend();
    }

    /**
     * @return RedirectHeadersRequestCoreService
     */
    public function getRedirectHeadersService()
    {
        return new RedirectHeadersRequestCoreService();
    }

    /**
     * @return RetrieveValuesRequestCoreService
     */
    public function getRetrieveValuesService()
    {
        return new RetrieveValuesRequestCoreService();
    }

    /**
     * @return SendRequestCoreService
     */
    public function getSendRequestService()
    {
        return new SendRequestCoreService();
    }
}