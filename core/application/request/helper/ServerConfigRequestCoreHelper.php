<?php


class ServerConfigRequestCoreHelper extends CoreBase
{
    /**
     * @param array $serverConfigVOs
     * @return ServerConfigRequestCoreVO
     */
    public function configVO( array $serverConfigVOs )
    {
        $serverName = CoreRequest::getHelper()->serverName();

        foreach( $serverConfigVOs as $vo )
        {
            /* @var $vo ServerConfigRequestCoreVO */
            if( $vo->hasServerName( $serverName ) )
            {
                return $vo;
            }
        }
        return null;
    }
} 