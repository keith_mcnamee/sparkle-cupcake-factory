<?php


class RequestCoreHelper extends CoreBase
{
    /**
     * @return string
     */
    public function baseDirectoryName()
    {
        $name = basename( getcwd() );

        return $name;
    }


    /**
     * @return string
     */
    public function serverName()
    {
        $name = $_SERVER[ 'SERVER_NAME' ];

        return $name;
    }

    /**
     * @return array
     */
    public function postVars()
    {
        if(isset($_POST))
        {
            $vars = $_POST;
            return $vars;
        }

        return array();
    }

    /**
     * @return array
     */
    public function getVars()
    {
        $vars = $this->rewriteGet();
        return $vars;
    }

    /**
     * @param string $jsonParam
     * @return array
     */
    public function postJsonVars( $jsonParam )
    {
        $postVars = $this->postVars();
        $vars = $this->jsonVars( $jsonParam, $postVars );
        return $vars;
    }

    /**
     * @param string $jsonParam
     * @return array
     */
    public function getJsonVars( $jsonParam )
    {
        $getVars = $this->getVars();
        $vars = $this->jsonVars( $jsonParam, $getVars );
        return $vars;
    }

    /**
     * @param array $fromArray
     * @param string $jsonParam
     * @return array
     */
    public function jsonVars( $jsonParam, array $fromArray )
    {
        if(isset($fromArray[ $jsonParam ]))
        {
            $json = $fromArray[ $jsonParam ];
            if( $this->isJson( $json ) )
            {
                $vars = json_decode($json, true);
                return $vars;
            }
        }

        return array();
    }

    /**
     * @param string $string
     * @return bool
     */
    public function isJson( $string )
    {
        //$validLocal=  "&params={\"season\":[1,2,3]}";
        //$validURL=  "&params=%7b%22season%22%3a%5b1%2c2%2c3%5d%7d";
        json_decode( $string );
        return (json_last_error() == JSON_ERROR_NONE);
    }

    /**
     * @return string
     */
    public function httpType()
    {
        $http = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
        return $http;
    }

    /**
     * @return string
     */
    public function isHttps()
    {
        $http = $this->httpType();
        return $http == "https://";
    }

    /**
     * @param boolean $forceSecure
     * @param boolean $forceInsecure
     * @return string
     */
    public function baseURL( $forceSecure = false, $forceInsecure = false )
    {
        $portNeeded = false;
        $httpType = null;
        if( $forceSecure )
        {
            $httpType = "https://";
        }
        else if( $forceInsecure )
        {
            $httpType = "http://";
        }
        else
        {
            $isHTTPS = $this->isHttps();
            $httpType = $this->httpType();

            $portNeeded = (isset($_SERVER["SERVER_PORT"]) && ((!$isHTTPS && $_SERVER["SERVER_PORT"] != "80") || ($isHTTPS && $_SERVER["SERVER_PORT"] != "443")));
        };

        $port = $portNeeded ? ':'.$_SERVER["SERVER_PORT"] : '';

        if(dirname($_SERVER["PHP_SELF"]) && dirname($_SERVER["PHP_SELF"]) != "/")
        {
            $url = $httpType.$_SERVER["SERVER_NAME"].$port.dirname($_SERVER["PHP_SELF"])."/";
        }
        else
        {
            $url = $httpType.$_SERVER["SERVER_NAME"].$port."/";
        }

        return $url;
    }

    /**
     * @param boolean $forceSecure
     * @param boolean $forceInsecure
     * @return string
     */
    public function visibleURL( $forceSecure = false, $forceInsecure = false )
    {
        $baseURl = $this->baseURL( $forceSecure, $forceInsecure );
        $workingDirectory = $this->workingDirectory();
        $slash =  $workingDirectory && strpos( $workingDirectory,'.') === false && strpos( $workingDirectory,'?') === false ? "/" : "";
        return $baseURl . $workingDirectory . $slash;
    }

    /**
     * @return array
     */
    public function workingDirectoryPaths()
    {
        $workingDirectory = $this->workingDirectory();
        $paths = explode( '/', $workingDirectory );

        return $paths;
    }

    /**
     * @param boolean $reverse
     * @return array
     */
    public function applicationDirectoryPaths( $reverse = false )
    {
        $currentApplicationDirectory = dirname( $_SERVER['PHP_SELF'] );
        $currentApplicationDirectory = trim($currentApplicationDirectory, "/");
        $paths = explode( '/', $currentApplicationDirectory );
        if( $reverse )
        {
            $paths = array_reverse($paths);
        }

        return $paths;
    }

    /**
     * @return string
     */
    public function workingDirectory()
    {
        $requestPath = strstr( $_SERVER[ 'REQUEST_URI' ], '?', true );
        $requestPath = $requestPath ? $requestPath : $_SERVER[ 'REQUEST_URI' ];
        $requestPath = trim($requestPath, "/");
        $currentApplicationDirectory = dirname( $_SERVER['PHP_SELF'] );
        $currentApplicationDirectory = trim($currentApplicationDirectory, "/");
        $workingDirectory = str_replace( $currentApplicationDirectory, "", $requestPath );
        $workingDirectory = trim($workingDirectory, "/");

        return $workingDirectory;
    }

    /**
     * @return string
     */
    public function rewriteQueryString()
    {
        $value = ltrim( strstr( $_SERVER[ 'REQUEST_URI' ], '?' ), "?" );
        return $value;
    }

    /**
     * @return array
     */
    public function rewriteGet()
    {
        $queryString = $this->rewriteQueryString();
        $queries = $queryString ? explode( "&", $queryString ) : array();
        $gets = array();
        foreach( $queries as $query )
        {
            /* @var $query string */
            $get = $query ? explode( "=", $query, 2 ) : array();
            if( count( $get ) == 1 )
            {
                $get[] = null;
            }
            if( count( $get ) == 2 )
            {
                $gets[ $get [ 0 ] ] = $get [ 1 ];
            }
        }
        return $gets;
    }

    /**
     * @return string
     */
    public function baseDirectory()
    {
        $requestURI = $_SERVER['SCRIPT_NAME'];
        $pos = strrpos($requestURI, "/");
        $directory = substr($requestURI, 0, $pos + 1);
        return $directory;
    }

    /**
     * @return string
     */
    public function getAppType()
    {
        $paths = $this->workingDirectoryPaths();
        $appType = count( $paths ) > 0 ? $paths[ 0 ] : null;
        /* @var $appType string */

        if ( !$appType )
        {
            $requests = $this->getCoreRequest()->getRetrieveValuesService()->requests();
            $appType = $this->getCoreRequest()->getRequestParser()->appType( $requests );
        }
        return $appType;
    }

    /**
     * @param array $paths
     * @return string
     */
    public function nextPath( array $paths )
    {
        $thisPath = count( $paths ) > 0 ? $paths[ 0 ] : null;
        return $thisPath;
    }

    /**
     * @param array $paths
     * @return array
     */
    public function remainingPaths( array $paths )
    {
        if( empty( $paths ) )
        {
            return $paths;
        }
        array_shift( $paths );
        return $paths;
    }

    /**
     * @param array $paths
     * @return bool
     */
    public function isFinalPath( array $paths )
    {
        if( empty( $paths ) || count( $paths ) == 1 && !$paths[ 0 ] )
        {
            return true;
        }
        return false;
    }
} 