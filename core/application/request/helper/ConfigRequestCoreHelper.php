<?php


class ConfigRequestCoreHelper extends CoreBase
{
    /**
     * @param $configID
     * @return ConfigRequestCoreVO
     */
    public function configVO( $configID )
    {
        $allVOs = $this->getCoreRequest()->getRequestConfig()->getConfigVOs();

        foreach( $allVOs as $vo )
        {
            /* @var $vo ConfigRequestCoreVO */
            if( $vo->getConfigID() == $configID )
            {
                return $vo;
            }
        }
        return null;
    }
} 