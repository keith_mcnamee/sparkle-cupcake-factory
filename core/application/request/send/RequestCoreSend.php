<?php


class RequestCoreSend extends CoreBase
{

    /**
     * @param $url
     * @param array $params
     */
    public function postDiscrete($url, array $params = null )
    {
        $params = $params ? $params : array();
        $postString = http_build_query( $params );
        $postString = $postString ? $postString : "";

        $urlParts = parse_url( $url );

        $host = $urlParts[ 'host' ];
        $port = isset( $urlParts['port'] ) ? $urlParts[ 'port' ] : 80;
        $path = $urlParts['path'];
        $error = null;
        $errorString = null;
        $timeOut = 30;

        $fSockOpen = fsockopen($host, $port, $error, $errorString, $timeOut );

        $connectionRequest = "POST " . $path . " HTTP/1.1\r\n";
        $connectionRequest.= "Host: " . $host . "\r\n";
        $connectionRequest.= "Content-Type: application/x-www-form-urlencoded\r\n";
        $connectionRequest.= "Content-Length: " . strlen( $postString ) . "\r\n";
        $connectionRequest.= "Connection: Close\r\n\r\n";
        $connectionRequest.= $postString;

        fwrite($fSockOpen, $connectionRequest);
        fclose($fSockOpen);
    }

    /**
     * @param $url
     * @param array $params
     */
    public function postDisplayInline($url, array $params = null )
    {
        $params = $params ? $params : array();
        $request = array(
            'http' => array(
                'method' => 'POST',
                'content' => http_build_query( $params )
            )
        );

        $streamContext = stream_context_create( $request );
        $fOpen = @fopen($url, 'rb', false, $streamContext);
        if ($fOpen)
        {
            echo @stream_get_contents($fOpen);
        }
    }

    /**
     * @param $url
     * @param array $params
     * @param string $requestParam
     */
    public function postRedirectViewAsJson($url, array $params = null, $requestParam = null )
    {
        ?>
        <form action='<?echo $url;?>' method='post' name='redirectForm'>
            <?php
            if( $params && !empty( $params ) )
            {
                $requestParam = $requestParam ? $requestParam : ParameterRequestCoreConstants::$PARAMS_JSON;
                echo "<input type='hidden' name='".$requestParam."' value='".htmlentities( json_encode( $params ) )."'>";
            }
            ?>
        </form>
        <script language="JavaScript">
            document.redirectForm.submit();
        </script>
    <?
        die();
    }

    /**
     * @param $url
     * @param array $params
     */
    public function postRedirectView($url, array $params = null )
    {
        ?>
        <form action='<?echo $url;?>' method='post' name='redirectForm'>
            <?php
            if( $params && !empty( $params ) )
            {
                foreach( $params as $key => $value )
                {
                    /* @var $value mixed */
                    echo "<input type='hidden' name='".$key."' value='".$value."'>";
                }
            }
            ?>
        </form>
        <script language="JavaScript">
            document.redirectForm.submit();
        </script>
    <?
        die();
    }


    /**
     * @param $url
     */
    public function redirectView( $url )
    {
        ?>
        <script type="text/javascript">
            window.location = "<?PHP echo $url;?>"
        </script>
        <?PHP
        die();
    }
} 