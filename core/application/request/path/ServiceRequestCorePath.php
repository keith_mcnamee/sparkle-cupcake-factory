<?php


class ServiceRequestCorePath
{

    /**
     * @var ConfigRequestCoreService
     */
    private $config;

    /**
     * @var RequestCoreService
     */
    private $service;

    /**
     * @var ServerConfigRequestCoreService
     */
    private $serverConfig;

    /**
     * @var ValuesRequestCoreService
     */
    private $values;

    public function __destruct()
    {
        $this->config = null;
        $this->service = null;
        $this->serverConfig = null;
        $this->values = null;
    }

    /**
     * @param $singleInstance
     * @return ConfigRequestCoreService
     */
    private function createConfig( $singleInstance )
    {
        $instance = new ConfigRequestCoreService();
        $instance->init();
        if( !$singleInstance )
        {
            $this->config = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return RequestCoreService
     */
    private function createService( $singleInstance )
    {
        $instance = new RequestCoreService();
        $instance->init();
        if( !$singleInstance )
        {
            $this->service = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return ServerConfigRequestCoreService
     */
    private function createServerConfig( $singleInstance )
    {
        $instance = new ServerConfigRequestCoreService();
        $instance->init();
        if( !$singleInstance )
        {
            $this->serverConfig = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return ValuesRequestCoreService
     */
    private function createValues( $singleInstance )
    {
        $instance = new ValuesRequestCoreService();
        $instance->init();
        if( !$singleInstance )
        {
            $this->values = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return ConfigRequestCoreService
     */
    public function getConfig( $singleInstance = true )
    {
        return $this->config && !$singleInstance ? $this->config : $this->createConfig( $singleInstance );
    }

    /**
     * @param $singleInstance
     * @return RequestCoreService
     */
    public function getService( $singleInstance = true )
    {
        return $this->service && !$singleInstance ? $this->service : $this->createService( $singleInstance );
    }

    /**
     * @param $singleInstance
     * @return ServerConfigRequestCoreService
     */
    public function getServerConfig( $singleInstance = true )
    {
        return $this->serverConfig && !$singleInstance ? $this->serverConfig : $this->createServerConfig( $singleInstance );
    }

    /**
     * @param $singleInstance
     * @return ValuesRequestCoreService
     */
    public function getValues( $singleInstance = true )
    {
        return $this->values && !$singleInstance ? $this->values : $this->createValues( $singleInstance );
    }
}