<?php


class ConfigRequestCorePath
{

    /**
     * @var RequestCoreConfig
     */
    private $config;

    public function __destruct()
    {
        $this->default = null;
        $this->config = null;
    }

    /**
     * @param $singleInstance
     * @return RequestCoreConfig
     */
    private function createConfig( $singleInstance )
    {
        $instance = new RequestCoreConfig();
        $instance->init();
        if( !$singleInstance )
        {
            $this->config = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return RequestCoreConfig
     */
    public function getConfig( $singleInstance = false )
    {
        return $this->config && !$singleInstance ? $this->config : $this->createConfig( $singleInstance );
    }
}