<?php


class ConstructRequestCorePath
{

    /**
     * @var RetrieveRequestCoreConstructor
     */
    private $retrieve;

    public function __destruct()
    {
        $this->retrieve = null;
    }

    /**
     * @param $singleInstance
     * @return RetrieveRequestCoreConstructor
     */
    private function createRetrieve( $singleInstance )
    {
        $instance = new RetrieveRequestCoreConstructor();
        $instance->init();
        if( !$singleInstance )
        {
            $this->retrieve = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return RetrieveRequestCoreConstructor
     */
    public function getRetrieve( $singleInstance = true )
    {
        return $this->retrieve && !$singleInstance ? $this->retrieve : $this->createRetrieve( $singleInstance );
    }
}