<?php


class ModelRequestCorePath
{

    /**
     * @var RequestCoreModel
     */
    private $model;

    public function __destruct()
    {
        $this->model = null;
    }

    /**
     * @param $singleInstance
     * @return RequestCoreModel
     */
    private function createModel( $singleInstance )
    {
        $instance = new RequestCoreModel();
        $instance->init();
        if( !$singleInstance )
        {
            $this->model = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return RequestCoreModel
     */
    public function getModel( $singleInstance = false )
    {
        return $this->model && !$singleInstance ? $this->model : $this->createModel( $singleInstance );
    }
}