<?php


class ParseRequestCorePath
{

    /**
     * @var RequestCoreParser
     */
    private $parse;

    public function __destruct()
    {
        $this->parse = null;
    }

    /**
     * @param $singleInstance
     * @return RequestCoreParser
     */
    private function createParse( $singleInstance )
    {
        $instance = new RequestCoreParser();
        $instance->init();
        if( !$singleInstance )
        {
            $this->parse = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return RequestCoreParser
     */
    public function getParse( $singleInstance = true )
    {
        return $this->parse && !$singleInstance ? $this->parse : $this->createParse( $singleInstance );
    }
}