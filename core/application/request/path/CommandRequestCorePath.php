<?php


class CommandRequestCorePath
{

    /**
     * @var InitializeRequestCoreCommand
     */
    private $initialize;

    /**
     * @var ProcessPostRequestCoreCommand
     */
    private $processPost;

    /**
     * @var RedirectHeadersRequestCoreCommand
     */
    private $redirectHeaders;

    /**
     * @var SendRequestCoreCommand
     */
    private $send;

    public function __destruct()
    {
        $this->initialize = null;
        $this->processPost = null;
        $this->redirectHeaders = null;
        $this->send = null;
    }

    /**
     * @param $singleInstance
     * @return InitializeRequestCoreCommand
     */
    private function createInitialize( $singleInstance )
    {
        $instance = new InitializeRequestCoreCommand();
        $instance->init();
        if( !$singleInstance )
        {
            $this->initialize = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return ProcessPostRequestCoreCommand
     */
    private function createProcessPost( $singleInstance )
    {
        $instance = new ProcessPostRequestCoreCommand();
        $instance->init();
        if( !$singleInstance )
        {
            $this->processPost = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return RedirectHeadersRequestCoreCommand
     */
    private function createRedirectHeaders( $singleInstance )
    {
        $instance = new RedirectHeadersRequestCoreCommand();
        $instance->init();
        if( !$singleInstance )
        {
            $this->redirectHeaders = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return ValuesRequestCoreService
     */
    private function createSend( $singleInstance )
    {
        $instance = new SendRequestCoreCommand();
        $instance->init();
        if( !$singleInstance )
        {
            $this->send = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return InitializeRequestCoreCommand
     */
    public function getInitialize( $singleInstance = true )
    {
        return $this->initialize && !$singleInstance ? $this->initialize : $this->createInitialize( $singleInstance );
    }

    /**
     * @param $singleInstance
     * @return ProcessPostRequestCoreCommand
     */
    public function getProcessPost( $singleInstance = true )
    {
        return $this->processPost && !$singleInstance ? $this->processPost : $this->createProcessPost( $singleInstance );
    }

    /**
     * @param $singleInstance
     * @return RedirectHeadersRequestCoreCommand
     */
    public function getRedirectHeaders( $singleInstance = true )
    {
        return $this->redirectHeaders && !$singleInstance ? $this->redirectHeaders : $this->createRedirectHeaders( $singleInstance );
    }

    /**
     * @param $singleInstance
     * @return SendRequestCoreCommand
     */
    public function getSend( $singleInstance = true )
    {
        return $this->send && !$singleInstance ? $this->send : $this->createSend( $singleInstance );
    }
}