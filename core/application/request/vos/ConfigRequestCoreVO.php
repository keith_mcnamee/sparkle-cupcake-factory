<?php


class ConfigRequestCoreVO
{
    /**
     * @var string
     */
    private $configID;

    /**
     * @var array;
     */
    private $requestOrder;

    /**
     * @param string $configID
     */
    public function __construct( $configID )
    {
        $this->setConfigID( $configID );

        $this->setRequestOrder( array() );
    }

    public function addPostBasic()
    {
        $vo = new OrderConfigRequestCoreVO( InternalRequestCoreConstants::$POST );
        $this->setRequestOrder( CoreHelper::getArrayHelper()->addObject( $this->getRequestOrder(), $vo ) );
    }

    public function addGetBasic()
    {
        $vo = new OrderConfigRequestCoreVO( InternalRequestCoreConstants::$GET );
        $this->setRequestOrder( CoreHelper::getArrayHelper()->addObject( $this->getRequestOrder(), $vo ) );
    }

    /**
     * @param string $jsonParam
     */
    public function addPostJson( $jsonParam )
    {
        $vo = new OrderConfigRequestCoreVO( InternalRequestCoreConstants::$POST, $jsonParam );
        $this->setRequestOrder( CoreHelper::getArrayHelper()->addObject( $this->getRequestOrder(), $vo ) );
    }

    /**
     * @param string $jsonParam
     */
    public function addGetJson( $jsonParam )
    {
        $vo = new OrderConfigRequestCoreVO( InternalRequestCoreConstants::$GET, $jsonParam );
        $this->setRequestOrder( CoreHelper::getArrayHelper()->addObject( $this->getRequestOrder(), $vo ) );
    }

    /**
     * @return array
     */
    public function getRequestOrder()
    {
        return $this->requestOrder;
    }

    /**
     * @param array $value
     */
    public function setRequestOrder( array $value )
    {
        $this->requestOrder = $value;
    }

    /**
     * @return string
     */
    public function getConfigID()
    {
        return $this->configID;
    }

    /**
     * @param string $value
     */
    public function setConfigID( $value )
    {
        $this->configID = $value;
    }
} 