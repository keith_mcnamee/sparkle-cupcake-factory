<?php


class OrderConfigRequestCoreVO
{
    /**
     * @var string
     */
    private $requestType;

    /**
     * @var string
     */
    private $param;

    /**
     * @param string $requestType
     * @param string $param
     */
    public function __construct( $requestType, $param = null )
    {
        $this->setRequestType( $requestType );
        $this->setParam( $param );
    }

    /**
     * @return string
     */
    public function getRequestType()
    {
        return $this->requestType;
    }

    /**
     * @param string $value
     */
    public function setRequestType( $value )
    {
        $this->requestType = $value;
    }

    /**
     * @return string
     */
    public function getParam()
    {
        return $this->param;
    }

    /**
     * @param string $value
     */
    public function setParam( $value )
    {
        $this->param = $value;
    }
} 