<?php


class ServerConfigRequestCoreVO
{
    /**
     * @var string;
     */
    private $configID;

    /**
     * @var boolean;
     */
    private $usingStagingDatabase;

    /**
     * @var array;
     */
    private $serverNames;

    /**
     * @var array;
     */
    private $stagingDirectories;

    /**
     * @param string $configID
     */
    public function __construct( $configID )
    {
        $this->setConfigID( $configID );

        $this->setServerNames( array() );
        $this->setStagingDirectories( array() );
    }

    /**
     * @param string $name
     */
    public function addServerName( $name )
    {
        $this->setServerNames( CoreHelper::getArrayHelper()->setObjectForKey( $this->getServerNames(), true, $name ) );
    }

    /**
     * @param StagingDirectoryConfigRequestCoreVO $vo
     */
    public function addStagingDirectory( StagingDirectoryConfigRequestCoreVO $vo )
    {
        $this->setStagingDirectories( CoreHelper::getArrayHelper()->addObject( $this->getStagingDirectories(), $vo ) );
    }

    /**
     * @param string $name
     * @return bool
     */
    public function hasServerName( $name )
    {
        $hasKey = (bool)CoreHelper::getArrayHelper()->getObjectForKey( $this->getServerNames(), $name );

        return $hasKey;
    }

    /**
     * @return array
     */
    public function getServerNames()
    {
        return $this->serverNames;
    }

    /**
     * @param array $value
     */
    public function setServerNames( array $value )
    {
        $this->serverNames = $value;
    }

    /**
     * @return string
     */
    public function getConfigID()
    {
        return $this->configID;
    }

    /**
     * @param string $value
     */
    public function setConfigID( $value )
    {
        $this->configID = $value;
    }

    /**
     * @return boolean
     */
    public function isUsingStagingDatabase()
    {
        return $this->usingStagingDatabase;
    }

    /**
     * @param boolean $value
     */
    public function setUsingStagingDatabase( $value )
    {
        $this->usingStagingDatabase = $value;
    }

    /**
     * @return array
     */
    public function getStagingDirectories()
    {
        return $this->stagingDirectories;
    }

    /**
     * @param array $value
     */
    public function setStagingDirectories( array  $value )
    {
        $this->stagingDirectories = $value;
    }
} 