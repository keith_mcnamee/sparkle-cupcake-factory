<?php


class StagingDirectoryConfigRequestCoreVO
{
    /**
     * @var string;
     */
    private $directoryName;

    /**
     * @var int;
     */
    private $directoryIndex;

    /**
     * @param string $directoryName
     * @param int $directoryIndex
     */
    public function __construct( $directoryName, $directoryIndex )
    {
        $this->setDirectoryName( $directoryName );

        $this->setDirectoryIndex( $directoryIndex );
    }

    /**
     * @return string
     */
    public function getDirectoryName()
    {
        return $this->directoryName;
    }

    /**
     * @param string $value
     */
    public function setDirectoryName( $value )
    {
        $this->directoryName = $value;
    }

    /**
     * @return int
     */
    public function getDirectoryIndex()
    {
        return $this->directoryIndex;
    }

    /**
     * @param int $value
     */
    public function setDirectoryIndex( $value )
    {
        $this->directoryIndex = $value;
    }
}