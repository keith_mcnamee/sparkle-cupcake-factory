<?php


class RetrieveRequestCoreConstructor extends CoreBase
{

    /**
     * @param array $data
     * @param string $param
     * @param array $defaultValue
     * @param boolean $sanitize
     * @param boolean $forceToArray
     * @param boolean $convertToDictionary
     * @return mixed
     */
    public function arrayConstruct( array $data, $param, $defaultValue = null, $sanitize = false, $forceToArray = false, $convertToDictionary = false )
    {
        if( !isset( $data[ $param ] ) )
        {
            return $defaultValue;
        }
        $returnArray = $data[ $param ];
        /* @var $returnArray mixed */
        if( !is_array( $returnArray ) )
        {
            if( $forceToArray )
            {
                $returnArray = array( $returnArray );
            }
            else
            {
                return $defaultValue;
            }
        }
        /* @var $returnArray array */

        if( $convertToDictionary && !CoreHelper::getArrayHelper()->isDictionaryArray( $returnArray ) )
        {
            $modifiedArray = array();
            foreach( $returnArray as $data )
            {
                /* @var $data mixed */
                $modifiedArray[ (string)$data ] = true;
            }
            $returnArray = $modifiedArray;
        }

        if( $sanitize )
        {
            $modifiedArray = array();
            foreach( $returnArray as $key => $object )
            {
                /* @var $object mixed */
                $key = CoreHelper::getStringHelper()->sanitizeInput( $key );
                $object = CoreHelper::getStringHelper()->sanitizeInput( $object );
                $modifiedArray[ $key ] = $object;
            }
            $returnArray = $modifiedArray;
        }

        return $returnArray;
    }

    /**
     * @param array $data
     * @param string $param
     * @param mixed $defaultValue
     * @param boolean $sanitize
     * @return mixed
     */
    public function valueConstruct( array $data, $param, $defaultValue = null, $sanitize = false )
    {
        if( !isset( $data[ $param ] ) )
        {
            return $defaultValue;
        }

        $value = $data[ $param ];
        if( $sanitize )
        {
            $value = CoreHelper::getStringHelper()->sanitizeInput( $value );
        }
        return $value;
    }
} 