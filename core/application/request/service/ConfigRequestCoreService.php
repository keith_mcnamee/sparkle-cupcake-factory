<?php


class ConfigRequestCoreService extends CoreBase
{
    /**
     * @param $configID
     * @return ConfigRequestCoreVO
     */
    public function query( $configID )
    {
        $allVOs = $this->getCoreRequest()->getConfig()->getConfig()->getConfigVOs();

        foreach( $allVOs as $vo )
        {
            /* @var $vo ConfigRequestCoreVO */
            if( $vo->getConfigID() == $configID )
            {
                return $vo;
            }
        }
        return null;
    }
} 