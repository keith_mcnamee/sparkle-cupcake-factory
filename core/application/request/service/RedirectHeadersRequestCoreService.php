<?php


class RedirectHeadersRequestCoreService extends CoreBase
{

    /**
     * @param string $toLocation
     */
    public function redirect( $toLocation )
    {
        $queryString = CoreRequest::getHelper()->rewriteQueryString();
        $postParams = $_POST;
        if( $queryString )
        {
            $toLocation .= "?".$queryString;
        }
        $requestSend = $this->getCoreRequest()->getRequestSend();
        if( $postParams && !empty( $postParams ) )
        {
            $requestSend->postRedirectView( $toLocation, $postParams );
        }
        else
        {
            $requestSend->redirectView( $toLocation );
        }
    }

    /**
     * @param string $path
     */
    public function internalLocation( $path )
    {
        $currentUrl = CoreRequest::getHelper()->baseURL();
        $redirectTo = $currentUrl.$path;
        $this->redirect( $redirectTo );
    }
}