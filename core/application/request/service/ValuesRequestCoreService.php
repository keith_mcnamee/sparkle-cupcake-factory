<?php


class ValuesRequestCoreService extends CoreBase
{
    /**
     * @return array
     */
    public function query()
    {
        $coreRequestModel = $this->getCoreRequest()->getModel()->getModel();
        if( $coreRequestModel->getRequests() )
        {
            return $coreRequestModel->getRequests();
        }
        $requests = array();
        $requestOrder = $coreRequestModel->getConfigVO()->getRequestOrder();
        foreach( $requestOrder as $requestOrderVO)
        {
            /* @var $requestOrderVO OrderConfigRequestCoreVO */

            if( $requestOrderVO->getRequestType() == InternalRequestCoreConstants::$POST )
            {
                if( !$requestOrderVO->getParam() )
                {
                    $requests[] = CoreRequest::getService()->postVars();
                }
                else
                {
                    $requests[] = CoreRequest::getService()->postJsonVars( $requestOrderVO->getParam() );
                }
            }
            else if( $requestOrderVO->getRequestType() == InternalRequestCoreConstants::$GET )
            {
                if( !$requestOrderVO->getParam() )
                {
                    $requests[] = CoreRequest::getService()->getVars();
                }
                else
                {
                    $requests[] = CoreRequest::getService()->getJsonVars( $requestOrderVO->getParam() );
                }
            }
        }
        $coreRequestModel->setRequests( $requests );
        return $requests;
    }
}