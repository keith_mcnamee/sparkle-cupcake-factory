<?php


class ServerConfigRequestCoreService extends CoreBase
{
    /**
     * @param array $serverConfigVOs
     * @return ServerConfigRequestCoreVO
     */
    public function query( array $serverConfigVOs )
    {
        $serverName = CoreRequest::getService()->serverName();

        foreach( $serverConfigVOs as $vo )
        {
            /* @var $vo ServerConfigRequestCoreVO */
            if( $vo->hasServerName( $serverName ) )
            {
                return $vo;
            }
        }
        return null;
    }
} 