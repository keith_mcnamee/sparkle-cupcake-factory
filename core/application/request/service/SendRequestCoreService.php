<?php


class SendRequestCoreService extends CoreBase
{
    /**
     * @param string $name
     * @param boolean $discrete
     * @param array $params
     * @param boolean $allowTesting
     * @param string $requestParam
     */
    public function sendRequest( $name, $discrete, $params = null, $allowTesting = false, $requestParam = null )
    {
        $currentUrl = CoreRequest::getHelper()->baseURL();
        $postTo = $currentUrl.$name."/";

        $infoAppHelper = $this->getCoreApp()->getInfoHelper();
        $debug = $allowTesting && ( $infoAppHelper->isTestServer() || $infoAppHelper->isStagingDirectory() );

        $requestSend = $this->getCoreRequest()->getRequestSend();
        if( $debug )
        {
            $requestSend->postRedirectViewAsJson( $postTo, $params, $requestParam );
        }
        else if( $discrete )
        {
            $requestSend->postDiscrete( $postTo, $params );
        }
        else
        {
            $requestSend->postDisplayInline( $postTo, $params );
        }
    }
} 