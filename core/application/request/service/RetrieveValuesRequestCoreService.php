<?php


class RetrieveValuesRequestCoreService extends CoreBase
{
    /**
     * @return array
     */
    public function requests()
    {
        $coreRequestModel = $this->getCoreRequest()->getRequestModel();
        if( $coreRequestModel->getRequests() )
        {
            return $coreRequestModel->getRequests();
        }
        $requests = array();
        $requestOrder = $coreRequestModel->getConfigVO()->getRequestOrder();
        foreach( $requestOrder as $requestOrderVO)
        {
            /* @var $requestOrderVO OrderConfigRequestCoreVO */

            if( $requestOrderVO->getRequestType() == InternalRequestCoreConstants::$POST )
            {
                if( !$requestOrderVO->getParam() )
                {
                    $requests[] = CoreRequest::getHelper()->postVars();
                }
                else
                {
                    $requests[] = CoreRequest::getHelper()->postJsonVars( $requestOrderVO->getParam() );
                }
            }
            else if( $requestOrderVO->getRequestType() == InternalRequestCoreConstants::$GET )
            {
                if( !$requestOrderVO->getParam() )
                {
                    $requests[] = CoreRequest::getHelper()->getVars();
                }
                else
                {
                    $requests[] = CoreRequest::getHelper()->getJsonVars( $requestOrderVO->getParam() );
                }
            }
        }
        $coreRequestModel->setRequests( $requests );
        return $requests;
    }
}