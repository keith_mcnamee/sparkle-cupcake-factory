<?php


class AppCoreConfig extends CoreBase
{

    /**
     * @var array
     */
    private $configVOs;

    public function __destruct()
    {
        $this->configVOs = null;
    }

    /**
     * @return array
     */
    public function getConfigVOs()
    {
        return $this->configVOs ? $this->configVOs : $this->createConfigVOs();
    }

    /**
     * @return array
     */
    protected function createConfigVOs()
    {
        $this->configVOs = array();

        $this->configVOs[] = $this->defaultConfigVO();

        return $this->configVOs;
    }

    /**
     * @return ConfigAppCoreVO
     */
    protected function defaultConfigVO()
    {
        $configVO = new ConfigAppCoreVO( AppCoreConstants::$DEFAULT_CONFIG_TYPE );

        $configVO->addTestServerName( InternalRequestCoreConstants::$LOCALHOST_SERVER_CONFIG_ID );

        $configVO->setDefaultLocalizationType( "en" );

        return $configVO;
    }
} 