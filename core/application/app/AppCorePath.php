<?php


class AppCorePath
{

    /**
     * @var AppCoreConfig
     */
    private $appConfig;

    /**
     * @var AppCoreModel
     */
    private $appModel;

    public function __destruct()
    {
        $this->appModel = null;
        $this->appConfig = null;
    }

    /**
     * @return AppCoreConfig
     */
    public function getAppConfig()
    {
        if( !$this->appConfig )
        {
            $this->appConfig = new AppCoreConfig();
        }
        return $this->appConfig;
    }


    /**
     * @return AppCoreModel
     */
    public function getAppModel()
    {
        if( !$this->appModel )
        {
            $this->appModel = new AppCoreModel();
        }
        return $this->appModel;
    }

    /**
     * @return InitializeAppCoreCommand
     */
    public function getInitializeCommand()
    {
        return new InitializeAppCoreCommand();
    }

    /**
     * @return InfoAppCoreHelper
     */
    public function getInfoHelper()
    {
        return new InfoAppCoreHelper();
    }

    /**
     * @return ConfigAppCoreHelper
     */
    public function getConfigHelper()
    {
        return new ConfigAppCoreHelper();
    }

    /**
     * @return LogTimeAppCoreService
     */
    public function getLogTimeService()
    {
        return new LogTimeAppCoreService();
    }

}