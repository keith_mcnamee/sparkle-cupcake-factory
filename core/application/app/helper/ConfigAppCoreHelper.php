<?php


class ConfigAppCoreHelper extends CoreBase
{
    /**
     * @param $configType
     * @return ConfigAppCoreVO
     */
    public function configVO( $configType )
    {
        $allVOs = $this->getCoreApp()->getAppConfig()->getConfigVOs();

        foreach( $allVOs as $vo )
        {
            /* @var $vo ConfigAppCoreVO */
            if( $vo->getConfigType() == $configType )
            {
                return $vo;
            }
        }
        return null;
    }
} 