<?php


class InfoAppCoreHelper extends CoreBase
{

    /**
     * @var AppCoreModel
     */
    private $coreAppModel;

    /**
     * @var RequestCoreModel
     */
    private $coreRequestModel;

    public function __destruct()
    {
        $this->coreAppModel = null;
        $this->coreRequestModel = null;

        parent::__destruct();
    }

    /**
     * @return bool
     */
    public function isTestServer()
    {
        $serverName = CoreRequest::getHelper()->serverName();
        $isTest = $this->getCoreAppModel()->getConfigVO()->hasTestServerName( $serverName );

        return $isTest;
    }

    /**
     * @return bool
     */
    public function isStagingDirectory()
    {
        $applicationDirectoryPaths = CoreRequest::getHelper()->applicationDirectoryPaths( true );

        $stagingDirectories = $this->getCoreRequestModel()->getServerConfigVO()->getStagingDirectories();
        $isTest = false;
        foreach( $stagingDirectories as $stagingDirectory )
        {
            /* @var $stagingDirectory StagingDirectoryConfigRequestCoreVO */
            if( isset( $applicationDirectoryPaths[ $stagingDirectory->getDirectoryIndex() ] ) && $applicationDirectoryPaths[ $stagingDirectory->getDirectoryIndex() ] == $stagingDirectory->getDirectoryName() )
            {
                $isTest = true;
            }
        }

        return $isTest;
    }

    /**
     * @return AppCoreModel
     */
    private function getCoreAppModel()
    {
        if( !$this->coreAppModel )
        {
            $this->coreAppModel = $this->getCoreApp()->getAppModel();
        }
        return $this->coreAppModel;
    }

    /**
     * @return RequestCoreModel
     */
    private function getCoreRequestModel()
    {
        if( !$this->coreRequestModel )
        {
            $this->coreRequestModel = $this->getCoreRequest()->getRequestModel();
        }
        return $this->coreRequestModel;
    }
} 