<?php


class ConfigAppCoreVO
{

    /**
     * @var string
     */
    private $configType;

    /**
     * @var array
     */
    private $testServerNames;

    /**
     * @var string
     */
    private $defaultLocalizationType;

    /**
     * @param string $configType
     */
    public function __construct( $configType )
    {
        $this->setConfigType( $configType );

        $this->setTestServerNames( array () );
    }

    /**
     * @param int $key
     */
    public function addTestServerName( $key )
    {
        $this->setTestServerNames( CoreHelper::getArrayHelper()->setObjectForKey( $this->getTestServerNames(), true, $key ) );
    }

    /**
     * @return array
     */
    public function getTestServerNames()
    {
        return $this->testServerNames;
    }

    /**
     * @param array $value
     */
    public function setTestServerNames( array $value )
    {
        $this->testServerNames = $value;
    }

    /**
     * @param $key
     * @return bool
     */
    public function hasTestServerName( $key )
    {
        $hasKey = CoreHelper::getArrayHelper()->getObjectForKey( $this->getTestServerNames(), $key );

        return $hasKey;
    }

    /**
     * @return string
     */
    public function getDefaultLocalizationType()
    {
        return $this->defaultLocalizationType;
    }

    /**
     * @param string $value
     */
    public function setDefaultLocalizationType( $value )
    {
        $this->defaultLocalizationType = $value;
    }

    /**
     * @return string
     */
    public function getConfigType()
    {
        return $this->configType;
    }

    /**
     * @param string $value
     */
    public function setConfigType( $value )
    {
        $this->configType = $value;
    }
} 