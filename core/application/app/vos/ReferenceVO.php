<?php

class ReferenceVO
{
    /**
     * @var int
     */
    private $ref;

    /**
     * @var string
     */
    private $marker;


    public function __construct()
    {

    }

    /**
     * @param ReferenceVO $vo
     * @return ReferenceVO
     */
    public function cloneReferenceVO( ReferenceVO $vo = null )
    {
        $vo = $vo ? $vo : new ReferenceVO();
        $vo->setRef( $this->getRef() );
        return $vo;
    }

    /**
     * @return int
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * @param int $value
     */
    public function setRef( $value )
    {
        $this->ref = $value;
    }

    /**
     * @return string
     */
    public function getMarker()
    {
        return $this->marker;
    }

    /**
     * @param string $value
     */
    public function setMarker( $value )
    {
        $this->marker = $value;
    }

} 