<?php


class DefineConfigAppCoreCommand extends CoreBase
{
    public function command()
    {
        $configVO = $this->getCoreApp()->getService()->getConfig()->query( AppCoreConstants::$DEFAULT_CONFIG_TYPE );

        $this->getCoreApp()->getModel()->getModel()->setConfigVO( $configVO );
    }
} 