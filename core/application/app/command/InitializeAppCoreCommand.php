<?php


class InitializeAppCoreCommand extends CoreBase
{
    /**
     * @param array $serverRequestConfigVOs
     */
    public function command( array $serverRequestConfigVOs )
    {
        $configVO = $this->getCoreApp()->getConfigHelper()->configVO( AppCoreConstants::$DEFAULT_CONFIG_TYPE );

        $this->getCoreApp()->getAppModel()->setConfigVO( $configVO );

        $this->getCoreRequest()->getInitializeCommand()->initialize( $serverRequestConfigVOs );
        $this->getCoreApp()->getLogTimeService()->initTimeLog();
    }
} 