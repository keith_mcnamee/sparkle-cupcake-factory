<?php


class LogTimeAppCoreCommand extends CoreBase
{

    /**
     * @var AppCoreModel
     */
    private $coreAppModel;

    public function init()
    {
        parent::init();

        $this->coreAppModel = $this->getCoreApp()->getModel()->getModel();
    }

    public function __destruct()
    {
        $this->coreAppModel = null;

        parent::__destruct();
    }

    public function initTimeLogCommand()
    {
        $this->coreAppModel->setTimeLog( array() );
        $this->addTimeLog( "Init" );
    }


    public function addTimeLog( $logID )
    {
        $infoAppService = $this->getCoreApp()->getService()->getInfo();
        if( !( $infoAppService->isTestServer() || $infoAppService->isStagingDirectory() ) )
        {
            return;
        }
        $microTime = $this->microTime();
        $this->coreAppModel->setTimeLog( CoreHelper::getArrayCommand()->setObjectForKey( $this->coreAppModel->getTimeLog(), $microTime, $logID ) );
    }

    private function microTime()
    {
        list($usec, $sec) = explode( " ", microtime() );

        return ( (float)$usec + (float)$sec );
    }
} 