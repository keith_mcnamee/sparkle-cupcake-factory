<?php


class ServiceAppCorePath
{

    /**
     * @var ConfigAppCoreService
     */
    private $config;

    /**
     * @var InfoAppCoreService
     */
    private $info;

    public function __destruct()
    {
        $this->config = null;
        $this->info = null;
    }

    /**
     * @param $singleInstance
     * @return ConfigAppCoreService
     */
    private function createConfig( $singleInstance )
    {
        $instance = new ConfigAppCoreService();
        $instance->init();
        if( !$singleInstance )
        {
            $this->config = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return InfoAppCoreService
     */
    private function createInfo( $singleInstance )
    {
        $instance = new InfoAppCoreService();
        $instance->init();
        if( !$singleInstance )
        {
            $this->info = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return ConfigAppCoreService
     */
    public function getConfig( $singleInstance = true )
    {
        return $this->config && !$singleInstance ? $this->config : $this->createConfig( $singleInstance );
    }

    /**
     * @param $singleInstance
     * @return InfoAppCoreService
     */
    public function getInfo( $singleInstance = true )
    {
        return $this->info && !$singleInstance ? $this->info : $this->createInfo( $singleInstance );
    }

}