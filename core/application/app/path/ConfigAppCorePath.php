<?php


class ConfigAppCorePath
{

    /**
     * @var AppCoreConfig
     */
    private $config;

    public function __destruct()
    {
        $this->config = null;
    }

    /**
     * @param $singleInstance
     * @return AppCoreConfig
     */
    private function createConfig( $singleInstance )
    {
        $instance = new AppCoreConfig();
        $instance->init();
        if( !$singleInstance )
        {
            $this->config = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return AppCoreConfig
     */
    public function getConfig( $singleInstance = false )
    {
        return $this->config && !$singleInstance ? $this->config : $this->createConfig( $singleInstance );
    }
}