<?php


class ModelAppCorePath
{

    /**
     * @var AppCoreModel
     */
    private $model;

    public function __destruct()
    {
        $this->model = null;
    }

    /**
     * @param $singleInstance
     * @return AppCoreModel
     */
    private function createModel( $singleInstance )
    {
        $instance = new AppCoreModel();
        $instance->init();
        if( !$singleInstance )
        {
            $this->model = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return AppCoreModel
     */
    public function getModel( $singleInstance = false )
    {
        return $this->model && !$singleInstance ? $this->model : $this->createModel( $singleInstance );
    }
}