<?php


class CommandAppCorePath
{

    /**
     * @var DefineConfigAppCoreCommand
     */
    private $defineConfig;

    /**
     * @var InitializeAppCoreCommand
     */
    private $initialize;

    /**
     * @var LogTimeAppCoreCommand
     */
    private $logTime;

    public function __destruct()
    {
        $this->defineConfig = null;
        $this->initialize = null;
        $this->logTime = null;
    }

    /**
     * @param $singleInstance
     * @return DefineConfigAppCoreCommand
     */
    private function createDefineConfig( $singleInstance )
    {
        $instance = new DefineConfigAppCoreCommand();
        $instance->init();
        if( !$singleInstance )
        {
            $this->defineConfig = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return InitializeAppCoreCommand
     */
    private function createInitialize( $singleInstance )
    {
        $instance = new InitializeAppCoreCommand();
        $instance->init();
        if( !$singleInstance )
        {
            $this->initialize = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return LogTimeAppCoreCommand
     */
    private function createLogTime( $singleInstance )
    {
        $instance = new LogTimeAppCoreCommand();
        $instance->init();
        if( !$singleInstance )
        {
            $this->logTime = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return DefineConfigAppCoreCommand
     */
    public function getDefineConfig( $singleInstance = true )
    {
        return $this->defineConfig && !$singleInstance ? $this->defineConfig : $this->createDefineConfig( $singleInstance );
    }

    /**
     * @param $singleInstance
     * @return InitializeAppCoreCommand
     */
    public function getInitialize( $singleInstance = true )
    {
        return $this->initialize && !$singleInstance ? $this->initialize : $this->createInitialize( $singleInstance );
    }

    /**
     * @param $singleInstance
     * @return LogTimeAppCoreCommand
     */
    public function getLogTime( $singleInstance = true )
    {
        return $this->logTime && !$singleInstance ? $this->logTime : $this->createLogTime( $singleInstance );
    }
}