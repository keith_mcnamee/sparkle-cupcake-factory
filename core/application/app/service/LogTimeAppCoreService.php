<?php


class LogTimeAppCoreService extends CoreBase
{

    /**
     * @var AppCoreModel
     */
    private $coreAppModel;

    public function __destruct()
    {
        $this->coreAppModel = null;

        parent::__destruct();
    }

    public function initTimeLog()
    {
        $this->getCoreAppModel()->setTimeLog( array() );
        $this->addTimeLog( "Init" );
    }


    public function addTimeLog( $logID )
    {
        $infoAppHelper = $this->getCoreApp()->getInfoHelper();
        if( !( $infoAppHelper->isTestServer() || $infoAppHelper->isStagingDirectory() ) )
        {
            return;
        }
        $microTime = $this->microTime();
        $this->getCoreAppModel()->setTimeLog( CoreHelper::getArrayHelper()->setObjectForKey( $this->getCoreAppModel()->getTimeLog(), $microTime, $logID ) );
    }

    private function microTime()
    {
        list($usec, $sec) = explode( " ", microtime() );

        return ( (float)$usec + (float)$sec );
    }

    /**
     * @return AppCoreModel
     */
    private function getCoreAppModel()
    {
        if( !$this->coreAppModel )
        {

            $this->coreAppModel = $this->getCoreApp()->getAppModel();
        }
        return $this->coreAppModel;
    }
} 