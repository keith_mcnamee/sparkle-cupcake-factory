<?php


class InfoAppCoreService extends CoreBase
{

    /**
     * @var AppCoreModel
     */
    private $coreAppModel;

    /**
     * @var RequestCoreModel
     */
    private $coreRequestModel;

    public function init()
    {
        parent::init();

        $this->coreAppModel = $this->getCoreApp()->getModel()->getModel();
        $this->coreRequestModel = $this->getCoreRequest()->getModel()->getModel();
    }

    public function __destruct()
    {
        $this->coreAppModel = null;
        $this->coreRequestModel = null;

        parent::__destruct();
    }

    /**
     * @return bool
     */
    public function isTestServer()
    {
        $serverName = CoreRequest::getService()->serverName();
        $isTest = $this->coreAppModel->getConfigVO()->hasTestServerName( $serverName );

        return $isTest;
    }

    /**
     * @return bool
     */
    public function isStagingDirectory()
    {
        $applicationDirectoryPaths = CoreRequest::getService()->applicationDirectoryPaths( true );

        $stagingDirectories = $this->coreRequestModel->getServerConfigVO()->getStagingDirectories();
        $isTest = false;
        foreach( $stagingDirectories as $stagingDirectory )
        {
            /* @var $stagingDirectory StagingDirectoryConfigRequestCoreVO */
            if( isset( $applicationDirectoryPaths[ $stagingDirectory->getDirectoryIndex() ] ) && $applicationDirectoryPaths[ $stagingDirectory->getDirectoryIndex() ] == $stagingDirectory->getDirectoryName() )
            {
                $isTest = true;
            }
        }

        return $isTest;
    }
} 