<?php


class ConfigAppCoreService extends CoreBase
{
    /**
     * @param $configType
     * @return ConfigAppCoreVO
     */
    public function query( $configType )
    {
        $allVOs = $this->getCoreApp()->getConfig()->getConfig()->getConfigVOs();

        foreach( $allVOs as $vo )
        {
            /* @var $vo ConfigAppCoreVO */
            if( $vo->getConfigType() == $configType )
            {
                return $vo;
            }
        }
        return null;
    }
} 