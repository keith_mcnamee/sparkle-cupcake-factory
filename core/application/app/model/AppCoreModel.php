<?php


class AppCoreModel extends CoreBase
{

    /**
     * @var ConfigAppCoreVO
     */
    private $configVO;

    /**
     * @var array
     */
    private $timeLog;

    /**
     * @return ConfigAppCoreVO
     */
    public function getConfigVO()
    {
        return $this->configVO;
    }

    /**
     * @param ConfigAppCoreVO $value
     */
    public function setConfigVO( ConfigAppCoreVO $value )
    {
        $this->configVO = $value;
    }

    /**
     * @return array
     */
    public function getTimeLog()
    {
        return $this->timeLog;
    }

    /**
     * @param array $value
     */
    public function setTimeLog( $value )
    {
        $this->timeLog = $value;
    }
} 