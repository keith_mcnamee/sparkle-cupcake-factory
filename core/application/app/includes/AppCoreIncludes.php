<?php

require_once( DIR_CORE . "/CoreBase.php" );
require_once( DIR_CORE . "/application/ApplicationCorePath.php" );
require_once( DIR_CORE . "/application/app/AppCorePath.php" );

require_once( DIR_CORE . "/application/app/command/InitializeAppCoreCommand.php" );

require_once( DIR_CORE . "/application/app/component/sort/SortComponent.php" );
require_once( DIR_CORE . "/application/app/component/sort/vos/SortComponentKeyVO.php" );

require_once( DIR_CORE . "/application/app/config/AppCoreConfig.php" );

require_once( DIR_CORE . "/application/app/constants/AppCoreConstants.php" );
require_once( DIR_CORE . "/application/app/constants/ResultAppCoreConstants.php" );

require_once( DIR_CORE . "/application/app/helper/ConfigAppCoreHelper.php" );
require_once( DIR_CORE . "/application/app/helper/InfoAppCoreHelper.php" );

require_once( DIR_CORE . "/application/app/model/AppCoreModel.php" );

require_once( DIR_CORE . "/application/app/service/LogTimeAppCoreService.php" );

require_once( DIR_CORE . "/application/app/vos/ConfigAppCoreVO.php" );
require_once( DIR_CORE . "/application/app/vos/ReferenceVO.php" );

require_once( DIR_CORE . "/application/helper/CoreHelper.php" );
require_once( DIR_CORE . "/application/localization/CoreLocalization.php" );
require_once( DIR_CORE . "/application/request/CoreRequest.php" );

