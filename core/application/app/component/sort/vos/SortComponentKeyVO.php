<?php


class SortComponentKeyVO
{

    /**
     * @var string
     */
    private $name;

    /**
     * @var array
     */
    private $properties;

    /**
     * @var array
     */
    private $values;


    /**
     * @param string $name
     * @param array $properties
     */
    public function __construct( $name, array $properties = null )
    {
        $this->setName( $name );

        $this->setProperties( array() );
        $this->setValues( array() );

        if( $properties )
        {
            foreach( $properties as $property )
            {
                /* @var $property int */
                $this->addProperty( $property );
            }
        }
    }

    /**
     * @param int $value
     */
    public function addProperty( $value )
    {
        foreach( $this->getProperties() as $property )
        {
            /* @var $property int */
            if( $property == $value )
            {
                return;
            }
        }
        $this->setProperties( CoreHelper::getArrayHelper()->addObject( $this->getProperties(), $value ) );
    }

    /**
     * @param mixed $value
     */
    public function addValue( $value )
    {
        $this->setValues( CoreHelper::getArrayHelper()->addObject( $this->getValues(), $value ) );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $value
     */
    public function setName( $value )
    {
        $this->name = $value;
    }

    /**
     * @return array
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * @param array $value
     */
    public function setProperties( array $value )
    {
        $this->properties = $value;
    }

    /**
     * @return array
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * @param array $value
     */
    public function setValues( array $value )
    {
        $this->values = $value;
    }
} 