<?php


class SortComponent
{
    private $voName = "vo";
    private $paramName = "param_";

    private $sortType = "array_multisort";
    /**
     * @var array
     */
    private $vosToSort;

    /**
     * @var array
     */
    private $keyVOs;


    public function __construct()
    {
        $this->setVosToSort( array() );
        $this->setKeyVOs( array() );
    }

    /**
     * @return array
     */
    public function sortedVOs()
    {
        $vosToSort = $this->getVosToSort();
        $keyVOs = array();

        foreach( $this->getKeyVOs() as $keyVO )
        {
            /* @var $keyVO SortComponentKeyVO */
            $properties = $keyVO->getProperties();
            if( !empty( $properties ) )
            {
                $keyVOs[] = $keyVO;
            }
        }

        if( empty( $vosToSort ) || empty( $keyVOs ) )
        {
            return $vosToSort;
        }
        $dictionaryArray = array();
        foreach ( $vosToSort as $voIndex => $vo )
        {
            /* @var $vo mixed */
            $dictionary = array();
            $dictionary[ $this->voName ] = $vo;
            foreach( $keyVOs as $keyVO )
            {
                /* @var $keyVO SortComponentKeyVO */
                $dictionary[ $keyVO->getName() ] = $keyVO->getValues()[ $voIndex ];
            }
            $dictionaryArray[] = $dictionary;
        }

        $multiSortParams = array();
        $paramIndex = -1;
        foreach( $keyVOs as $keyVO )
        {
            /* @var $keyVO SortComponentKeyVO */

            $paramIndex ++;
            $paramName = $this->paramName . $paramIndex;
            ${ $paramName } = array();
            $multiSortParams[] = &${ $paramName };
            foreach ( $vosToSort as $voIndex => $vo )
            {
                /* @var $vo mixed */
                $values = $keyVO->getValues();
                $value = $values[ $voIndex ];
                ${ $paramName }[]  = $value;
            }
            foreach ( $keyVO->getProperties() as $property )
            {
                /* @var $property int */
                $paramIndex ++;
                $paramName = $this->paramName . $paramIndex;
                ${ $paramName } = $property;
                $multiSortParams[] = &${ $paramName };
            }
        }
        $multiSortParams[] = &$dictionaryArray;

        call_user_func_array( $this->sortType, $multiSortParams );

        $returnVOs = array();

        foreach( $dictionaryArray as $dictionary )
        {
            /* @var $dictionary array */
            $returnVOs[] = $dictionary[ $this->voName ];

        }

        return $returnVOs;
    }

    /**
     * @param string $keyName
     * @param array $properties
     */
    public function addKey( $keyName, array $properties = null )
    {
        $existing = $this->getKeyVO( $keyName );
        if( !$existing )
        {
            $vo = new SortComponentKeyVO( $keyName, $properties );
            $this->setKeyVOs( CoreHelper::getArrayHelper()->addObject( $this->getKeyVOs(), $vo ) );
        }
        else if( $properties )
        {
            foreach( $properties as $property )
            {
                /* @var $property int */
                $existing->addProperty( $property );
            }
        }
    }


    /**
     * @param mixed $vo
     * @param array $values
     */
    public function addVoValues( $vo, array $values )
    {
        $this->setVosToSort( CoreHelper::getArrayHelper()->addObject( $this->getVosToSort(), $vo ) );
        foreach( $this->getKeyVOs() as $keyVO )
        {
            /* @var $keyVO SortComponentKeyVO */
            if( isset( $values[ $keyVO->getName() ] ) )
            {
                $value = $values[ $keyVO->getName() ];
            }
            else
            {
                $value = null;
            }
            $keyVO->addValue( $value );
        }
    }

    /**
     * @param string $keyName
     * @param int $property
     */
    public function addKeyProperty( $keyName, $property )
    {
        $existing = $this->getKeyVO( $keyName );
        if( !$existing )
        {
            return;
        }
        $existing->addProperty( $property );
    }

    /**
     * @param string $keyName
     * @return SortComponentKeyVO
     */
    public function getKeyVO( $keyName )
    {
        foreach( $this->getKeyVOs() as $keyVO )
        {
            /* @var $keyVO SortComponentKeyVO */
            if( $keyVO->getName() == $keyName )
            {
                return $keyVO;
            }
        }
        return null;
    }

    /**
     * @return array
     */
    public function getVosToSort()
    {
        return $this->vosToSort;
    }

    /**
     * @param array $value
     */
    public function setVosToSort( $value )
    {
        $this->vosToSort = $value;
    }

    /**
     * @return array
     */
    public function getKeyVOs()
    {
        return $this->keyVOs;
    }

    /**
     * @param array $value
     */
    public function setKeyVOs( array $value )
    {
        $this->keyVOs = $value;
    }
} 