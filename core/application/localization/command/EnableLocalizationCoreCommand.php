<?php


class EnableLocalizationCoreCommand extends CoreBase
{

    /**
     * @var LocalizationCoreModel
     */
    private $coreLocalizationModel;

    public function init()
    {
        parent::init();

        $this->coreLocalizationModel = $this->getCoreLocalization()->getModel()->getModel();
    }

    public function __destruct()
    {
        $this->coreLocalizationModel = null;

        parent::__destruct();
    }

    public function command()
    {
        if( $this->getCoreLocalization()->getModel()->getModel()->isEnabled() )
        {
            return;
        }

        $this->initializeType();
        $this->initializeFileInfo();
        $this->parseFile();
        $this->coreLocalizationModel->setEnabled( true );
    }

    private function initializeType()
    {
        $this->getCoreLocalization()->getModel()->getModel()->setCurrentLocalizationType( $this->getCoreApp()->getModel()->getModel()->getConfigVO()->getDefaultLocalizationType() );
    }

    private function initializeFileInfo()
    {
        $defaultLocalizationType = $this->coreLocalizationModel->getCurrentLocalizationType();
        $defaultFileInfoVO = $this->getCoreLocalizationDatabase()->getRequest()->getRequest()->fileInfoRequestType( $defaultLocalizationType );
        $defaultFileInfoVO = $defaultFileInfoVO ? $defaultFileInfoVO : new FileInfoLocalizationCoreVO();
        $this->coreLocalizationModel->addFileInfoVO( $defaultFileInfoVO, $defaultFileInfoVO->getLanguage() );
    }

    private function parseFile()
    {
        $fileInfoVO = $this->coreLocalizationModel->getCurrentFileInfoVO();
        if( $fileInfoVO )
        {
            $fileLocation = $this->coreLocalizationModel->getCurrentFileInfoVO()->getFileLocation();
            $translationDictionary = $this->getCoreLocalization()->getParse()->getParse(true)->parse( $fileLocation );
        }
        else
        {
            $translationDictionary = array();
        }
        $this->coreLocalizationModel->addTranslationDictionary( $translationDictionary, $this->coreLocalizationModel->getCurrentLocalizationType() );
    }
} 