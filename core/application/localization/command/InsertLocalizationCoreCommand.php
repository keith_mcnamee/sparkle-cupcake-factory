<?php


class InsertLocalizationCoreCommand extends CoreBase
{

    public function command( array $configVOs )
    {
        if( empty( $configVOs ) )
        {
            $this->getDebugCommand()->trace( DebugViewText::$NO_LOCALIZATION_TO_UPDATE, true );
            return;
        }

        $insertVOs = array();

        $coreDatabaseModel = $this->getCoreDatabase()->getModel()->getModel();
        $coreLocalizationModel = $this->getCoreLocalization()->getModel()->getModel();
        $localizationConstructor =  $this->getCoreLocalizationDatabase()->getConstruct()->getConstruct();
        foreach( $configVOs as $language => $configVO )
        {
            /* @var $configVO FileInfoLocalizationCoreVO */

            $currentVO = $this->getCoreLocalizationDatabase()->getRequest()->getRequest()->fileInfoRequestType( $configVO->getLanguage() );

            $version = $currentVO ? $currentVO->getVersion() : 0;
            $configVersion = (float)CoreHelper::getVersionService()->validVersion( $version );
            $currentVersion = $currentVO ? (float)CoreHelper::getVersionService()->validVersion( $version ) : 0;

            if( $configVersion && $configVersion < $currentVersion )
            {
                $coreLocalizationModel->setProcessFailMessage( DebugViewText::$TOO_LOW_LOCALIZATION_VERSION_FOR." ".$currentVO->getVersion() );
                return;
            }
            $versionChanged = $configVersion > $currentVersion;

            if ( !CoreHelper::getFileService()->fileContentExists( $configVO->getFileLocation() ) )
            {
                $coreLocalizationModel->setProcessFailMessage(DebugViewText::$INVALID_LOCALIZATION_CONTENT_FOR." ".$currentVO->getVersion() );
                return;
            }

            $lastModifiedDate = CoreHelper::getFileService()->lastModifiedDate( $configVO->getFileLocation() );
            $fileSize = CoreHelper::getFileService()->fileSize( $configVO->getFileLocation() );
            $contentChanged = !$currentVO || $lastModifiedDate != $currentVO->getLastModifiedDate() || $fileSize != $currentVO->getFileSize();

            $locationChanged = !$currentVO || CoreHelper::getStringService()->stripSanitization( $configVO->getFileLocation() ) != CoreHelper::getStringService()->stripSanitization( $currentVO->getFileLocation() );

            if( ! ( $versionChanged  || $contentChanged  || $locationChanged ) )
            {
                $this->getDebugCommand()->traceValues( array( DebugViewText::$LOCALIZATION_EXISTS_FOR, $configVO->getLanguage() ), true );
                continue;
            }

            if( $versionChanged )
            {
                $version = CoreHelper::getVersionService()->validVersion( $configVersion );
            }
            else
            {
                $version = CoreHelper::getVersionService()->nextVersion( $currentVersion );
            }

            if( !$locationChanged )
            {
                $currentVO->setVersion( $version );
                $currentVO->setFileLocation( $configVO->getFileLocation() );
                $currentVO->setLastModifiedDate( $lastModifiedDate );
                $currentVO->setFileSize( $fileSize );
                $updateVO = $localizationConstructor->update( $currentVO );
                $coreDatabaseModel->getQueueVO()->addUpdateVO( $updateVO );
                continue;
            }
            $configVO->setVersion( $version );
            $configVO->setLastModifiedDate( $lastModifiedDate );
            $configVO->setFileSize( $fileSize );
            $insertVOs[] = $configVO;
        }

        if( !empty( $insertVOs ) )
        {
            $insertVO = $localizationConstructor->insert( $insertVOs );
            $coreDatabaseModel->getQueueVO()->addInsertVO( $insertVO );
        }
    }
}