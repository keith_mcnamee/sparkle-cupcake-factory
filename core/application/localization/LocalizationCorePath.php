<?php


class LocalizationCorePath
{

    /**
     * @var LocalizationCoreModel
     */
    private $localizationModel;

    public function __construct()
    {
        $this->getIncludes();
    }

    public function __destruct()
    {
        $this->localizationModel = null;
    }

    private function getIncludes()
    {
        require_once( DIR_CORE . "/application/localization/includes/LocalizationCoreIncludes.php" );
    }

    /**
     * @return LocalizationCoreModel
     */
    public function getLocalizationModel()
    {
        if( !$this->localizationModel )
        {
            $this->localizationModel = new LocalizationCoreModel();
        }
        return $this->localizationModel;
    }

    /**
     * @return LocalizationCoreHelper
     */
    public function getLocalizationHelper()
    {
        return new LocalizationCoreHelper();
    }

    /**
     * @return LocalizationCoreParser
     */
    public function getLocalizationParser()
    {
        return new LocalizationCoreParser();
    }

    /**
     * @return EnableLocalizationCoreService
     */
    public function getEnableService()
    {
        return new EnableLocalizationCoreService();
    }

    /**
     * @return InsertLocalizationCoreService
     */
    public function getInsertService()
    {
        return new InsertLocalizationCoreService();
    }
}