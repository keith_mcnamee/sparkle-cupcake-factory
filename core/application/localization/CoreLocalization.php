<?php


class CoreLocalization 
{

    /**
     * @var LocalizationCoreHelper
     */
    private static $localizationHelper;

    /**
     * @return LocalizationCorePath
     */
    private static function localizationPath()
    {
        return CorePath::getInstance()->getApplication()->getLocalization();
    }

    /**
     * @return LocalizationCoreHelper
     */
    public static function getLocalizationHelper()
    {
        if( !self::$localizationHelper )
        {
            self::$localizationHelper = self::localizationPath()->getLocalizationHelper();
        }
        return self::$localizationHelper;
    }
    
}