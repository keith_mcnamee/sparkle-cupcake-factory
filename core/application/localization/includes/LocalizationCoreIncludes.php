<?php

require_once( DIR_CORE . "/application/localization/helper/LocalizationCoreHelper.php" );

require_once( DIR_CORE . "/application/localization/parse/LocalizationCoreParser.php" );

require_once( DIR_CORE . "/application/localization/model/LocalizationCoreModel.php" );

require_once( DIR_CORE . "/application/localization/service/EnableLocalizationCoreService.php" );
require_once( DIR_CORE . "/application/localization/service/InsertLocalizationCoreService.php" );

require_once( DIR_CORE . "/application/localization/vos/FileInfoLocalizationCoreVO.php" );