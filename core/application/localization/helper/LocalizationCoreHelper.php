<?php


class LocalizationCoreHelper extends CoreBase
{

    /**
     * @var LocalizationCoreModel
     */
    private $coreLocalizationModel;

    public function __destruct()
    {
        $this->coreLocalizationModel = null;

        parent::__destruct();
    }

    /**
     * @param string $textID
     * @param string $language
     * @return string
     */
    public function localize($textID, $language = null)
    {
        if( !$language )
        {
            $language = $this->getCoreLocalizationModel()->getCurrentLocalizationType();
        }
        $translationDictionary = $this->getCoreLocalizationModel()->getTranslationDictionary( $language );
        if( !$translationDictionary )
        {
            return "";
        }
        if( isset( $translationDictionary[$textID] ) )
        {
            return $translationDictionary[$textID];
        }
        return $textID;
    }

    /**
     * @return LocalizationCoreModel
     */
    private function getCoreLocalizationModel()
    {
        if( !$this->coreLocalizationModel )
        {
            $this->coreLocalizationModel = $this->getCoreLocalization()->getLocalizationModel();
        }
        return $this->coreLocalizationModel;
    }
} 