<?php


class LocalizationCoreParser extends CoreBase
{

    /**
     * @param string $fileLocation
     * @return array
     */
    public function parse( $fileLocation )
    {
        $fileContent = $this->fileContent( $fileLocation );
        $translations = $this->parseContent( $fileContent );
        return $translations;
    }

    /**
     * @param string $fileLocation
     * @return string
     */
    private function fileContent( $fileLocation )
    {
        $content = file_get_contents( $fileLocation );
        return $content;
    }

    /**
     * @param string $fileContent
     * @return array
     */
    private function parseContent( $fileContent )
    {
        preg_match_all('~^\s*"((?:\\\\.|[^"])*)"[^"]+"((?:\\\\.|[^"])*)"~m', $fileContent, $matches, PREG_SET_ORDER);
        $translations = array();
        foreach($matches as $match)
        {
            $translationID = $match[ 1 ];
            $translatedValue = $match[ 2 ];
            $translatedValue = CoreHelper::getStringHelper()->sanitizeInput( $translatedValue );
            $translations[ $translationID ] = $translatedValue;
        }
        return $translations;
    }
} 