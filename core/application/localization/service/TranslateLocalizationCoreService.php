<?php


class TranslateLocalizationCoreService extends CoreBase
{

    /**
     * @var LocalizationCoreModel
     */
    private $coreLocalizationModel;

    public function init()
    {
        parent::init();

        $this->coreLocalizationModel = $this->getCoreLocalization()->getModel()->getModel();
    }

    public function __destruct()
    {
        $this->coreLocalizationModel = null;

        parent::__destruct();
    }

    /**
     * @param string $textID
     * @param string $language
     * @return string
     */
    public function query($textID, $language = null)
    {
        if( !$language )
        {
            $language = $this->coreLocalizationModel->getCurrentLocalizationType();
        }
        $translationDictionary = $this->coreLocalizationModel->getTranslationDictionary( $language );
        if( !$translationDictionary )
        {
            return "";
        }
        if( isset( $translationDictionary[$textID] ) )
        {
            return $translationDictionary[$textID];
        }
        return $textID;
    }
} 