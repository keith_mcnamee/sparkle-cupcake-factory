<?php


class EnableLocalizationCoreService extends CoreBase
{

    /**
     * @var LocalizationCoreModel
     */
    private $coreLocalizationModel;

    public function __destruct()
    {
        $this->coreLocalizationModel = null;

        parent::__destruct();
    }

    public function enable()
    {
        if( $this->getCoreLocalization()->getLocalizationModel()->isEnabled() )
        {
            return;
        }

        $this->initializeType();
        $this->initializeFileInfo();
        $this->parseFile();
        $this->getCoreLocalizationModel()->setEnabled( true );
    }

    private function initializeType()
    {
        $this->getCoreLocalization()->getLocalizationModel()->setCurrentLocalizationType( $this->getCoreApp()->getAppModel()->getConfigVO()->getDefaultLocalizationType() );
    }

    private function initializeFileInfo()
    {
        $defaultLocalizationType = $this->getCoreLocalizationModel()->getCurrentLocalizationType();
        $defaultFileInfoVO = $this->getCoreLocalizationDatabase()->getLocalizationRequest()->fileInfoRequestType( $defaultLocalizationType );
        $defaultFileInfoVO = $defaultFileInfoVO ? $defaultFileInfoVO : new FileInfoLocalizationCoreVO();
        $this->getCoreLocalizationModel()->addFileInfoVO( $defaultFileInfoVO, $defaultFileInfoVO->getLanguage() );
    }

    private function parseFile()
    {
        $fileInfoVO = $this->getCoreLocalizationModel()->getCurrentFileInfoVO();
        if( $fileInfoVO )
        {
            $fileLocation = $this->getCoreLocalizationModel()->getCurrentFileInfoVO()->getFileLocation();
            $translationDictionary = $this->getCoreLocalization()->getLocalizationParser()->parse( $fileLocation );
        }
        else
        {
            $translationDictionary = array();
        }
        $this->getCoreLocalizationModel()->addTranslationDictionary( $translationDictionary, $this->getCoreLocalizationModel()->getCurrentLocalizationType() );
    }

    /**
     * @return LocalizationCoreModel
     */
    private function getCoreLocalizationModel()
    {
        if( !$this->coreLocalizationModel )
        {
            $this->coreLocalizationModel = $this->getCoreLocalization()->getLocalizationModel();
        }
        return $this->coreLocalizationModel;
    }
} 