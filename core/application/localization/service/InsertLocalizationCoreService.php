<?php


class InsertLocalizationCoreService extends CoreBase
{

    /**
     * @var string
     */
    private $processFailMessage;

    public function insert( array $configVOs )
    {
        if( empty( $configVOs ) )
        {
            $this->getDebugService()->trace( DebugViewText::$NO_LOCALIZATION_TO_UPDATE, true );
            return;
        }

        $insertVOs = array();

        $coreDatabaseModel = $this->getCoreDatabase()->getDatabaseModel();
        $coreLocalizationModel = $this->getCoreLocalization()->getLocalizationModel();
        $localizationConstructor =  $this->getCoreLocalizationDatabase()->getLocalizationConstructor();
        foreach( $configVOs as $language => $configVO )
        {
            /* @var $configVO FileInfoLocalizationCoreVO */

            $currentVO = $this->getCoreLocalizationDatabase()->getLocalizationRequest()->fileInfoRequestType( $configVO->getLanguage() );

            $version = $currentVO ? $currentVO->getVersion() : 0;
            $configVersion = (float)CoreHelper::getVersionHelper()->validVersion( $version );
            $currentVersion = $currentVO ? (float)CoreHelper::getVersionHelper()->validVersion( $version ) : 0;

            if( $configVersion && $configVersion < $currentVersion )
            {
                $this->processFailMessage = DebugViewText::$TOO_LOW_LOCALIZATION_VERSION_FOR." ".$currentVO->getVersion();
                return;
            }
            $versionChanged = $configVersion > $currentVersion;

            if ( !CoreHelper::getFileHelper()->fileContentExists( $configVO->getFileLocation() ) )
            {
                $this->processFailMessage = DebugViewText::$INVALID_LOCALIZATION_CONTENT_FOR." ".$currentVO->getVersion();
                return;
            }

            $lastModifiedDate = CoreHelper::getFileHelper()->lastModifiedDate( $configVO->getFileLocation() );
            $fileSize = CoreHelper::getFileHelper()->fileSize( $configVO->getFileLocation() );
            $contentChanged = !$currentVO || $lastModifiedDate != $currentVO->getLastModifiedDate() || $fileSize != $currentVO->getFileSize();

            $locationChanged = !$currentVO || CoreHelper::getStringHelper()->stripSanitization( $configVO->getFileLocation() ) != CoreHelper::getStringHelper()->stripSanitization( $currentVO->getFileLocation() );

            if( ! ( $versionChanged  || $contentChanged  || $locationChanged ) )
            {
                $this->getDebugService()->traceValues( array( DebugViewText::$LOCALIZATION_EXISTS_FOR, $configVO->getLanguage() ), true );
                continue;
            }

            if( $versionChanged )
            {
                $version = CoreHelper::getVersionHelper()->validVersion( $configVersion );
            }
            else
            {
                $version = CoreHelper::getVersionHelper()->nextVersion( $currentVersion );
            }

            if( !$locationChanged )
            {
                $currentVO->setVersion( $version );
                $currentVO->setFileLocation( $configVO->getFileLocation() );
                $currentVO->setLastModifiedDate( $lastModifiedDate );
                $currentVO->setFileSize( $fileSize );
                $updateVO = $localizationConstructor->update( $currentVO );
                $coreDatabaseModel->getQueueVO()->addUpdateVO( $updateVO );
                continue;
            }
            $configVO->setVersion( $version );
            $configVO->setLastModifiedDate( $lastModifiedDate );
            $configVO->setFileSize( $fileSize );
            $insertVOs[] = $configVO;
        }

        if( !empty( $insertVOs ) )
        {
            $insertVO = $localizationConstructor->insert( $insertVOs );
            $coreDatabaseModel->getQueueVO()->addInsertVO( $insertVO );
        }
    }

    /**
     * @return string
     */
    public function getProcessFailMessage()
    {
        return $this->processFailMessage;
    }
}