<?php


class ParseLocalizationCorePath
{

    /**
     * @var LocalizationCoreParser
     */
    private $parse;

    public function __destruct()
    {
        $this->parse = null;
    }

    /**
     * @param $singleInstance
     * @return LocalizationCoreParser
     */
    private function createParse( $singleInstance )
    {
        $instance = new LocalizationCoreParser();
        $instance->init();
        if( !$singleInstance )
        {
            $this->parse = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return LocalizationCoreParser
     */
    public function getParse( $singleInstance = true )
    {
        return $this->parse && !$singleInstance ? $this->parse : $this->createParse( $singleInstance );
    }
}