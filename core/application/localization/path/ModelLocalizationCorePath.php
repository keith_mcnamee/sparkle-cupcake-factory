<?php


class ModelLocalizationCorePath
{

    /**
     * @var LocalizationCoreModel
     */
    private $model;

    public function __destruct()
    {
        $this->model = null;
    }

    /**
     * @param $singleInstance
     * @return LocalizationCoreModel
     */
    private function createModel( $singleInstance )
    {
        $instance = new LocalizationCoreModel();
        $instance->init();
        if( !$singleInstance )
        {
            $this->model = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return LocalizationCoreModel
     */
    public function getModel( $singleInstance = false )
    {
        return $this->model && !$singleInstance ? $this->model : $this->createModel( $singleInstance );
    }
}