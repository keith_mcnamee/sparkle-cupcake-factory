<?php


class CommandLocalizationCorePath
{

    /**
     * @var EnableLocalizationCoreCommand
     */
    private $enable;

    /**
     * @var InsertLocalizationCoreCommand
     */
    private $insert;

    public function __destruct()
    {
        $this->enable = null;
        $this->insert = null;
    }

    /**
     * @param $singleInstance
     * @return EnableLocalizationCoreCommand
     */
    private function createEnable( $singleInstance )
    {
        $instance = new EnableLocalizationCoreCommand();
        $instance->init();
        if( !$singleInstance )
        {
            $this->enable = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return InsertLocalizationCoreCommand
     */
    private function createInsert( $singleInstance )
    {
        $instance = new InsertLocalizationCoreCommand();
        $instance->init();
        if( !$singleInstance )
        {
            $this->insert = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return EnableLocalizationCoreCommand
     */
    public function getEnable( $singleInstance = true )
    {
        return $this->enable && !$singleInstance ? $this->enable : $this->createEnable( $singleInstance );
    }

    /**
     * @param $singleInstance
     * @return InsertLocalizationCoreCommand
     */
    public function getInsert( $singleInstance = true )
    {
        return $this->insert && !$singleInstance ? $this->insert : $this->createInsert( $singleInstance );
    }
}