<?php


class ServiceLocalizationCorePath
{

    /**
     * @var TranslateLocalizationCoreService
     */
    private $translate;

    public function __destruct()
    {
        $this->translate = null;
    }

    /**
     * @param $singleInstance
     * @return TranslateLocalizationCoreService
     */
    private function createTranslate( $singleInstance )
    {
        $instance = new TranslateLocalizationCoreService();
        $instance->init();
        if( !$singleInstance )
        {
            $this->translate = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return TranslateLocalizationCoreService
     */
    public function getTranslate( $singleInstance = true )
    {
        return $this->translate && !$singleInstance ? $this->translate : $this->createTranslate( $singleInstance );
    }
}