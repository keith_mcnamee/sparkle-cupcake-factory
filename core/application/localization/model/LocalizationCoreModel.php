<?php


class LocalizationCoreModel extends CoreBase
{

    /**
     * @var string
     */
    private $currentLocalizationType;

    /**
     * @var array
     */
    private $fileInfoVOs;

    /**
     * @var array
     */
    private $translations;

    /**
     * @var boolean
     */
    private $enabled;

    public function __construct()
    {
        $this->setFileInfoVOs( array() );
        $this->setTranslations( array() );
    }

    /**
     * @return string
     */
    public function getCurrentLocalizationType()
    {
        return $this->currentLocalizationType;
    }

    /**
     * @param string $value
     */
    public function setCurrentLocalizationType( $value )
    {
        $this->currentLocalizationType = $value;
    }

    /**
     * @return FileInfoLocalizationCoreVO
     */
    public function getCurrentFileInfoVO()
    {
        $localizationType = $this->getCurrentLocalizationType();
        $fileInfoVO = $this->getFileInfoVO( $localizationType );

        return $fileInfoVO;
    }

    /**
     * @param FileInfoLocalizationCoreVO $object
     * @param int $key
     */
    public function addFileInfoVO( FileInfoLocalizationCoreVO $object, $key )
    {
        $this->setFileInfoVOs( CoreHelper::getArrayHelper()->setObjectForKey( $this->getFileInfoVOs(), $object, $key ) );
    }

    /**
     * @param int $key
     * @return FileInfoLocalizationCoreVO
     */
    public function getFileInfoVO( $key )
    {
        $object = CoreHelper::getArrayHelper()->getObjectForKey( $this->getFileInfoVOs(), $key );

        return $object;
    }

    /**
     * @return array
     */
    public function getFileInfoVOs()
    {
        return $this->fileInfoVOs;
    }

    /**
     * @param array $value
     */
    public function setFileInfoVOs( array $value )
    {
        $this->fileInfoVOs = $value;
    }

    /**
     * @return array
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * @param array $value
     */
    public function setTranslations( $value )
    {
        $this->translations = $value;
    }

    /**
     * @param array $object
     * @param int $key
     */
    public function addTranslationDictionary( array $object, $key )
    {
        $this->setTranslations( CoreHelper::getArrayHelper()->setObjectForKey( $this->getTranslations(), $object, $key ) );
    }

    /**
     * @return array
     */
    public function getCurrentTranslationDictionary()
    {
        $localizationType = $this->getCurrentLocalizationType();
        $translationDictionary = $this->getTranslationDictionary( $localizationType );

        return $translationDictionary;
    }

    /**
     * @param int $key
     * @return array
     */
    public function getTranslationDictionary( $key )
    {
        $object = CoreHelper::getArrayHelper()->getObjectForKey( $this->getTranslations(), $key );

        return $object;
    }

    /**
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param boolean $value
     */
    public function setEnabled( $value )
    {
        $this->enabled = $value;
    }
} 