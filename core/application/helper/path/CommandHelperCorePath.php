<?php


class CommandHelperCorePath
{

    /**
     * @var ArrayHelperCommand
     */
    private $array;

    public function __destruct()
    {
        $this->array = null;
    }

    /**
     * @param $singleInstance
     * @return ArrayHelperCommand
     */
    private function createArray( $singleInstance )
    {
        $instance = new ArrayHelperCommand();
        $instance->init();
        if( !$singleInstance )
        {
            $this->array = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return ArrayHelperCommand
     */
    public function getArray( $singleInstance = true )
    {
        return $this->array && !$singleInstance ? $this->array : $this->createArray( $singleInstance );
    }
}