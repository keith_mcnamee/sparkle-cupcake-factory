<?php


class ServiceHelperCorePath
{

    /**
     * @var ArrayHelperService
     */
    private $array;

    /**
     * @var DateHelperService
     */
    private $date;

    /**
     * @var FileHelperService
     */
    private $file;

    /**
     * @var IdHelperService
     */
    private $id;

    /**
     * @var ReferenceHelperService
     */
    private $reference;

    /**
     * @var StringHelperService
     */
    private $string;

    /**
     * @var VersionHelperService
     */
    private $version;

    public function __destruct()
    {
        $this->array = null;
        $this->date = null;
        $this->file = null;
        $this->id = null;
        $this->reference = null;
        $this->string = null;
        $this->version = null;
    }

    /**
     * @param $singleInstance
     * @return ArrayHelperService
     */
    private function createArray( $singleInstance )
    {
        $instance = new ArrayHelperService();
        $instance->init();
        if( !$singleInstance )
        {
            $this->array = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return DateHelperService
     */
    private function createDate( $singleInstance )
    {
        $instance = new DateHelperService();
        $instance->init();
        if( !$singleInstance )
        {
            $this->date = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return FileHelperService
     */
    private function createFile( $singleInstance )
    {
        $instance = new FileHelperService();
        $instance->init();
        if( !$singleInstance )
        {
            $this->file = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return IdHelperService
     */
    private function createId( $singleInstance )
    {
        $instance = new IdHelperService();
        $instance->init();
        if( !$singleInstance )
        {
            $this->id = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return ReferenceHelperService
     */
    private function createReference( $singleInstance )
    {
        $instance = new ReferenceHelperService();
        $instance->init();
        if( !$singleInstance )
        {
            $this->reference = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return StringHelperService
     */
    private function createString( $singleInstance )
    {
        $instance = new StringHelperService();
        $instance->init();
        if( !$singleInstance )
        {
            $this->string = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return VersionHelperService
     */
    private function createVersion( $singleInstance )
    {
        $instance = new VersionHelperService();
        $instance->init();
        if( !$singleInstance )
        {
            $this->version = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return ArrayHelperService
     */
    public function getArray( $singleInstance = true )
    {
        return $this->array && !$singleInstance ? $this->array : $this->createArray( $singleInstance );
    }

    /**
     * @param $singleInstance
     * @return DateHelperService
     */
    public function getDate( $singleInstance = true )
    {
        return $this->date && !$singleInstance ? $this->date : $this->createDate( $singleInstance );
    }

    /**
     * @param $singleInstance
     * @return FileHelperService
     */
    public function getFile( $singleInstance = true )
    {
        return $this->file && !$singleInstance ? $this->file : $this->createFile( $singleInstance );
    }

    /**
     * @param $singleInstance
     * @return IdHelperService
     */
    public function getId( $singleInstance = true )
    {
        return $this->id && !$singleInstance ? $this->id : $this->createId( $singleInstance );
    }

    /**
     * @param $singleInstance
     * @return ReferenceHelperService
     */
    public function getReference( $singleInstance = true )
    {
        return $this->reference && !$singleInstance ? $this->reference : $this->createReference( $singleInstance );
    }

    /**
     * @param $singleInstance
     * @return StringHelperService
     */
    public function getString( $singleInstance = true )
    {
        return $this->string && !$singleInstance ? $this->string : $this->createString( $singleInstance );
    }

    /**
     * @param $singleInstance
     * @return VersionHelperService
     */
    public function getVersion( $singleInstance = true )
    {
        return $this->version && !$singleInstance ? $this->version : $this->createVersion( $singleInstance );
    }
}