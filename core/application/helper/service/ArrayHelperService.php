<?php


class ArrayHelperService extends CoreBase
{

    /**
     * @param array $dictionary
     * @param int|string $key
     * @param mixed $default
     * @return mixed
     */
    public function getObjectForKey( array $dictionary, $key, $default = null )
    {
        if ( !$dictionary || !isset( $dictionary[ $key ] ) )
        {
            return $default;
        }

        return $dictionary[ $key ];
    }

    /**
     * @param array $array
     * @return bool
     */
    public function isDictionaryArray( array $array )
    {
        if( !is_array( $array ) )
        {
            return false;
        }

        return array_keys( $array ) !== range(0, count( $array ) - 1);
    }


    /**
     * @param array $array
     * @return mixed
     */
    public function firstObject( array $array )
    {
        ksort( $array );

        foreach ( $array as $value )
        {
            return $value;
        }

        return null;
    }


    /**
     * @param array $array
     * @return mixed
     */
    public function lastObject( array $array )
    {
        krsort( $array );

        foreach ( $array as $value )
        {
            return $value;
        }

        return null;
    }


    /**
     * @param array $array
     * @return mixed
     */
    public function firstKey( array $array )
    {
        ksort( $array );

        foreach ( $array as $key => $value )
        {
            return $key;
        }

        return null;
    }

    /**
     * @param mixed $value
     * @param array $validOptions
     * @return string
     */
    public function validValue( $value, array $validOptions )
    {
        foreach( $validOptions as $validOption )
        {
            /* @var $validOption mixed */
            if( $value == $validOption )
            {
                return true;
            }
        }

        return false;
    }


    /**
     * @param array $array
     * @param mixed $value
     * @return boolean
     */
    public function arrayContains( array $array, $value )
    {
        foreach ( $array as $arrayValue )
        {
            if( $arrayValue == $value )
            {
                return true;
            }
        }

        return false;
    }

    /**
     * @param array $array
     * @param array $rangeOfValues
     * @return boolean
     */
    public function containsAny( array $array, $rangeOfValues )
    {
        foreach ( $array as $arrayValue )
        {
            /* @var $arrayValue mixed */
            foreach ( $rangeOfValues as $value )
            {
                /* @var $value mixed */
                if( $arrayValue == $value )
                {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param array $array
     * @param array $rangeOfValues
     * @return boolean
     */
    public function containsAll( array $array, $rangeOfValues )
    {
        foreach ( $rangeOfValues as $value )
        {
            /* @var $value mixed */
            $found = false;
            foreach ( $array as $arrayValue )
            {
                /* @var $arrayValue mixed */
                if( $arrayValue == $value )
                {
                    $found = true;
                }
            }
            if( !$found )
            {
                return false;
            }
        }

        return true;
    }

    /**
     * @param array $dictionary
     * @param string $key
     * @param int $defaultValue
     * @param boolean $positiveOnly
     * @param boolean $negativeOnly
     * @param boolean $ignoreZero
     * @return int
     */
    public function intFromDictionary( array $dictionary, $key, $defaultValue = 0, $positiveOnly = false, $negativeOnly = false, $ignoreZero = false )
    {
        if( isset($dictionary[$key]) )
        {
            $value = $dictionary[$key];
            if( is_numeric( $value ) && (int)$value == $value )
            {
                if( $ignoreZero && $value == 0 || $positiveOnly && $value < 0 || $negativeOnly && $value > 0 )
                {
                    return $defaultValue;
                }
                return (int)$value;
            }
        }
        return $defaultValue;
    }

    /**
     * @param array $dictionary
     * @param string $key
     * @param boolean $defaultValue
     * @return int
     */
    public function booleanFromDictionary( array $dictionary, $key, $defaultValue = false )
    {
        if( isset($dictionary[$key]) )
        {
            $value = $dictionary[$key];
            return (bool)$value;
        }
        return $defaultValue;
    }

    /**
     * @param array $dictionary
     * @param string $key
     * @param boolean $sanitize
     * @param string $defaultValue
     * @return string
     */
    public function stringFromDictionary( array $dictionary, $key, $sanitize = false, $defaultValue = null )
    {
        if( isset($dictionary[$key]) )
        {
            $value = $dictionary[$key];
            if(is_string( $value ))
            {
                if( $sanitize )
                {
                    $value = CoreHelper::getStringService()->sanitizeInput( $value );
                }
                return (string)$value;
            }
        }
        return $defaultValue;
    }

    /**
     * @param array $dictionary
     * @param string $key
     * @param boolean $sanitize
     * @param mixed $defaultValue
     * @param string $separator
     * @return array
     */
    public function arrayFromDictionaryStringEntry( array $dictionary, $key, $sanitize = false, $defaultValue = null, $separator = "," )
    {
        $string = $this->stringFromDictionary( $dictionary, $key, $sanitize );
        if( !$string )
        {
            return $defaultValue;
        }
        $entries = explode( $separator, $string );
        return $entries;
    }

    /**
     * @param array $dictionary
     * @param string $key
     * @param boolean $sanitize
     * @param mixed $defaultValue
     * @param string $separator
     * @return array
     */
    public function dictionaryFromDictionaryStringEntry( array $dictionary, $key, $sanitize = false, $defaultValue = null, $separator = "," )
    {
        $array = $this->arrayFromDictionaryStringEntry( $dictionary, $key, $sanitize, $defaultValue, $separator );
        if( !$array )
        {
            return $defaultValue;
        }
        $dictionary = CoreHelper::getArrayCommand()->arrayToDictionary( $array );
        return $dictionary;
    }

    /**
     * @param array $dictionary
     * @param boolean $ignoreZero
     * @param boolean $positiveOnly
     * @param boolean $negativeOnly
     * @return array
     */
    public function intKeysOnly( array $dictionary, $ignoreZero = false, $positiveOnly = false, $negativeOnly = false )
    {
        $returnDictionary = array();
        foreach( $dictionary as $key => $value )
        {
            /* @var $value mixed */
            if( is_numeric( $key ) && (int)$key == $key )
            {
                /* @var $key int */
                if( $ignoreZero && $key == 0 || $positiveOnly && $key < 0 || $negativeOnly && $key > 0 )
                {
                    continue;
                }
                $returnDictionary[ $key ] = $value;
            }
        }
        return $returnDictionary;
    }

    /**
     * @param array $array
     * @param boolean $ignoreZero
     * @param boolean $positiveOnly
     * @param boolean $negativeOnly
     * @return array
     */
    public function intArrayValuesOnly( array $array, $ignoreZero = false, $positiveOnly = false, $negativeOnly = false )
    {
        $returnArray = array();
        foreach( $array as $value )
        {
            /* @var $value mixed */
            if( is_numeric( $value ) && (int)$value == $value )
            {
                /* @var $value int */
                if( $ignoreZero && $value == 0  || $positiveOnly && $value < 0 || $negativeOnly && $value > 0 )
                {
                    continue;
                }
                $returnArray[] = $value;
            }
        }
        return $returnArray;
    }
} 