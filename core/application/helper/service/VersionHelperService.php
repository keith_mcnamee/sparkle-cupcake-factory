<?php


class VersionHelperService extends CoreBase
{
    private static $trailingZeros = 2;

    /**
     * @return int
     */
    public function getTrailingZeros()
    {
        return self::$trailingZeros;
    }

    /**
     * @param string $string
     * @return string
     */
    public function validVersion( $string )
    {
        $val = $this->numericVersion( $string );
        $string = sprintf( "%0." . $this->getTrailingZeros() . "f" , $val);
        return $string;
    }

    /**
     * @param $string
     * @return number
     */
    public function numericVersion( $string )
    {
        $val = (int)CoreHelper::getStringService()->numbersOnly( $string );
        $val /= pow( 10, $this->getTrailingZeros() );
        return $val;
    }

    /**
     * @param string $string
     * @return int
     */
    public function nextVersion( $string )
    {
        $val = (int)CoreHelper::getStringService()->numbersOnly( $string );
        $val /= pow( 10, $this->getTrailingZeros() );
        $inc = 1 / pow( 10, $this->getTrailingZeros() );
        $val += $inc;
        $string = sprintf( "%0." . $this->getTrailingZeros() . "f" , $val);
        return $string;
    }
}