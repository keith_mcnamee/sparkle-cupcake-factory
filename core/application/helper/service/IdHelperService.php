<?php


class IdHelperService extends CoreBase
{

    /**
     * @return string
     */
    public function randomID()
    {
        $id = sha1(microtime(true).mt_rand(10000,90000));
        return $id;
    }
}