<?php


class ReferenceHelperService extends CoreBase
{
    /**
     * @param array $vos
     * @return array
     */
    public function refsFromVOs( array $vos )
    {
        $refs = array();
        foreach ( $vos as $vo )
        {
            /* @var $vo ReferenceVO */
            $refs[ ] = $vo->getRef();
        }

        return $refs;
    }


    /**
     * @param array $referenceVOs
     * @param array $refs
     * @return array
     */
    public function getVOsByRefs( array $referenceVOs, array $refs )
    {
        $returnVOs = array();
        foreach( $refs as $ref => $nullValue )
        {
            $returnVO = CoreHelper::getArrayService()->getObjectForKey( $referenceVOs, $ref );
            if( $returnVO )
            {
                /* @var $returnVO ReferenceVO */
                $returnVOs = CoreHelper::getArrayCommand()->setObjectForKey( $returnVOs, $returnVO, $ref );
            }
        }
        return $returnVOs;
    }
} 