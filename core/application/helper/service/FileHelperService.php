<?php


class FileHelperService extends CoreBase
{

    /**
     * @param string $filePath
     * @return int
     */
    public function lastModifiedDate( $filePath )
    {
        $curl = curl_init( $filePath );

        curl_setopt($curl, CURLOPT_NOBODY, true);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($curl, CURLOPT_FILETIME, true);

        $result = curl_exec($curl);

        if ($result === false)
        {
            die (curl_error($curl));
        }

        $timestamp = curl_getinfo($curl, CURLINFO_FILETIME);
        return $timestamp;
    }
    /**
     * @param string $filePath
     * @return int
     */
    public function fileSize( $filePath )
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HEADER, true);

        curl_setopt($ch, CURLOPT_NOBODY, true);

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        curl_setopt($ch, CURLOPT_URL, $filePath);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_exec($ch);
        $size = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
        return $size;
    }

    /**
     * @param string $filePath
     * @return int
     */
    public function fileContentExists( $filePath )
    {
        $content = file_get_contents( $filePath );
        return (boolean)$content;
    }
}