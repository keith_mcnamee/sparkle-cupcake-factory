<?php


class HelperCorePath
{

    public function __construct()
    {
        $this->getIncludes();
    }

    private function getIncludes()
    {
        require_once( DIR_CORE . "/application/helper/includes/HelperCoreIncludes.php" );
    }

    /**
     * @return ArrayHelper
     */
    public function getArrayHelper()
    {
        return new ArrayHelper();
    }

    /**
     * @return DateHelper
     */
    public function getDateHelper()
    {
        return new DateHelper();
    }

    /**
     * @return FileHelper
     */
    public function getFileHelper()
    {
        return new FileHelper();
    }

    /**
     * @return IdHelper
     */
    public function getIdHelper()
    {
        return new IdHelper();
    }

    /**
     * @return ReferenceHelper
     */
    public function getReferenceHelper()
    {
        return new ReferenceHelper();
    }

    /**
     * @return StringHelper
     */
    public function getStringHelper()
    {
        return new StringHelper();
    }

    /**
     * @return VersionHelper
     */
    public function getVersionHelper()
    {
        return new VersionHelper();
    }

}