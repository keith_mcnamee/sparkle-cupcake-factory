<?php


class StringHelper extends CoreBase
{
    /**
     * @param string $oldValue
     * @return int
     */
    public function stripSanitization( $oldValue )
    {
        $newValue = str_replace("\\\\", "\\", $oldValue);
        if( $newValue != $oldValue )
        {
            $returnValue = $this->stripSanitization( $newValue );;
            return $returnValue;
        }

        $newValue = str_replace("\\\"", "\"", $oldValue);
        if( $newValue != $oldValue )
        {
            $returnValue = $this->stripSanitization( $newValue );;
            return $returnValue;
        }

        $newValue = str_replace('\\\'', '\'', $oldValue);
        if( $newValue != $oldValue )
        {
            $returnValue = $this->stripSanitization( $newValue );;
            return $returnValue;
        }

        $newValue = html_entity_decode( $oldValue, ENT_QUOTES );
        if( $newValue != $oldValue )
        {
            $returnValue = $this->stripSanitization( $newValue );;
            return $returnValue;
        }


        return $newValue;
    }

    /**
     * @param string $value
     * @return mixed
     */
    public function sanitizeInput( $value )
    {
        if( is_numeric( $value ) || is_bool( $value ) )
        {
            return $value;
        }
        $value = (string)$value;
        $value = $this->stripSanitization( $value );

        $newValue = htmlspecialchars( $value, ENT_QUOTES );

        return $newValue;
    }

    /**
     * @param string $string
     * @return string
     */
    public function alphaNumLower( $string )
    {
        $string = preg_replace( "/[^A-Za-z0-9]/", '', $string );
        $string = strtolower( $string );

        return $string;
    }

    /**
     * @param string $string
     * @return string
     */
    public function alphaLower( $string )
    {
        $string = preg_replace( "/[^A-Za-z]/", '', $string );
        $string = strtolower( $string );

        return $string;
    }

    /**
     * @param string $string
     * @return string
     */
    public function numbersOnly( $string )
    {
        $string = preg_replace("/[^0-9]/","",$string);

        return $string;
    }

    /**
     * @param $string
     * @param array $splits
     * @return array
     */
    public function subString( $string, array $splits )
    {
        $foundValues = array();
        $index = -1;
        foreach( $splits as $split )
        {
            /* @var $split string */
            $index ++;
            if( $split )
            {
                $subStrings = explode( $split, $string );
                if ( count( $subStrings ) != 2 )
                {
                    return null;
                }
                if( $subStrings[ 0 ] )
                {
                    $foundValues[] = $subStrings[ 0 ];
                }
                if( $subStrings[ 1 ] )
                {
                    if( $index < count( $splits ) - 1 )
                    {
                        $string = $subStrings[ 1 ];
                    }
                    else
                    {
                        $foundValues[] = $subStrings[ 1 ];
                    }
                }
            }
        }
        return $foundValues;
    }
} 