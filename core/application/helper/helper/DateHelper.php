<?php


class DateHelper extends CoreBase
{
    private static $day0 = "0000-00-00 00:00:00";
    private static $day1 = "0000-01-01 00:00:00";
    private static $timeFormat = "Y-m-d H:i:s";

    private static $overrideNowUnix = null;

    /**
     * @param string $value
     */
    public function setOverrideNow( $value )
    {
        self::$overrideNowUnix = $this->unixFromString( $value);
    }

    /**
     * @return string
     */
    public function getStringDay0()
    {
        return self::$day0;
    }

    /**
     * @return string
     */
    public function getStringDay1()
    {
        return self::$day1;
    }

    /**
     * @return string
     */
    public function getTimeFormat()
    {
        return self::$timeFormat;
    }

    /**
     * @return int
     */
    public function oneDay()
    {
        return 60 * 60 * 24;
    }

    /**
     * @param string $stringDate
     * @return int
     */
    public function unixFromString( $stringDate )
    {
        $unixDate = strtotime( $stringDate );

        return $unixDate;
    }

    /**
     * @param int $unixDate
     * @param string $format
     * @return string
     */
    public function stringFromUnix( $unixDate, $format = null )
    {
        $format = $format ? $format : $this->getTimeFormat();

        if( $unixDate < $this->minimumDate() )
        {
            return date( $format, $this->minimumDate() );
        }
        $stringDate = date( $format, $unixDate );

        return $stringDate;
    }

    /**
     * @return int
     */
    public function minimumDate()
    {
        return $this->unixFromString( $this->getStringDay1() );
    }

    /**
     * @return int
     */
    public function maximumDate()
    {
        return PHP_INT_MAX;
    }

    /**
     * @return int
     */
    public function unixDay0()
    {
        return $this->unixFromString( $this->getStringDay0() );
    }

    /**
     * @return int
     */
    public function now()
    {
        $unixDate = self::$overrideNowUnix === null ? time() : self::$overrideNowUnix;

        return $unixDate;
    }

    /**
     * @return string
     */
    public function nowString()
    {
        $stringDate = date( $this->getTimeFormat(), $this->now() );

        return $stringDate;
    }

    /**
     * @param int $date
     * @param string $timezoneWithoutDaylightSavings
     * @return int
     */
    public function dateWithoutDaylightSavings( $date, $timezoneWithoutDaylightSavings = "GMT" )
    {
        $dateString = date( $this->getTimeFormat(), $date );
        $dateWithoutDaylightSavings = strtotime( $dateString . " ".$timezoneWithoutDaylightSavings );

        $daylightSavings = $dateWithoutDaylightSavings - $date;

        $date -= $daylightSavings;

        return $date;
    }

    /**
     * @param int $date
     * @param string $timezoneWithoutDaylightSavings
     * @return int
     */
    public function dateWithDaylightSavings( $date, $timezoneWithoutDaylightSavings = "GMT" )
    {
        $dateString = date( $this->getTimeFormat(), $date );
        $dateWithoutDaylightSavings = strtotime( $dateString . " ".$timezoneWithoutDaylightSavings );

        $daylightSavings = $dateWithoutDaylightSavings - $date;

        $date += $daylightSavings;

        return $date;
    }
} 