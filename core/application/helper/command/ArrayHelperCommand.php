<?php


class ArrayHelperCommand extends CoreBase
{

    /**
     * @param array $dictionary
     * @param mixed $object
     * @param int|string $key
     * @return array
     */
    public function setObjectForKey( array $dictionary, $object, $key )
    {
        $dictionary[ $key ] = $object;

        return $dictionary;
    }

    /**
     * @param array $dictionary
     * @param int|string $key
     * @return array
     */
    public function deleteObjectForKey( array $dictionary, $key )
    {
        if ( isset( $dictionary[ $key ] ) )
        {
            unset( $dictionary[ $key ] );
        }

        return $dictionary;
    }

    /**
     * @param array $array
     * @param int $index
     * @return array
     */
    public function deleteObjectAtIndex( array $array, $index )
    {
        if( isset( $array[ $index ] ) )
        {
            array_splice($array, $index, 1);
        }

        return $array;
    }

    /**
     * @param array $array
     * @param mixed $object
     * @return array
     */
    public function addObject( array $array, $object )
    {
        $array[ ] = $object;

        return $array;
    }

    /**
     * @param array $returnDictionary
     * @param array $addDictionary
     * @return array
     */
    public function dictionaryMerge( array $returnDictionary, array $addDictionary )
    {
        foreach( $addDictionary as $key => $object )
        {
            /* @var $object mixed */
            $returnDictionary[ $key ] = $object;
        }

        return $returnDictionary;
    }

    /**
     * @param array $dictionary
     * @return array
     */
    public function dictionaryToArray( array $dictionary )
    {
        $array = array();
        foreach( $dictionary as $key => $object )
        {
            /* @var $object mixed */
            $array[] = $object;
        }

        return $array;
    }

    /**
     * @param array $dictionary
     * @return array
     */
    public function dictionaryKeyToArray( array $dictionary )
    {
        $array = array();
        foreach( $dictionary as $key => $object )
        {
            /* @var $object mixed */
            $array[] = $key;
        }

        return $array;
    }

    /**
     * @param array $array
     * @return array
     */
    public function arrayToDictionary( array $array )
    {
        $dictionary = array();
        foreach( $array as $value )
        {
            /* @var $value string */
            $dictionary[ $value ] = true;
        }

        return $dictionary;
    }
} 