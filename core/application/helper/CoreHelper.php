<?php


class CoreHelper
{

    /**
     * @var ArrayHelper
     */
    private static $arrayHelper;

    /**
     * @var DateHelper
     */
    private static $dateHelper;

    /**
     * @var FileHelper
     */
    private static $fileHelper;

    /**
     * @var IdHelper
     */
    private static $idHelper;

    /**
     * @var ReferenceHelper
     */
    private static $referenceHelper;

    /**
     * @var StringHelper
     */
    private static $stringHelper;

    /**
     * @var VersionHelper
     */
    private static $versionHelper;

    /**
     * @return HelperCorePath
     */
    private static function helperPath()
    {
        return CorePath::getInstance()->getApplication()->getHelper();
    }

    /**
     * @return ArrayHelper
     */
    public static function getArrayHelper()
    {
        if( !self::$arrayHelper )
        {
            self::$arrayHelper = self::helperPath()->getArrayHelper();
        }
        return self::$arrayHelper;
    }

    /**
     * @return DateHelper
     */
    public static function getDateHelper()
    {
        if( !self::$dateHelper )
        {
            self::$dateHelper = self::helperPath()->getDateHelper();
        }
        return self::$dateHelper;
    }

    /**
     * @return FileHelper
     */
    public static function getFileHelper()
    {
        if( !self::$fileHelper )
        {
            self::$fileHelper = self::helperPath()->getFileHelper();
        }
        return self::$fileHelper;
    }

    /**
     * @return IdHelper
     */
    public static function getIdHelper()
    {
        if( !self::$idHelper )
        {
            self::$idHelper = self::helperPath()->getIdHelper();
        }
        return self::$idHelper;
    }

    /**
     * @return ReferenceHelper
     */
    public static function getReferenceHelper()
    {
        if( !self::$referenceHelper )
        {
            self::$referenceHelper = self::helperPath()->getReferenceHelper();
        }
        return self::$referenceHelper;
    }

    /**
     * @return StringHelper
     */
    public static function getStringHelper()
    {
        if( !self::$stringHelper )
        {
            self::$stringHelper = self::helperPath()->getStringHelper();
        }
        return self::$stringHelper;
    }

    /**
     * @return VersionHelper
     */
    public static function getVersionHelper()
    {
        if( !self::$versionHelper )
        {
            self::$versionHelper = self::helperPath()->getVersionHelper();
        }
        return self::$versionHelper;
    }

}