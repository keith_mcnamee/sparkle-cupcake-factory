<?php

class ApplicationCorePath
{

    /**
     * @var AppCorePath
     */
    private $app;

    /**
     * @var BalancingCorePath
     */
    private $balancing;

    /**
     * @var DatabaseCorePath
     */
    private $database;

    /**
     * @var HelperCorePath
     */
    private $helper;

    /**
     * @var LocalizationCorePath
     */
    private $localization;

    /**
     * @var ReadExternalCorePath
     */
    private $readExternal;

    /**
     * @var RequestCorePath
     */
    private $request;

    /**
     * @var SystemCorePath
     */
    private $system;

    public function __destruct()
    {
        $this->app = null;
        $this->balancing = null;
        $this->database = null;
        $this->helper = null;
        $this->localization = null;
        $this->readExternal = null;
        $this->request = null;
        $this->system = null;
    }

    /**
     * @return AppCorePath
     */
    private function createApp()
    {
        require_once( DIR_CORE . "/application/app/includes/AppCoreIncludes.php" );
        $this->app = new AppCorePath();
        return $this->app;
    }

    /**
     * @return BalancingCorePath
     */
    private function createBalancing()
    {
        require_once( DIR_CORE . "/application/balancing/BalancingCorePath.php" );
        $this->balancing = new BalancingCorePath();
        return $this->balancing;
    }

    /**
     * @return DatabaseCorePath
     */
    private function createDatabase()
    {
        require_once( DIR_CORE . "/application/database/app/DatabaseCorePath.php" );
        $this->database = new DatabaseCorePath();
        return $this->database;
    }

    /**
     * @return HelperCorePath
     */
    private function createHelper()
    {
        require_once( DIR_CORE . "/application/helper/HelperCorePath.php" );
        $this->helper = new HelperCorePath();
        return $this->helper;
    }

    /**
     * @return LocalizationCorePath
     */
    private function createLocalization()
    {
        require_once( DIR_CORE . "/application/localization/LocalizationCorePath.php" );
        $this->localization = new LocalizationCorePath();
        return $this->localization;
    }

    /**
     * @return ReadExternalCorePath
     */
    private function createReadExternal()
    {
        require_once( DIR_CORE . "/application/readExternal/ReadExternalCorePath.php" );
        $this->readExternal = new ReadExternalCorePath();
        return $this->readExternal;
    }

    /**
     * @return RequestCorePath
     */
    private function createRequest()
    {
        require_once( DIR_CORE . "/application/request/RequestCorePath.php" );
        $this->request = new RequestCorePath();
        return $this->request;
    }

    /**
     * @return SystemCorePath
     */
    private function createSystem()
    {
        require_once( DIR_CORE . "/application/system/SystemCorePath.php" );
        $this->system  = new SystemCorePath();
        return $this->system ;
    }

    /**
     * @return AppCorePath
     */
    public function getApp()
    {
        return $this->app ? $this->app : $this->createApp();
    }

    /**
     * @return BalancingCorePath
     */
    public function getBalancing()
    {
        return $this->balancing ? $this->balancing : $this->createBalancing();
    }

    /**
     * @return DatabaseCorePath
     */
    public function getDatabase()
    {
        return $this->database ? $this->database : $this->createDatabase();
    }

    /**
     * @return HelperCorePath
     */
    public function getHelper()
    {
        return $this->helper ? $this->helper : $this->createHelper();
    }

    /**
     * @return LocalizationCorePath
     */
    public function getLocalization()
    {
        return $this->localization ? $this->localization : $this->createLocalization();
    }

    /**
     * @return ReadExternalCorePath
     */
    public function getReadExternal()
    {
        return $this->readExternal ? $this->readExternal : $this->createReadExternal();
    }

    /**
     * @return RequestCorePath
     */
    public function getRequest()
    {
        return $this->request ? $this->request : $this->createRequest();
    }

    /**
     * @return SystemCorePath
     */
    public function getSystem()
    {
        return $this->system ? $this->system : $this->createSystem();
    }
}