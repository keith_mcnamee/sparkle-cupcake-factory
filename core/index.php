<?php

set_time_limit( 0 );
ini_set( 'display_errors', 1 );
error_reporting( E_ALL );
date_default_timezone_set( 'Europe/London' );

define( 'DIR_EXAMPLE_ROOT', dirname( __FILE__ ) );

require_once( DIR_EXAMPLE_ROOT . "/example/DefineExample.php" );

require_once( DIR_EXAMPLE_APP . "/SharedExamplePath.php" );

SharedExamplePath::getInstance()->process();
