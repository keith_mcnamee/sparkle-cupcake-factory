<?php


class CoreViewModel extends CoreBase
{
    /**
     * @var ViewPropertiesVO
     */
    private $viewPropertiesVO;

    /**
     * @var String
     */
    private $jsonViewProperties;

    public function __destruct()
    {
        $this->viewPropertiesVO = null;
    }

    /**
     * @return ViewPropertiesVO
     */
    public function getViewPropertiesVO()
    {
        return $this->viewPropertiesVO;
    }

    /**
     * @param ViewPropertiesVO $value
     */
    public function setViewPropertiesVO( ViewPropertiesVO $value = null )
    {
        $this->viewPropertiesVO = $value;
    }

    /**
     * @return String
     */
    public function getJsonViewProperties()
    {
        return $this->jsonViewProperties;
    }

    /**
     * @param String $value
     */
    public function setJsonViewProperties( $value )
    {
        $this->jsonViewProperties = $value;
    }

}