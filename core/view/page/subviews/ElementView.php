<?php


class ElementView extends ElementHtml
{

    /**
     * @var ViewCorePath
     */
    private $corePageView;

    public function __destruct()
    {
        $this->corePageView = null;
    }

    /**
     * @param $singleInstance
     * @return PageViewCorePath
     */
    protected function addCorePageView( $singleInstance = false )
    {
        $instance = CorePath::getInstance()->getView()->getPage();
        if( !$singleInstance )
        {
            $this->corePageView = $instance;
        }
        return $instance;
    }

    /**
     * @return ViewPropertiesVO
     */
    protected function getViewPropertiesVO()
    {
        return $this->getCorePageView()->getModel()->getViewPropertiesVO();
    }

    /**
     * @return string
     */
    protected function getJsonViewProperties()
    {
        return $this->getCorePageView()->getModel()->getJsonViewProperties();
    }

    /**
     * @param $singleInstance
     * @return PageViewCorePath
     */
    protected function getCorePageView( $singleInstance = false )
    {
        return $this->corePageView ? $this->corePageView : $this->addCorePageView( $singleInstance );
    }
    
}