<?php


class PageView extends ContainerView
{

    /**
     * @var ElementHtml
     */
    private $html;

    /**
     * @var HeadView
     */
    private $head;

    /**
     * @var ElementHtml
     */
    private $body;

    public function init()
    {

    }
    public function showView()
    {
        parent::__construct();

        $this->output( true );
    }


    /**
     * @return ElementHtml
     */
    public function getHtml()
    {
        return $this->html;
    }

    protected function defineChildContainers()
    {
        parent::defineChildContainers();
        $this->defineHtmlContainer();
        $this->defineHeadContainer();
        $this->defineBodyContainer();
    }

    /**
     * @param ElementHtml $elementHtml
     */
    protected function defineHtmlContainer( ElementHtml $elementHtml = null )
    {
        $this->html = $elementHtml ? $elementHtml : new ElementHtml( "html" );
    }

    /**
     * @param HeadView $headView
     */
    protected function defineHeadContainer( HeadView $headView = null )
    {
        $this->head = $headView ? $headView : new HeadView();
    }

    /**
     * @param ElementHtml $elementHtml
     */
    protected function defineBodyContainer( ElementHtml $elementHtml = null )
    {
        $this->body = $elementHtml ? $elementHtml : new ElementHtml( "body" );
    }

    protected function addChildContainers()
    {
        parent::addChildContainers();
        $this->addHtmlContainer();
        $this->addHeadContainer();
        $this->addBodyContainer();
    }

    protected function addHtmlContainer()
    {
        $this->addContainerHtml( $this->html );
    }

    protected function addHeadContainer()
    {
        $this->html->addContainerHtml( $this->head );
    }

    protected function addBodyContainer()
    {
        $this->html->addContainerHtml( $this->body );
    }

    /**
     * @return HeadView
     */
    public function getHead()
    {
        return $this->head;
    }
}