<?php


class JsContainerView extends ContainerView
{
    protected $DEFAULT_JS = "end";

    /**
     * @var array
     */
    private $jsElements;

    protected function defineValues()
    {
        $this->jsElements = array();
    }

    protected function defineChildContainers()
    {
        parent::defineChildContainers();
        $this->defineJs();
    }

    /**
     * @param string $position
     * @return JsHtml
     */
    public function getJsElement( $position = "" )
    {
        $position = $this->getPosition( $position );
        return CoreHelper::getArrayHelper()->getObjectForKey( $this->jsElements, $position );
    }

    /**
     * @param string $position
     * @return string
     */
    protected function getPosition( $position )
    {
        $position = $position ? $position : $this->DEFAULT_JS;
        return $position;
    }

    /**
     * @param string $position
     * @param JsHtml $js
     */
    protected function defineJs( $position = "", JsHtml $js = null )
    {
        $position = $this->getPosition( $position );
        $js = $js ? $js : new JsHtml();
        $this->jsElements = CoreHelper::getArrayHelper()->setObjectForKey( $this->jsElements, $js, $position );
    }

    /**
     * @param string $position
     * @param string $type
     */
    public function addJs( $position = "", $type = "" )
    {
        $js = $this->getJsElement( $position );
        if( !$js )
        {
            return;
        }

        if( !$js->hasBodyContent() )
        {
            return;
        }
        $properties = $js->getProperties();
        if( !$type && $type !== null )
        {
            $type = ["text/javascript"];
        }
        if( $type && !isset( $properties[ "type" ] ) )
        {
            $js->addProperty( "type", $type );
        }
        $this->addContainerHtml( $js );
    }
}