<?php


class HeadView extends ElementView
{

    public function __construct()
    {
        parent::__construct( "head" );
    }

    protected function addChildContainers()
    {
        parent::addChildContainers();
        $this->addTitle();
    }

    /**
     * @param string $title
     */
    protected function addTitle( $title = "" )
    {
        if( !$title )
        {
            return;
        }
        $title = new ElementHtml( "title", true, null, $title );
        $this->addContainerHtml( $title );
    }

}