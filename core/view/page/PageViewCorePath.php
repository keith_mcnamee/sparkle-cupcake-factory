<?php


class PageViewCorePath extends CoreBase
{

    /**
     * @var CoreViewModel
     */
    private $model;

    public function __construct()
    {
        parent::__construct();

        $this->getIncludes();
    }

    public function __destruct()
    {
        $this->model = null;

        parent::__destruct();
    }

    private function getIncludes()
    {
        require_once( DIR_CORE . "/view/page/includes/CoreViewIncludes.php" );
    }

    /**
     * @return CoreViewModel
     */
    public function getModel()
    {
        if( !$this->model )
        {
            $this->model = new CoreViewModel();
        }
        return $this->model;
    }
    
}