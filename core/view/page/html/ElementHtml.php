<?php


class ElementHtml extends ContainerHtml
{

    /**
     * @var string
     */
    private $element;

    /**
     * @var array
     */
    private $properties;

    /**
     * @var bool
     */
    private $allowSingleTag;

    /**
     * @var bool
     */
    private $ignoreEndSlash;

    /**
     * @param string $element
     * @param bool $inline
     * @param array $properties
     * @param string $text
     * @param array $html
     * @param bool $allowSingleTag
     * @param bool $ignoreEndSlash
     */
    public function __construct( $element, $inline = false, array $properties = null, $text = null, array $html = null, $allowSingleTag = false, $ignoreEndSlash= false )
    {
        $this->element = $element;
        $this->properties = $properties ? $properties : array();
        $this->allowSingleTag = $allowSingleTag;
        $this->ignoreEndSlash = $ignoreEndSlash;

        parent::__construct( $inline, $text, $html );

        $this->addProperties();
    }

    protected function addProperties()
    {

    }

    /**
     * @param string $key
     * @param string $value
     */
    public function addProperty( $key, $value )
    {
        $this->properties[ $key ] = $value;
    }

    /**
     * @param boolean $print
     * @param boolean $inline
     * @param int $indent
     * @param boolean $needsLineBreak
     * @return string
     */
    public function output( $print = false, $inline = false, $indent = 0, $needsLineBreak = false )
    {
        $tab = $this->tab( $inline, $indent );
        $headContent = $tab."<".$this->element;
        $inline = $inline || $this->isInline();

        foreach( $this->properties as $key => $values )
        {
            /* @var $values array */
            $allValues = "\"";
            for( $i = 0; $i < count( $values ); $i ++ )
            {
                $value = $values[ $i ];
                /* @var $value string */
                if( $i != 0 )
                {
                    $allValues .= " ";
                }
                $allValues .= $value;
            }
            $allValues .= "\"";
            $headContent .= " ".$key."=".$allValues;
        }
        if( !$this->hasBodyContent() && $this->allowSingleTag )
        {
            $endSlash = !$this->ignoreEndSlash ? " /" : "";
            $footContent = $endSlash.">";
            if( $needsLineBreak )
            {
                $footContent .= "\n";
            }

            $content = $headContent . $footContent;
            $this->printContent( $print, $content );
            return $content;
        }

        $lineBreak = $this->lineBreak( $inline );
        $headContent .= ">".$lineBreak;
        $this->printContent( $print, $headContent );

        $bodyContent = $this->bodyContent( $print, $inline, $indent );

        $footContent = "";
        $tab = $this->tab( $inline, $indent );
        $footContent .= $tab."</".$this->element.">";
        if( $needsLineBreak )
        {
            $footContent .= "\n";
        }
        $this->printContent( $print, $footContent );

        $content = $headContent . $bodyContent . $footContent;
        return $content;
    }

    /**
     * @param boolean $print
     * @param boolean $inline
     * @param int $indent
     * @return string
     */
    protected function bodyContent ( $print = false, $inline = false, $indent = 0 )
    {
        $content = parent::output( $print, $inline , $indent + 1 );
        return $content;
    }


    /**
     * @return string
     */
    public function getElement()
    {
        return $this->element;
    }

    /**
     * @return array
     */
    public function getProperties()
    {
        return $this->properties;
    }
}