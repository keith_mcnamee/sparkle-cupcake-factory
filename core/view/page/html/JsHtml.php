<?php


class JsHtml extends ElementHtml
{

    /**
     * @var array
     */
    private $vars;

    /**
     * @param array $properties
     * @param array $vars
     */
    public function __construct( array $properties = null, array $vars = null )
    {
        $this->vars = $vars ? $vars : array();

        parent::__construct( "script", false, $properties );
    }

    /**
     * @param string $key
     * @param mixed $value
     */
    public function addVar( $key, $value )
    {
        $this->vars[ $key ] = $value;
    }

    /**
     * @return boolean
     */
    public function hasBodyContent()
    {
        return count( $this->vars ) > 0;
    }

    /**
     * @param boolean $print
     * @param boolean $inline
     * @param int $indent
     * @return string
     */
    protected function bodyContent ( $print = false, $inline = false, $indent = 0 )
    {
        $tab = $this->tab( $inline, $indent + 1 );
        $content = "";
        $lineBreak = $this->lineBreak( $inline );

        foreach( $this->vars as $key => $value )
        {
            /* @var $value mixed */
            $quot = is_string( $value ) ? "'" : "";
            $value = is_bool( $value ) && $value ? "true" : $value;
            $value = is_bool( $value ) && !$value ? "false" : $value;
            $content .= $tab."var ".$key." = ".$quot.$value.$quot.";".$lineBreak;
        }
        $this->printContent( $print, $content );
        return $content;
    }
}