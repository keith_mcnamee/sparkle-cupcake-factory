<?php


class ContainerHtml
{

    /**
     * @var boolean
     */
    private $inline;

    /**
     * @var string
     */
    private $text;

    /**
     * @var array
     */
    private $containerHtmls;

    /**
     * @var int
     */
    private $indent;

    /**
     * @param bool $inline
     * @param string $text
     * @param array $containerHtmls
     */
    public function __construct( $inline = false, $text = null, array $containerHtmls = null )
    {
        $this->inline = $inline;
        $this->text = $text ? $text : "";
        $this->containerHtmls = $containerHtmls ? $containerHtmls : array();

        $this->defineValues();
        $this->defineChildContainers();
        $this->addChildContainers();
    }

    protected function defineValues()
    {

    }

    protected function defineChildContainers()
    {

    }

    protected function addChildContainers()
    {

    }

    /**
     * @param ContainerHtml $containerHtml
     */
    public function addContainerHtml( ContainerHtml $containerHtml )
    {
        if( !$this->inline )
        {
            $this->containerHtmls = CoreHelper::getArrayHelper()->addObject( $this->containerHtmls, $containerHtml );
        }
        else
        {
            $thisText = $containerHtml->output( false, true );
            $this->addText( $thisText );
        }
    }


    /**
     * @param string $text
     */
    public function addText( $text )
    {
        $this->text .= $text;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }


    /**
     * @return boolean
     */
    public function hasBodyContent()
    {
        return count( $this->containerHtmls ) > 0 || $this->text;
    }

    /**
     * @param boolean $print
     * @param boolean $inline
     * @param int $indent
     * @param boolean $needsLineBreak
     * @return string
     */
    public function output( $print = false, $inline = false, $indent = 0, $needsLineBreak = false )
    {
        $this->indent = $indent;
        $this->beforeOutput();
        $bodyContent = "";
        foreach( $this->containerHtmls as $containerHtml )
        {
            /* @var ContainerHtml $containerHtml */
            $bodyContent .= $containerHtml->output( $print, $inline || $this->inline, $indent, !$inline );
        }
        $bodyContent .= $this->text;
        $this->printContent( $print, $this->text );
        $this->afterOutput();

        return $bodyContent;
    }


    protected function beforeOutput()
    {
    }


    protected function afterOutput()
    {

    }

    /**
     * @return int
     */
    public function getIndent()
    {
        return $this->indent;
    }

    /**
     * @param boolean $inline
     * @return string
     */
    protected function lineBreak( $inline )
    {
        return $inline ? "" : "\n";
    }


    /**
     * @param boolean $inline
     * @param int $indent
     * @return string
     */
    protected function tab( $inline, $indent )
    {
        $value = "";
        if( $inline || $indent < 1 )
        {
            return $value;
        }
        for( $i = 1; $i <= $indent ; $i ++ )
        {
            $value .= "\t";
        }
        return $value;
    }

    /**
     * @param boolean $print
     * @param string $content
     */
    protected function printContent( $print, $content )
    {
        if( $print )
        {
            echo $content;
        }
    }

    /**
     * @return boolean
     */
    public function isInline()
    {
        return $this->inline;
    }
}