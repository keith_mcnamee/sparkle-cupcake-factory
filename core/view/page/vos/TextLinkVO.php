<?php


class TextLinkVO
{

    /**
     * @var string
     */
    private $text;

    /**
     * @var string
     */
    private $link;

    /**
     * @var string
     */
    private $linkID;

    /**
     * @param string $text
     * @param string $link
     * @param string $linkID
     */
    public function __construct( $text, $link = "", $linkID = "" )
    {
        $this->text = $text;
        $this->link = $link;
        $this->linkID = $linkID;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @return string
     */
    public function getLinkID()
    {
        return $this->linkID;
    }

    /**
     * @return boolean
     */
    public function isLink()
    {
        return $this->link !== "";
    }
}