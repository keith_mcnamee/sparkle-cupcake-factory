<?php

require_once( DIR_CORE . "/view/page/html/ContainerHtml.php" );
require_once( DIR_CORE . "/view/page/html/ElementHtml.php" );
require_once( DIR_CORE . "/view/page/html/JsHtml.php" );

require_once( DIR_CORE . "/view/page/subviews/ContainerView.php" );
require_once( DIR_CORE . "/view/page/subviews/ElementView.php" );
require_once( DIR_CORE . "/view/page/subviews/HeadView.php" );
require_once( DIR_CORE . "/view/page/subviews/JsContainerView.php" );
require_once( DIR_CORE . "/view/page/subviews/PageView.php" );

require_once( DIR_CORE . "/view/page/model/CoreViewModel.php" );

require_once( DIR_CORE . "/view/page/vos/TextLinkVO.php" );
require_once( DIR_CORE . "/view/page/vos/ViewPropertiesVO.php" );