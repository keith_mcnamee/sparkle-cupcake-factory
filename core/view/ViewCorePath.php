<?php


class ViewCorePath
{

    /**
     * @var DebugViewCorePath
     */
    private $debug;

    /**
     * @var PageViewCorePath
     */
    private $page;

    public function __destruct()
    {
        $this->debug = null;
        $this->page = null;
    }

    /**
     * @return DebugViewCorePath
     */
    private function createDebug()
    {
        require_once( DIR_CORE . "/view/debug/DebugViewCorePath.php" );
        $this->debug = new DebugViewCorePath();
        return $this->debug;
    }

    /**
     * @return PageViewCorePath
     */
    private function createPage()
    {
        require_once( DIR_CORE . "/view/page/PageViewCorePath.php" );
        $this->page = new PageViewCorePath();
        return $this->page;
    }

    /**
     * @return DebugViewCorePath
     */
    public function getDebug()
    {
        return $this->debug ? $this->debug : $this->createDebug();
    }

    /**
     * @return PageViewCorePath
     */
    public function getPage()
    {
        return $this->page ? $this->page : $this->createPage();
    }
    
}