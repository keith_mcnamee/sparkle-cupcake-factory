<?php


class DebugViewText
{
    public static $COMPLETE = "Complete";
    public static $ALL_COMPLETE = "All Complete";
    public static $REDIRECTING = "Redirecting";
    public static $WARNING = "WARNING";
    public static $REQUESTED_REDIRECT = "Requested redirect";
    public static $BALANCING_NOT_REGISTERED = "Balancing not registered";
    public static $INITIALIZE_BALANCING_MANUALLY = "Initialize balancing manually";

    public static $MISMATCHED_COLUMN_COUNT = "Mismatched Column Count";

    public static $NO_LOCALIZATION_TO_UPDATE = "No Localization to update";
    public static $INVALID_LOCALIZATION_CONTENT_FOR = "Invalid Localization Content for:";
    public static $TOO_LOW_LOCALIZATION_VERSION_FOR = "Defined Localization Version Number must be higher than existing for :";
    public static $LOCALIZATION_EXISTS_FOR = "Matching Localization file already exists for:";

    public static $NO_BALANCING_TO_UPDATE = "No Balancing to update";
    public static $TOO_LOW_BALANCING_VERSION = "Defined Balancing Version Number must be higher than existing";
    public static $INVALID_BALANCING_CONTENT = "Invalid Balancing Content";
    public static $BALANCING_EXISTS = "Matching Balancing file already exists";
} 