<?php


class DebugViewService extends CoreBase
{

    /**
     * @var InfoAppCoreHelper
     */
    private $infoAppHelper;

    public function __destruct()
    {
        $this->infoAppHelper = null;

        parent::__destruct();
    }

    public function printTimeLog()
    {
        if( !( $this->getInfoAppHelper()->isTestServer() || $this->getInfoAppHelper()->isStagingDirectory() ) )
        {
            return;
        }

        $timeLog = $this->getCoreApp()->getAppModel()->getTimeLog();
        $lastTime = -1;
        foreach( $timeLog as $logID => $entry )
        {
            if( $lastTime < 0 )
            {
                $diff = 0;
            }
            else
            {
                $diff = $entry - $lastTime;
            }
            echo $logID."=  ".$diff."<br />";
            $lastTime = $entry;
        }
    }

    /**
     * @param mixed $value
     * @param boolean $anyEnvironment
     * @param boolean $warning
     */
    public function trace( $value, $anyEnvironment = false, $warning = false  )
    {
        $this->traceValues( array( $value ), $anyEnvironment, $warning );
    }

    /**
     * @param array $strings
     * @param boolean $anyEnvironment
     * @param boolean $warning
     */
    public function traceValues( array $strings, $anyEnvironment = false, $warning = false  )
    {
        if ( $anyEnvironment || $this->getInfoAppHelper()->isTestServer() || $this->getInfoAppHelper()->isStagingDirectory() )
        {
            $startWrapper = "";
            $endWrapper =  "";
            if( $warning )
            {
                array_unshift( $strings, DebugViewText::$WARNING );
                $startWrapper = "<b>";
                $endWrapper = "</b>";
            }
            echo $startWrapper;
            for( $i = 0; $i < count( $strings ); $i ++ )
            {
                if( $i != 0 )
                {
                    echo " ";
                }
                echo $strings[ $i ];
            }
            echo $endWrapper;

            echo "<br />";
        }
    }

    /**
     * @return InfoAppCoreHelper
     */
    private function getInfoAppHelper()
    {
        if( !$this->infoAppHelper )
        {
            $this->infoAppHelper = $infoAppService = $this->getCoreApp()->getInfoHelper();
        }
        return $this->infoAppHelper;
    }
}