<?php


class DebugViewCorePath extends CoreBase
{

    public function __construct()
    {
        parent::__construct();

        $this->getIncludes();
    }

    private function getIncludes()
    {
        require_once( DIR_CORE . "/view/debug/includes/DebugViewIncludes.php" );
    }

    /**
     * @return DebugViewService
     */
    public function getService()
    {
        return new DebugViewService();
    }
}