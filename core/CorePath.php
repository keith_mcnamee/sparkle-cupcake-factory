<?php

class CorePath
{

    /**
     * @var ApplicationCorePath
     */
    private $application;

    /**
     * @var CoreViewModel
     */
    private $view;

    public function __construct()
    {
        $this->getIncludes();
    }

    public function __destruct()
    {
        $this->application = null;
        $this->view = null;
    }

    public function process( array $requests = null )
    {
        //None
    }

    private function getIncludes()
    {
        require_once( DIR_CORE . "/application/app/includes/AppCoreIncludes.php" );
    }

    public static function getInstance()
    {
        static $instance = null;

        if (null === $instance)
        {
            $instance = new static();
        }

        return $instance;
    }

    /**
     * @return ApplicationCorePath
     */
    private function createApplication()
    {
        $this->application = new ApplicationCorePath();
        return $this->application;
    }

    /**
     * @return ViewCorePath
     */
    private function createView()
    {
        require_once( DIR_CORE . "/view/ViewCorePath.php" );
        $this->view = new ViewCorePath();
        return $this->view;
    }

    /**
     * @return ApplicationCorePath
     */
    public function getApplication()
    {
        return $this->application ? $this->application : $this->createApplication();
    }

    /**
     * @return ViewCorePath
     */
    public function getView()
    {
        return $this->view ? $this->view : $this->createView();
    }
}