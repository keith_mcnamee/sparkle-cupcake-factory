<?php


class ServerRequestExampleConfig extends SharedExampleBase
{

    /**
     * @var array
     */
    private $configVOs;

    public function __destruct()
    {
        $this->configVOs = null;
    }

    /**
     * @return array
     */
    public function getConfigVOs()
    {
        return $this->configVOs ? $this->configVOs : $this->createConfigVOs();
    }

    /**
     * @return array
     */
    protected function createConfigVOs()
    {
        $this->configVOs = array();

        $this->configVOs[] = $this->localhostConfigVO();

        return $this->configVOs;
    }

    /**
     * @return ServerConfigRequestCoreVO
     */
    protected function localhostConfigVO()
    {
        $configVO = new ServerConfigRequestCoreVO( InternalRequestCoreConstants::$LOCALHOST_SERVER_CONFIG_ID );

        $configVO->setUsingStagingDatabase( true );

        $configVO->addServerName( "localhost" );

        $configVO->addStagingDirectory( new StagingDirectoryConfigRequestCoreVO( "staging", 0 ) );


        return $configVO;
    }
}