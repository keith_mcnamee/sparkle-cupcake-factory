<?php


class RequestSharedExamplePath extends SharedExampleBase
{

    /**
     * @var ServerRequestExampleConfig
     */
    private $serverConfig;

    public function __construct()
    {
        parent::__construct();

        $this->getCoreRequest();
        $this->getIncludes();
    }

    private function getIncludes()
    {
        require_once( DIR_EXAMPLE_SHARED . "/application/request/includes/RequestExampleIncludes.php" );
    }

    public function __destruct()
    {
        $this->serverConfig = null;
    }

    /**
     * @return ServerRequestExampleConfig
     */
    public function getServerConfig()
    {
        if( !$this->serverConfig )
        {
            $this->serverConfig = new ServerRequestExampleConfig();
        }
        return $this->serverConfig;
    }
}