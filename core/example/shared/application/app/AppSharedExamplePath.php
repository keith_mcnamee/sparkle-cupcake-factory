<?php


class AppSharedExamplePath extends SharedExampleBase
{

    public function __construct()
    {
        parent::__construct();

        $this->getCoreApp();
    }

    /**
     * @return ProcessAppExampleCommand
     */
    public function getProcessCommand()
    {
        return new ProcessAppExampleCommand();
    }
    
}