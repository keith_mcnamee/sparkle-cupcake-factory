<?php

require_once( DIR_EXAMPLE_APP . "/SharedExampleBase.php" );
require_once( DIR_EXAMPLE_APP . "/LinkedSharedExampleBase.php" );
require_once( DIR_EXAMPLE_SHARED . "/application/ApplicationSharedExamplePath.php" );
require_once( DIR_EXAMPLE_SHARED . "/application/app/AppSharedExamplePath.php" );

require_once( DIR_EXAMPLE_SHARED . "/application/app/command/ProcessAppExampleCommand.php" );