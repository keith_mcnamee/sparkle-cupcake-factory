<?php


class ProcessAppExampleCommand extends LinkedSharedExampleBase
{

    public function command()
    {
        $serverRequestConfigVOs = $this->getSharedRequest()->getServerConfig()->getConfigVOs();
        $this->getCoreApp()->getInitializeCommand()->command( $serverRequestConfigVOs );

        $appType = CoreRequest::getHelper()->getAppType();
        $this->processAppType( $appType );

    }

    /**
     * @param string $appType
     */
    protected function processAppType( $appType )
    {
        switch ( $appType )
        {

            case ParameterRequestCoreConstants::$EXAMPLE_APP_TYPE:
            default:
                $this->getSite( true )->process();
                break;
        }
    }
}