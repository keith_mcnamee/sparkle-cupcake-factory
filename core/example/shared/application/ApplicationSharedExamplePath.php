<?php

class ApplicationSharedExamplePath
{

    /**
     * @var AppSharedExamplePath
     */
    private $app;

    /**
     * @var RequestSharedExamplePath
     */
    private $request;

    public function __destruct()
    {
        $this->app = null;
        $this->request = null;
    }

    /**
     * @return AppSharedExamplePath
     */
    private function createApp()
    {
        $this->app = new AppSharedExamplePath();
        return $this->app;
    }

    /**
     * @return RequestSharedExamplePath
     */
    private function createRequest()
    {
        require_once( DIR_EXAMPLE_SHARED . "/application/request/RequestSharedExamplePath.php" );
        $this->request = new RequestSharedExamplePath();
        return $this->request;
    }

    /**
     * @return AppSharedExamplePath
     */
    public function getApp()
    {
        return $this->app ? $this->app : $this->createApp();
    }

    /**
     * @return RequestSharedExamplePath
     */
    public function getRequest()
    {
        return $this->request ? $this->request : $this->createRequest();
    }
}