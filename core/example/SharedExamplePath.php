<?php

class SharedExamplePath
{

    /**
     * @var ApplicationSharedExamplePath
     */
    private $application;

    public function __construct()
    {
        $this->createCore();
        $this->getIncludes();
    }

    public function __destruct()
    {
        $this->application = null;
    }

    public function process()
    {
        $this->getApplication()->getApp()->getProcessCommand()->command();
    }

    private function createCore()
    {
        require_once( DIR_CORE . "/CorePath.php" );
        CorePath::getInstance();
    }

    private function getIncludes()
    {
        require_once( DIR_EXAMPLE_SHARED . "/application/app/includes/AppExampleIncludes.php" );
    }

    /**
     * @return static
     */
    public static function getInstance()
    {
        static $instance = null;

        if (null === $instance)
        {
            $instance = new static();
        }

        return $instance;
    }

    /**
     * @return ApplicationSharedExamplePath
     */
    private function createApplication()
    {
        $this->application = new ApplicationSharedExamplePath();
        return $this->application;
    }

    /**
     * @return ApplicationSharedExamplePath
     */
    public function getApplication()
    {
        return $this->application ? $this->application : $this->createApplication();
    }
}