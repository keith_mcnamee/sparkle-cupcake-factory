<?php


class PageSiteExampleBodyView extends ElementView
{

    public function __construct()
    {
        parent::__construct( "body" );
    }


    protected function afterOutput()
    {
        parent::afterOutput();
        $this->addPageContent();

    }

    protected function addPageContent()
    {
        echo SiteExampleViewText::$EXAMPLE_VALUE;
        ?>
        <br />
        <form action="<?php echo $this->getPageViewPropertiesVO()->getCurrentURL();?>" method="post">
            <textarea id="exampleValue" name="exampleValue" maxlength="1000" placeholder="<?php echo $this->getPageViewPropertiesVO()->getExampleValue();?>"></textarea>
            <input type="submit" value="Submit">
        </form>
        <?php
        echo $this->getPageViewPropertiesVO()->getCurrentURL()."<br />";
    }

    /**
     * @return PageSiteExampleViewPropertiesVO
     */
    public function getPageViewPropertiesVO()
    {
        return $this->getViewPropertiesVO();
    }
    
}