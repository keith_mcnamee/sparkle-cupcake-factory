<?php


class PageViewSiteExamplePath extends SiteExampleBase
{

    public function __construct()
    {
        $this->getCorePageView();
        $this->getIncludes();
    }

    private function getIncludes()
    {
        require_once( DIR_EXAMPLE_SITE . "/view/page/includes/PageSiteExampleViewIncludes.php" );
    }

    /**
     * @return PageSiteExampleViewCommand
     */
    public function getCommand()
    {
        return new PageSiteExampleViewCommand();
    }

    /**
     * @return PageSiteExamplePageView
     */
    public function getPageView()
    {
        return new PageSiteExamplePageView();
    }
    
}