<?php


class PageSiteExampleViewPropertiesVO extends ViewPropertiesVO
{
    /**
     * @var string
     */
    private $exampleValue;

    /**
     * @var string
     */
    private $currentURL;


    public function __construct()
    {

    }

    /**
     * @return string
     */
    public function getExampleValue()
    {
        return $this->exampleValue;
    }

    /**
     * @param string $value
     */
    public function setExampleValue( $value )
    {
        $this->exampleValue = $value;
    }

    /**
     * @return string
     */
    public function getCurrentURL()
    {
        return $this->currentURL;
    }

    /**
     * @param string $value
     */
    public function setCurrentURL( $value )
    {
        $this->currentURL = $value;
    }

} 