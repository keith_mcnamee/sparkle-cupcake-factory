<?php


class PageSiteExamplePageView extends PageView
{

    /**
     * @param ElementHtml $elementHtml
     */
    protected function defineBodyContainer( ElementHtml $elementHtml = null )
    {
        $elementHtml = $elementHtml ? $elementHtml : new PageSiteExampleBodyView();
        parent::defineBodyContainer( $elementHtml );
    }
}