<?php

require_once( DIR_EXAMPLE_SITE . "/view/page/PageSiteExamplePageView.php" );
require_once( DIR_EXAMPLE_SITE . "/view/page/PageViewSiteExamplePath.php" );

require_once( DIR_EXAMPLE_SITE . "/view/page/command/PageSiteExampleViewCommand.php" );
require_once( DIR_EXAMPLE_SITE . "/view/page/pageContent/PageSiteExampleBodyView.php" );
require_once( DIR_EXAMPLE_SITE . "/view/page/text/SiteExampleViewText.php" );
require_once( DIR_EXAMPLE_SITE . "/view/page/vos/PageSiteExampleViewPropertiesVO.php" );