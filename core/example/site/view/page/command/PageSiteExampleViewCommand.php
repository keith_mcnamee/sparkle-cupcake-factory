<?php


class PageSiteExampleViewCommand extends SiteExampleBase
{

    public function showView()
    {

        $viewPropertiesVO = new PageSiteExampleViewPropertiesVO();
        $exampleVOs = $this->getSiteApp()->getAppModel()->getExampleVOs();

        $exampleValue = null;
        if( empty( $exampleVOs ) )
        {
            $exampleValue = SiteExampleViewText::$NONE;
        }
        else
        {
            foreach( $exampleVOs as $exampleVO )
            {
                /* @var $exampleVO SiteExampleVO */
                $exampleValue = $exampleVO->getExampleValue();
                break;
            }
        }

        $viewPropertiesVO->setExampleValue( $exampleValue );
        $viewPropertiesVO->setCurrentURL( CoreRequest::getHelper()->baseURL().CoreRequest::getHelper()->workingDirectory() );

        $this->getCorePageView()->getModel()->setViewPropertiesVO( $viewPropertiesVO );
        $this->getPageExampleView()->getPageView()->showView();
    }
}