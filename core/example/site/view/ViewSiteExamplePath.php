<?php


class ViewSiteExamplePath
{

    /**
     * @var PageViewSiteExamplePath
     */
    private $pageExample;

    public function __destruct()
    {
        $this->pageExample = null;
    }

    /**
     * @return PageViewSiteExamplePath
     */
    private function createPageExample()
    {
        require_once( DIR_EXAMPLE_SITE . "/view/page/PageViewSiteExamplePath.php" );
        $this->pageExample = new PageViewSiteExamplePath();
        return $this->pageExample;
    }

    /**
     * @return PageViewSiteExamplePath
     */
    public function getPageExample()
    {
        return $this->pageExample ? $this->pageExample : $this->createPageExample();
    }

}