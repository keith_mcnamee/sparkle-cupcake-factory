<?php


class SiteExamplePath 
{

    /**
     * @var ApplicationSiteExamplePath
     */
    private $application;

    /**
     * @var ViewSiteExamplePath
     */
    private $view;

    public function __construct()
    {
        $this->getIncludes();
    }

    public function __destruct()
    {
        $this->application = null;
        $this->view = null;
    }

    public function process()
    {
        $this->getApplication()->getApp()->getProcessCommand()->command();
    }

    private function getIncludes()
    {
        require_once( DIR_EXAMPLE_SITE . "/application/app/includes/AppSiteExampleIncludes.php" );
    }

    /**
     * @return static
     */
    public static function getInstance()
    {
        static $instance = null;

        if (null === $instance)
        {
            $instance = new static();
        }

        return $instance;
    }

    /**
     * @return ApplicationSiteExamplePath
     */
    private function createApplication()
    {
        $this->application = new ApplicationSiteExamplePath();
        return $this->application;
    }

    /**
     * @return ViewSiteExamplePath
     */
    private function createView()
    {
        require_once( DIR_EXAMPLE_SITE . "/view/ViewSiteExamplePath.php" );
        $this->view = new ViewSiteExamplePath();
        return $this->view;
    }

    /**
     * @return ApplicationSiteExamplePath
     */
    public function getApplication()
    {
        return $this->application ? $this->application : $this->createApplication();
    }

    /**
     * @return ViewSiteExamplePath
     */
    public function getView()
    {
        return $this->view ? $this->view : $this->createView();
    }
}