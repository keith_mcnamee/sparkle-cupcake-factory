<?php


class SiteExampleBase extends SharedExampleBase
{

    /**
     * @var SiteExamplePath
     */
    private $sitePath;

    public function __destruct()
    {
        $this->sitePath = null;

        parent::__destruct();
    }

    /**
     * @return SiteExamplePath
     */
    private function createSitePath()
    {
        $this->sitePath  = SiteExamplePath::getInstance();
        return $this->sitePath;
    }

    /**
     * @return SiteExamplePath
     */
    protected function getSitePath()
    {
        return $this->sitePath ? $this->sitePath : $this->createSitePath();
    }

    /**
     * @return AppSiteExamplePath
     */
    protected function getSiteApp()
    {
        return $this->getSitePath()->getApplication()->getApp();
    }

    /**
     * @return DatabaseSiteExamplePath
     */
    protected function getSiteDatabase()
    {
        return $this->getSitePath()->getApplication()->getDatabase();
    }

    /**
     * @return RequestSiteExamplePath
     */
    protected function getSiteRequest()
    {
        return $this->getSitePath()->getApplication()->getRequest();
    }

    /**
     * @return PageViewSiteExamplePath
     */
    protected function getPageExampleView()
    {
        return $this->getSitePath()->getView()->getPageExample();
    }
}