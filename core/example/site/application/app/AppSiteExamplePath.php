<?php


class AppSiteExamplePath extends SiteExampleBase
{

    /**
     * @var AppSiteExampleModel
     */
    private $appModel;

    public function __construct()
    {
        parent::__construct();

        $this->getSharedApp();
    }

    public function __destruct()
    {
        $this->appModel = null;
    }

    /**
     * @return AppSiteExampleModel
     */
    public function getAppModel()
    {
        if( !$this->appModel )
        {
            $this->appModel = new AppSiteExampleModel();
        }
        return $this->appModel;
    }

    /**
     * @return InitializeAppSiteExampleCommand
     */
    public function getInitializeCommand()
    {
        return new InitializeAppSiteExampleCommand();
    }

    /**
     * @return ProcessAppSiteExampleCommand
     */
    public function getProcessCommand()
    {
        return new ProcessAppSiteExampleCommand();
    }

    /**
     * @return ApplyModificationsExampleService
     */
    public function getApplyModificationsService()
    {
        return new ApplyModificationsExampleService();
    }
}