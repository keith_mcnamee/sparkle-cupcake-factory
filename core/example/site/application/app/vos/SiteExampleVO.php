<?php


class SiteExampleVO extends ReferenceVO
{

    /**
     * @var string
     */
    private $exampleValue;

    /**
     * @return string
     */
    public function getExampleValue()
    {
        return $this->exampleValue;
    }

    /**
     * @param string $value
     */
    public function setExampleValue( $value )
    {
        $this->exampleValue = $value;
    }
} 