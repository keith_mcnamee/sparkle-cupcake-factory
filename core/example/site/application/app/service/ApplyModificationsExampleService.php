<?php


class ApplyModificationsExampleService extends SiteExampleBase
{

    /**
     * @var ConditionDatabaseConstructor
     */
    private $conditionConstructor;

    /**
     * @var AppSiteExampleModel
     */
    private $appModel;

    public function __destruct()
    {
        $this->conditionConstructor = null;
        $this->appModel = null;

        parent::__destruct();
    }

    public function apply()
    {
        $this->insertExample();
        //$this->selectExample();
        //$this->updateExample();
        //$this->deleteExample();
    }

    private function insertExample()
    {
        $exampleRequestVO = $this->getSiteRequest()->getRequestModel()->getExampleRequestVO();
        if( !$exampleRequestVO->getExampleValue() )
        {
            return;
        }

        $exampleRequestVOs = array();
        $exampleRequestVOs[] = $exampleRequestVO;
        $selectVO = $this->selectVO();
        $exampleVOs = $this->getSiteDatabase()->getInsertRequest()->exampleInsertRequest( $exampleRequestVOs, null, $selectVO );

        foreach( $exampleVOs as $key => $exampleVO )
        {
            /* @var $exampleVO SiteExampleVO */
            $this->getAppModel()->addExampleVO( $exampleVO, $exampleVO->getRef() );
        }
    }

    private function selectExample()
    {
        $selectVO = $this->selectVO();

        $selectVO->addCondition( $this->getConditionConstructor()->equalsUnknown( SqlDatabaseCoreConstants::$REF ) );
        $selectVO->addIntValueToCondition( 350 );

        $exampleVOs = $this->getSiteDatabase()->getSelectRequest()->exampleSelectRequest( $selectVO );

        foreach( $exampleVOs as $key => $exampleVO )
        {
            /* @var $exampleVO SiteExampleVO */
            $this->getAppModel()->addExampleVO( $exampleVO, $exampleVO->getRef() );
        }
    }

    private function updateExample()
    {
        $updateVO = $this->getSiteDatabase()->getUpdateConstructor()->exampleVO();

        $updateVO->addStringColumnValue( SqlDatabaseExampleConstants::$EXAMPLE_VALUE, "KMc" );

        $updateVO->addCondition( $this->getConditionConstructor()->equalsUnknown( SqlDatabaseCoreConstants::$REF ) );
        $updateVO->addIntValueToCondition( 351 );

        $exampleVOs = $this->getSiteDatabase()->getUpdateRequest()->exampleUpdateSelectRequest( $updateVO );

        foreach( $exampleVOs as $key => $exampleVO )
        {
            /* @var $exampleVO SiteExampleVO */
            $this->getAppModel()->addExampleVO( $exampleVO, $exampleVO->getRef() );
        }
    }

    private function deleteExample()
    {
        $deleteVO = $this->getSiteDatabase()->getDeleteConstructor()->exampleVO();

        $deleteVO->addCondition( $this->getConditionConstructor()->equalsUnknown( SqlDatabaseCoreConstants::$REF ) );
        $deleteVO->addIntValueToCondition( 351 );

        $this->getSiteDatabase()->getDeleteRequest()->exampleDeleteRequest( $deleteVO );
    }

    /**
     * @return SelectRequestDatabaseCoreVO
     */
    private function selectVO()
    {
        return $this->getSiteDatabase()->getSelectConstructor()->exampleVO();
    }

    /**
     * @return ConditionDatabaseConstructor
     */
    private function getConditionConstructor()
    {
        if( !$this->conditionConstructor )
        {
            $this->conditionConstructor = $this->getCoreDatabase()->getConditionConstructor();
        }
        return $this->conditionConstructor;
    }

    /**
     * @return AppSiteExampleModel
     */
    private function getAppModel()
    {
        if( !$this->appModel )
        {
            $this->appModel = $this->getSiteApp()->getAppModel();
        }
        return $this->appModel;
    }
    
}