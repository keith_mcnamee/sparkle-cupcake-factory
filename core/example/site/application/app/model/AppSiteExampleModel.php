<?php


class AppSiteExampleModel extends SiteExampleBase
{

    /**
     * @var array
     */
    private $exampleVOs;

    function __construct()
    {

        $this->setExampleVOs( array () );
    }


    // Add to dictionary


    /**
     * @param SiteExampleVO $object
     * @param int $key
     */
    public function addExampleVO( SiteExampleVO $object, $key )
    {
        $this->setExampleVOs( CoreHelper::getArrayHelper()->setObjectForKey( $this->getExampleVOs(), $object, $key ) );
    }

    /**
     * @return array
     */
    public function getExampleVOs()
    {
        return $this->exampleVOs;
    }

    /**
     * @param array $value
     */
    public function setExampleVOs( $value )
    {
        $this->exampleVOs = $value;
    }

} 