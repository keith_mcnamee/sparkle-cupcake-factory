<?php


class ProcessAppSiteExampleCommand extends SiteExampleBase
{

    public function command()
    {
        $this->getSiteApp()->getInitializeCommand()->command();
        $requests = $this->getCoreRequest()->getRetrieveValuesService()->requests();
        $this->getSiteRequest()->getProcessCommand()->command( $requests );
        $this->getSiteApp()->getApplyModificationsService()->apply();

        $this->showView();
    }

    private function showView()
    {
        $this->getPageExampleView()->getCommand()->showView();
    }
}