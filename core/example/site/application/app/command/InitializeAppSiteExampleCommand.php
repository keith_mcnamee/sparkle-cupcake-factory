<?php


class InitializeAppSiteExampleCommand extends SiteExampleBase
{

    public function command()
    {
        $databaseConfigVOs = $this->getSiteDatabase()->getDatabaseConfig()->getConfigVOs();

        $databaseConfigVOs = $this->getCoreDatabase()->getDetermineConfigService()->multipleConfigs( $databaseConfigVOs );

        $this->getCoreDatabase()->getInitializeCommand()->command( DatabaseSiteExampleConstants::$UPDATE_DATABASE_CONFIG_ID, $databaseConfigVOs );
    }
} 