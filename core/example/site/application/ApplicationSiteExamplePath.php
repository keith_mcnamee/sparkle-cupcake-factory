<?php


class ApplicationSiteExamplePath
{

    /**
     * @var AppSiteExamplePath
     */
    private $app;

    /**
     * @var DatabaseSiteExamplePath
     */
    private $database;

    /**
     * @var RequestSiteExamplePath
     */
    private $request;


    public function __destruct()
    {
        $this->app = null;
        $this->database = null;
        $this->request = null;
    }

    /**
     * @return AppSiteExamplePath
     */
    private function createApp()
    {
        $this->app = new AppSiteExamplePath();
        return $this->app;
    }

    /**
     * @return DatabaseSiteExamplePath
     */
    private function createDatabase()
    {
        require_once( DIR_EXAMPLE_SITE . "/application/database/DatabaseSiteExamplePath.php" );
        $this->database = new DatabaseSiteExamplePath();
        return $this->database;
    }

    /**
     * @return RequestSiteExamplePath
     */
    private function createRequest()
    {
        require_once( DIR_EXAMPLE_SITE . "/application/request/RequestSiteExamplePath.php" );
        $this->request = new RequestSiteExamplePath();
        return $this->request;
    }

    /**
     * @return AppSiteExamplePath
     */
    public function getApp()
    {
        return $this->app ? $this->app : $this->createApp();
    }

    /**
     * @return DatabaseSiteExamplePath
     */
    public function getDatabase()
    {
        return $this->database ? $this->database : $this->createDatabase();
    }

    /**
     * @return RequestSiteExamplePath
     */
    public function getRequest()
    {
        return $this->request ? $this->request : $this->createRequest();
    }
}