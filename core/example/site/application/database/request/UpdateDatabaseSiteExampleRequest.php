<?php


class UpdateDatabaseSiteExampleRequest extends SiteExampleBase
{

    /**
     * @var BasicDatabaseRequest
     */
    private $basicDatabaseRequest;

    public function __destruct()
    {
        $this->basicDatabaseRequest = null;

        parent::__destruct();
    }

    /**
     * @param UpdateRequestDatabaseCoreVO $updateVO
     * @param SelectRequestDatabaseCoreVO $selectVO
     * @return array
     */
    public function exampleUpdateSelectRequest( UpdateRequestDatabaseCoreVO $updateVO = null, SelectRequestDatabaseCoreVO $selectVO = null )
    {
        if( !$selectVO )
        {
            $selectVO = $this->getSiteDatabase()->getSelectConstructor()->exampleVO();
        }
        $vos = $this->exampleUpdateRequest( $updateVO, $selectVO );
        return $vos;
    }

    /**
     * @param UpdateRequestDatabaseCoreVO $updateVO
     * @param SelectRequestDatabaseCoreVO $selectVO
     * @return array
     */
    public function exampleUpdateRequest( UpdateRequestDatabaseCoreVO $updateVO = null, SelectRequestDatabaseCoreVO $selectVO = null )
    {
        if( !$updateVO )
        {
            $updateVO = $this->getSiteDatabase()->getUpdateConstructor()->exampleVO();
        }
        $entryData = $this->getBasicDatabaseRequest()->update( $updateVO, $selectVO );
        if($entryData)
        {
            $vos = $this->getSiteDatabase()->getDatabaseParser()->exampleVOs( $entryData );
            return $vos;
        }
        return array();
    }

    /**
     * @return BasicDatabaseRequest
     */
    private function getBasicDatabaseRequest()
    {
        if( !$this->basicDatabaseRequest )
        {
            $this->basicDatabaseRequest = $this->getCoreDatabase()->getBasicRequest();
        }
        return $this->basicDatabaseRequest;
    }
} 