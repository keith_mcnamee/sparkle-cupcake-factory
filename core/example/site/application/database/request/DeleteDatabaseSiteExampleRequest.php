<?php


class DeleteDatabaseSiteExampleRequest extends SiteExampleBase
{

    /**
     * @var BasicDatabaseRequest
     */
    private $basicDatabaseRequest;

    public function __destruct()
    {
        $this->basicDatabaseRequest = null;

        parent::__destruct();
    }

    /**
     * @param DeleteRequestDatabaseCoreVO $deleteVO
     */
    public function exampleDeleteRequest( DeleteRequestDatabaseCoreVO $deleteVO = null )
    {
        if( !$deleteVO )
        {
            $deleteVO = $this->getSiteDatabase()->getDeleteConstructor()->exampleVO();
        }
        $this->getBasicDatabaseRequest()->delete( $deleteVO );
    }

    /**
     * @return BasicDatabaseRequest
     */
    private function getBasicDatabaseRequest()
    {
        if( !$this->basicDatabaseRequest )
        {
            $this->basicDatabaseRequest = $this->getCoreDatabase()->getBasicRequest();
        }
        return $this->basicDatabaseRequest;
    }
} 