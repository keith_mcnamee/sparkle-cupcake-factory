<?php


class InsertDatabaseSiteExampleRequest extends SiteExampleBase
{

    /**
     * @var BasicDatabaseRequest
     */
    private $basicDatabaseRequest;

    public function __destruct()
    {
        $this->basicDatabaseRequest = null;

        parent::__destruct();
    }

    /**
     * @param array $exampleVOs
     * @param InsertRequestDatabaseCoreVO $insertVO
     * @param SelectRequestDatabaseCoreVO $selectVO
     * @return array
     */
    public function exampleInsertRequest( array $exampleVOs, InsertRequestDatabaseCoreVO $insertVO = null, SelectRequestDatabaseCoreVO $selectVO = null )
    {
        if( !$insertVO )
        {
            $insertVO = $this->getSiteDatabase()->getInsertConstructor()->exampleVO( $exampleVOs );
        }
        $entries = $this->getBasicDatabaseRequest()->insert( $insertVO, $selectVO );

        if( !$entries )
        {
            return null;
        }
        $vos = $this->getSiteDatabase()->getDatabaseParser()->exampleVOs( $entries );
        return $vos;
    }

    /**
     * @return BasicDatabaseRequest
     */
    private function getBasicDatabaseRequest()
    {
        if( !$this->basicDatabaseRequest )
        {
            $this->basicDatabaseRequest = $this->getCoreDatabase()->getBasicRequest();
        }
        return $this->basicDatabaseRequest;
    }
} 