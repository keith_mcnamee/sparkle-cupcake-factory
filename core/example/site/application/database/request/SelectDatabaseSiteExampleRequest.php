<?php


class SelectDatabaseSiteExampleRequest extends SiteExampleBase
{

    /**
     * @var BasicDatabaseRequest
     */
    private $basicDatabaseRequest;

    public function __destruct()
    {
        $this->basicDatabaseRequest = null;

        parent::__destruct();
    }

    /**
     * @param SelectRequestDatabaseCoreVO $selectVO
     * @return array
     */
    public function exampleSelectRequest( SelectRequestDatabaseCoreVO $selectVO = null )
    {
        if( !$selectVO )
        {
            $selectVO = $this->getSiteDatabase()->getSelectConstructor()->exampleVO();
        }
        $entryData = $this->getBasicDatabaseRequest()->select( $selectVO );
        $vos = $this->getSiteDatabase()->getDatabaseParser()->exampleVOs( $entryData );
        return $vos;
    }

    /**
     * @return BasicDatabaseRequest
     */
    private function getBasicDatabaseRequest()
    {
        if( !$this->basicDatabaseRequest )
        {
            $this->basicDatabaseRequest = $this->getCoreDatabase()->getBasicRequest();
        }
        return $this->basicDatabaseRequest;
    }
} 