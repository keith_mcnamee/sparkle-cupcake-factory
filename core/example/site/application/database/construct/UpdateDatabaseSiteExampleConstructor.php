<?php


class UpdateDatabaseSiteExampleConstructor extends SiteExampleBase
{
    /**
     * @return UpdateRequestDatabaseCoreVO
     */
    public function exampleVO()
    {
        $updateVO = new UpdateRequestDatabaseCoreVO( SqlDatabaseExampleConstants::$EXAMPLE_TABLE );

        return $updateVO;
    }
} 