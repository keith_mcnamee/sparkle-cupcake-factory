<?php


class DeleteDatabaseSiteExampleConstructor extends SiteExampleBase
{
    /**
     * @return DeleteRequestDatabaseCoreVO
     */
    public function exampleVO()
    {
        $deleteVO = new DeleteRequestDatabaseCoreVO( SqlDatabaseExampleConstants::$EXAMPLE_TABLE );

        return $deleteVO;
    }
} 