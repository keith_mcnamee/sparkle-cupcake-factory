<?php


class SelectDatabaseSiteExampleConstructor extends SiteExampleBase
{

    /**
     * @return SelectRequestDatabaseCoreVO
     */
    public function exampleVO()
    {
        $selectVO = new SelectRequestDatabaseCoreVO( SqlDatabaseExampleConstants::$EXAMPLE_TABLE );

        $selectVO->addColumn( SqlDatabaseCoreConstants::$REF );
        $selectVO->addColumn( SqlDatabaseExampleConstants::$EXAMPLE_VALUE );

        return $selectVO;
    }
} 