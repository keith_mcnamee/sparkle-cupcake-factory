<?php


class InsertDatabaseSiteExampleConstructor extends SiteExampleBase
{

    /**
     * @param array $vos
     * @return InsertRequestDatabaseCoreVO
     */
    public function exampleVO( array $vos )
    {
        $insertVO = new InsertRequestDatabaseCoreVO( SqlDatabaseExampleConstants::$EXAMPLE_TABLE );

        foreach( $vos as $vo )
        {
            /* @var $vo RequestSiteExampleVO */
            $insertVO->addStringColumnValue( SqlDatabaseExampleConstants::$EXAMPLE_VALUE, $vo->getExampleValue() );
        }

        $insertVO->setPrimaryKey( SqlDatabaseCoreConstants::$REF );

        return $insertVO;
    }
} 