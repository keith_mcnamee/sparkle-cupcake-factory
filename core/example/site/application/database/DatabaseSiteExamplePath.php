<?php


class DatabaseSiteExamplePath extends SiteExampleBase
{

    /**
     * @var DatabaseSiteExampleConfig
     */
    private $databaseConfig;

    public function __construct()
    {
        parent::__construct();

        $this->getCoreDatabase();
        $this->getIncludes();
    }

    public function __destruct()
    {
        $this->databaseConfig = null;
    }

    private function getIncludes()
    {
        require_once( DIR_EXAMPLE_SITE . "/application/database/includes/DatabaseSiteExampleIncludes.php" );
    }

    /**
     * @return DatabaseSiteExampleConfig
     */
    public function getDatabaseConfig()
    {
        if( !$this->databaseConfig )
        {
            $this->databaseConfig = new DatabaseSiteExampleConfig();
        }
        return $this->databaseConfig;
    }

    /**
     * @return DeleteDatabaseSiteExampleConstructor
     */
    public function getDeleteConstructor()
    {
        return new DeleteDatabaseSiteExampleConstructor();
    }

    /**
     * @return InsertDatabaseSiteExampleConstructor
     */
    public function getInsertConstructor()
    {
        return new InsertDatabaseSiteExampleConstructor();
    }

    /**
     * @return SelectDatabaseSiteExampleConstructor
     */
    public function getSelectConstructor()
    {
        return new SelectDatabaseSiteExampleConstructor();
    }

    /**
     * @return UpdateDatabaseSiteExampleConstructor
     */
    public function getUpdateConstructor()
    {
        return new UpdateDatabaseSiteExampleConstructor();
    }

    /**
     * @return DatabaseSiteExampleParser
     */
    public function getDatabaseParser()
    {
        return new DatabaseSiteExampleParser();
    }

    /**
     * @return DeleteDatabaseSiteExampleRequest
     */
    public function getDeleteRequest()
    {
        return new DeleteDatabaseSiteExampleRequest();
    }

    /**
     * @return InsertDatabaseSiteExampleRequest
     */
    public function getInsertRequest()
    {
        return new InsertDatabaseSiteExampleRequest();
    }

    /**
     * @return SelectDatabaseSiteExampleRequest
     */
    public function getSelectRequest()
    {
        return new SelectDatabaseSiteExampleRequest();
    }

    /**
     * @return UpdateDatabaseSiteExampleRequest
     */
    public function getUpdateRequest()
    {
        return new UpdateDatabaseSiteExampleRequest();
    }
}