<?php


class DatabaseSiteExampleConfig extends SiteExampleBase
{

    /**
     * @var array
     */
    private $configVOs;

    public function __destruct()
    {
        $this->configVOs = null;
    }

    /**
     * @return array
     */
    public function getConfigVOs()
    {
        return $this->configVOs ? $this->configVOs : $this->createConfigVOs();
    }

    /**
     * @return array
     */
    protected function createConfigVOs()
    {
        $this->configVOs = array();

        $this->configVOs[] = $this->localhostConfigVO();

        return $this->configVOs;
    }

    /**
     * @return ConfigDatabaseCoreVO
     */
    protected function localhostConfigVO()
    {
        $configVO = new ConfigDatabaseCoreVO( DatabaseSiteExampleConstants::$UPDATE_DATABASE_CONFIG_ID  );

        $configVO->setServerConfigType( InternalRequestCoreConstants::$LOCALHOST_SERVER_CONFIG_ID );

        $configVO->setHostName("localhost");
        $configVO->setUser("root");
        $configVO->setPassword("root");

        $configVO->setStagingDatabase( true );

        $configVO->setDatabaseName("HappenedUpdateStaging");

        return $configVO;
    }
}