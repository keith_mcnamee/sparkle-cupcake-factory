<?php


class DatabaseSiteExampleParser extends SiteExampleBase
{

    /**
     * @param array $datas
     * @return array
     */
    public function exampleVOs( array $datas )
    {
        $vos = array();
        foreach ( $datas as $data )
        {
            /* @var $data array */
            $vo = $this->exampleVO( $data );
            $vos[ $vo->getRef() ] = $vo;
        }
        return $vos;
    }

    /**
     * @param array $data
     * @param SiteExampleVO $vo
     * @return SiteExampleVO
     */
    public function exampleVO( array $data, SiteExampleVO $vo = null )
    {
        if( !$vo )
        {
            $vo = new SiteExampleVO();
        }

        $vo->setRef( CoreHelper::getArrayHelper()->intFromDictionary( $data, SqlDatabaseCoreConstants::$REF ) );
        $vo->setExampleValue( CoreHelper::getArrayHelper()->stringFromDictionary( $data, SqlDatabaseExampleConstants::$EXAMPLE_VALUE ) );

        return $vo;
    }
} 