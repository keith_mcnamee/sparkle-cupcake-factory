<?php


class ProcessRequestSiteExampleCommand extends SiteExampleBase
{

    /**
     * @param array $requests
     */
    public function command( array $requests )
    {
        $requestObject = $this->getSiteRequest()->getRequestParser()->parse( $requests );

        $this->addRequests( $requestObject );
    }

    /**
     * @param array $requestObject
     */
    private function addRequests( array $requestObject )
    {

        foreach( $requestObject as $category => $requests )
        {
            /* @var $requests array */

            switch( $category )
            {
                case RequestSiteExampleConstants::$EXAMPLE_CATEGORY:
                    $this->addExampleRequests( $requests );
                    break;

                default:
                    break;

            }
        }
    }


    /**
     * @param array $requests
     */
    private function addExampleRequests( array $requests )
    {
        $requestModel = $this->getSiteRequest()->getRequestModel();

        foreach( $requests as $type => $request )
        {
            /* @var $request mixed */

            switch( $type )
            {
                case RequestSiteExampleConstants::$EXAMPLE_REQUEST:
                    /* @var $request RequestSiteExampleVO */
                    $requestModel->setExampleRequestVO( $request );
                    break;

                default:
                    break;

            }
        }
    }
} 