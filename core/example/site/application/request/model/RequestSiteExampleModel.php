<?php


class RequestSiteExampleModel extends SiteExampleBase
{

    /**
     * @var RequestSiteExampleVO
     */
    private $exampleRequestVO;

    public function __construct()
    {

    }

    /**
     * @return RequestSiteExampleVO
     */
    public function getExampleRequestVO()
    {
        return $this->exampleRequestVO;
    }

    /**
     * @param RequestSiteExampleVO $value
     */
    public function setExampleRequestVO( $value )
    {
        $this->exampleRequestVO = $value;
    }

} 