<?php


class RequestSiteExampleVO
{
    /**
     * @var string
     */
    private $exampleValue;

    public function __construct()
    {

    }

    /**
     * @return string
     */
    public function getExampleValue()
    {
        return $this->exampleValue;
    }

    /**
     * @param string $value
     */
    public function setExampleValue( $value )
    {
        $this->exampleValue = $value;
    }
} 