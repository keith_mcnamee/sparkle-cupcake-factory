<?php


class RequestSiteExamplePath extends SiteExampleBase
{

    /**
     * @var RequestSiteExampleModel
     */
    private $requestModel;

    public function __construct()
    {
        parent::__construct();

        $this->getSharedRequest();
        $this->getIncludes();
    }

    public function __destruct()
    {
        $this->requestModel = null;
    }

    private function getIncludes()
    {
        require_once( DIR_EXAMPLE_SITE . "/application/request/includes/RequestSiteExampleIncludes.php" );
    }

    /**
     * @return RequestSiteExampleModel
     */
    public function getRequestModel()
    {
        if( !$this->requestModel )
        {
            $this->requestModel = new RequestSiteExampleModel();
        }
        return $this->requestModel;
    }

    /**
     * @return ProcessRequestSiteExampleCommand
     */
    public function getProcessCommand()
    {
        return new ProcessRequestSiteExampleCommand();
    }

    /**
     * @return RequestSiteExampleParser
     */
    public function getRequestParser()
    {
        return new RequestSiteExampleParser();
    }
}