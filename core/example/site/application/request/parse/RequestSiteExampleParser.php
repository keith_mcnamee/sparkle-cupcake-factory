<?php


class RequestSiteExampleParser extends SiteExampleBase
{

    /**
     * @var RetrieveRequestCoreConstructor
     */
    private $retrieveConstructor;

    public function __destruct()
    {
        $this->retrieveConstructor = null;

        parent::__destruct();
    }

    /**
     * @param array $requests
     * @return array
     */
    public function parse( array $requests )
    {
        $requestObject = array();
        $requestObject[ RequestSiteExampleConstants::$EXAMPLE_CATEGORY ] = array();

        $exampleRequestVO = new RequestSiteExampleVO();

        foreach( $requests as $request )
        {
            /* @var $request array */
            $exampleRequestVO = $this->exampleVO( $request, $exampleRequestVO );
        }
        $requestObject[ RequestSiteExampleConstants::$EXAMPLE_CATEGORY ][ RequestSiteExampleConstants::$EXAMPLE_REQUEST ] = $exampleRequestVO;

        return $requestObject;
    }
    /**
     * @param array $data
     * @param RequestSiteExampleVO $vo
     * @return RequestSiteExampleVO
     */
    public function exampleVO( array $data, RequestSiteExampleVO $vo )
    {
        $exampleValue = $this->getRetrieveConstructor()->valueConstruct( $data, ParameterRequestExampleConstants::$EXAMPLE_VALUE, $vo->getExampleValue(), true );

        $vo->setExampleValue( $exampleValue );

        return $vo;
    }

    /**
     * @return RetrieveRequestCoreConstructor
     */
    private function getRetrieveConstructor()
    {
        if( !$this->retrieveConstructor )
        {
            $this->retrieveConstructor = $this->getCoreRequest()->getRetrieveConstructor();
        }
        return $this->retrieveConstructor;
    }
} 