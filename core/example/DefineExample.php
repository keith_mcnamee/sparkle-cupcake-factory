<?php

require_once( DIR_EXAMPLE_ROOT . "/DefineCore.php" );

define( 'DIR_EXAMPLE_APP', DIR_EXAMPLE_ROOT . "/example" );
define( 'DIR_EXAMPLE_SHARED', DIR_EXAMPLE_APP . "/shared" );
define( 'DIR_EXAMPLE_SITE', DIR_EXAMPLE_APP . "/site" );