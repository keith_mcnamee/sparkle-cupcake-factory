<?php


class SharedExampleBase extends CoreBase
{

    /**
     * @var SharedExamplePath
     */
    private $sharedPath;

    public function __destruct()
    {
        $this->sharedPath = null;

        parent::__destruct();
    }

    /**
     * @return SharedExamplePath
     */
    private function createSharedPath()
    {
        $this->sharedPath = SharedExamplePath::getInstance();
        return $this->sharedPath;
    }

    /**
     * @return SharedExamplePath
     */
    protected function getSharedPath()
    {
        return $this->sharedPath ? $this->sharedPath : $this->createSharedPath();
    }

    /**
     * @return AppSharedExamplePath
     */
    protected function getSharedApp()
    {
        return $this->getSharedPath()->getApplication()->getApp();
    }

    /**
     * @return RequestSharedExamplePath
     */
    protected function getSharedRequest()
    {
        return $this->getSharedPath()->getApplication()->getRequest();
    }
}