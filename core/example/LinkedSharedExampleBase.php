<?php


class LinkedSharedExampleBase extends SharedExampleBase
{

    /**
     * @var SiteExamplePath
     */
    private $site;

    public function __destruct()
    {
        $this->site = null;

        parent::__destruct();
    }

    /**
     * @param $singleInstance
     * @return SiteExamplePath
     */
    private function createSite( $singleInstance )
    {
        require_once( DIR_EXAMPLE_SITE . "/SiteExamplePath.php" );
        $instance = SiteExamplePath::getInstance();
        if( !$singleInstance )
        {
            $this->site = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return SiteExamplePath
     */
    protected function getSite( $singleInstance = false )
    {
        return $this->site ? $this->site : $this->createSite( $singleInstance );
    }
}