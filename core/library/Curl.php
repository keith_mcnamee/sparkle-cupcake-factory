<?php

class Curl
{
    /**
     * @var string
     */
    public $cookieJar;

    /**
     * @var resource
     */
    public $curl;

    /**
     * @param string $cookieJarFile
     */
    public function __construct( $cookieJarFile = 'cookies.txt' )
    {
        $this->setCookieJar( $cookieJarFile );
    }

    /**
     * @param $url
     * @return resource
     */
    public function get( $url )
    {
        $curl = curl_init( $url );
        $this->setCurl( $curl );
        $this->setup();

        return $this->request();
    }

    protected function setup()
    {
        $header = array ();
        $header[ 0 ] = "Accept: text/xml,application/xml,application/xhtml+xml,";
        $header[ 0 ] .= "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
        $header[ ] = "Cache-Control: max-age=0";
        $header[ ] = "Connection: keep-alive";
        $header[ ] = "Keep-Alive: 300";
        $header[ ] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
        $header[ ] = "Accept-Language: en-us,en;q=0.5";
        $header[ ] = "Pragma: "; // browsers keep this blank.

        curl_setopt( $this->getCurl(), CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US; rv:1.8.1.7) Gecko/20070914 Firefox/2.0.0.7' );
        curl_setopt( $this->getCurl(), CURLOPT_HTTPHEADER, $header );
        curl_setopt( $this->getCurl(), CURLOPT_COOKIEJAR, $this->getCookieJar() );
        curl_setopt( $this->getCurl(), CURLOPT_COOKIEFILE, $this->getCookieJar() );
        curl_setopt( $this->getCurl(), CURLOPT_AUTOREFERER, true );
        curl_setopt( $this->getCurl(), CURLOPT_FOLLOWLOCATION, true );
        curl_setopt( $this->getCurl(), CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $this->getCurl(), CURLOPT_HEADER,true);
    }

    /**
     * @return resource
     */
    protected function request()
    {
        $request = curl_exec( $this->getCurl() );

        return $request;
    }

    /**
     * @param $reg
     * @param $str
     * @return mixed
     */
    protected function getAll( $reg, $str )
    {
        preg_match_all( $reg, $str, $matches );

        return $matches[ 1 ];
    }

    /**
     * @param $url
     * @param $fields
     * @param string $referer
     * @return mixed
     */
    protected function postForm( $url, $fields, $referer = '' )
    {
        $curl = curl_init( $url );
        $this->setCurl( $curl );
        $this->setup();
        curl_setopt( $this->getCurl(), CURLOPT_URL, $url );
        curl_setopt( $this->getCurl(), CURLOPT_POST, 1 );
        curl_setopt( $this->getCurl(), CURLOPT_REFERER, $referer );
        curl_setopt( $this->getCurl(), CURLOPT_POSTFIELDS, $fields );

        return $this->request();
    }

    /**
     * @param $info
     * @return mixed
     */
    protected function getInfo( $info )
    {
        $info = ( $info == 'lasturl' ) ? curl_getinfo( $this->getCurl(), CURLINFO_EFFECTIVE_URL ) : curl_getinfo( $this->getCurl(), $info );

        return $info;
    }

    /**
     * @return string
     */
    public function getCookieJar()
    {
        return $this->cookieJar;
    }

    /**
     * @param string $value
     */
    public function setCookieJar( $value )
    {
        $this->cookieJar = $value;
    }

    /**
     * @return resource
     */
    public function getCurl()
    {
        return $this->curl;
    }

    /**
     * @param resource $value
     */
    public function setCurl( $value )
    {
        $this->curl = $value;
    }
}