<?php


class CoreBase
{

    /**
     * @var CorePath
     */
    private $corePath;

    public function __construct()
    {

    }

    public function applyEventState()
    {

    }

    public function __destruct()
    {
        $this->corePath = null;
    }

    /**
     * @return CorePath
     */
    private function addCorePath()
    {
        $this->corePath = CorePath::getInstance();
        return $this->corePath;
    }

    /**
     * @return CorePath
     */
    protected function getCorePath()
    {
        return $this->corePath ? $this->corePath : $this->addCorePath();
    }

    /**
     * @return AppCorePath
     */
    protected function getCoreApp()
    {
        return $this->getCorePath()->getApplication()->getApp();
    }

    /**
     * @return BalancingCorePath
     */
    protected function getCoreBalancing()
    {
        return $this->getCorePath()->getApplication()->getBalancing();
    }

    /**
     * @return DatabaseCorePath
     */
    protected function getCoreDatabase()
    {
        return $this->getCorePath()->getApplication()->getDatabase();
    }

    /**
     * @return BalancingDatabaseCorePath
     */
    protected function getCoreBalancingDatabase()
    {
        return $this->getCorePath()->getApplication()->getDatabase()->getBalancing();
    }

    /**
     * @return LocalizationDatabaseCorePath
     */
    protected function getCoreLocalizationDatabase()
    {
        return $this->getCorePath()->getApplication()->getDatabase()->getLocalization();
    }

    /**
     * @return RequestInfoDatabaseCorePath
     */
    protected function getCoreRequestInfoDatabase()
    {
        return $this->getCorePath()->getApplication()->getDatabase()->getRequestInfo();
    }

    /**
     * @return HelperCorePath
     */
    protected function getCoreHelper()
    {
        return $this->getCorePath()->getApplication()->getHelper();
    }

    /**
     * @return LocalizationCorePath
     */
    protected function getCoreLocalization()
    {
        return $this->getCorePath()->getApplication()->getLocalization();
    }

    /**
     * @return ReadExternalCorePath
     */
    protected function getCoreReadExternal()
    {
        return $this->getCorePath()->getApplication()->getReadExternal();
    }

    /**
     * @return RequestCorePath
     */
    protected function getCoreRequest()
    {
        return $this->getCorePath()->getApplication()->getRequest();
    }

    /**
     * @return SystemCorePath
     */
    protected function getCoreSystem()
    {
        return $this->getCorePath()->getApplication()->getSystem();
    }

    /**
     * @return PageViewCorePath
     */
    protected function getCorePageView()
    {
        return $this->getCorePath()->getView()->getPage();
    }

    /**
     * @return DebugViewCorePath
     */
    protected function getCoreDebugView()
    {
        return $this->getCorePath()->getView()->getDebug();
    }

    /**
     * @return DebugViewService
     */
    protected function getDebugService()
    {
        return $this->getCoreDebugView()->getService();
    }

}