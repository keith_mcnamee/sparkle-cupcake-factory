-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Aug 17, 2015 at 03:41 PM
-- Server version: 5.5.38
-- PHP Version: 5.5.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cupcake`
--

-- --------------------------------------------------------

--
-- Table structure for table `available_cupcakes`
--

CREATE TABLE `available_cupcakes` (
`id` int(11) NOT NULL,
  `file_name` varchar(100) NOT NULL,
  `priority` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `available_cupcakes`
--

INSERT INTO `available_cupcakes` (`id`, `file_name`, `priority`, `created`) VALUES
(1, 'cupcake1.png', 1, '2015-08-16 18:21:37'),
(2, 'cupcake2.png', 2, '2015-08-16 18:21:37'),
(3, 'cupcake3.png', 3, '2015-08-16 18:21:37'),
(4, 'cupcake4.png', 4, '2015-08-16 18:21:37'),
(5, 'cupcake5.png', 5, '2015-08-16 18:21:37'),
(6, 'cupcake6.png', 6, '2015-08-16 18:21:37'),
(7, 'cupcake7.png', 7, '2015-08-16 18:21:37'),
(8, 'cupcake8.png', 8, '2015-08-16 18:21:37');

-- --------------------------------------------------------

--
-- Table structure for table `saved_orders`
--

CREATE TABLE `saved_orders` (
`id` int(11) NOT NULL,
  `order_cupcakes` varchar(1000) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `saved_orders`
--

INSERT INTO `saved_orders` (`id`, `order_cupcakes`, `created`) VALUES
(1, '[\\"1\\",\\"2\\",\\"3\\",\\"4\\",\\"5\\",\\"6\\",\\"7\\",\\"8\\"]', '2015-08-17 02:44:06'),
(2, '[\\"1\\",\\"2\\",\\"3\\",\\"4\\",\\"5\\",\\"6\\",\\"7\\",\\"8\\"]', '2015-08-17 02:44:25'),
(3, '[\\"1\\",\\"2\\",\\"3\\",\\"4\\",\\"5\\",\\"6\\",\\"7\\",\\"8\\"]', '2015-08-17 02:45:47'),
(4, '[\\"1\\",\\"2\\",\\"3\\",\\"4\\",\\"5\\",\\"6\\",\\"7\\",\\"8\\"]', '2015-08-17 02:47:22'),
(5, '[\\"1\\",\\"2\\",\\"3\\",\\"4\\",\\"5\\",\\"6\\",\\"7\\",\\"8\\"]', '2015-08-17 02:49:09'),
(6, '[\\"1\\",\\"2\\",\\"3\\",\\"4\\",\\"5\\",\\"6\\",\\"7\\",\\"8\\"]', '2015-08-17 02:49:45'),
(7, '[\\"1\\",\\"2\\",\\"3\\",\\"4\\",\\"5\\",\\"6\\",\\"7\\",\\"8\\"]', '2015-08-17 02:50:18'),
(8, '[\\"1\\",\\"2\\",\\"3\\",\\"4\\",\\"5\\",\\"6\\",\\"7\\",\\"8\\"]', '2015-08-17 02:50:59'),
(9, '[\\"1\\",\\"2\\",\\"3\\",\\"4\\",\\"5\\",\\"6\\",\\"7\\",\\"8\\"]', '2015-08-17 02:51:12'),
(10, '[1,2,3,4,5,6,7,8]', '2015-08-17 02:54:15'),
(11, '[1,2,3,4,5,6,7,8]', '2015-08-17 03:13:05'),
(12, '[1,2,3,4,5,6,7,8]', '2015-08-17 03:13:05'),
(13, '[1,2,3,4,5,6,7,8]', '2015-08-17 03:13:47'),
(14, '[1,2,3,4,5,6,7,8]', '2015-08-17 03:14:34'),
(15, '[1,2,3,4,5,6,7,8]', '2015-08-17 03:14:48'),
(16, '[1,7]', '2015-08-17 03:21:09'),
(17, '[7,3,1]', '2015-08-17 03:21:35'),
(18, '[1]', '2015-08-17 03:22:01'),
(19, '[3]', '2015-08-17 03:23:07'),
(20, '[2,3,6]', '2015-08-17 03:23:22'),
(21, '[2]', '2015-08-17 08:33:49'),
(22, '[1]', '2015-08-17 08:40:43'),
(23, '[3]', '2015-08-17 09:42:18'),
(24, '[1,6]', '2015-08-17 10:38:47'),
(25, '[8]', '2015-08-17 13:31:56'),
(26, '[1,6]', '2015-08-17 13:34:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `available_cupcakes`
--
ALTER TABLE `available_cupcakes`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `saved_orders`
--
ALTER TABLE `saved_orders`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `available_cupcakes`
--
ALTER TABLE `available_cupcakes`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `saved_orders`
--
ALTER TABLE `saved_orders`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
