<?php

require_once( dirname( __FILE__ ) . "/core/DefineCore.php" );

define( 'DIR_APP', DIR_ROOT . "/cupcake" );
define( 'DIR_SHARED', DIR_APP . "/shared" );
define( 'DIR_SITE', DIR_APP . "/site" );
