<?php

class SharedPath
{

    /**
     * @var ApplicationSharedPath
     */
    private $application;

    public function __construct()
    {
        $this->createCore();
        $this->getIncludes();
    }

    public function __destruct()
    {
        $this->application = null;
    }

    public function process()
    {
        $this->getApplication()->getApp()->getProcessCommand()->command();
    }

    private function createCore()
    {
        require_once( DIR_CORE . "/CorePath.php" );
        CorePath::getInstance();
    }

    private function getIncludes()
    {
        require_once( DIR_SHARED . "/application/app/includes/AppIncludes.php" );
    }

    /**
     * @return static
     */
    public static function getInstance()
    {
        static $instance = null;

        if (null === $instance)
        {
            $instance = new static();
        }

        return $instance;
    }

    /**
     * @return ApplicationSharedPath
     */
    private function createApplication()
    {
        $this->application = new ApplicationSharedPath();
        return $this->application;
    }

    /**
     * @return ApplicationSharedPath
     */
    public function getApplication()
    {
        return $this->application ? $this->application : $this->createApplication();
    }
}