<?php


class LinkedSharedBase extends SharedBase
{

    /**
     * @var SitePath
     */
    private $site;

    public function __destruct()
    {
        $this->site = null;

        parent::__destruct();
    }

    /**
     * @param $singleInstance
     * @return SitePath
     */
    private function createSite( $singleInstance )
    {
        require_once( DIR_SITE . "/SitePath.php" );
        $instance = SitePath::getInstance();
        if( !$singleInstance )
        {
            $this->site = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return SitePath
     */
    protected function getSite( $singleInstance = false )
    {
        return $this->site ? $this->site : $this->createSite( $singleInstance );
    }
    
}