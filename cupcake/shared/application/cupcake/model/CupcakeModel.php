<?php


class CupcakeModel extends SharedBase
{

    /**
     * @var array
     */
    private $availableCupcakeVOs;

    /**
     * @var array
     */
    private $savedOrderVOs;

    function __construct()
    {

    }


    // Add to dictionary


    /**
     * @param AvailableCupcakeVO $object
     */
    public function addAvailableCupcakeVO( AvailableCupcakeVO $object )
    {
        $this->setAvailableCupcakeVOs( CoreHelper::getArrayHelper()->setObjectForKey( $this->getAvailableCupcakeVOs(), $object, $object->getId() ) );
    }
    /**
     * @param SavedOrderVO $object
     */
    public function addSavedOrderVO( SavedOrderVO $object )
    {
        $this->setSavedOrderVOs( CoreHelper::getArrayHelper()->setObjectForKey( $this->getSavedOrderVOs(), $object, $object->getId() ) );
    }

    /**
     * @return array
     */
    public function getAvailableCupcakeVOs()
    {
        return $this->availableCupcakeVOs;
    }

    /**
     * @param array $value
     */
    public function setAvailableCupcakeVOs( array $value )
    {
        $this->availableCupcakeVOs = $value;
    }

    /**
     * @return array
     */
    public function getSavedOrderVOs()
    {
        return $this->savedOrderVOs;
    }

    /**
     * @param array $value
     */
    public function setSavedOrderVOs( array $value )
    {
        $this->savedOrderVOs = $value;
    }

}