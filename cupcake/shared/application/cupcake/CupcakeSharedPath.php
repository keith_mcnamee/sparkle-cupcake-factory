<?php


class CupcakeSharedPath extends SharedBase
{

    /**
     * @var CupcakeModel
     */
    private $cupcakeModel;

    public function __construct()
    {
        parent::__construct();
        $this->getIncludes();
    }

    public function __destruct()
    {
        $this->cupcakeModel = null;
    }

    private function getIncludes()
    {
        require_once( DIR_SHARED . "/application/cupcake/includes/CupcakeIncludes.php" );
    }

    /**
     * @return CupcakeModel
     */
    public function getCupcakeModel()
    {
        if( !$this->cupcakeModel )
        {
            $this->cupcakeModel = new CupcakeModel();
        }
        return $this->cupcakeModel;
    }

    /**
     * @return RetrieveAvailableCupcakeCommand
     */
    public function getRetrieveAvailableCupcakeCommand()
    {
        return new RetrieveAvailableCupcakeCommand();
    }
}