<?php


class RetrieveAvailableCupcakeCommand extends SharedBase
{

    /**
     * @param bool|false $ifNoteExistsOnly
     */
    public function command( $ifNoteExistsOnly = false )
    {
        $cupcakeModel = $this->getSharedCupcake()->getCupcakeModel();
        if( $ifNoteExistsOnly && $cupcakeModel->getAvailableCupcakeVOs() !== null )
        {
            return;
        }
        $selectCupcakeDatabaseRequest = $this->getSharedDatabase()->getSelectCupcakeDatabaseRequest();
        $vos = $selectCupcakeDatabaseRequest->availableCupcakesSelectRequest();
        $this->getSharedCupcake()->getCupcakeModel()->setAvailableCupcakeVOs( $vos );
    }

}