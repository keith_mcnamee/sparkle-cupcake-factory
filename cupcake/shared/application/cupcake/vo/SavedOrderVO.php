<?php


class SavedOrderVO
{

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $orderCupcakesJson;

    /**
     * @return string
     */
    public function getOrderCupcakesJson()
    {
        return $this->orderCupcakesJson;
    }

    /**
     * @param string $value
     */
    public function setOrderCupcakesJson( $value )
    {
        $this->orderCupcakesJson = $value;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $value
     */
    public function setId( $value )
    {
        $this->id = $value;
    }

}