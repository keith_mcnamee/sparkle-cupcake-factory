<?php


class AvailableCupcakeVO
{

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $fileName;

    /**
     * @var int
     */
    private $priority;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $value
     */
    public function setId( $value )
    {
        $this->id = $value;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param string $value
     */
    public function setFileName( $value )
    {
        $this->fileName = $value;
    }

    /**
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param int $value
     */
    public function setPriority( $value )
    {
        $this->priority = $value;
    }
}