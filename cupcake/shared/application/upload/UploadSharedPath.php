<?php


class UploadSharedPath extends SharedBase
{

    public function __construct()
    {
        parent::__construct();
        $this->getIncludes();
    }

    private function getIncludes()
    {
        require_once( DIR_SHARED . "/application/upload/includes/UploadIncludes.php" );
    }

    /**
     * @return ProcessUploadCommand
     */
    public function getProcessUploadCommand()
    {
        return new ProcessUploadCommand();
    }

    /**
     * @return UploadHandler
     */
    public function getUploadHandler()
    {
        return new UploadHandler();
    }

}