<?php

class ApplicationSharedPath
{

    /**
     * @var AppSharedPath
     */
    private $app;

    /**
     * @var CupcakeSharedPath
     */
    private $cupcake;

    /**
     * @var DatabaseSharedPath
     */
    private $database;

    /**
     * @var RequestSharedPath
     */
    private $request;

    /**
     * @var UploadSharedPath
     */
    private $upload;

    public function __destruct()
    {
        $this->app = null;
        $this->cupcake = null;
        $this->database = null;
        $this->request = null;
        $this->upload = null;
    }

    /**
     * @return AppSharedPath
     */
    private function createApp()
    {
        $this->app = new AppSharedPath();
        return $this->app;
    }

    /**
     * @return CupcakeSharedPath
     */
    private function createCupcake()
    {
        require_once( DIR_SHARED . "/application/cupcake/CupcakeSharedPath.php" );
        $this->cupcake = new CupcakeSharedPath();
        return $this->cupcake;
    }

    /**
     * @return DatabaseSharedPath
     */
    private function createDatabase()
    {
        require_once( DIR_SHARED . "/application/database/DatabaseSharedPath.php" );
        $this->database = new DatabaseSharedPath();
        return $this->database;
    }

    /**
     * @return RequestSharedPath
     */
    private function createRequest()
    {
        require_once( DIR_SHARED . "/application/request/RequestSharedPath.php" );
        $this->request = new RequestSharedPath();
        return $this->request;
    }

    /**
     * @return UploadSharedPath
     */
    private function createUpload()
    {
        require_once( DIR_SHARED . "/application/upload/UploadSharedPath.php" );
        $this->upload = new UploadSharedPath();
        return $this->upload;
    }

    /**
     * @return AppSharedPath
     */
    public function getApp()
    {
        return $this->app ? $this->app : $this->createApp();
    }

    /**
     * @return CupcakeSharedPath
     */
    public function getCupcake()
    {
        return $this->cupcake ? $this->cupcake : $this->createCupcake();
    }

    /**
     * @return DatabaseSharedPath
     */
    public function getDatabase()
    {
        return $this->database ? $this->database : $this->createDatabase();
    }

    /**
     * @return RequestSharedPath
     */
    public function getRequest()
    {
        return $this->request ? $this->request : $this->createRequest();
    }

    /**
     * @return UploadSharedPath
     */
    public function getUpload()
    {
        return $this->upload ? $this->upload : $this->createUpload();
    }

}