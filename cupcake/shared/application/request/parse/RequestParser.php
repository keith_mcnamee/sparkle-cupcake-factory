<?php


class RequestParser extends SharedBase
{

    /**
     * @var RetrieveRequestCoreConstructor
     */
    private $retrieveConstructor;

    public function __destruct()
    {
        $this->retrieveConstructor = null;

        parent::__destruct();
    }

    /**
     * @param array $requests
     * @return BasicRequestVO
     */
    public function parse( array $requests )
    {

        foreach( $requests as $request )
        {
            /* @var $request array */
            $actionValue = $this->getRetrieveConstructor()->valueConstruct( $request, ParameterRequestConstants::$ACTION_VAR, null, true );
            if( $actionValue )
            {
                $vo = $this->createRequestVO( $actionValue, $request );
                if( $vo )
                {
                    $vo->setAction( $actionValue );
                    return $vo;
                }
            }
        }
        return null;
    }

    /**
     * @param $action
     * @param array $data
     * @return BasicRequestVO
     */
    public function createRequestVO( $action, array $data )
    {
        switch( $action )
        {
            case ParameterRequestConstants::$SAVE_ACTION:
                return $this->saveDataVO( $data );
            default:
                break;
        }
        return null;
    }

    /**
     * @param array $data
     * @return SaveDataRequestVO
     */
    public function saveDataVO( array $data )
    {
        $submitIDs = $this->getRetrieveConstructor()->arrayConstruct( $data, ParameterRequestConstants::$SUBMIT_IDS, null, true );
        if( !CoreHelper::getArrayHelper()->isNumberedArray( $submitIDs ) )
        {
            return null;
        }

        $ids = array();
        foreach( $submitIDs as $value )
        {
            if( !is_numeric ( $value ) )
            {
                return null;
            }
            $ids[] = (int)( $value );
        }

        $vo = new SaveDataRequestVO();
        $vo->setIds( $ids );

        return $vo;
    }

    /**
     * @return RetrieveRequestCoreConstructor
     */
    private function getRetrieveConstructor()
    {
        if( !$this->retrieveConstructor )
        {
            $this->retrieveConstructor = $this->getCoreRequest()->getRetrieveConstructor();
        }
        return $this->retrieveConstructor;
    }

}