<?php


class ConfigRequestSharedPath
{

    /**
     * @var ServerRequestConfig
     */
    private $serverConfig;

    public function __destruct()
    {
        $this->serverConfig = null;
    }

    /**
     * @param $singleInstance
     * @return ServerRequestConfig
     */
    private function createServerConfig( $singleInstance )
    {
        $instance = new ServerRequestConfig();
        if( !$singleInstance )
        {
            $this->serverConfig = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return ServerRequestConfig
     */
    public function getServerConfig( $singleInstance = false )
    {
        return $this->serverConfig && !$singleInstance ? $this->serverConfig : $this->createServerConfig( $singleInstance );
    }

}