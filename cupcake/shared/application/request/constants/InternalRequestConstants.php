<?php


class InternalRequestConstants
{

    //Server Config IDs
    public static $SHEEFO_SERVER_CONFIG_ID = "scifo";

    //Request Categories
    public static $SAVE_CATEGORY = "save";

    //Request Types
    public static $SAVE_REQUEST = "save";
} 