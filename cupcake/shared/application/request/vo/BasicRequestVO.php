<?php


class BasicRequestVO
{

    /**
     * @var array
     */
    private $action;

    public function __construct()
    {

    }

    /**
     * @return array
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param array $value
     */
    public function setAction( $value )
    {
        $this->action = $value;
    }

}