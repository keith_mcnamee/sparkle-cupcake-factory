<?php


class SaveDataRequestVO extends BasicRequestVO
{

    /**
     * @var array
     */
    private $ids;

    public function __construct()
    {
        parent::__construct();
        $this->setIds( array() );
    }

    /**
     * @return array
     */
    public function getIds()
    {
        return $this->ids;
    }

    /**
     * @param array $value
     */
    public function setIds( $value )
    {
        $this->ids = $value;
    }

}