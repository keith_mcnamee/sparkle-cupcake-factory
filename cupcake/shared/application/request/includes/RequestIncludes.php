<?php

require_once( DIR_SHARED . "/application/request/command/ProcessRequestCommand.php" );
require_once( DIR_SHARED . "/application/request/config/ServerRequestConfig.php" );

require_once( DIR_SHARED . "/application/request/constants/InternalRequestConstants.php" );
require_once( DIR_SHARED . "/application/request/constants/ParameterRequestConstants.php" );
require_once( DIR_SHARED . "/application/request/model/RequestModel.php" );
require_once( DIR_SHARED . "/application/request/parse/RequestParser.php" );
require_once( DIR_SHARED . "/application/request/vo/BasicRequestVO.php" );
require_once( DIR_SHARED . "/application/request/vo/SaveDataRequestVO.php" );
