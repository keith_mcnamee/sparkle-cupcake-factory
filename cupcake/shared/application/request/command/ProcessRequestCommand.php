<?php


class ProcessRequestCommand extends SharedBase
{

    /**
     * @param array $requests
     */
    public function command( array $requests )
    {
        $requestVO = $this->getSharedRequest()->getRequestParser()->parse( $requests );

        if( $requestVO )
        {
            $this->addRequest( $requestVO );
        }
    }

    /**
     * @param BasicRequestVO $requestVO
     */
    private function addRequest( $requestVO )
    {

        switch( $requestVO->getAction() )
        {
            case ParameterRequestConstants::$SAVE_ACTION:
                /* @var $requestVO SaveDataRequestVO */
                $this->getRequestModel()->setSaveDataRequestVO( $requestVO );
                break;

            default:
                break;

        }
    }

    private function getRequestModel()
    {
        return $this->getSharedRequest()->getRequestModel();
    }

}