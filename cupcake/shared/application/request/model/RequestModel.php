<?php


class RequestModel extends SharedBase
{

    /**
     * @var SaveDataRequestVO
     */
    private $saveDataRequestVO;

    public function __construct()
    {

    }

    /**
     * @return SaveDataRequestVO
     */
    public function getSaveDataRequestVO()
    {
        return $this->saveDataRequestVO;
    }

    /**
     * @param SaveDataRequestVO $value
     */
    public function setSaveDataRequestVO( $value )
    {
        $this->saveDataRequestVO = $value;
    }

}