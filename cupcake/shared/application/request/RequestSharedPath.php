<?php


class RequestSharedPath extends SharedBase
{

    /**
     * @var ServerRequestConfig
     */
    private $serverConfig;

    /**
     * @var RequestModel
     */
    private $requestModel;


    public function __construct()
    {
        parent::__construct();

        $this->getCoreRequest();
        $this->getIncludes();
    }

    public function __destruct()
    {
        $this->serverConfig = null;
        $this->requestModel = null;
    }

    private function getIncludes()
    {
        require_once( DIR_SHARED . "/application/request/includes/RequestIncludes.php" );
    }

    /**
     * @return RequestModel
     */
    public function getRequestModel()
    {
        if( !$this->requestModel )
        {
            $this->requestModel = new RequestModel();
        }
        return $this->requestModel;
    }

    /**
     * @return ServerRequestConfig
     */
    public function getServerConfig()
    {
        if(!$this->serverConfig )
        {
            $this->serverConfig = new ServerRequestConfig();
        }
        return $this->serverConfig;
    }

    /**
     * @return ProcessRequestCommand
     */
    public function getProcessRequestCommand()
    {
        return new ProcessRequestCommand();
    }

    /**
     * @return RequestParser
     */
    public function getRequestParser()
    {
        return new RequestParser();
    }
    
}