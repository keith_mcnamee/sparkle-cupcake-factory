<?php


class ServerRequestConfig extends SharedBase
{

    /**
     * @var array
     */
    private $configVOs;

    public function __destruct()
    {
        $this->configVOs = null;
    }

    /**
     * @return array
     */
    public function getConfigVOs()
    {
        return $this->configVOs ? $this->configVOs : $this->createConfigVOs();
    }

    /**
     * @return array
     */
    protected function createConfigVOs()
    {
        $this->configVOs = array();

        $this->configVOs[] =  $this->localhostConfigVO();
        $this->configVOs[] = $this->sheefoConfigVO();

        return $this->configVOs;
    }

    /**
     * @return ServerConfigRequestCoreVO
     */
    public function localhostConfigVO()
    {
        $configVO = new ServerConfigRequestCoreVO( InternalRequestCoreConstants::$LOCALHOST_SERVER_CONFIG_ID );

        $configVO->setUsingStagingDatabase( true );

        $configVO->addServerName( "localhost" );

        $configVO->addStagingDirectory( new StagingDirectoryConfigRequestCoreVO( "staging", 0 ) );


        return $configVO;
    }

    /**
     * @return ServerConfigRequestCoreVO
     */
    public function sheefoConfigVO()
    {
        $configVO = new ServerConfigRequestCoreVO( InternalRequestConstants::$SHEEFO_SERVER_CONFIG_ID );

        $configVO->setUsingStagingDatabase( true );

        $configVO->addServerName( "www.sheefo.com" );
        $configVO->addServerName( "sheefo.com" );
        $configVO->addServerName( "www.keithmcnamee.com" );
        $configVO->addServerName( "keithmcnamee.com" );

        $configVO->addStagingDirectory( new StagingDirectoryConfigRequestCoreVO( "staging", 0 ) );

        return $configVO;
    }
} 