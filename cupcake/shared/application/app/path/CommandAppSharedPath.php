<?php


class CommandAppSharedPath
{

    /**
     * @var ProcessAppCommand
     */
    private $process;

    public function __destruct()
    {
        $this->process = null;
    }

    /**
     * @param $singleInstance
     * @return ProcessAppCommand
     */
    private function createProcess( $singleInstance )
    {
        $instance = new ProcessAppCommand();
        $instance->init();
        if( !$singleInstance )
        {
            $this->process = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return ProcessAppCommand
     */
    public function getProcess( $singleInstance = true )
    {
        return $this->process && !$singleInstance ? $this->process : $this->createProcess( $singleInstance );
    }

}