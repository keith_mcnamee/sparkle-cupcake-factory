<?php


class ProcessAppCommand extends LinkedSharedBase
{

    public function command()
    {
        $serverConfigVOs = $this->getSharedRequest()->getServerConfig()->getConfigVOs();
        $this->getSharedApp()->getInitializeCommand()->command( $serverConfigVOs );

        $requests = $this->getCoreRequest()->getRetrieveValuesService()->requests();
        $this->getSharedRequest()->getProcessRequestCommand()->command( $requests );

        $appType = CoreRequest::getHelper()->getAppType();
        $this->processAppType( $appType );

    }

    /**
     * @param string $appType
     */
    protected function processAppType( $appType )
    {
        switch ( $appType )
        {
            case ParameterRequestConstants::$UPLOAD:
                $this->getSharedUpload()->getProcessUploadCommand()->command();
                break;

            default:
                $this->getSite()->process();
                break;
        }
    }
} 