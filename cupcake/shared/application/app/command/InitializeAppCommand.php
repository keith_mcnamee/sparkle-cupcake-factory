<?php


class InitializeAppCommand extends LinkedSharedBase
{

    /**
     * @param array $serverRequestConfigVOs
     */
    public function command( array $serverRequestConfigVOs )
    {
        $this->getCoreApp()->getInitializeCommand()->command( $serverRequestConfigVOs );

        $databaseConfigVOs = $this->getSharedDatabase()->getDatabaseConfig()->getConfigVOs();

        $databaseConfigVOs = $this->getCoreDatabase()->getDetermineConfigService()->multipleConfigs( $databaseConfigVOs, true );

        $this->getCoreDatabase()->getInitializeCommand()->command( DatabaseConstants::$UPDATE_DATABASE_CONFIG_ID, $databaseConfigVOs );
    }

}