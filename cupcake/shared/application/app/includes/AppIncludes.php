<?php

require_once( DIR_APP . "/SharedBase.php" );
require_once( DIR_APP . "/LinkedSharedBase.php" );
require_once( DIR_SHARED . "/application/ApplicationSharedPath.php" );
require_once( DIR_SHARED . "/application/app/AppSharedPath.php" );

require_once( DIR_SHARED . "/application/app/command/InitializeAppCommand.php" );
require_once( DIR_SHARED . "/application/app/command/ProcessAppCommand.php" );
