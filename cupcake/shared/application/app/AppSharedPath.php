<?php


class AppSharedPath extends SharedBase
{

    /**
     * @var DatabaseSharedPath
     */
    private $database;

    public function __destruct()
    {
        $this->database = null;
    }


    public function __construct()
    {
        parent::__construct();

        $this->getCoreApp();
    }

    /**
     * @return InitializeAppCommand
     */
    public function getInitializeCommand()
    {
        return new InitializeAppCommand();
    }

    /**
     * @return ProcessAppCommand
     */
    public function getProcessCommand()
    {
        return new ProcessAppCommand();
    }

    /**
     * @return DatabaseSharedPath
     */
    private function createDatabase()
    {
        require_once( DIR_SHARED . "/application/database/DatabaseSharedPath.php" );
        $this->database = new DatabaseSharedPath();
        return $this->database;
    }

    /**
     * @return DatabaseSharedPath
     */
    public function getDatabase()
    {
        return $this->database ? $this->database : $this->createDatabase();
    }

}