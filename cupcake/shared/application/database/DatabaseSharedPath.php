<?php


class DatabaseSharedPath extends SharedBase
{

    /**
     * @var DatabaseSiteExampleConfig
     */
    private $databaseConfig;

    public function __construct()
    {
        parent::__construct();

        $this->getCoreDatabase();
        $this->getIncludes();
    }

    private function getIncludes()
    {
        require_once( DIR_SHARED . "/application/database/includes/DatabaseIncludes.php" );
    }

    public function __destruct()
    {
        $this->databaseConfig = null;
    }

    /**
     * @return DatabaseConfig
     */
    public function getDatabaseConfig()
    {
        if( !$this->databaseConfig )
        {
            $this->databaseConfig = new DatabaseConfig();
        }
        return $this->databaseConfig;
    }


    /**
     * @return InsertDatabaseCupcakeConstructor
     */
    public function getInsertDatabaseCupcakeConstructor()
    {
        return new InsertDatabaseCupcakeConstructor();
    }

    /**
     * @return SelectDatabaseCupcakeConstructor
     */
    public function getSelectConstructor()
    {
        return new SelectDatabaseCupcakeConstructor();
    }

    /**
     * @return DatabaseCupcakeParser
     */
    public function getDatabaseParser()
    {
        return new DatabaseCupcakeParser();
    }

    /**
     * @return InsertDatabaseCupcakeRequest
     */
    public function getInsertDatabaseCupcakeRequest()
    {
        return new InsertDatabaseCupcakeRequest();
    }

    /**
     * @return SelectDatabaseCupcakeRequest
     */
    public function getSelectCupcakeDatabaseRequest()
    {
        return new SelectDatabaseCupcakeRequest();
    }

}