<?php


class InsertDatabaseCupcakeRequest extends SharedBase
{

    /**
     * @var BasicDatabaseRequest
     */
    private $basicDatabaseRequest;

    public function __destruct()
    {
        $this->basicDatabaseRequest = null;

        parent::__destruct();
    }

    /**
     * @param array $orderIDs
     */
    public function savedOrderInsertRequest( array $orderIDs )
    {
        $insertVO = $this->getSharedDatabase()->getInsertDatabaseCupcakeConstructor()->savedOrderVO( $orderIDs );
        $this->getBasicDatabaseRequest()->insert( $insertVO );
    }

    /**
     * @return BasicDatabaseRequest
     */
    private function getBasicDatabaseRequest()
    {
        if( !$this->basicDatabaseRequest )
        {
            $this->basicDatabaseRequest = $this->getCoreDatabase()->getBasicRequest();
        }
        return $this->basicDatabaseRequest;
    }

}