<?php


class SelectDatabaseCupcakeRequest extends SharedBase
{

    /**
     * @var BasicDatabaseRequest
     */
    private $basicDatabaseRequest;

    public function __destruct()
    {
        $this->basicDatabaseRequest = null;

        parent::__destruct();
    }

    /**
     * @param array $inIDs
     * @return array
     */
    public function availableCupcakesSelectRequest( array $inIDs = null )
    {
        $selectVO = $this->getSharedDatabase()->getSelectConstructor()->availableCupcakesVO();
        $entryData = $this->getBasicDatabaseRequest()->select( $selectVO );
        $vos = $this->getSharedDatabase()->getDatabaseParser()->availableCupcakeVOs( $entryData );
        return $vos;
    }

    /**
     * @return BasicDatabaseRequest
     */
    private function getBasicDatabaseRequest()
    {
        if( !$this->basicDatabaseRequest )
        {
            $this->basicDatabaseRequest = $this->getCoreDatabase()->getBasicRequest();
        }
        return $this->basicDatabaseRequest;
    }

}