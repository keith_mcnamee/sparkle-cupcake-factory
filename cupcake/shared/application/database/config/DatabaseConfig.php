<?php


class DatabaseConfig extends SharedBase
{

    /**
     * @var array
     */
    private $configVOs;

    public function __destruct()
    {
        $this->configVOs = null;
    }

    /**
     * @return array
     */
    public function getConfigVOs()
    {
        return $this->configVOs ? $this->configVOs : $this->createConfigVOs();
    }

    /**
     * @return array
     */
    protected function createConfigVOs()
    {
        $this->configVOs = array();

        $this->configVOs[] = $this->localhostUpdateConfigVO();

        $this->configVOs[] = $this->sheefoUpdateConfigVO();

        return $this->configVOs;
    }

    /**
     * @return ConfigDatabaseCoreVO
     */
    protected function localhostUpdateConfigVO()
    {
        $configVO = new ConfigDatabaseCoreVO( DatabaseConstants::$UPDATE_DATABASE_CONFIG_ID  );

        $configVO->setServerConfigType( InternalRequestCoreConstants::$LOCALHOST_SERVER_CONFIG_ID );

        $configVO->setHostName("** Add Here **");
        $configVO->setUser("** Add Here **");
        $configVO->setPassword("** Add Here **");
        $configVO->setDatabaseName("** Add Here **");

        return $configVO;
    }

    /**
     * @return ConfigDatabaseCoreVO
     */
    protected function sheefoUpdateConfigVO()
    {
        $configVO = new ConfigDatabaseCoreVO( DatabaseConstants::$UPDATE_DATABASE_CONFIG_ID );

        $configVO->setServerConfigType( InternalRequestConstants::$SHEEFO_SERVER_CONFIG_ID );

        $configVO->setHostName("** Add Here **");
        $configVO->setDatabaseName("** Add Here **");
        $configVO->setUser("** Add Here **");
        $configVO->setPassword("** Add Here **");

        return $configVO;
    }
} 