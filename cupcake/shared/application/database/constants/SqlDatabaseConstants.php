<?php


class SqlDatabaseConstants
{
    /*  Table Names  */

    public static $AVAILABLE_CUPCAKES = "available_cupcakes";
    public static $SAVED_ORDERS = "saved_orders";

    /*  general  */
    public static $ID = "id";
    public static $CREATED = "created";

    /*  available_cupcakes  */
    public static $FILE_NAME = "file_name";
    public static $PRIORITY = "priority";


    /*  saved_orders  */
    public static $ORDER_CUPCAKES = "order_cupcakes";

} 