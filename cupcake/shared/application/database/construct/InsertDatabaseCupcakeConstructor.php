<?php


class InsertDatabaseCupcakeConstructor extends SharedBase
{

    /**
     * @param array $ids
     * @return InsertRequestDatabaseCoreVO
     */
    public function savedOrderVO( array $ids )
    {
        $insertVO = new InsertRequestDatabaseCoreVO( SqlDatabaseConstants::$SAVED_ORDERS );

        $idString = html_entity_decode (json_encode($ids ));

        $insertVO->addStringColumnValue( SqlDatabaseConstants::$ORDER_CUPCAKES, $idString );

        return $insertVO;
    }

}