<?php


class SelectDatabaseCupcakeConstructor extends SharedBase
{

    /**
     * @var ConditionDatabaseConstructor
     */
    private $conditionConstructor;

    public function __destruct()
    {
        $this->conditionConstructor = null;

        parent::__destruct();
    }

    /**
     * @param array $inIDs
     * @return SelectRequestDatabaseCoreVO
     */
    public function availableCupcakesVO( array $inIDs = null )
    {
        $selectVO = new SelectRequestDatabaseCoreVO( SqlDatabaseConstants::$AVAILABLE_CUPCAKES );

        $selectVO->addColumn( SqlDatabaseConstants::$ID );
        $selectVO->addColumn( SqlDatabaseConstants::$FILE_NAME );
        $selectVO->addColumn( SqlDatabaseConstants::$PRIORITY );

        if( $inIDs )
        {
            $selectVO->addCondition( $this->getConditionConstructor()->inUnknowns( SqlDatabaseConstants::$ID, count( $inIDs ) )  );

            foreach( $inIDs as $id )
            {
                /* @var $id int */
                $selectVO->addIntValueToCondition( $id );
            }
        }


        return $selectVO;
    }

    /**
     * @return SelectRequestDatabaseCoreVO
     */
    public function savedOrdersVO()
    {
        $selectVO = new SelectRequestDatabaseCoreVO( SqlDatabaseConstants::$SAVED_ORDERS );

        $selectVO->addColumn( SqlDatabaseConstants::$ID );
        $selectVO->addColumn( SqlDatabaseConstants::$ORDER_CUPCAKES );

        return $selectVO;
    }

    /**
     * @return ConditionDatabaseConstructor
     */
    private function getConditionConstructor()
    {
        if( !$this->conditionConstructor )
        {
            $this->conditionConstructor = $this->getCoreDatabase()->getConditionConstructor();
        }
        return $this->conditionConstructor;
    }

}