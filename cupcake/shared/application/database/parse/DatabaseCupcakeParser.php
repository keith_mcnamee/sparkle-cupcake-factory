<?php


class DatabaseCupcakeParser extends SharedBase
{

    /**
     * @param array $datas
     * @return array
     */
    public function availableCupcakeVOs( array $datas )
    {
        $vos = array();
        foreach ( $datas as $data )
        {
            /* @var $data array */
            $vo = $this->availableCupcakeVO( $data );
            $vos[ $vo->getId() ] = $vo;
        }
        return $vos;
    }

    /**
     * @param array $datas
     * @return array
     */
    public function savedOrderVOs( array $datas )
    {
        $vos = array();
        foreach ( $datas as $data )
        {
            /* @var $data array */
            $vo = $this->savedOrderVO( $data );
            $vos[ $vo->getId() ] = $vo;
        }
        return $vos;
    }

    /**
     * @param array $data
     * @param AvailableCupcakeVO $vo
     * @return AvailableCupcakeVO
     */
    public function availableCupcakeVO( array $data, AvailableCupcakeVO $vo = null )
    {
        if( !$vo )
        {
            $vo = new AvailableCupcakeVO();
        }

        $vo->setId( CoreHelper::getArrayHelper()->intFromDictionary( $data, SqlDatabaseConstants::$ID ) );
        $vo->setFileName( CoreHelper::getArrayHelper()->stringFromDictionary( $data, SqlDatabaseConstants::$FILE_NAME ) );
        $vo->setPriority( CoreHelper::getArrayHelper()->intFromDictionary( $data, SqlDatabaseConstants::$PRIORITY ) );

        return $vo;
    }

    /**
     * @param array $data
     * @param SavedOrderVO $vo
     * @return SavedOrderVO
     */
    public function savedOrderVO( array $data, SavedOrderVO $vo = null )
    {
        if( !$vo )
        {
            $vo = new SavedOrderVO();
        }

        $vo->setId( CoreHelper::getArrayHelper()->intFromDictionary( $data, SqlDatabaseConstants::$ID ) );
        $vo->setOrderCupcakesJson( CoreHelper::getArrayHelper()->stringFromDictionary( $data, SqlDatabaseConstants::$ORDER_CUPCAKES ) );

        return $vo;
    }

}