<?php


class WelcomeViewPropertiesConstructor extends CupcakeViewPropertiesConstructor
{

    /**
     * @param array $availableCupcakeVOs
     * @return array
     */
    protected function welcomeViewObject( array $availableCupcakeVOs )
    {
        $object = $this->cupcakeViewObject();
        $availableCupcakes = array();
        foreach ( $availableCupcakeVOs as $availableCupcakeVO )
        {
            /* @var $availableCupcakeVO AvailableCupcakeVO */
            $cupcake = array();
            $cupcake[CupcakeJsonViewConstants::$ID] = $availableCupcakeVO->getId();
            $cupcake[CupcakeJsonViewConstants::$FILE_NAME] = $availableCupcakeVO->getFileName();
            $cupcake[CupcakeJsonViewConstants::$PRIORITY] = $availableCupcakeVO->getPriority();
            $availableCupcakes[] = $cupcake;
        }
        $object[CupcakeJsonViewConstants::$AVAILABLE_CUPCAKES] = $availableCupcakes;
        return $object;
    }

    /**
     * @param array $availableCupcakeVOs
     * @return String
     */
    public function welcomeJsonViewProperties( array $availableCupcakeVOs )
    {
        return $this->constructJson( $this->welcomeViewObject( $availableCupcakeVOs ) );
    }
}