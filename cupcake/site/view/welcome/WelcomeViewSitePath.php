<?php


class WelcomeViewSitePath extends SiteBase
{

    /**
     * @var WelcomeViewCommand
     */
    private $command;

    /**
     * @var WelcomeViewPropertiesConstructor
     */
    private $viewPropertiesConstructor;

    /**
     * @var WelcomePageView
     */
    private $pageView;

    public function __construct()
    {
        parent::__construct();

        $this->getSitePath()->getView()->getCupcake();
        $this->getIncludes();
    }

    public function __destruct()
    {
        $this->command = null;
        $this->viewPropertiesConstructor = null;
        $this->pageView = null;

        parent::__destruct();
    }

    private function getIncludes()
    {
        require_once( DIR_SITE . "/view/welcome/includes/WelcomeViewIncludes.php" );
    }

    /**
     * @param $singleInstance
     * @return WelcomeViewCommand
     */
    private function createCommand( $singleInstance )
    {
        $instance = new WelcomeViewCommand();
        if( !$singleInstance )
        {
            $this->command = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return WelcomeViewPropertiesConstructor
     */
    private function createViewPropertiesConstructor( $singleInstance )
    {
        $instance = new WelcomeViewPropertiesConstructor();
        if( !$singleInstance )
        {
            $this->viewPropertiesConstructor = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return WelcomePageView
     */
    private function createPageView( $singleInstance )
    {
        $instance = new WelcomePageView();
        $instance->init();
        if( !$singleInstance )
        {
            $this->command = $instance;
        }
        return $instance;
    }

    /**
     * @param boolean
     * @return WelcomeViewCommand
     */
    public function getCommand( $singleInstance = true )
    {
        return $this->command && !$singleInstance ? $this->command : $this->createCommand( $singleInstance );
    }

    /**
     * @param boolean
     * @return WelcomeViewPropertiesConstructor
     */
    public function getViewPropertiesConstructor( $singleInstance = false )
    {
        return $this->viewPropertiesConstructor && !$singleInstance ? $this->viewPropertiesConstructor : $this->createViewPropertiesConstructor( $singleInstance );
    }

    /**
     * @param boolean
     * @return WelcomePageView
     */
    public function getPageView( $singleInstance = false )
    {
        return $this->pageView && !$singleInstance ? $this->pageView : $this->createPageView( $singleInstance );
    }

}