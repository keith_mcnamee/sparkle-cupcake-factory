<?php

require_once( DIR_SITE . "/view/welcome/WelcomePageView.php" );

require_once( DIR_SITE . "/view/welcome/command/WelcomeViewCommand.php" );

require_once( DIR_SITE . "/view/welcome/construct/WelcomeViewPropertiesConstructor.php" );

require_once( DIR_SITE . "/view/welcome/pageContent/WelcomeBodyView.php" );
require_once( DIR_SITE . "/view/welcome/pageContent/WelcomeHeadView.php" );
require_once( DIR_SITE . "/view/welcome/pageContent/WelcomeInvisibleFooterView.php" );
require_once( DIR_SITE . "/view/welcome/pageContent/WelcomeMainBodyView.php" );