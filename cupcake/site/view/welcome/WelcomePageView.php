<?php


class WelcomePageView extends CupcakePageView
{

    /**
     * @param HeadView $headView
     */
    protected function defineHeadContainer( HeadView $headView = null )
    {
        $headView = $headView ? $headView :  new WelcomeHeadView();
        parent::defineHeadContainer( $headView );
    }

    /**
     * @param ElementHtml $elementHtml
     */
    protected function defineBodyContainer( ElementHtml $elementHtml = null )
    {
        $elementHtml = $elementHtml ? $elementHtml : new WelcomeBodyView();
        parent::defineBodyContainer( $elementHtml );
    }
}