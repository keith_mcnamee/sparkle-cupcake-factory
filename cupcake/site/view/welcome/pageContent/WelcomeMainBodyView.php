<?php


class WelcomeMainBodyView extends JsContainerView
{

    protected function afterOutput()
    {
        parent::afterOutput();
        ?>
        <div id="header">
            <img id= "titleImage" src="<?PHP echo CoreRequest::getHelper()->baseDirectory()?>public/images/sparkle_logo.jpg"/>
            <div id="subHeader"><h1>CUPCAKE FACTORY</h1></div>
        </div>
        <div id="cakeFactory"></div>

        <?PHP
    }
}