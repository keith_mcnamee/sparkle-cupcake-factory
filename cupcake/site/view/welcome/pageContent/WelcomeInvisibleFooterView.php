<?php


class WelcomeInvisibleFooterView extends CupcakeInvisibleFooterView
{

    protected function afterOutput()
    {
        parent::afterOutput();
        $this->addJsContent();
        ?>
        <script src="<?PHP echo CoreRequest::getHelper()->baseDirectory()?>public/library/jquery/jquery-ui.min.js"></script>
        <script src="<?PHP echo CoreRequest::getHelper()->baseDirectory()?>public/library/jquery/jquery.touchSwipe.min.js"></script>
        <script src="<?PHP echo CoreRequest::getHelper()->baseDirectory()?>public/js/cakefactory_site.js"></script>
        <script src="<?PHP echo CoreRequest::getHelper()->baseDirectory()?>public/js/cakefactory.js"></script>
<?PHP
    }

}