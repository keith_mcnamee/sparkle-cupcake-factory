<?php


class WelcomeBodyView extends CupcakeBodyView
{

    /**
     * @param JsContainerView $containerHtml
     */
    protected function addMainBodyContainer( JsContainerView $containerHtml = null )
    {
        $containerHtml = $containerHtml ? $containerHtml : new WelcomeMainBodyView();
        parent::addMainBodyContainer( $containerHtml );
    }

    /**
     * @param CupcakeInvisibleFooterView $invisibleFooterHtml
     */
    protected function addInvisibleFooterContainer( CupcakeInvisibleFooterView $invisibleFooterHtml = null )
    {
        $this->setInvisibleFooterView( $invisibleFooterHtml ? $invisibleFooterHtml : new WelcomeInvisibleFooterView() );
        $this->addContainerHtml( $this->getInvisibleFooterView() );
    }
}