<?php


class WelcomeViewCommand extends SiteBase
{

    public function showView()
    {
        $this->getSharedCupcake()->getRetrieveAvailableCupcakeCommand()->command( true );
        $vos = $this->getSharedCupcake()->getCupcakeModel()->getAvailableCupcakeVOs();
        $jsonViewProperties = $this->getWelcomeView()->getViewPropertiesConstructor()->welcomeJsonViewProperties( $vos );

        $this->getCorePageView()->getModel()->setJsonViewProperties( $jsonViewProperties );
        $this->getWelcomeView()->getPageView()->showView();
    }
}