<?php


class CupcakePageView extends PageView
{

    protected function beforeOutput()
    {
        parent::beforeOutput();

        ?><!DOCTYPE html>
<?PHP
    }

    /**
     * @param ElementHtml $elementHtml
     */
    protected function defineHtmlContainer( ElementHtml $elementHtml = null )
    {
        parent::defineHtmlContainer( $elementHtml );
        $this->getHtml()->addProperty( "lang", ["en"] );
    }

    /**
     * @param HeadView $headView
     */
    protected function defineHeadContainer( HeadView $headView = null )
    {
        $headView = $headView ? $headView :  new CupcakeHeadView();
        parent::defineHeadContainer( $headView );
    }

    /**
     * @param ElementHtml $elementHtml
     */
    protected function defineBodyContainer( ElementHtml $elementHtml = null )
    {
        $elementHtml = $elementHtml ? $elementHtml : new CupcakeBodyView();
        parent::defineBodyContainer( $elementHtml );
    }
}