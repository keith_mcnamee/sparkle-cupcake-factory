<?php

class CupcakeJsonViewConstants
{
    public static $APPLICATION_ROOT = "applicationRoot";
    public static $JSON_VIEW_PROPERTIES = "jsonViewProperties";

    public static $AVAILABLE_CUPCAKES = "availableCupcakes";
    public static $SAVED_ORDERS = "savedOrders";

    /*  general  */
    public static $ID = "id";
    public static $CREATED = "created";

    /*  available_cupcakes  */
    public static $FILE_NAME = "fileName";
    public static $PRIORITY = "priority";


    /*  saved_orders  */
    public static $ORDER_CUPCAKES = "orderCupcakes";
}