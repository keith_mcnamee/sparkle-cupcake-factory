<?php


class CupcakeHeadView extends HeadView
{

    /**
     * @var JsContainerView
     */
    private $jsContainerView;

    protected function defineChildContainers()
    {
        parent::defineChildContainers();
        $this->defineJsContainerView();
    }

    /**
     * @param JsContainerView $jsContainerView
     */
    protected function defineJsContainerView( JsContainerView $jsContainerView = null )
    {
        $this->jsContainerView = $jsContainerView ? $jsContainerView : new JsContainerView();
    }

    /**
     * @param string $title
     */
    protected function addTitle( $title = "" )
    {
        //title added later
    }

    protected function afterOutput()
    {
        parent::afterOutput();

        ?>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Sparkle Games - Cupcake Factory</title>
        <link href='http://fonts.googleapis.com/css?family=Lato:700,300' rel='stylesheet' type='text/css'>
        <link href="<?PHP echo CoreRequest::getHelper()->baseDirectory()?>public/css/cakefactory_site.css" rel="stylesheet">
<?PHP
        $this->addJsContent();
    }


    protected function addJsContent()
    {
        $this->addContainerHtml( $this->jsContainerView );
        $this->addJs();
        $js = $this->jsContainerView->getJsElement();
        if( $js->getProperties() )
        {
            $js->output( true, false, 1, true );
        }

    }

    /**
     * @param string $position
     * @param string $type
     */
    protected function addJs( $position = "", $type = "" )
    {
        $jsonViewProperties = $this->getJsonViewProperties();
        $js = $this->jsContainerView->getJsElement();
        $js->addVar( CupcakeJsonViewConstants::$APPLICATION_ROOT, CoreRequest::getHelper()->baseDirectory() );
        $js->addVar( CupcakeJsonViewConstants::$JSON_VIEW_PROPERTIES, $jsonViewProperties );
        $this->jsContainerView->addJs( $position, $type );
    }
}