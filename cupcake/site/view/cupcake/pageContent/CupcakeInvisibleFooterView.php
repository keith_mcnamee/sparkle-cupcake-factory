<?php


class CupcakeInvisibleFooterView extends JsContainerView
{

    protected function afterOutput()
    {
        parent::afterOutput();
        $this->addJsContent();
        ?>
        <script src="<?PHP echo CoreRequest::getHelper()->baseDirectory()?>public/library/jquery/jquery-1.11.3.min.js"></script>
<?PHP
    }


    protected function addJsContent()
    {
        $this->addJs();
        $js = $this->getJsElement();
        if( !$js->hasBodyContent() )
        {
            return;
        }
        $js->output( true, false, 2, true );

    }
}