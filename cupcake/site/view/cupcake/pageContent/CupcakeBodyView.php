<?php


class CupcakeBodyView extends ElementView
{

    /**
     * @var CupcakeHeaderView
     */
    private $headerView;

    /**
     * @var CupcakeHeaderView
     */
    private $mainBodyView;

    /**
     * @var CupcakeFooterView
     */
    private $footerView;

    /**
     * @var CupcakeInvisibleFooterView
     */
    private $invisibleFooterView;

    /**
     * @param array $properties
     */
    public function __construct( array $properties = null)
    {
        parent::__construct( "body", false, $properties );
    }



    protected function beforeOutput()
    {
        parent::beforeOutput();
        $this->addHeaderContainer();
        $this->addMainBodyContainer();
        $this->addFooterContainer();
        $this->addInvisibleFooterContainer();
    }

    /**
     * @param CupcakeHeaderView $headerHtml
     */
    protected function addHeaderContainer( CupcakeHeaderView $headerHtml = null )
    {
        $this->headerView = $headerHtml ? $headerHtml : new CupcakeHeaderView();
        $this->addContainerHtml( $this->headerView );
    }

    /**
     * @param JsContainerView $containerHtml
     */
    protected function addMainBodyContainer( JsContainerView $containerHtml = null )
    {
        $this->mainBodyView = $containerHtml ? $containerHtml : new JsContainerView();
        $this->addContainerHtml( $this->mainBodyView );
    }

    /**
     * @param CupcakeFooterView $footerHtml
     */
    protected function addFooterContainer( CupcakeFooterView $footerHtml = null )
    {
        $this->footerView = $footerHtml ? $footerHtml : new CupcakeFooterView();
        $this->addContainerHtml( $this->footerView );
    }

    /**
     * @param CupcakeInvisibleFooterView $invisibleFooterHtml
     */
    protected function addInvisibleFooterContainer( CupcakeInvisibleFooterView $invisibleFooterHtml = null )
    {
        $this->invisibleFooterView = $invisibleFooterHtml ? $invisibleFooterHtml : new CupcakeInvisibleFooterView();
        $this->addContainerHtml( $this->invisibleFooterView );
    }

    /**
     * @return CupcakeInvisibleFooterView
     */
    protected function getInvisibleFooterView()
    {
        return $this->invisibleFooterView;
    }

    /**
     * @param CupcakeInvisibleFooterView $value
     */
    protected function setInvisibleFooterView( $value )
    {
        $this->invisibleFooterView = $value;
    }
}