<?php


class CupcakeViewPropertiesConstructor extends SiteBase
{

    /**
     * @return array
     */
    protected function cupcakeViewObject()
    {
        return array();
    }

    /**
     * @return String
     */
    public function cupcakeJsonViewProperties()
    {
        return $this->constructJson( $this->cupcakeViewObject() );
    }

    /**
     * @param $obj
     * @return string
     */
    protected function constructJson( $obj )
    {
        return json_encode($obj );
    }
    
}