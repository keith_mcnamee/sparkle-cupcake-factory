<?php


class CupcakeViewSitePath extends SiteBase
{

    /**
     * @var CupcakeViewPropertiesConstructor
     */
    private $viewPropertiesConstructor;

    /**
     * @var CupcakePageView
     */
    private $pageView;

    public function __construct()
    {
        parent::__construct();

        $this->getCorePageView();
        $this->getIncludes();
    }

    public function __destruct()
    {
        $this->viewPropertiesConstructor = null;
        $this->pageView = null;

        parent::__destruct();
    }

    private function getIncludes()
    {
        require_once( DIR_SITE . "/view/cupcake/includes/CupcakeViewIncludes.php" );
    }

    /**
     * @param $singleInstance
     * @return CupcakeViewPropertiesConstructor
     */
    private function createViewPropertiesConstructor( $singleInstance )
    {
        $instance = new CupcakeViewPropertiesConstructor();
        if( !$singleInstance )
        {
            $this->viewPropertiesConstructor = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return CupcakePageView
     */
    private function createPageView( $singleInstance )
    {
        $instance = new CupcakePageView();
        $instance->init();
        if( !$singleInstance )
        {
            $this->viewPropertiesConstructor = $instance;
        }
        return $instance;
    }

    /**
     * @param boolean
     * @return CupcakeViewPropertiesConstructor
     */
    public function getViewPropertiesConstructor( $singleInstance = false )
    {
        return $this->viewPropertiesConstructor && !$singleInstance ? $this->viewPropertiesConstructor : $this->createViewPropertiesConstructor( $singleInstance );
    }

    /**
     * @param boolean
     * @return CupcakePageView
     */
    public function getPageView( $singleInstance = false )
    {
        return $this->pageView && !$singleInstance ? $this->pageView : $this->createPageView( $singleInstance );
    }
    
}