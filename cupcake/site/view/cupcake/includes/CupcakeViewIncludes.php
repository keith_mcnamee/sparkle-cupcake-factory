<?php

require_once( DIR_SITE . "/view/cupcake/CupcakePageView.php" );

require_once( DIR_SITE . "/view/cupcake/constants/CupcakeJsonViewConstants.php" );

require_once( DIR_SITE . "/view/cupcake/construct/CupcakeViewPropertiesConstructor.php" );

require_once( DIR_SITE . "/view/cupcake/pageContent/CupcakeBodyView.php" );
require_once( DIR_SITE . "/view/cupcake/pageContent/CupcakeFooterView.php" );
require_once( DIR_SITE . "/view/cupcake/pageContent/CupcakeHeaderView.php" );
require_once( DIR_SITE . "/view/cupcake/pageContent/CupcakeInvisibleFooterView.php" );
require_once( DIR_SITE . "/view/cupcake/pageContent/CupcakeHeadView.php" );