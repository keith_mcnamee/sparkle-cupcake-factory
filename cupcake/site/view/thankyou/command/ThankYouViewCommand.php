<?php


class ThankYouViewCommand extends SiteBase
{

    public function showView()
    {
        $saveDataRequestVO = $this->getSharedRequest()->getRequestModel()->getSaveDataRequestVO();
        $submittedVOs = array();
        if( $saveDataRequestVO )
        {
            $vos = $this->getSharedDatabase()->getSelectCupcakeDatabaseRequest()->availableCupcakesSelectRequest( $saveDataRequestVO->getIds() );
            foreach( $saveDataRequestVO->getIds() as $id )
            {
                /* @var $id int */
                $submittedVOs[] = $vos[ $id ];
            }
        }

        $jsonViewProperties = $this->getThankYouView()->getViewPropertiesConstructor()->thankYouJsonViewProperties( $submittedVOs );

        $this->getCorePageView()->getModel()->setJsonViewProperties( $jsonViewProperties );
        $this->getThankYouView()->getPageView()->showView();
    }
}