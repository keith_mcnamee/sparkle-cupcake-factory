<?php

require_once( DIR_SITE . "/view/thankyou/ThankYouPageView.php" );

require_once( DIR_SITE . "/view/thankyou/command/ThankYouViewCommand.php" );

require_once( DIR_SITE . "/view/thankyou/construct/ThankYouViewPropertiesConstructor.php" );

require_once( DIR_SITE . "/view/thankyou/pageContent/ThankYouBodyView.php" );
require_once( DIR_SITE . "/view/thankyou/pageContent/ThankYouInvisibleFooterView.php" );
require_once( DIR_SITE . "/view/thankyou/pageContent/ThankYouMainBodyView.php" );
