<?php


class ThankYouViewPropertiesConstructor extends CupcakeViewPropertiesConstructor
{

    /**
     * @param array $submittedVOs
     * @return array
     */
    protected function thankYouViewObject( array $submittedVOs )
    {
        $object = $this->cupcakeViewObject();
        $orderCupcakes = array();
        foreach( $submittedVOs as $submittedVO )
        {
            /* @var $submittedVO AvailableCupcakeVO */
            $submittedObject = array();
            $submittedObject[CupcakeJsonViewConstants::$ID] = $submittedVO->getId();
            $submittedObject[CupcakeJsonViewConstants::$FILE_NAME] = $submittedVO->getFileName();
            $orderCupcakes[] = $submittedObject;
        }
        $object[CupcakeJsonViewConstants::$ORDER_CUPCAKES] = $orderCupcakes;
        return $object;
    }

    /**
     * @param array $submittedVOs
     * @return String
     */
    public function thankYouJsonViewProperties( array $submittedVOs )
    {
        return $this->constructJson( $this->thankYouViewObject( $submittedVOs ) );
    }
}