<?php


class ThankYouBodyView extends CupcakeBodyView
{

    /**
     * @param array $properties
     */
    public function __construct( array $properties = null)
    {
        $properties = $properties ? $properties : array();
        $properties[ "id" ] = array( "thankYou" );

        parent::__construct( $properties );
    }

    /**
     * @param JsContainerView $containerHtml
     */
    protected function addMainBodyContainer( JsContainerView $containerHtml = null )
    {
        $containerHtml = $containerHtml ? $containerHtml : new ThankYouMainBodyView();
        parent::addMainBodyContainer( $containerHtml );
    }

    /**
     * @param CupcakeInvisibleFooterView $invisibleFooterHtml
     */
    protected function addInvisibleFooterContainer( CupcakeInvisibleFooterView $invisibleFooterHtml = null )
    {
        $this->setInvisibleFooterView( $invisibleFooterHtml ? $invisibleFooterHtml : new ThankYouInvisibleFooterView() );
        $this->addContainerHtml( $this->getInvisibleFooterView() );
    }
}