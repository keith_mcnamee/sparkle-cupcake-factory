<?php


class ThankYouMainBodyView extends JsContainerView
{

    protected function afterOutput()
    {
        parent::afterOutput();
        ?>
        <div id="header">
            <img id= "titleImage" src="<?PHP echo CoreRequest::getHelper()->baseDirectory()?>public/images/sparkle_logo.jpg"/>
            <div id="subHeader"><h1>THANK YOU</h1></div>
        </div>

        <div id="thankYouInterface" class="interface">
            <h2>Your order has been submitted</h2>
            <p>Copy the JSON array of your order below to your clipboard and paste it in "JSON Editor Online" (linked below) to view your order</p>

            <div style="display: block;">
                <label for="jsonTextArea" id="jsonTextAreaLabel">JSON:</label>
                <div class="textwrapper">
                    <textarea cols="2" rows="6" id="jsonTextArea" readonly></textarea>
                </div>
                <button id="jsonEditorButton">JSON editor</button>
                <button id="orderAgainButton">Place another order</button>
            </div>
        </div>

        <div id="errorContainer">
        </div>

        <?PHP
    }
}