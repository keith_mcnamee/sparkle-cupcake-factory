<?php


class ThankYouInvisibleFooterView extends CupcakeInvisibleFooterView
{

    protected function afterOutput()
    {
        parent::afterOutput();
        $this->addJsContent();
        ?>
        <script src="<?PHP echo CoreRequest::getHelper()->baseDirectory()?>public/js/cakefactory_site.js"></script>
        <script src="<?PHP echo CoreRequest::getHelper()->baseDirectory()?>public/js/thankyou.js"></script>
        <?PHP
    }

}