<?php


class ThankYouViewSitePath extends SiteBase
{

    /**
     * @var ThankYouViewCommand
     */
    private $command;

    /**
     * @var ThankYouViewPropertiesConstructor
     */
    private $viewPropertiesConstructor;

    /**
     * @var ThankYouPageView
     */
    private $pageView;

    public function __construct()
    {
        parent::__construct();

        $this->getSitePath()->getView()->getCupcake();
        $this->getIncludes();
    }

    public function __destruct()
    {
        $this->command = null;
        $this->viewPropertiesConstructor = null;
        $this->pageView = null;

        parent::__destruct();
    }

    private function getIncludes()
    {
        require_once( DIR_SITE . "/view/thankyou/includes/ThankYouViewIncludes.php" );
    }

    /**
     * @param $singleInstance
     * @return ThankYouViewCommand
     */
    private function createCommand( $singleInstance )
    {
        $instance = new ThankYouViewCommand();
        if( !$singleInstance )
        {
            $this->command = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return ThankYouViewPropertiesConstructor
     */
    private function createViewPropertiesConstructor( $singleInstance )
    {
        $instance = new ThankYouViewPropertiesConstructor();
        if( !$singleInstance )
        {
            $this->viewPropertiesConstructor = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return ThankYouPageView
     */
    private function createPageView( $singleInstance )
    {
        $instance = new ThankYouPageView();
        $instance->init();
        if( !$singleInstance )
        {
            $this->command = $instance;
        }
        return $instance;
    }

    /**
     * @param boolean
     * @return ThankYouViewCommand
     */
    public function getCommand( $singleInstance = true )
    {
        return $this->command && !$singleInstance ? $this->command : $this->createCommand( $singleInstance );
    }

    /**
     * @param boolean
     * @return ThankYouViewPropertiesConstructor
     */
    public function getViewPropertiesConstructor( $singleInstance = false )
    {
        return $this->viewPropertiesConstructor && !$singleInstance ? $this->viewPropertiesConstructor : $this->createViewPropertiesConstructor( $singleInstance );
    }

    /**
     * @param boolean
     * @return ThankYouPageView
     */
    public function getPageView( $singleInstance = false )
    {
        return $this->pageView && !$singleInstance ? $this->pageView : $this->createPageView( $singleInstance );
    }
}