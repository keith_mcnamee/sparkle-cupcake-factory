<?php


class ThankYouPageView extends CupcakePageView
{

    /**
     * @param ElementHtml $elementHtml
     */
    protected function defineBodyContainer( ElementHtml $elementHtml = null )
    {
        $elementHtml = $elementHtml ? $elementHtml : new ThankYouBodyView();
        parent::defineBodyContainer( $elementHtml );
    }
}