<?php


class ViewSitePath
{

    /**
     * @var CupcakeViewSitePath
     */
    private $cupcake;

    /**
     * @var WelcomeViewSitePath
     */
    private $welcome;

    /**
     * @var ThankYouViewSitePath
     */
    private $thankYou;

    /**
     * @var AdminViewSitePath
     */
    private $admin;

    public function __destruct()
    {
        $this->cupcake = null;
        $this->welcome = null;
        $this->thankYou = null;
        $this->admin = null;
    }

    /**
     * @return CupcakeViewSitePath
     */
    protected function createCupcake()
    {
        require_once( DIR_SITE . "/view/cupcake/CupcakeViewSitePath.php" );
        $this->cupcake = new CupcakeViewSitePath();
        return $this->cupcake;
    }

    /**
     * @return WelcomeViewSitePath
     */
    protected function createWelcome()
    {
        require_once( DIR_SITE . "/view/welcome/WelcomeViewSitePath.php" );
        $this->welcome = new WelcomeViewSitePath();
        return $this->welcome;
    }

    /**
     * @return ThankYouViewSitePath
     */
    protected function createThankYou()
    {
        require_once( DIR_SITE . "/view/thankyou/ThankYouViewSitePath.php" );
        $this->thankYou = new ThankYouViewSitePath();
        return $this->thankYou;
    }

    /**
     * @return AdminViewSitePath
     */
    protected function createAdmin()
    {
        require_once( DIR_SITE . "/view/admin/AdminViewSitePath.php" );
        $this->admin = new AdminViewSitePath();
        return $this->admin;
    }

    /**
     * @return CupcakeViewSitePath
     */
    public function getCupcake()
    {
        return $this->cupcake ? $this->cupcake : $this->createCupcake();
    }

    /**
     * @return WelcomeViewSitePath
     */
    public function getWelcome()
    {
        return $this->welcome ? $this->welcome : $this->createWelcome();
    }

    /**
     * @return ThankYouViewSitePath
     */
    public function getThankYou()
    {
        return $this->thankYou ? $this->thankYou : $this->createThankYou();
    }

    /**
     * @return AdminViewSitePath
     */
    public function getAdmin()
    {
        return $this->admin ? $this->admin : $this->createAdmin();
    }
    
}