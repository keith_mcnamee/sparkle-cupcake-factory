<?php


class AdminViewCommand extends SiteBase
{

    public function showView()
    {
        $jsonViewProperties = $this->getAdminView()->getViewPropertiesConstructor()->adminJsonViewProperties();

        $this->getCorePageView()->getModel()->setJsonViewProperties( $jsonViewProperties );
        $this->getAdminView()->getPageView()->showView();
    }
}