<?php


class AdminViewSitePath extends SiteBase
{

    /**
     * @var AdminViewCommand
     */
    private $command;

    /**
     * @var AdminViewPropertiesConstructor
     */
    private $viewPropertiesConstructor;

    /**
     * @var AdminPageView
     */
    private $pageView;

    public function __construct()
    {
        parent::__construct();

        $this->getSitePath()->getView()->getCupcake();
        $this->getIncludes();
    }

    public function __destruct()
    {
        $this->command = null;
        $this->viewPropertiesConstructor = null;
        $this->pageView = null;

        parent::__destruct();
    }

    private function getIncludes()
    {
        require_once( DIR_SITE . "/view/admin/includes/AdminViewIncludes.php" );
    }

    /**
     * @param $singleInstance
     * @return AdminViewCommand
     */
    private function createCommand( $singleInstance )
    {
        $instance = new AdminViewCommand();
        if( !$singleInstance )
        {
            $this->command = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return AdminViewPropertiesConstructor
     */
    private function createViewPropertiesConstructor( $singleInstance )
    {
        $instance = new AdminViewPropertiesConstructor();
        if( !$singleInstance )
        {
            $this->viewPropertiesConstructor = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return AdminPageView
     */
    private function createPageView( $singleInstance )
    {
        $instance = new AdminPageView();
        $instance->init();
        if( !$singleInstance )
        {
            $this->command = $instance;
        }
        return $instance;
    }

    /**
     * @param boolean
     * @return AdminViewCommand
     */
    public function getCommand( $singleInstance = true )
    {
        return $this->command && !$singleInstance ? $this->command : $this->createCommand( $singleInstance );
    }

    /**
     * @param boolean
     * @return AdminViewPropertiesConstructor
     */
    public function getViewPropertiesConstructor( $singleInstance = false )
    {
        return $this->viewPropertiesConstructor && !$singleInstance ? $this->viewPropertiesConstructor : $this->createViewPropertiesConstructor( $singleInstance );
    }

    /**
     * @param boolean
     * @return AdminPageView
     */
    public function getPageView( $singleInstance = false )
    {
        return $this->pageView && !$singleInstance ? $this->pageView : $this->createPageView( $singleInstance );
    }
}