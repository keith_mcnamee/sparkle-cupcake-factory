<?php


class AdminViewPropertiesConstructor extends CupcakeViewPropertiesConstructor
{

    /**
     * @return array
     */
    protected function adminViewObject()
    {
        $object = $this->cupcakeViewObject();
        return $object;
    }

    /**
     * @return String
     */
    public function adminJsonViewProperties()
    {
        return $this->constructJson( $this->adminViewObject() );
    }
}