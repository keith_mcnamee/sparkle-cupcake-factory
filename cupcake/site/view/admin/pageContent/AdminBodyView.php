<?php


class AdminBodyView extends CupcakeBodyView
{

    /**
     * @param JsContainerView $containerHtml
     */
    protected function addMainBodyContainer( JsContainerView $containerHtml = null )
    {
        $containerHtml = $containerHtml ? $containerHtml : new AdminMainBodyView();
        parent::addMainBodyContainer( $containerHtml );
    }
}