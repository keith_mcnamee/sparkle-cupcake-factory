<?php

require_once( DIR_SITE . "/view/admin/AdminPageView.php" );

require_once( DIR_SITE . "/view/admin/command/AdminViewCommand.php" );

require_once( DIR_SITE . "/view/admin/construct/AdminViewPropertiesConstructor.php" );

require_once( DIR_SITE . "/view/admin/pageContent/AdminBodyView.php" );
require_once( DIR_SITE . "/view/admin/pageContent/AdminMainBodyView.php" );
