<?php


class AdminPageView extends CupcakePageView
{

    /**
     * @param ElementHtml $elementHtml
     */
    protected function defineBodyContainer( ElementHtml $elementHtml = null )
    {
        $elementHtml = $elementHtml ? $elementHtml : new AdminBodyView();
        parent::defineBodyContainer( $elementHtml );
    }
}