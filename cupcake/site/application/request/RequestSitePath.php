<?php


class RequestSitePath extends SiteBase
{

    public function __construct()
    {
        parent::__construct();

        $this->getSharedRequest();
        $this->getIncludes();
    }

    private function getIncludes()
    {
        require_once( DIR_SITE . "/application/request/includes/RequestSiteIncludes.php" );
    }
}