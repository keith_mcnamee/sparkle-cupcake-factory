<?php


class ProcessAppSiteCommand extends SiteBase
{

    public function command()
    {
        $this->getSiteRequest();

        $paths = CoreRequest::getHelper()->workingDirectoryPaths();

        $nextPath = CoreRequest::getHelper()->nextPath( $paths );
        $nextPath = $nextPath ? $nextPath : PathsRequestSiteConstants::$WELCOME;
        $paths = CoreRequest::getHelper()->remainingPaths( $paths );

        $validPath = "";
        switch ( $nextPath )
        {
            case PathsRequestSiteConstants::$WELCOME:
                $this->showWelcomeView( $paths, $validPath.$nextPath."/" );
                break;

            case PathsRequestSiteConstants::$THANK_YOU:
                $this->showThankYouView( $paths, $validPath.$nextPath."/" );
                break;

            case PathsRequestSiteConstants::$ADMIN:
                $this->showAdminView( $paths, $validPath.$nextPath."/" );
                break;

            default:
                $this->getRedirectCommand()->internalLocation( $validPath );
                break;
        }
    }

    /**
     * @param array $paths
     * @param string $validPath
     * @return string
     */
    private function showWelcomeView( array $paths, $validPath )
    {
        if( CoreRequest::getHelper()->isFinalPath( $paths ) )
        {
            $this->getWelcomeView()->getCommand()->showView();
        }
        else
        {
            $this->getRedirectCommand()->internalLocation( $validPath );
        }
    }

    /**
     * @param array $paths
     * @param string $validPath
     * @return string
     */
    private function showThankYouView( array $paths, $validPath )
    {

        if( CoreRequest::getHelper()->isFinalPath( $paths ) )
        {
            $this->getSharedCupcake();
            $saveDataRequestVO = $this->getSharedRequest()->getRequestModel()->getSaveDataRequestVO();
            if( $saveDataRequestVO )
            {
                $this->getSharedDatabase()->getInsertDatabaseCupcakeRequest()->savedOrderInsertRequest( $saveDataRequestVO->getIds() );
            }
            $this->getThankYouView()->getCommand()->showView();
        }
        else
        {
            $this->getRedirectCommand()->internalLocation( $validPath );
        }
    }

    /**
     * @param array $paths
     * @param string $validPath
     * @return string
     */
    private function showAdminView( array $paths, $validPath )
    {
        if( CoreRequest::getHelper()->isFinalPath( $paths ) )
        {
            $this->getAdminView()->getCommand()->showView();
        }
        else
        {
            $this->getRedirectCommand()->internalLocation( $validPath );
        }
    }

    /**
     * @return RedirectHeadersRequestCoreService
     */
    private function getRedirectCommand()
    {
        return $this->getCoreRequest()->getRedirectHeadersService();
    }
} 