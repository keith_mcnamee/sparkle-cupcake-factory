<?php


class AppSitePath extends SiteBase
{

    public function __construct()
    {
        parent::__construct();

        $this->getSharedApp();
    }

    /**
     * @return ProcessAppSiteCommand
     */
    public function getProcessCommand()
    {
        return new ProcessAppSiteCommand();
    }
    
}