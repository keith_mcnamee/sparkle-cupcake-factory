<?php


class CommandAppSitePath
{

    /**
     * @var ProcessAppSiteCommand
     */
    private $process;

    public function __destruct()
    {
        $this->process = null;
    }

    /**
     * @param $singleInstance
     * @return ProcessAppSiteCommand
     */
    private function createProcess( $singleInstance )
    {
        $instance = new ProcessAppSiteCommand();
        $instance->init();
        if( !$singleInstance )
        {
            $this->process = $instance;
        }
        return $instance;
    }

    /**
     * @param $singleInstance
     * @return ProcessAppSiteCommand
     */
    public function getProcess( $singleInstance = true )
    {
        return $this->process && !$singleInstance ? $this->process : $this->createProcess( $singleInstance );
    }
    
}