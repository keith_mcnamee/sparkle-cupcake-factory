<?php


class ApplicationSitePath
{

    /**
     * @var AppSitePath
     */
    private $app;

    /**
     * @var RequestSitePath
     */
    private $request;


    public function __destruct()
    {
        $this->app = null;
        $this->request = null;
    }

    /**
     * @return AppSitePath
     */
    private function createApp()
    {
        $this->app = new AppSitePath();
        return $this->app;
    }

    /**
     * @return RequestSitePath
     */
    private function createRequest()
    {
        require_once( DIR_SITE . "/application/request/RequestSitePath.php" );
        $this->request = new RequestSitePath();
        return $this->request;
    }

    /**
     * @return AppSitePath
     */
    public function getApp()
    {
        return $this->app ? $this->app : $this->createApp();
    }

    /**
     * @return RequestSitePath
     */
    public function getRequest()
    {
        return $this->request ? $this->request : $this->createRequest();
    }
    
}