<?php


class SitePath
{

    /**
     * @var ApplicationSitePath
     */
    private $application;

    /**
     * @var ViewSitePath
     */
    private $view;

    public function __construct()
    {
        $this->getIncludes();
    }

    public function __destruct()
    {
        $this->application = null;
        $this->view = null;
    }

    public function process()
    {
        $this->getApplication()->getApp()->getProcessCommand()->command();
    }

    private function getIncludes()
    {
        require_once( DIR_SITE . "/application/app/includes/AppSiteIncludes.php" );
    }

    /**
     * @return static
     */
    public static function getInstance()
    {
        static $instance = null;

        if (null === $instance)
        {
            $instance = new static();
        }

        return $instance;
    }

    /**
     * @return ApplicationSitePath
     */
    private function createApplication()
    {
        $this->application = new ApplicationSitePath();
        return $this->application;
    }

    /**
     * @return ViewSitePath
     */
    private function createView()
    {
        require_once( DIR_SITE . "/view/ViewSitePath.php" );
        $this->view  = new ViewSitePath();
        return $this->view ;
    }

    /**
     * @return ApplicationSitePath
     */
    public function getApplication()
    {
        return $this->application ? $this->application : $this->createApplication();
    }

    /**
     * @return ViewSitePath
     */
    public function getView()
    {
        return $this->view ? $this->view : $this->createView();
    }
}