<?php


class SiteBase extends SharedBase
{

    /**
     * @var SitePath
     */
    private $sitePath;

    public function __destruct()
    {
        $this->sitePath = null;

        parent::__destruct();
    }

    /**
     * @return SitePath
     */
    private function createSitePath()
    {
        $this->sitePath = SitePath::getInstance();
        return $this->sitePath;
    }

    /**
     * @return SitePath
     */
    protected function getSitePath()
    {
        return $this->sitePath ? $this->sitePath : $this->createSitePath();
    }

    /**
     * @return AppSitePath
     */
    protected function getSiteApp()
    {
        return $this->getSitePath()->getApplication()->getApp();
    }

    /**
     * @return RequestSitePath
     */
    protected function getSiteRequest()
    {
        return $this->getSitePath()->getApplication()->getRequest();
    }

    /**
     * @return WelcomeViewSitePath
     */
    protected function getWelcomeView()
    {
        return $this->getSitePath()->getView()->getWelcome();
    }

    /**
     * @return ThankYouViewSitePath
     */
    protected function getThankYouView()
    {
        return $this->getSitePath()->getView()->getThankYou();
    }

    /**
     * @return AdminViewSitePath
     */
    protected function getAdminView()
    {
        return $this->getSitePath()->getView()->getAdmin();
    }
}