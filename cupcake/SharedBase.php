<?php


class SharedBase extends CoreBase
{

    /**
     * @var SharedPath
     */
    private $sharedPath;

    public function __destruct()
    {
        $this->sharedPath = null;

        parent::__destruct();
    }

    /**
     * @return SharedPath
     */
    private function createSharedPath()
    {
        $this->sharedPath = SharedPath::getInstance();
        return $this->sharedPath;
    }

    /**
     * @return SharedPath
     */
    protected function getSharedPath()
    {
        return $this->sharedPath ? $this->sharedPath : $this->createSharedPath();
    }

    /**
     * @return AppSharedPath
     */
    protected function getSharedApp()
    {
        return $this->getSharedPath()->getApplication()->getApp();
    }

    /**
     * @return CupcakeSharedPath
     */
    protected function getSharedCupcake()
    {
        return $this->getSharedPath()->getApplication()->getCupcake();
    }

    /**
     * @return DatabaseSharedPath
     */
    protected function getSharedDatabase()
    {
        return $this->getSharedPath()->getApplication()->getDatabase();
    }

    /**
     * @return RequestSharedPath
     */
    protected function getSharedRequest()
    {
        return $this->getSharedPath()->getApplication()->getRequest();
    }

    /**
     * @return UploadSharedPath
     */
    protected function getSharedUpload()
    {
        return $this->getSharedPath()->getApplication()->getUpload();
    }
}