<?php
set_time_limit( 0 );
ini_set( 'display_errors', 1 );
error_reporting( E_ALL );
date_default_timezone_set( 'Europe/London' );

define( 'DIR_ROOT', dirname( __FILE__ ) );

require_once( DIR_ROOT . "/DefineCupcakeFactory.php" );

require_once( DIR_APP . "/SharedPath.php" );

SharedPath::getInstance()->process();